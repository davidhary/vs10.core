﻿''' <summary>
''' The contract implemented by all classes requiring restoring default status and state.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Created
''' </history>
Public Interface IResettable

  ''' <summary>
  ''' Restore class elements to their default known values.
  ''' </summary>
  Function Reset() As Boolean

End Interface

''' <summary>
''' The contract implemented by all classes requiring restoring default status and state.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Created
''' </history>
Public Interface IResettable(Of T As Structure)

  Inherits IResettable

  ''' <summary>
  ''' Gets or sets the value that is set when the structure is reset. This is the default value.
  ''' </summary>
  Property DefaultValue() As T?

End Interface
