﻿''' <summary>
''' Provides an interface for all classes requiring clearing or restoring status and state.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/07/2008" by="David Hary" revision="1.0.2926.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' Rename device clear to clear active state.
''' </history>
Public Interface IResettableDevice

  ''' <summary>
  ''' Aborts execution and clears the device.
  ''' Clears communication functions and breaks up any hung condition allowing the resumption of blocked
  ''' data transfer.
  ''' Typically this is accomplished by sending a selective device clear command to the instrument.
  ''' Selective device clear does not reset the instrument (use *RST), or resets the error 
  ''' queue and registers(use *CLS), or clears the device memory.
  ''' </summary>
  Function ClearActiveState() As Boolean

  ''' <summary>
  ''' Clears the error queue and resets all registers to zero.
  ''' </summary>
  Function ClearExecutionState() As Boolean

  ''' <summary>Resets and clears the device. 
  ''' Issues device clear (e.g., SDC), reset to known state (e.g., RST), 
  ''' and Clear Execution State (e.g., CLS and clear error queue).</summary>
  ''' <remarks></remarks>
  ''' <history>
  ''' </history>
  Function ResetAndClear() As Boolean

  ''' <summary>
  ''' Returns the device to its default known state.
  ''' This resets the device functionality.
  ''' </summary>
  Function ResetKnownState() As Boolean

End Interface

