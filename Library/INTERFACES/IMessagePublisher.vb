﻿''' <summary>
''' The contract implemented by the class publishing messages.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/21/2011" by="David Hary" revision="1.2.4038.x">
''' Created
''' </history>
Public Interface IMessagePublisher

  ''' <summary>
  ''' Occurs when a message is available.
  ''' </summary>
  Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs)

  ''' <summary>
  ''' Raises the <see cref="E:MessageAvailable" /> event.
  ''' </summary>
  ''' <param name="e">The <see cref="isr.Core.MessageEventArgs" /> instance containing the event data.</param><returns></returns>
  Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As String

  ''' <summary>
  ''' Called when [message available].
  ''' </summary>
  ''' <param name="traceLevel">The trace level.</param>
  ''' <param name="synopsis">The synopsis.</param>
  ''' <param name="format">The format.</param>
  ''' <param name="args">The args.</param><returns></returns>
  Function OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType, _
                                ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As String

  ''' <summary>
  ''' Called when [message available].
  ''' </summary>
  ''' <param name="broadcastLevel">The broadcast level.</param>
  ''' <param name="traceLevel">The trace level.</param>
  ''' <param name="synopsis">The synopsis.</param>
  ''' <param name="format">The format.</param>
  ''' <param name="args">The args.</param><returns></returns>
  Function OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType, _
                              ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As String

End Interface

