﻿''' <summary>
''' Defines an interface allowing objects to be included in a Dictionary collection of String and Value
''' using a diretly settable key.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
Public Interface IKeyable

  ''' <summary>Gets the unique Key for an instance of this class.</summary>
  Property UniqueKey() As String

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="IKeyable">Keyable</see> 
''' items keyed by the <see cref="IKeyable.UniqueKey">unique key.</see>
''' </summary>
''' <remarks></remarks>
Public Class KeyableCollection(Of TItem As IKeyable)
  Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)

  Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
    Return item.UniqueKey
  End Function

End Class

