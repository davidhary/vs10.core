﻿''' <summary>
''' Provides an interface for elements that require 
''' clearing of status and state with a timeout.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/07/2008" by="David Hary" revision="1.0.2926.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
Public Interface IResettableTimeout
  Inherits IResettableDevice

  ''' <summary>
  ''' <see cref="IResettableDevice.ClearActiveState">Clears the device</see>.
  ''' </summary>
  ''' <param name="timeout">Specifies the timeout interval to use for clearing.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Overloads Function ClearActiveState(ByVal timeout As Integer) As Boolean

  ''' <summary>
  ''' <see cref="IResettableDevice.ClearExecutionState">Clears execution state.</see>
  ''' </summary>
  ''' <param name="timeout">Specifies the timeout interval to use for clearing.</param>
  Overloads Function ClearExecutionState(ByVal timeout As Integer) As Boolean

  ''' <summary>
  ''' <see cref="IResettableDevice.ResetAndClear">Resets and clears.</see>
  ''' </summary>
  ''' <param name="timeout">Specifies the timeout interval to use for resetting.</param>
  Overloads Function ResetAndClear(ByVal timeout As Integer) As Boolean

  ''' <summary>
  ''' <see cref="IResettableDevice.ResetKnownState">Resets known state.</see>
  ''' </summary>
  ''' <param name="timeout">Specifies the timeout interval to use for resetting.</param>
  Overloads Function ResetKnownState(ByVal timeout As Integer) As Boolean

  ''' <summary>
  ''' Restores device timeout.
  ''' </summary>
  ''' <remarks></remarks>
  Sub RestoreTimeout()

  ''' <summary>
  ''' Stores the current device timeout and set the timeout to the specific value.
  ''' </summary>
  ''' <param name="value">Specifies the new timeout in milliseconds.</param>
  ''' <remarks></remarks>
  Sub StoreTimeout(ByVal value As Integer)

End Interface

