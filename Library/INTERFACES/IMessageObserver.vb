﻿''' <summary>
''' The contract implemented by the class displaying publisher messages.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="4.0.3928.x">
''' Created
''' </history>
Public Interface IMessageObserver

  ''' <summary>
  ''' Reports status.
  ''' </summary>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean

End Interface
