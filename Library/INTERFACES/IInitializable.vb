﻿''' <summary>
''' Defines an interface for entities that require initialization when started or connected.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
Public Interface IInitializable

  ''' <summary>
  ''' Initializes the system state.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function InitializeSystemState() As Boolean

  ''' <summary>
  ''' Initializes the system state.
  ''' </summary>
  ''' <param name="timeout">Specifies initialization timeout in millisecods.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function InitializeSystemState(ByVal timeout As Integer) As Boolean

  ''' <summary>
  ''' Gets or sets the time out for initializing the system state.
  ''' </summary>
  Property SystemInitTimeout() As Integer

End Interface
