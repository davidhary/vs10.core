﻿''' <summary>
''' Defines an interface for raising event messages.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/06/2008" by="David Hary" revision="1.0.3627.x">
''' Created
''' </history>
Public Interface IMessageHandler

  ''' <summary>The event delivers an <see cref="isr.Core.ExtendedMessage">extended message</see>.</summary>
  ''' <param name="e">A reference to the <see cref="T:isr.Core.GenericEventArgs"/> event arguments</param>
  Event MessageAvailable As EventHandler(Of isr.Core.GenericEventArgs(Of isr.Core.ExtendedMessage))

  ''' <summary>Raises the <see cref="MessageAvailable">message</see> event.</summary>
  ''' <param name="level">Specifies the message <see cref="System.Diagnostics.TraceEventType">level</see>.</param>
  ''' <param name="synopsis">Specifies the message synopsis.</param>
  ''' <param name="details">Specifies the message details.</param>
  ''' <remarks></remarks>
  Sub OnMessageAvailable(ByVal level As System.Diagnostics.TraceEventType, ByVal synopsis As String, ByVal details As String)

  ''' <summary>Raises the <see cref="MessageAvailable">message</see> event.</summary>
  ''' <param name="level">Specifies the message <see cref="System.Diagnostics.TraceEventType">level</see>.</param>
  ''' <param name="synopsis">Specifies the message synopsis.</param>
  ''' <param name="format">Specifies the message details format.</param>
  ''' <param name="args">Specifies the message details arguments.</param>
  ''' <remarks></remarks>
  Sub OnMessageAvailable(ByVal level As System.Diagnostics.TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)

End Interface
