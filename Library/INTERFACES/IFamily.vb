﻿''' <summary>
''' Defines an interface allowing objects to be enumerated as familiies.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
Public Interface IFamily

  ''' <summary>
  ''' Gets or sets the family name
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Name() As String

  ''' <summary>
  ''' Gets or set the number of family members. 
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Count() As Integer

  ''' <summary>
  ''' Adds a member to the family.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function Add() As IFamily

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="isr.Core.ICollectable">collectible</see> 
''' items keyed by the <see cref="isr.Core.ICollectable.UniqueKey">unique key.</see>
''' </summary>
''' <remarks></remarks>
Public Class FamilyCollection(Of TItem As IFamily)
  Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)

  Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
    Return item.Name
  End Function

  ''' <summary>
  ''' Adds a family member to the family.
  ''' </summary>
  ''' <param name="family"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function AddFamily(ByVal family As IFamily) As IFamily
    If MyBase.Contains(family.Name) Then
      Me.Item(family.Name).Add()
    Else
      Me.Add(CType(family, TItem))
    End If
    Return Me.Item(family.Name)
  End Function

End Class

