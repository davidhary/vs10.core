﻿''' <summary>
''' A base control implementing an Anonymous Publisher.
''' Useful for a settings publisher
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/02/2010" by="David Hary" revision="1.2.3988.x">
''' Created
''' </history>
<System.ComponentModel.Description("Anon Publisher User Control"), _
 System.Drawing.ToolboxBitmap(GetType(AnonPublisherUserControl))> _
Public Class AnonPublisherUserControl
  Implements IAnonPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>A private constructor for this class making it not publicly creatable.
  ''' This ensure using the class as a singleton.
  ''' </summary>
  Protected Sub New()
    MyBase.new()
    Me.InitializeComponent()
  End Sub

#End Region

#Region " PUBLISH "

  Private _Publishable As Boolean
  ''' <summary>
  ''' Gets or sets the publishable status of the publisher. 
  ''' When created, the publisher is not publishable. 
  ''' This allows changing properties without affecting the observers.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), _
   System.ComponentModel.Browsable(False)> _
  Public Overridable Property Publishable() As Boolean Implements IAnonPublisher.Publishable
    Get
      Return _Publishable
    End Get
    Set(ByVal value As Boolean)
      _Publishable = value
    End Set
  End Property

  ''' <summary>
  ''' Publishes all values by raising the changed events.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub Publish() Implements IAnonPublisher.Publish
  End Sub

#End Region

#Region " I NOTIFY PROPERTY CHANGED "

  ''' <summary>
  ''' Raised whenever a property is changed.
  ''' </summary>
  ''' <remarks></remarks>
  Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements IAnonPublisher.PropertyChanged

  ''' <summary>
  ''' Raised to notify the binadable object of a change.
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks>
  ''' Includes a work around because for some reason, the dinding wrte value does not occure.
  ''' This is dengerous becuase it could lead to a stack overflow.
  ''' It can be used if the property changed event is raised only if the property changed.
  ''' </remarks>
  Public Overridable Sub OnPropertyChanged(ByVal name As String) Implements IAnonPublisher.OnPropertyChanged
    If Me.Publishable Then
      EventHandlerExtensions.SafeInvoke(PropertyChangedEvent, Me, New System.ComponentModel.PropertyChangedEventArgs(name))
    End If
  End Sub

  ''' <summary>
  ''' Raised to notify the binadable object of a change.
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks>Strips the "set_" prefix derived from using reflection to
  ''' get the current function name from a property set construct.</remarks>
  Protected Sub OnSetPropertyChanged(ByVal name As String)
    Me.OnPropertyChanged(name.TrimStart("set_".ToCharArray()))
  End Sub

#End Region

End Class
