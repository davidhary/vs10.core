﻿''' <summary>
''' A base class implementing an Anonymous Sync Publisher.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/02/2010" by="David Hary" revision="1.2.3988.x">
''' Created
''' </history>
Public MustInherit Class AnonSyncPublisherBase

  Implements IAnonSyncPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>A private constructor for this class making it not publicly creatable.
  ''' This ensure using the class as a singleton.
  ''' </summary>
  Protected Sub New()
    MyBase.new()
  End Sub

  ''' <summary>
  ''' Constructs the class for linking to a controller type instrument.
  ''' When construted, publishing is suspended.
  ''' </summary>
  ''' <param name="synchronizer">Specifies reference to a class implementing the 
  ''' <see cref="System.ComponentModel.ISynchronizeInvoke">synchronizer</see> contract</param>
  ''' <remarks></remarks>
  Protected Sub New(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke)
    MyBase.New()
    _synchronizer = synchronizer
  End Sub

  ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
  ''' <remarks>Do not make this method overridable (virtual) because a derived 
  '''   class should not be able to override this method.</remarks>
  Public Sub Dispose() Implements IDisposable.Dispose

    ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

    ' this disposes all child classes.
    Dispose(True)

    ' Take this object off the finalization(Queue) and prevent finalization code 
    ' from executing a second time.
    GC.SuppressFinalize(Me)

  End Sub

  Private _isDisposed As Boolean
  ''' <summary>
  ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
  ''' provided proper implementation.
  ''' </summary>
  Protected Property IsDisposed() As Boolean
    Get
      Return _isDisposed
    End Get
    Private Set(ByVal value As Boolean)
      _isDisposed = value
    End Set
  End Property

  ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
  ''' <param name="disposing">True if this method releases both managed and unmanaged 
  '''   resources; False if this method releases only unmanaged resources.</param>
  ''' <remarks>Executes in two distinct scenarios as determined by
  '''   its disposing parameter.  If True, the method has been called directly or 
  '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
  '''   If disposing equals False, the method has been called by the 
  '''   runtime from inside the finalizer and you should not reference 
  '''   other objects--only unmanaged resources can be disposed.</remarks>
  Protected Overridable Sub Dispose(ByVal disposing As Boolean)

    If Not Me.IsDisposed Then

      Try

        If disposing Then

          ' remove reference to the synchronizer
          _synchronizer = Nothing

        End If

        ' Free shared unmanaged resources

      Finally

        ' set the sentinel indicating that the class was disposed.
        Me.IsDisposed = True

      End Try

    End If

  End Sub

  ''' <summary>This destructor will run only if the Dispose method 
  '''   does not get called. It gives the base class the opportunity to 
  '''   finalize. Do not provide destructors in types derived from this class.</summary>
  Protected Overrides Sub Finalize()
    ' Do not re-create Dispose clean-up code here.
    ' Calling Dispose(false) is optimal in terms of
    ' readability and maintainability.
    Dispose(False)
    ' The compiler automatically adds a call to the base class finalizer 
    ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
    MyBase.Finalize()
  End Sub

#End Region

#Region " PUBLISH "

  Private _Publishable As Boolean
  ''' <summary>
  ''' Gets or sets the publishable status of the publisher. 
  ''' When created, the publisher is not publishable. 
  ''' This allows changing properties without affecting the observers.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable Property Publishable() As Boolean Implements IAnonSyncPublisher.Publishable
    Get
      Return _Publishable
    End Get
    Set(ByVal value As Boolean)
      _Publishable = value
    End Set
  End Property

  ''' <summary>
  ''' Publishes all values by raising the changed events.
  ''' </summary>
  ''' <remarks></remarks>
  Public MustOverride Sub Publish() Implements IAnonSyncPublisher.Publish

#End Region

#Region " I NOTIFY PROPERTY CHANGED "

  ''' <summary>
  ''' Raised whenever a property is changed.
  ''' </summary>
  ''' <remarks></remarks>
  Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements IAnonSyncPublisher.PropertyChanged

  ''' <summary>
  ''' Raised to notify the binadable object of a change.
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks>
  ''' Includes a work around because for some reason, the dinding wrte value does not occure.
  ''' This is dengerous becuase it could lead to a stack overflow.
  ''' It can be used if the property changed event is raised only if the property changed.
  ''' </remarks>
  Public Overridable Sub OnPropertyChanged(ByVal name As String) Implements IAnonSyncPublisher.OnPropertyChanged
    If Me.IsDisposed OrElse Not Me.Publishable OrElse PropertyChangedEvent Is Nothing Then Return
    EventHandlerExtensions.SafeInvoke(PropertyChangedEvent, Me, New System.ComponentModel.PropertyChangedEventArgs(name))
  End Sub

  ''' <summary>
  ''' Raised to notify the binadable object of a change.
  ''' </summary>
  ''' <param name="name"></param>
  ''' <remarks>Strips the "set_" prefix derived from using reflection to
  ''' get the current function name from a property set construct.</remarks>
  Protected Sub OnSetPropertyChanged(ByVal name As String)
    Me.OnPropertyChanged(name.TrimStart("set_".ToCharArray()))
  End Sub

#End Region

#Region " SYNCHRONIZER "

  Private _synchronizer As System.ComponentModel.ISynchronizeInvoke
  ''' <summary>
  ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see> 
  ''' to use for marshaling events.
  ''' </summary>
  Public Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke Implements IAnonSyncPublisher.Synchronizer
    Get
      If Me.IsDisposed Then
        Throw New ObjectDisposedException("AnonSyncPublisherBase")
      End If
      Return _synchronizer
    End Get
    Set(ByVal value As System.ComponentModel.ISynchronizeInvoke)
      If Me.IsDisposed Then
        Throw New ObjectDisposedException("AnonSyncPublisherBase")
      End If
      _synchronizer = value
    End Set
  End Property

#End Region

End Class

