﻿''' <summary>
''' The contract implemented by publishes to anon observers with synchronization.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/18/2010" by="David Hary" revision="1.2.3943.x">
''' Adds suspension management to prevent the publication of events during operations such as adding database records.
''' </history>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
Public Interface ISyncPublisher

  Inherits IDisposable

#Region " SUSPENSION MANAGEMENT "

  ''' <summary>
  ''' Gets the suspension status of the publisher. 
  ''' Suspension allows changing information without changing the observers.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property IsSuspended() As Boolean

  ''' <summary>
  ''' Publishes all values by raising the changed events.
  ''' </summary>
  ''' <remarks></remarks>
  Sub Publish()

  ''' <summary>
  ''' Resumes real time publishing. Values are published whenever they change.
  ''' </summary>
  ''' <remarks></remarks>
  Sub ResumePublishing()

  ''' <summary>
  ''' Suspends real time publishing. Values will be published only after publishing resumes or using
  ''' the <see cref="Publish">publish method</see>.
  ''' </summary>
  ''' <remarks></remarks>
  Sub SuspendPublishing()

#End Region

#Region " NOTIFIERS "

  ''' <summary>
  ''' Executes the <paramref name="Action">Delegate</paramref> in the observer thread.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <typeparam name="TArg4"></typeparam>
  ''' <param name="action"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <param name="arg4"></param>
  ''' <remarks></remarks>
  Sub NotifyObserver(Of TArg1, TArg2, TArg3, TArg4)(ByVal action As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                   ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3, ByVal arg4 As TArg4)


  ''' <summary>
  ''' Executes the <paramref name="Action">Delegate</paramref> in the observer thread.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <param name="action"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <remarks></remarks>
  Sub NotifyObserver(Of TArg1, TArg2, TArg3)(ByVal action As Action(Of TArg1, TArg2, TArg3), _
                                             ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3)

  ''' <summary>
  ''' Executes the <paramref name="Action">Delegate</paramref> in the observer thread.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <param name="action"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <remarks></remarks>
  Sub NotifyObserver(Of TArg1, TArg2)(ByVal action As Action(Of TArg1, TArg2), _
                                      ByVal arg1 As TArg1, ByVal arg2 As TArg2)

  ''' <summary>
  ''' Executes the <paramref name="Action">Delegate</paramref> in the observer thread.
  ''' </summary>
  ''' <typeparam name="T"></typeparam>
  ''' <param name="action"></param>
  ''' <param name="arg"></param>
  ''' <remarks></remarks>
  Sub NotifyObserver(Of T)(ByVal action As Action(Of T), ByVal arg As T)

  ''' <summary>
  ''' Executes the <paramref name="Action">Delegate</paramref> in the observer thread.
  ''' </summary>
  ''' <param name="action"></param>
  ''' <remarks></remarks>
  Sub NotifyObserver(ByVal action As Action)

#End Region

#Region " SYNCHRONIZER "

  ''' <summary>
  ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see> 
  ''' to use for marshaling events.
  ''' </summary>
  Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke

#End Region

End Interface
