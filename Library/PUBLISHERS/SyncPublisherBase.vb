﻿''' <summary>
''' A base class implementing a Publisher with synchronization.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/18/2010" by="David Hary" revision="1.2.3943.x">
''' Adds suspension management to prevent the publication of events during operations such as adding database records.
''' </history>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
Public MustInherit Class SyncPublisherBase

  Implements ISyncPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Constructs the class for linking to a controller type instrument.
  ''' When construted, publishing is suspended.
  ''' </summary>
  ''' <param name="synchronizer">Specifies reference to a class implementing the 
  ''' <see cref="System.ComponentModel.ISynchronizeInvoke">synchronizer</see> contract</param>
  ''' <remarks></remarks>
  Protected Sub New(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke)
    MyBase.New()
    _synchronizer = synchronizer
    _isSuspended = True
  End Sub

  ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
  ''' <remarks>Do not make this method overridable (virtual) because a derived 
  '''   class should not be able to override this method.</remarks>
  Public Sub Dispose() Implements IDisposable.Dispose

    ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

    ' this disposes all child classes.
    Dispose(True)

    ' Take this object off the finalization(Queue) and prevent finalization code 
    ' from executing a second time.
    GC.SuppressFinalize(Me)

  End Sub

  Private _isDisposed As Boolean
  ''' <summary>
  ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
  ''' provided proper implementation.
  ''' </summary>
  Protected Property IsDisposed() As Boolean
    Get
      Return _isDisposed
    End Get
    Private Set(ByVal value As Boolean)
      _isDisposed = value
    End Set
  End Property

  ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
  ''' <param name="disposing">True if this method releases both managed and unmanaged 
  '''   resources; False if this method releases only unmanaged resources.</param>
  ''' <remarks>Executes in two distinct scenarios as determined by
  '''   its disposing parameter.  If True, the method has been called directly or 
  '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
  '''   If disposing equals False, the method has been called by the 
  '''   runtime from inside the finalizer and you should not reference 
  '''   other objects--only unmanaged resources can be disposed.</remarks>
  Protected Overridable Sub Dispose(ByVal disposing As Boolean)

    If Not Me.IsDisposed Then

      Try

        If disposing Then

          ' remove reference to the synchronizer
          _synchronizer = Nothing

        End If

        ' Free shared unmanaged resources

      Finally

        ' set the sentinel indicating that the class was disposed.
        Me.IsDisposed = True

      End Try

    End If

  End Sub

  ''' <summary>This destructor will run only if the Dispose method 
  '''   does not get called. It gives the base class the opportunity to 
  '''   finalize. Do not provide destructors in types derived from this class.</summary>
  Protected Overrides Sub Finalize()
    ' Do not re-create Dispose clean-up code here.
    ' Calling Dispose(false) is optimal in terms of
    ' readability and maintainability.
    Dispose(False)
    ' The compiler automatically adds a call to the base class finalizer 
    ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
    MyBase.Finalize()
  End Sub

#End Region

#Region " SUSPENSION MANAGEMENT "

  Private _isSuspended As Boolean
  ''' <summary>
  ''' Gets the suspension status of the publisher. 
  ''' Suspension allows changing information without changing the observers.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable ReadOnly Property IsSuspended() As Boolean Implements ISyncPublisher.IsSuspended
    Get
      Return _isSuspended
    End Get
  End Property

  ''' <summary>
  ''' Publishes all values by raising the changed events.
  ''' </summary>
  ''' <remarks></remarks>
  Public MustOverride Sub Publish() Implements ISyncPublisher.Publish

  ''' <summary>
  ''' Resumes real time publishing. Values are published whenever they change.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub ResumePublishing() Implements ISyncPublisher.ResumePublishing
    _isSuspended = False
  End Sub

  ''' <summary>
  ''' Suspends real time publishing. Values will be published only after publishing resumes or using
  ''' the <see cref="Publish">publish method</see>.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub SuspendPublishing() Implements ISyncPublisher.SuspendPublishing
    _isSuspended = True
  End Sub


#End Region

#Region " SYNCHRONIZER "

  ''' <summary>
  ''' Invokes the delegate in the thread of the <paramref name="synchronizer">synchronizer</paramref> if invoke required.
  ''' Otherwises, returns false allowing a direct execution.
  ''' </summary>
  ''' <param name="action"></param>
  ''' <param name="args"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function invokeObserver(ByVal action As [Delegate], ByVal ParamArray args As Object()) As Boolean

    If Me.IsSuspended OrElse _synchronizer Is Nothing OrElse action Is Nothing Then Return True
    If _synchronizer.InvokeRequired Then
      _synchronizer.BeginInvoke(action, args)
      Return True
    End If
    Return False

  End Function

  ''' <summary>
  ''' Executes the Delegate in the Gui-thread.
  ''' </summary>
  Protected Sub NotifyObserver(Of TArg1, TArg2, TArg3, TArg4)(ByVal action As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                              ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                              ByVal arg3 As TArg3, ByVal arg4 As TArg4) Implements ISyncPublisher.NotifyObserver
    ' if no Invoking required execute action directly
    If Not invokeObserver(action, arg1, arg2, arg3, arg4) Then action(arg1, arg2, arg3, arg4)

  End Sub

  ''' <summary>
  ''' Executes the Delegate in the Gui-thread.
  ''' </summary>
  Protected Sub NotifyObserver(Of TArg1, TArg2, TArg3)(ByVal action As Action(Of TArg1, TArg2, TArg3), _
                                                       ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                       ByVal arg3 As TArg3) Implements ISyncPublisher.NotifyObserver
    ' if no Invoking required execute action directly
    If Not invokeObserver(action, arg1, arg2, arg3) Then action(arg1, arg2, arg3)

  End Sub

  Protected Sub NotifyObserver(Of TArg1, TArg2)(ByVal action As Action(Of TArg1, TArg2), _
                                                ByVal arg1 As TArg1, ByVal arg2 As TArg2) Implements ISyncPublisher.NotifyObserver
    If Not invokeObserver(action, arg1, arg2) Then action(arg1, arg2)
  End Sub

  Protected Sub NotifyObserver(Of T)(ByVal action As Action(Of T), ByVal arg As T) Implements ISyncPublisher.NotifyObserver
    If Not invokeObserver(action, arg) Then action(arg)
  End Sub

  Protected Sub NotifyObserver(ByVal action As Action) Implements ISyncPublisher.NotifyObserver
    If Not invokeObserver(action) Then action()
  End Sub

  Private _synchronizer As System.ComponentModel.ISynchronizeInvoke
  ''' <summary>
  ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see> 
  ''' to use for marshaling events.
  ''' </summary>
  Public Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke Implements ISyncPublisher.Synchronizer
    Get
      If Me.IsDisposed Then
        Throw New ObjectDisposedException("AnonSyncPublisherBase")
      End If
      Return _synchronizer
    End Get
    Set(ByVal value As System.ComponentModel.ISynchronizeInvoke)
      If Me.IsDisposed Then
        Throw New ObjectDisposedException("AnonSyncPublisherBase")
      End If
      _synchronizer = value
    End Set
  End Property

#End Region

End Class

