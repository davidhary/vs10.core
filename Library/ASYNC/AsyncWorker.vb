Imports System.Threading
Imports isr.Core.ExceptionExtensions
Imports isr.Core.EventHandlerExtensions
''' <summary>
''' You can pass Delegates together with suitable arguments to the methods RunAsync(), 
''' NotifyGui() or ReportProgress(), and the Delegate will be executed either in a 
''' side-thread or in the Gui-thread
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/11/2010" by="Mr.PoorEnglish" revision="1.2.3753.x">
''' From Async Worker a Type Safe Backgroun Worker By Mr.PoorEnglish
''' http://www.codeproject.com/KB/vb/AsyncWorker2.aspx
''' Created
''' </history>
Public Class AsyncWorker

  ''' <summary>
  ''' Class constructor.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()
    MyBase.New()
    Me._reportInterval = 300
    ' Me._CancelRequested = 0
  End Sub

  Private _reportInterval As Integer
  ''' <summary>
  ''' Gets or sets the report interval in ticks.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property ReportInterval() As Integer
    Get
      Return _reportInterval
    End Get
    Set(ByVal value As Integer)
      _reportInterval = value
    End Set
  End Property

  Private _cancelRequested As Integer
  ''' <summary>
  ''' Gets the status of the cancelation requests.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property CancelRequested() As Boolean
    Get
      Return Thread.VolatileRead(_cancelRequested) <> 0
    End Get
  End Property

  ''' <summary>
  ''' Cancles a request.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Cancel()
    Thread.VolatileWrite(_cancelRequested, 1)
  End Sub

  Private _isRunning As Boolean
  ''' <summary>
  ''' Gets the running sentinel.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsRunning() As Boolean
    Get
      Return _IsRunning
    End Get
  End Property

  Public Event IsRunningChanged As EventHandler(Of System.EventArgs)
  ''' <summary>
  ''' Toggles the running sentinel.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub toggleIsRunning()
    _isRunning = Not _isRunning
    IsRunningChangedEvent.SafeInvoke(Me)
  End Sub

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Invokes the delegate if invoke required. Otherwises, returns False allowing a direct 
  ''' execution.
  ''' </summary>
  ''' <param name="action"></param>
  ''' <param name="args"></param>
  ''' <remarks></remarks>
  Private Sub invokeGui(ByVal action As [Delegate], ByVal ParamArray args As Object())
    Dim target As Windows.Forms.Control = TryCast(action.Target, Windows.Forms.Control)
    If Windows.Forms.Application.OpenForms.Count = 0 OrElse (target IsNot Nothing AndAlso target.IsDisposed) Then
      Cancel()
    Else
      Windows.Forms.Application.OpenForms(0).BeginInvoke(action, args)
    End If
  End Sub

  ''' <summary>
  ''' Toggles the running callback sentinel.
  ''' </summary>
  ''' <param name="ar"></param>
  ''' <remarks></remarks>
  Private Sub toggleIsRunningCallback(ByVal ar As IAsyncResult)
    invokeGui(New Action(AddressOf toggleIsRunning))
  End Sub

  Private _cancelNotified As Boolean
  Private _reportNext As Integer
  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Invokes the delegate if invoke required. Otherwises, returns False allowing a direct 
  ''' execution.
  ''' Allows cancelation and report of progress. 
  ''' </summary>
  ''' <param name="action"></param>
  ''' <param name="reportProgress"></param>
  ''' <param name="args"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function tryInvokeGui(ByVal action As [Delegate], ByVal reportProgress As Boolean, _
                                ByVal ParamArray args As Object()) As Boolean

    If Thread.VolatileRead(_cancelRequested) <> 0 Then

      If _cancelNotified Then Throw Me.DebugExceptionGetter("after cancelation you should notify Gui no longer.")

      _cancelNotified = True
      Return False

    End If

    If reportProgress Then
      Dim ticks As Integer = Environment.TickCount
      If ticks < _reportNext Then Return True
      InvokeGui(action, args)
      ' set the next reportingtime, aligned to  ReportInterval 
      _reportNext = ticks + ReportInterval - (ticks - _reportNext) Mod ReportInterval
    Else
      InvokeGui(action, args)
    End If
    Return True

  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Reports progress if the last progress-report is more than <see cref="ReportInterval"/>ago, and the process isn't canceled
  ''' </summary>
  ''' <param name="progressAction"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReportProgress(ByVal progressAction As Action) As Boolean
    Return tryInvokeGui(progressAction, True)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Reports progress if the last progress-report is more than <see cref="ReportInterval"/>ago, and the process isn't canceled
  ''' </summary>
  ''' <typeparam name="T"></typeparam>
  ''' <param name="progressAction"></param>
  ''' <param name="arg"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReportProgress(Of T)(ByVal progressAction As Action(Of T), ByVal arg As T) As Boolean
    Return TryInvokeGui(progressAction, True, arg)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Reports progress if the last progress-report is more than <see cref="ReportInterval"/>ago, and the process isn't canceled
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <param name="progressAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReportProgress(Of TArg1, TArg2)(ByVal progressAction As Action(Of TArg1, TArg2), _
                                                  ByVal arg1 As TArg1, ByVal arg2 As TArg2) As Boolean
    Return TryInvokeGui(progressAction, True, arg1, arg2)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Reports progress if the last progress-report is more than <see cref="ReportInterval"/>ago, and the process isn't canceled
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <param name="progressAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReportProgress(Of TArg1, TArg2, TArg3)(ByVal progressAction As Action(Of TArg1, TArg2, TArg3), _
                                                         ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                         ByVal arg3 As TArg3) As Boolean
    Return TryInvokeGui(progressAction, True, arg1, arg2, arg3)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
  ''' Reports progress if the last progress-report is more than <see cref="ReportInterval"/>ago, and the process isn't canceled
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <typeparam name="TArg4"></typeparam>
  ''' <param name="progressAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <param name="arg4"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ReportProgress(Of TArg1, TArg2, TArg3, TArg4)(ByVal progressAction As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                                ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                                ByVal arg3 As TArg3, ByVal arg4 As TArg4) As Boolean
    Return tryInvokeGui(progressAction, True, arg1, arg2, arg3, arg4)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on
  ''' if the process was not canceled.
  ''' </summary>
  ''' <param name="syncAction"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NotifyGui(ByVal syncAction As Action) As Boolean
    Return tryInvokeGui(syncAction, False)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on
  ''' if the process was not canceled.
  ''' </summary>
  ''' <typeparam name="T"></typeparam>
  ''' <param name="syncAction"></param>
  ''' <param name="arg"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NotifyGui(Of T)(ByVal syncAction As Action(Of T), ByVal arg As T) As Boolean
    Return tryInvokeGui(syncAction, False, arg)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on
  ''' if the process was not canceled.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <param name="syncAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NotifyGui(Of TArg1, TArg2)( _
        ByVal syncAction As Action(Of TArg1, TArg2), ByVal arg1 As TArg1, ByVal arg2 As TArg2) As Boolean
    Return TryInvokeGui(syncAction, False, arg1, arg2)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on
  ''' if the process was not canceled.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <param name="syncAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NotifyGui(Of TArg1, TArg2, TArg3)(ByVal syncAction As Action(Of TArg1, TArg2, TArg3), _
                                                    ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3) As Boolean
    Return TryInvokeGui(syncAction, False, arg1, arg2, arg3)
  End Function

  ''' <summary>
  ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on
  ''' if the process was not canceled.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <typeparam name="TArg4"></typeparam>
  ''' <param name="syncAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <param name="arg4"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NotifyGui(Of TArg1, TArg2, TArg3, TArg4)(ByVal syncAction As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                           ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3, _
                                                           ByVal arg4 As TArg4) As Boolean
    Return TryInvokeGui(syncAction, False, arg1, arg2, arg3, arg4)
  End Function

  Private _toggleIsRunningCallback As AsyncCallback = AddressOf toggleIsRunningCallback
  Private Function GetCallback(ByVal endInvoke As AsyncCallback) As AsyncCallback
    If IsRunning Then Throw Me.DebugExceptionGetter("I'm already running")
    _cancelNotified = False
    _cancelRequested = 0
    toggleIsRunning()
    _reportNext = Environment.TickCount
    Return DirectCast([Delegate].Combine(endInvoke, _toggleIsRunningCallback), AsyncCallback)
  End Function

  ''' <summary>
  ''' Executes the Delegate in a thread of the threadpool.
  ''' End Invoke action is delegated to Action.EndInvoke.
  ''' </summary>
  ''' <param name="asyncAction"></param>
  ''' <remarks></remarks>
  Public Sub RunAsync(ByVal asyncAction As Action)
    asyncAction.BeginInvoke(GetCallback(AddressOf asyncAction.EndInvoke), Nothing)
  End Sub

  ''' <summary>
  ''' Executes the Delegate in a thread of the threadpool.
  ''' End Invoke action is delegated to Action.EndInvoke.
  ''' </summary>
  ''' <typeparam name="T"></typeparam>
  ''' <param name="asyncAction"></param>
  ''' <param name="arg"></param>
  ''' <remarks></remarks>
  Public Sub RunAsync(Of T)(ByVal asyncAction As Action(Of T), ByVal arg As T)
    asyncAction.BeginInvoke(arg, GetCallback(AddressOf asyncAction.EndInvoke), Nothing)
  End Sub

  ''' <summary>
  ''' Executes the Delegate in a thread of the threadpool.
  ''' End Invoke action is delegated to Action.EndInvoke.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <param name="asyncAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <remarks></remarks>
  Public Sub RunAsync(Of TArg1, TArg2)(ByVal asyncAction As Action(Of TArg1, TArg2), ByVal arg1 As TArg1, ByVal arg2 As TArg2)
    asyncAction.BeginInvoke(arg1, arg2, GetCallback(AddressOf asyncAction.EndInvoke), Nothing)
  End Sub

  ''' <summary>
  ''' Executes the Delegate in a thread of the threadpool.
  ''' End Invoke action is delegated to Action.EndInvoke.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <param name="asyncAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <remarks></remarks>
  Public Sub RunAsync(Of TArg1, TArg2, TArg3)(ByVal asyncAction As Action(Of TArg1, TArg2, TArg3), _
                                              ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3)
    asyncAction.BeginInvoke(arg1, arg2, arg3, GetCallback(AddressOf asyncAction.EndInvoke), Nothing)
  End Sub

  ''' <summary>
  ''' Executes the Delegate in a thread of the threadpool.
  ''' End Invoke action is delegated to Action.EndInvoke.
  ''' </summary>
  ''' <typeparam name="TArg1"></typeparam>
  ''' <typeparam name="TArg2"></typeparam>
  ''' <typeparam name="TArg3"></typeparam>
  ''' <typeparam name="TArg4"></typeparam>
  ''' <param name="asyncAction"></param>
  ''' <param name="arg1"></param>
  ''' <param name="arg2"></param>
  ''' <param name="arg3"></param>
  ''' <param name="arg4"></param>
  ''' <remarks></remarks>
  Public Sub RunAsync(Of TArg1, TArg2, TArg3, TArg4)(ByVal asyncAction As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                     ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3, ByVal arg4 As TArg4)
    asyncAction.BeginInvoke(arg1, arg2, arg3, arg4, GetCallback(AddressOf asyncAction.EndInvoke), Nothing)
  End Sub

End Class

