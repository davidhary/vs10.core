Imports System.Runtime.CompilerServices

Namespace AsyncExtensions

  ''' <summary>
  ''' Includes extenstions to executed delegates in a thread of the threadpool or in the 
  ''' thread of the user interface.
  ''' </summary>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
  ''' Licensed under the Apache License Version 2.0. 
  ''' Unless required by applicable law or agreed to in writing, this software is provided
  ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="04/11/2010" by="Mr.PoorEnglish" revision="1.2.3753.x">
  ''' From Async Worker a Type Safe Backgroun Worker By Mr.PoorEnglish
  ''' http://www.codeproject.com/KB/vb/AsyncWorker2.aspx
  ''' Created
  ''' </history>
  Public Module [Extensions]

    ''' <summary>
    ''' Executes the Delegate in a thread of the threadpool.
    ''' End Invoke action is delegated to Action.EndInvoke.
    ''' </summary>
    ''' <typeparam name="TArg1"></typeparam>
    ''' <typeparam name="TArg2"></typeparam>
    ''' <typeparam name="TArg3"></typeparam>
    ''' <typeparam name="TArg4"></typeparam>
    ''' <param name="action"></param>
    ''' <param name="arg1"></param>
    ''' <param name="arg2"></param>
    ''' <param name="arg3"></param>
    ''' <param name="arg4"></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub RunAsync(Of TArg1, TArg2, TArg3, TArg4)(ByVal action As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                       ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                       ByVal arg3 As TArg3, ByVal arg4 As TArg4)
      ' setting action.EndInvoke() as Callback-Argument makes sure, .EndInvoke() will be called
      action.BeginInvoke(arg1, arg2, arg3, arg4, AddressOf action.EndInvoke, Nothing)
    End Sub


    ''' <summary>
    ''' Executes the Delegate in a thread of the threadpool.
    ''' End Invoke action is delegated to Action.EndInvoke.
    ''' </summary>
    ''' <typeparam name="TArg1"></typeparam>
    ''' <typeparam name="TArg2"></typeparam>
    ''' <typeparam name="TArg3"></typeparam>
    ''' <param name="action"></param>
    ''' <param name="arg1"></param>
    ''' <param name="arg2"></param>
    ''' <param name="arg3"></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub RunAsync(Of TArg1, TArg2, TArg3)(ByVal action As Action(Of TArg1, TArg2, TArg3), _
                                                ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                ByVal arg3 As TArg3)
      ' setting action.EndInvoke() as Callback-Argument makes shure, .EndInvoke() will be called
      action.BeginInvoke(arg1, arg2, arg3, AddressOf action.EndInvoke, Nothing)
    End Sub

    ''' <summary>
    ''' Executes the Delegate in a thread of the threadpool.
    ''' End Invoke action is delegated to Action.EndInvoke.
    ''' </summary>
    ''' <typeparam name="TArg1"></typeparam>
    ''' <typeparam name="TArg2"></typeparam>
    ''' <param name="action"></param>
    ''' <param name="arg1"></param>
    ''' <param name="arg2"></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub RunAsync(Of TArg1, TArg2)(ByVal action As Action(Of TArg1, TArg2), _
                                         ByVal arg1 As TArg1, ByVal arg2 As TArg2)
      action.BeginInvoke(arg1, arg2, AddressOf action.EndInvoke, Nothing)
    End Sub

    ''' <summary>
    ''' Executes the Delegate in a thread of the threadpool.
    ''' End Invoke action is delegated to Action.EndInvoke.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="action"></param>
    ''' <param name="arg1"></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub RunAsync(Of T)(ByVal action As Action(Of T), ByVal arg1 As T)
      action.BeginInvoke(arg1, AddressOf action.EndInvoke, Nothing)
    End Sub

    ''' <summary>
    ''' Executes the Delegate in a thread of the threadpool.
    ''' End Invoke action is delegated to Action.EndInvoke.
    ''' </summary>
    ''' <param name="action"></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub RunAsync(ByVal action As Action)
      action.BeginInvoke(AddressOf action.EndInvoke, Nothing)
    End Sub

    ''' <summary>
    ''' Executes the specified delegate asyncroneously on the thread that the control's underlying handle was created on.
    ''' Invokes the delegate if invoke required. Otherwises, returns False allowing a direct 
    ''' execution.
    ''' </summary>
    ''' <param name="action"></param>
    ''' <param name="args"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function invokeGui(ByVal action As [Delegate], ByVal ParamArray args As Object()) As Boolean
      If Windows.Forms.Application.OpenForms.Count = 0 Then Return True
      If Windows.Forms.Application.OpenForms(0).InvokeRequired Then
        Windows.Forms.Application.OpenForms(0).BeginInvoke(action, args)
        Return True
      End If
      Return False
    End Function

    ''' <summary>
    ''' Executes the Delegate in the Gui-thread.
    ''' </summary>
    <Extension()> _
    Public Sub NotifyGui(Of TArg1, TArg2, TArg3, TArg4)(ByVal action As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                       ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                       ByVal arg3 As TArg3, ByVal arg4 As TArg4)
      'if no Invoking required execute action directly
      If Not invokeGui(action, arg1, arg2, arg3, arg4) Then action(arg1, arg2, arg3, arg4)
    End Sub


    ''' <summary>
    ''' Executes the Delegate in the Gui-thread.
    ''' </summary>
    <Extension()> _
    Public Sub NotifyGui(Of TArg1, TArg2, TArg3)(ByVal action As Action(Of TArg1, TArg2, TArg3), _
          ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3)
      'if no Invoking required execute action directly
      If Not invokeGui(action, arg1, arg2, arg3) Then action(arg1, arg2, arg3)
    End Sub

    <Extension()> _
    Public Sub NotifyGui(Of TArg1, TArg2)(ByVal action As Action(Of TArg1, TArg2), _
                                          ByVal arg1 As TArg1, ByVal arg2 As TArg2)
      If Not invokeGui(action, arg1, arg2) Then action(arg1, arg2)
    End Sub

    <Extension()> _
    Public Sub NotifyGui(Of T)(ByVal action As Action(Of T), ByVal arg As T)
      If Not InvokeGui(action, arg) Then action(arg)
    End Sub

    <Extension()> _
    Public Sub NotifyGui(ByVal action As Action)
      If Not InvokeGui(action) Then action()
    End Sub

    ''' <summary>
    ''' Invokes the delegate in the thread of the <paramref name="synchronizer">synchronizer</paramref> if invoke required.
    ''' Otherwises, returns false allowing a direct execution.
    ''' </summary>
    ''' <param name="synchronizer"></param>
    ''' <param name="action"></param>
    ''' <param name="args"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function invokeGui(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, _
                               ByVal action As [Delegate], _
                               ByVal ParamArray args As Object()) As Boolean
      If synchronizer Is Nothing Then Return True
      If synchronizer.InvokeRequired Then
        synchronizer.BeginInvoke(action, args)
        Return True
      End If
      Return False
    End Function

    ''' <summary>
    ''' Executes the Delegate in the Gui-thread.
    ''' </summary>
    <Extension()> _
    Public Sub NotifyGui(Of TArg1, TArg2, TArg3, TArg4)(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, _
                                                        ByVal action As Action(Of TArg1, TArg2, TArg3, TArg4), _
                                                        ByVal arg1 As TArg1, ByVal arg2 As TArg2, _
                                                        ByVal arg3 As TArg3, ByVal arg4 As TArg4)
      'if no Invoking required execute action directly
      If Not invokeGui(synchronizer, action, arg1, arg2, arg3, arg4) Then action(arg1, arg2, arg3, arg4)
    End Sub

    ''' <summary>
    ''' Executes the Delegate in the Gui-thread.
    ''' </summary>
    <Extension()> _
    Public Sub NotifyGui(Of TArg1, TArg2, TArg3)(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, _
                                                ByVal action As Action(Of TArg1, TArg2, TArg3), _
                                                 ByVal arg1 As TArg1, ByVal arg2 As TArg2, ByVal arg3 As TArg3)
      'if no Invoking required execute action directly
      If Not invokeGui(synchronizer, action, arg1, arg2, arg3) Then action(arg1, arg2, arg3)
    End Sub

    <Extension()> _
    Public Sub NotifyGui(Of TArg1, TArg2)(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, _
                                          ByVal action As Action(Of TArg1, TArg2), _
                                          ByVal arg1 As TArg1, ByVal arg2 As TArg2)
      If Not invokeGui(synchronizer, action, arg1, arg2) Then action(arg1, arg2)
    End Sub

    <Extension()> _
    Public Sub NotifyGui(Of T)(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, _
                               ByVal action As Action(Of T), ByVal arg As T)
      If Not invokeGui(synchronizer, action, arg) Then action(arg)
    End Sub

    <Extension()> _
    Public Sub NotifyGui(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke, _
                         ByVal action As Action)
      If Not invokeGui(synchronizer, action) Then action()
    End Sub

  End Module

End Namespace
