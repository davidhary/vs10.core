﻿''' <summary>
''' Defines the contract that must be implemented by a connectible resource such as an instrument or interface..
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/21/2011" by="David Hary" revision="1.2.4038.x">
''' Created
''' </history>
Public Interface IConnectableResource
  Inherits IConnect, IMessagePublisher

#Region " RESET "

  ''' <summary>Resets and clears the resource.</summary>
  ''' <remarks></remarks>
  ''' <history>
  ''' </history>
  Function ClearResource() As Boolean

#End Region

#Region " RESOURCE INFORMATION "

  ''' <summary>Returns a string array of the resources for this connectible resource.</summary>
  Function FindResources() As String()

  ''' <summary>
  ''' Returns true if an general visa exception occurred finding resources.
  ''' The exeption points to failure not due to parsing of the resource name
  ''' as those are trapped and addressed.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property FailedFindingResources() As Boolean?

  ''' <summary>Gets or sets the instrument identiy. Reads the instrument identity the first time it is 
  ''' called. Setting the identity allows emulation.</summary>
  Property Identity() As String

  ''' <summary>
  ''' Explains failure finding resources.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property ResourceFindingFailureSymptom() As String

  ''' <summary>Gets or sets the resource name.</summary>
  ''' <value><c>ResourceName</c> is a <c>String</c> property.</value>
  Property ResourceName() As String

  ''' <summary>Gets or sets the resource human readable name.</summary>
  ''' <value><c>ResourceName</c> is a String property.</value>
  Property ResourceTitle() As String

#End Region

End Interface
