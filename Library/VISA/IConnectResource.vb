﻿''' <summary>
''' Defines the interface for connecting and disconnecting from an instrument.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/27/2008" by="David Hary" revision="1.0.3008.x">
''' Created
''' </history>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Moved to the <see cref="isr.Core">Core Library</see>.
''' </history>
''' <history date="05/07/2009" by="David Hary" revision="1.1.3414.x">
''' Inherits from Connect.
''' </history>
''' <history date="09/02/2009" by="David Hary" revision="1.2.3532.x">
''' Rename the functions connect to connect resource allowing the use of a string as the address in the 
''' <see cref="IConnect">connect</see> interface.
''' </history>
Public Interface IConnectResource
  Inherits IConnectableResource

  ''' <summary>Connects this instance.</summary>
  ''' <returns><c>True</c> if the instance connected.</returns>
  ''' <param name="resourceName">Specifies the name of the resource to which to connect.</param>
  ''' <param name="resourceTitle">Specifies the human readable name of the resource.</param>
  ''' <remarks></remarks>
  Overloads Function Connect(ByVal resourceName As String, ByVal resourceTitle As String) As Boolean

#Region " EVENTS "

  ''' <summary>
  ''' Occurs before the instrument is connected but after obtaining a valid session.
  ''' </summary>
  ''' <remarks></remarks>
  Event Connecting As EventHandler(Of System.ComponentModel.CancelEventArgs)

  ''' <summary>Raises the connecting event.</summary>
  ''' <param name="e">Passes reference to the 
  ''' <see cref="System.ComponentModel.CancelEventArgs">cancle event arguments</see>.</param>
  Sub OnConnecting(ByVal e As System.ComponentModel.CancelEventArgs)

  ''' <summary>
  ''' Occurs before the instrument is disconnected.
  ''' </summary>
  ''' <remarks></remarks>
  Event Disconnecting As EventHandler(Of System.ComponentModel.CancelEventArgs)

  ''' <summary>Raises the disconnecting event.</summary>
  ''' <param name="e">Passes reference to the 
  ''' <see cref="System.ComponentModel.CancelEventArgs">cancle event arguments</see>.</param>
  Sub OnDisconnecting(ByVal e As System.ComponentModel.CancelEventArgs)

#End Region

End Interface

