﻿''' <summary>
''' Defines the contract that must be implemented by a connectible interface such as an Gpib Interface.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/21/2011" by="David Hary" revision="1.2.4038.x">
''' Created
''' </history>
Public Interface IConnectableInterface
  Inherits IConnectableResource

#Region " RESET "

  ''' <summary>Resets and clears the selected rresources.</summary>
  ''' <remarks></remarks>
  ''' <history>
  ''' </history>
  Overloads Function ClearSelectiveResource(ByVal resourceName As String) As Boolean

  ''' <summary>Resets and clears all resources connected to the interface.</summary>
  ''' <remarks></remarks>
  ''' <history>
  ''' </history>
  Overloads Function ClearResources() As Boolean

#End Region

#Region " RESOURCE INFORMATION "

  ''' <summary>Returns a string array of the resources for this connectible resource.</summary>
  Function FindInterfaces() As String()

#End Region

End Interface
