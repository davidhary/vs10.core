﻿''' <summary>
''' Defines the contract for providing service request information to the 
''' event handler <see cref="IRequestable"></see> servicing the request.
''' </summary>
''' <remarks>
''' This would be implemented by the VISA instrument or VISA session.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/21/2011" by="David Hary" revision="3.0.4038.x">
''' Created
''' </history>
Public Interface IServicingRequest

#Region " READ REGISTERS AND MESSAGES "

  ''' <summary>
  ''' Reads a message from the instrument.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function ReadLineTrimEnd() As String

  ''' <summary>
  ''' Reads the measurement events register status.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Override to get the event status.</remarks>
  Function ReadMeasurementEventStatus() As Integer

  ''' <summary>
  ''' Reads the operation event register status.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Override to get the event status.</remarks>
  Function ReadOperationEventStatus() As Integer

  ''' <summary>
  ''' Reads the quetionable event register status.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Override to get the event status.</remarks>
  Function ReadQuestionableEventStatus() As Integer

  ''' <summary>
  ''' Reads the standard event register status.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Override to get the event status.</remarks>
  Function ReadStandardEventStatus() As Integer

  ''' <summary>
  ''' Reads the event status register.
  ''' </summary>
  Function ReadStatusByte1() As Integer

  ''' <summary>Returns the last error from the instrument.</summary>
  Function ReadLastError() As String

  ''' <summary>Reads the error queue from the instrument.
  ''' </summary>
  Function ReadErrorQueue() As String

#End Region

End Interface
