﻿Imports System.Runtime.CompilerServices

Namespace EncodingExtensions
  ''' <summary>
  ''' Includes extensions for encoding.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
  ''' Licensed under the Apache License Version 2.0. 
  ''' Unless required by applicable law or agreed to in writing, this software is provided
  ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="10/26/2010" by="David Hary" revision="1.2.3951.x">
  ''' Created
  ''' </history>
  Public Module [Extensions]

#If False Then
0xB8 ,    // Epsilon with Tonos
0xB9 ,    // Eta with Tonos
0xBA ,    // Iota wiht Tonos
0xBC ,    // Omicron with Tonos
0xBE ,    // Upsiloin with Tonos
0xBF ,    // Omega with Tonos
0x20 ,    // space
0xC1 ,    // Alpha (UC)
0xE1 ,    // Alpha
0xE2 ,    // Beta
0xE3 ,    // Gamma
0xE4 ,    // Delta
0xE5 ,    // Epsilon
0xE6 ,    // Zeta
0xE7 ,    // Eta
0xE8 ,    // Theta
0xE9 ,    // Iota
0xEA ,    // Kappa
0xEB ,    // Lamda
0xEC ,    // Mu
0xED ,    // Nu
0xEE ,    // Xi
0xEF ,    // Omicron
0xD0 ,    // Pi
0xD1 ,    // Rho
0xD3 ,    // Sigma
0xD4 ,    // Tau
0xD5 ,    // Upsilon
0xD6 ,    // Phi
0xD7 ,    // Chi
0xD8 ,    // Psi
0xD9 ,    // Omega
0x20 ,    // space
0xDA ,    // Iota with Dialytika
0xDB  } ; // Upsilon with Dialytika
System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HD9})(0)
System.Text.Encoding.GetEncoding(1252).GetChars(New Byte() {244})(0)
System.Text.Encoding.GetEncoding(1255).GetChars(New Byte() {&HE0})(0)
#End If

    ''' <summary>
    ''' Gets the symbol for Aleph.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")> _
    Public Function Aleph() As Char
      Return Text.Encoding.GetEncoding(1255).GetChars(New Byte() {&HE0})(0)
    End Function

    ''' <summary>
    ''' Gets the symbol for eta (nano).
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Eta")> _
    Public Function Eta() As Char
      Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HE7})(0)
    End Function

    ''' <summary>
    ''' Gets the symbol for Mu (micro).
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Mu")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")> _
    Public Function MU() As Char
      Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HEC})(0)
    End Function

    ''' <summary>
    ''' Gets the symbol for Omega.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")> _
    Public Function Omega() As Char
      Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HD9})(0)
    End Function

    ''' <summary>
    ''' Gets the symbol for rho.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Rho")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="value")> _
    Public Function Rho() As Char
      Return System.Text.Encoding.GetEncoding(1253).GetChars(New Byte() {&HF1})(0)
    End Function

  End Module

End Namespace
