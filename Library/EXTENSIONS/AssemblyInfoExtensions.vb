﻿Imports System.Runtime.CompilerServices
Namespace AssemblyInfoExtensions
  ''' <summary>
  ''' Includes extensions for 
  ''' <see cref="Microsoft.VisualBasic.ApplicationServices.AssemblyInfo">assembly information</see>.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
  Public Module [Extensions]

    ''' <summary>
    ''' Returns a standard caption for forms.
    ''' </summary>
    ''' <param name="value">Specifies the <see cref="Microsoft.VisualBasic.ApplicationServices.AssemblyInfo">assembly info</see>.</param>
    ''' <returns>A <see cref="System.String">String</see> data type in the form
    '''   ProductName ww.xx.yyy.zzzzß
    '''   or
    '''   ProductName ww.xx.yyy.zzzza</returns>
    ''' <remarks>Use this function to set the captions for forms.</remarks>
    ''' <history date="09/05/03" by="David Hary" revision="1.0.1342.x">
    '''   Add methods not requiring assembly specifications
    ''' </history>
    <Extension()> _
    Public Function ExtendedCaption(ByVal value As Microsoft.VisualBasic.ApplicationServices.AssemblyInfo) As String

      ' set the caption using the product name
      Dim builder As New System.Text.StringBuilder
      builder.Append(value.AssemblyName)
      builder.Append(" ")
      builder.Append(value.Version.ToString(4))
      If value.Version.Major < 1 Then
        builder.Append(".")
        Select Case value.Version.Minor
          Case 0
            builder.Append(Convert.ToChar(&H3B1))
            ' builder.Append(ChrW(&H3B1)) ' alpha Chr(224)
          Case 1
            builder.Append(Convert.ToChar(&H3B2))
            ' builder.Append(ChrW(&H3B2)) ' beta Chr(225)
          Case 2 To 8
            builder.Append(String.Format(System.Globalization.CultureInfo.CurrentCulture, "RC{0}", value.Version.Major - 1))
          Case Else
            builder.Append("Gold")
        End Select
      End If
      Return builder.ToString

    End Function

  End Module

End Namespace
