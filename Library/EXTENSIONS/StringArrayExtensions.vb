﻿Imports System.Runtime.CompilerServices
Namespace StringArrayExtensions
    ''' <summary>
    ''' Includes extensions for string arrays.
    ''' </summary>
    ''' <remarks></remarks>
    ''' <license>
    ''' (c) 2011 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="08/27/2011" by="David Hary" revision="1.2.4256.x">
    ''' Created
    ''' </history>  
    Public Module [Extensions]

        ''' <summary>
        ''' Combines the specified string array using the <paramref name="terminator">terminator</paramref> .
        ''' </summary>
        ''' <param name="value">The string array.</param>
        ''' <param name="terminator">The terminator.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Extension()> _
        Public Function Combine(ByVal value As String(), ByVal terminator As String) As String
            Dim builder As New System.Text.StringBuilder
            For Each s As String In value
                If builder.Length > 0 Then
                    builder.Append(terminator)
                End If
                builder.Append(s)
            Next
            Return builder.ToString
        End Function

    End Module

End Namespace
