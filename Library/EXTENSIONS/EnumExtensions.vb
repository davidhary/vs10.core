﻿Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Windows.Forms

Namespace EnumExtensions

  ''' <summary>
  ''' Includes extensions for enumerations and enumerated types.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2009 Integrated Scientific Resources, Inc.
  ''' Licensed under the Apache License Version 2.0. 
  ''' Unless required by applicable law or agreed to in writing, this software is provided
  ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
  ''' Created
  ''' </history>
  Public Module [Extensions]

    ''' <summary>
    ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
    ''' </summary>
    ''' <param name="value">The <see cref="System.Enum"/> type value.</param>
    ''' <returns>A string containing the text of the <see cref="DescriptionAttribute"/>.</returns>
    <Extension()> _
    Public Function Description(ByVal value As System.Enum) As String

      Dim candidate As String = value.ToString()
      Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
      Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

      If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
        candidate = attributes(0).Description
      End If
      Return candidate

    End Function

#Region " ENUM HELPERS "

    ''' <summary>
    ''' Gets a Key Value Pair description item.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueDescriptionPair(ByVal value As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
      Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(value, value.Description)
    End Function

    ''' <summary>
    '''  Converts the <see cref="System.Enum"/> type to an <see cref="IList"/> compatible object.
    ''' </summary>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <returns>An <see cref="IList"/> containing the enumerated type value and description.</returns>
    ''' <remarks>
    ''' <example>
    ''' The first step is to add a description attribute to your enum.
    ''' <code>
    '''     Public Enum SimpleEnum2
    '''       [System.ComponentModel.Description("Today")] Today
    '''       [System.ComponentModel.Description("Last 7 days")] Last7
    '''       [System.ComponentModel.Description("Last 14 days")] Last14
    '''       [System.ComponentModel.Description("Last 30 days")] Last30
    '''       [System.ComponentModel.Description("All")] All
    '''     End Enum
    '''     Dim combo As ComboBox = New ComboBox()
    '''     combo.DataSource = EnumHelper.ValueDescriptionPairs(GetType(SimpleEnum2))
    '''     combo.DisplayMember = "Value"
    '''     combo.ValueMember = "Key"
    ''' </code>
    ''' This works with any control that supports data binding, including the 
        ''' <see cref="ToolStripComboBox">tool strip combo box</see>, 
    ''' although you will need to cast the <see cref="ToolStripComboBox.Control">control property</see>
    ''' to a <see cref="ComboBox">Combo Box</see> to get to the DataSource property. 
    ''' In that case, you will also want to perform the same cast when you are referencing the 
    ''' selected value to work with it as your enum type.
    ''' </example>
    ''' </remarks>
    <Extension()> _
    Public Function ValueDescriptionPairs(ByVal type As Type) As IList

      Dim keyValuePairs As ArrayList = New ArrayList()
      For Each value As System.Enum In System.Enum.GetValues(type)
        keyValuePairs.Add(value.ValueDescriptionPair())
      Next value
      Return keyValuePairs

    End Function

    ''' <summary>
    '''  Converts the <see cref="System.Enum"/> type to an <see cref="IList"/> compatible object.
    ''' </summary>
    ''' <param name="type">Specifies th Enum type.</param>
    ''' <param name="mask">Specifies a mask for selecting the items to include in the list.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueDescriptionPairs(ByVal type As Type, ByVal mask As Integer) As IList

      Dim keyValuePairs As ArrayList = New ArrayList()
      For Each value As System.Enum In System.Enum.GetValues(type)
        Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
        If (enumValue And mask) <> 0 Then
          keyValuePairs.Add(value.ValueDescriptionPair())
        End If
      Next value

      Return keyValuePairs
    End Function

    ''' <summary>
    ''' Returns the descriptions of an enumeration.
    ''' </summary>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Descriptions(ByVal type As Type) As String()

      Dim values As New System.Collections.Generic.List(Of String)
      For Each enumValue As System.Enum In System.Enum.GetValues(type)
        values.Add(enumValue.Description())
      Next enumValue
      Return values.ToArray

    End Function

    ''' <summary>
    ''' Returns the descriptions of an flags enumerated list masked by the specified value.
    ''' </summary>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="mask">Allows returning only selected items.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Descriptions(ByVal type As Type, ByVal mask As Integer) As String()

      Dim values As New System.Collections.Generic.List(Of String)
      For Each value As System.Enum In System.Enum.GetValues(type)
        Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
        If (enumValue And mask) <> 0 Then
          values.Add(value.Description())
        End If
      Next value
      Return values.ToArray

    End Function

    ''' <summary>
    ''' Returns the Names of an flags enumerated list masked by the specified value.
    ''' </summary>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="mask">Allows returning only selected items.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Names(ByVal type As Type, ByVal mask As Integer) As String()

      Dim values As New System.Collections.Generic.List(Of String)
      For Each value As System.Enum In System.Enum.GetValues(type)
        Dim enumValue As Integer = Convert.ToInt32(value, Globalization.CultureInfo.CurrentCulture)
        If (enumValue And mask) <> 0 Then
          values.Add(System.Enum.GetName(type, value))
        End If
      Next value
      Return values.ToArray()

    End Function

#End Region

#Region " WEBDEV_HB. Under license from "

    ''' <summary>
    ''' Returns true if the specified bits are set.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to compare.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history date="06/05/2010" by="David Hary" revision="1.2.3807.x">
    ''' Returns true if equals (IS).
    ''' </history>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function IsSet(Of T)(ByVal type As System.Enum, ByVal value As T) As Boolean
      Return Has(type, value)
    End Function

    ''' <summary>
    ''' Returns true if the enumerated flag has the specified bits.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to compare.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history date="07/07/2009" by="David Hary" revision="1.2.3475.x">
    ''' http://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    ''' <history date="06/05/2010" by="David Hary" revision="1.2.3807.x">
    ''' Returns true if equals (IS).
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Has(Of T)(ByVal type As System.Enum, ByVal value As T) As Boolean
      Try
        Dim oldValue As Integer = convertToInteger(Of T)(type)
        Dim bitsValue As Integer = convertToInteger(Of T)(value)
        Return (oldValue = bitsValue) OrElse ((oldValue And bitsValue) = bitsValue)
        ' Return ((CInt(Fix(CObj(type))) And CInt(Fix(CObj(value)))) = CInt(Fix(CObj(value))))
      Catch
        Return False
      End Try
    End Function

    ''' <summary>
    ''' Returns true if value is only the provided type.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to compare.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history date="07/07/2009" by="David Hary" revision="1.2.3475.x">
    ''' http://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    <System.Runtime.CompilerServices.Extension()> _
    Public Function [Is](Of T)(ByVal type As System.Enum, ByVal value As T) As Boolean
      Try
        Dim oldValue As Integer = convertToInteger(Of T)(type)
        Dim bitsValue As Integer = convertToInteger(Of T)(value)
        Return oldValue = bitsValue
        ' Return CInt(Fix(CObj(type))) = CInt(Fix(CObj(value)))
      Catch
        Return False
      End Try
    End Function

    ''' <summary>
    ''' Set the specified bits.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to add.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function [Set](Of T)(ByVal type As System.Enum, ByVal value As T) As T
      Return Add(type, value)
    End Function

    ''' <summary>
    ''' Appends a value
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to add.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history date="07/07/2009" by="David Hary" revision="1.2.3475.x">
    ''' http://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Add(Of T)(ByVal type As System.Enum, ByVal value As T) As T
      Try
        Dim oldValue As Integer = convertToInteger(Of T)(type)
        Dim bitsValue As Integer = convertToInteger(Of T)(value)
        Dim newValue As Integer = oldValue Or bitsValue
        Return CType(CObj(newValue), T)
        ' Return CType(CObj((CInt(Fix(CObj(type))) Or CInt(Fix(CObj(value))))), T)
      Catch ex As Exception
        Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                  "Could not append value from enumerated type '{0}'.", GetType(T).Name), ex)
      End Try
    End Function

    ''' <summary>
    ''' Clears the specified bits.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to remove.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Clear(Of T)(ByVal type As System.Enum, ByVal value As T) As T
      Return Remove(type, value)
    End Function

    ''' <summary>
    ''' Completely removes the value
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="type">The <see cref="System.Enum"/> type.</param>
    ''' <param name="value">Specifies the bit values which to remove.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history date="07/07/2009" by="David Hary" revision="1.2.3475.x">
    ''' http://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-my-bad-pun.aspx
    ''' Under The Creative Commons Attribution-ShareAlike 2.5 License
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Remove(Of T)(ByVal type As System.Enum, ByVal value As T) As T
      Try
        Dim oldValue As Integer = convertToInteger(Of T)(type)
        Dim bitsValue As Integer = convertToInteger(Of T)(value)
        Dim newValue As Integer = oldValue And (Not bitsValue)
        Return CType(CObj(newValue), T)
        ' Return CType(CObj((CInt(Microsoft.VisualBasic.Fix(CObj(type))) And (Not CInt(Microsoft.VisualBasic.Fix(CObj(value)))))), T)
      Catch ex As Exception
        Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                  "Could not remove value from enumerated type '{0}'.", GetType(T).Name), ex)
      End Try
    End Function

    Private Function convertToInteger(Of T)(ByVal value As T) As Integer
      Return CInt(CObj(value))
      ' Return CInt(Microsoft.VisualBasic.Fix(CObj(value)))
    End Function

    Private Function convertToInteger(Of T)(ByVal type As System.Enum) As Integer
      Return CInt(CObj(type))
      ' Return CInt(Microsoft.VisualBasic.Fix(CObj(type)))
    End Function

#End Region

  End Module

End Namespace
