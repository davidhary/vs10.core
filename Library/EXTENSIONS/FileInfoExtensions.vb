﻿Imports System.Runtime.CompilerServices
Namespace IOExtensions
  ''' <summary>
  ''' Includes extensions for 
  ''' <see cref="system.IO.FileInfo">file info</see>.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
  ''' Licensed under the Apache License Version 2.0. 
  ''' Unless required by applicable law or agreed to in writing, this software is provided
  ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
  ''' Created
  ''' </history>
  Public Module [Extensions]

    ''' <summary>Returns the file size, -2 if path not specified or -1 if file not found.</summary>
    ''' <param name="pathName"></param>
    Public Function FileSize(ByVal pathName As String) As Long
      Dim info As System.IO.FileInfo = New System.IO.FileInfo(pathName)
      Return info.FileSize()
    End Function

    ''' <summary>
    ''' Returns the file size, -2 if path not specified or -1 if file not found.</summary>
    ''' <param name="value"></param>
    <Extension()> _
    Public Function FileSize(ByVal value As System.IO.FileInfo) As Long

      If value.Exists Then
        Return value.Length
      ElseIf String.IsNullOrEmpty(value.Name) Then
        Return -2
      Else
        Return -1
      End If

    End Function

    ''' <summary>
    ''' Returns the file name without extension.</summary>
    ''' <param name="value"></param>
    <Extension()> _
    Public Function Title(ByVal value As System.IO.FileInfo) As String

      If String.IsNullOrEmpty(value.Name) Then
        Return ""
      Else
        If String.IsNullOrEmpty(value.Extension) Then
          Return value.Name
        Else
          Return value.Name.Substring(0, value.Name.LastIndexOf(value.Extension, StringComparison.OrdinalIgnoreCase))
        End If
      End If

    End Function

  End Module
End Namespace
