﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms
Namespace ControlExtensions

  ''' <summary>
  ''' Includes extensions for controls.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
  ''' Licensed under the Apache License Version 2.0. 
  ''' Unless required by applicable law or agreed to in writing, this software is provided
  ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="11/19/2010" by="David Hary" revision="1.2.3975.x">
  ''' Created
  ''' </history>
  Public Module [Extensions]

#Region " BINDING "

    ''' <summary>
    ''' Replaces the binding for the bound property of the control
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="binding"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Replace(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Windows.Forms.Binding
      For i As Integer = 0 To value.Count - 1
        If value.Item(i).PropertyName = binding.PropertyName Then
          value.RemoveAt(i)
          Exit For
        End If
      Next
      value.Add(binding)
      Return binding
    End Function

    ''' <summary>
    ''' Adds a binding to the control binding collection if the biniding does not already exist on this control.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="binding"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function AddIfNew(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Windows.Forms.Binding
      For Each b As Binding In value
        If b.Equals(binding) Then
          Return binding
        End If
      Next
      value.Add(binding)
      Return binding
    End Function

    ''' <summary>
    ''' Adds a binding to the control. Disables the control while adding the binding to allow the
    ''' disabling of control events while the binding is added. Note that the control event of value 
    ''' change occurs before the bound count increments so the bound count cannot be used to determine
    ''' the control bindable status.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="binding"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentAdd(ByVal value As ControlBindingsCollection, ByVal binding As Binding) As Windows.Forms.Binding
      Dim isEnabled As Boolean = value.Control.Enabled
      value.Control.Enabled = False
      value.Add(binding)
      value.Control.Enabled = isEnabled
      Return binding
    End Function

#End Region

#Region " COMBO BOX SETTERS "

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Combo box control.</param>
    ''' <param name="value">The selected item value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSelectItem(ByVal control As ComboBox, ByVal value As Object) As Object
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ComboBox, Object)(AddressOf SafeSelectItem), New Object() {control, value})
      Else
        control.SelectedItem = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
    ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Combo box control.</param>
    ''' <param name="key">The selected item key.</param>
    ''' <param name="value">The selected item value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSelectItem(ByVal control As ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
      Return SafeSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Combo box control.</param>
    ''' <param name="value">The selected item value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentSelectItem(ByVal control As ComboBox, ByVal value As Object) As Object
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ComboBox, Object)(AddressOf SafeSilentSelectItem), New Object() {control, value})
      Else
        SilentSelectItem(control, value)
      End If
      Return value
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
    ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Combo box control.</param>
    ''' <param name="key">The selected item key.</param>
    ''' <param name="value">The selected item value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentSelectItem(ByVal control As ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
      Return SafeSilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
    ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">Combo box control.</param>
    ''' <param name="value">The selected item value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentSelectItem(ByVal control As ComboBox, ByVal value As Object) As Object
      Dim wasEnabled As Boolean = control.Enabled
      control.Enabled = False
      control.SelectedItem = value
      control.Enabled = wasEnabled
      Return value
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> item by setting the selected item to the 
    ''' <paramref name="System.Collections.Generic.KeyValuePair">key value pair</paramref>.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">Combo box control.</param>
    ''' <param name="key">The selected item key.</param>
    ''' <param name="value">The selected item value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentSelectItem(ByVal control As ComboBox, ByVal key As System.Enum, ByVal value As String) As Object
      Return SilentSelectItem(control, New System.Collections.Generic.KeyValuePair(Of [Enum], String)(key, value))
    End Function


    ''' <summary>
    ''' Sets the <see cref="ComboBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeTextSetter(ByVal control As ComboBox, ByVal value As String) As String
      If String.IsNullOrEmpty(value) Then
        value = ""
      End If
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ComboBox, String)(AddressOf SafeTextSetter), New Object() {control, value})
      Else
        control.Text = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="ComboBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeSilentTextSetter(ByVal control As ComboBox, ByVal value As String) As String
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ComboBox, String)(AddressOf SafeSilentTextSetter), New Object() {control, value})
      Else
        SilentTextSetter(control, value)
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="ComboBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SilentTextSetter(ByVal control As ComboBox, ByVal value As String) As String
      Dim enabled As Boolean = control.Enabled
      control.Enabled = False
      If String.IsNullOrEmpty(value) Then
        value = ""
      End If
      control.Text = value
      control.Enabled = enabled
      Return value
    End Function


#End Region

#Region " CHECK BOX SETTERS "

    ''' <summary>
    ''' Sets the <see cref="Control">check box</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">Check box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentCheckedSetter(ByVal control As CheckBox, ByVal value As Boolean) As Boolean
      Dim wasEnabled As Boolean = control.Enabled
      control.Enabled = False
      control.Checked = value
      control.Enabled = wasEnabled
      Return value
    End Function


    ''' <summary>
    ''' Sets the <see cref="Control">check box</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">Check box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentCheckedSetter(ByVal control As CheckBox, ByVal value As Boolean) As Boolean
      If control.InvokeRequired Then
        control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf SafeSilentCheckedSetter), New Object() {control, value})
      Else
        SilentCheckedSetter(control, value)
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="Control">check box</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Check box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeCheckedSetter(ByVal control As CheckBox, ByVal value As Boolean) As Boolean
      If control.InvokeRequired Then
        control.Invoke(New Action(Of CheckBox, Boolean)(AddressOf SafeCheckedSetter), New Object() {control, value})
      Else
        control.Checked = value
      End If
      Return value
    End Function

#End Region

#Region " CONTROL SAFE SETTERS "

    ''' <summary>
    ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see> center middle using the control font.
    ''' </summary>
    ''' <param name="control">The target progress bar to add text into</param>
    ''' <param name="value">The text to add into the progress bar. 
    ''' Leave null or empty to automatically add the percent.</param>
    ''' <remarks>
    ''' Call this method after changing the progress bar's value. 
    ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
    ''' call the Refresh method of the progress bar before calling this method.
    ''' </remarks>
    ''' <overloads>
    ''' Draws text into a control.
    ''' </overloads>
    ''' <shoutouts>
    ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
    ''' </shoutouts>
    <Extension()> _
    Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.Control, ByVal value As String)
      SafelyDrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
    End Sub

    ''' <summary>
    ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
    ''' </summary>
    ''' <param name="control">The target progress bar to add text into</param>
    ''' <param name="value">The text to add into the progress bar. 
    ''' Leave null or empty to automatically add the percent.</param>
    ''' <param name="textAlign">Where the text is to be placed</param>
    ''' <remarks>
    ''' Call this method after changing the progress bar's value. 
    ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
    ''' call the Refresh method of the progress bar before calling this method.
    ''' </remarks>
    ''' <overloads>
    ''' Draws text into a control.
    ''' </overloads>
    ''' <shoutouts>
    ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
    ''' </shoutouts>
    <Extension()> _
    Public Sub SafelyDrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
      SafelyDrawText(control, value, textAlign, control.Font)
    End Sub

    ''' <summary>
    ''' Draws text into a  <see cref="System.Windows.Forms.Control">windows control</see>.
    ''' </summary>
    ''' <param name="control">The target progress bar to add text into</param>
    ''' <param name="value">The text to add into the progress bar. 
    ''' Leave null or empty to automatically add the percent.</param>
    ''' <param name="textAlign">Where the text is to be placed</param>
    ''' <param name="textFont">The font the text should be drawn in</param>
    ''' <remarks>
    ''' Call this method after changing the progress bar's value. 
    ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
    ''' call the Refresh method of the progress bar before calling this method.
    ''' </remarks>
    ''' <overloads>
    ''' Draws text into a control.
    ''' </overloads>
    ''' <shoutouts>
    ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
    ''' </shoutouts>
    <Extension()> _
    Public Sub SafelyDrawText(ByVal control As Control, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment, _
                              ByVal textFont As System.Drawing.Font)

      If control.InvokeRequired Then
        control.Invoke(New Action(Of Control, String, Drawing.ContentAlignment, Drawing.Font)(AddressOf SafelyDrawText), New Object() {control, value, textAlign, textFont})
      Else
        Using gr As Drawing.Graphics = control.CreateGraphics()
          Const margin As Single = 0
          Dim x As Single = margin
          Dim y As Single = (control.Height - gr.MeasureString(value, textFont).Height) / 2.0F
          If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.MiddleCenter OrElse textAlign = Drawing.ContentAlignment.TopCenter Then
            x = (control.Width - gr.MeasureString(value, textFont).Width) / 2.0F
          ElseIf textAlign = Drawing.ContentAlignment.BottomRight OrElse textAlign = Drawing.ContentAlignment.MiddleRight OrElse textAlign = Drawing.ContentAlignment.TopRight Then
            x = control.Width - gr.MeasureString(value, textFont).Width - margin
          End If
          If textAlign = Drawing.ContentAlignment.BottomCenter OrElse textAlign = Drawing.ContentAlignment.BottomLeft OrElse textAlign = Drawing.ContentAlignment.BottomRight Then
            y = control.Height - gr.MeasureString(value, textFont).Height - margin
          ElseIf textAlign = Drawing.ContentAlignment.TopCenter OrElse textAlign = Drawing.ContentAlignment.TopLeft OrElse textAlign = Drawing.ContentAlignment.TopRight Then
            y = margin
          End If
          y = Math.Max(y, margin)
          x = Math.Max(x, margin)
          gr.DrawString(value, textFont, New Drawing.SolidBrush(control.ForeColor), New Drawing.PointF(x, y))
        End Using
      End If

    End Sub

    ''' <summary>
    ''' Sets the <see cref="Control">control</see> enabled value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeEnabledSetter(ByVal control As Control, ByVal value As Boolean) As Boolean
      If control.InvokeRequired Then
        control.Invoke(New Action(Of Control, Boolean)(AddressOf SafeEnabledSetter), New Object() {control, value})
      Else
        control.Enabled = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="Control">control</see> enabled value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeVisibleSetter(ByVal control As Control, ByVal value As Boolean) As Boolean
      If control.InvokeRequired Then
        control.Invoke(New Action(Of Control, Boolean)(AddressOf SafeVisibleSetter), New Object() {control, value})
      Else
        control.Visible = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeTextSetter(ByVal control As Control, ByVal value As String) As String
      If String.IsNullOrEmpty(value) Then
        value = ""
      End If
      If control.InvokeRequired Then
        control.Invoke(New Action(Of Control, String)(AddressOf SafeTextSetter), New Object() {control, value})
      Else
        control.Text = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="format">The text format.</param>
    ''' <param name="args">The format arguments.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
      Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeSilentTextSetter(ByVal control As Control, ByVal value As String) As String
      If control.InvokeRequired Then
        control.Invoke(New Action(Of Control, String)(AddressOf SafeSilentTextSetter), New Object() {control, value})
      Else
        SilentTextSetter(control, value)
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="format">The text format.</param>
    ''' <param name="args">The format arguments.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeSilentTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
      Return SafeSilentTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SilentTextSetter(ByVal control As Control, ByVal value As String) As String
      Dim enabled As Boolean = control.Enabled
      control.Enabled = False
      If String.IsNullOrEmpty(value) Then
        value = ""
      End If
      control.Text = value
      control.Enabled = enabled
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="format">The text format.</param>
    ''' <param name="args">The format arguments.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SilentTextSetter(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object) As String
      Return SilentTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

#End Region

#Region " DATA GRID VIEW "

    ''' <summary>
    ''' Replaces the column header text with one that has spaces between lower and upper case characters.
    ''' </summary>
    ''' <param name="column">The column</param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub ParseHeaderText(ByVal column As DataGridViewColumn)
      column.HeaderText = isr.Core.StringExtensions.SplitWords(column.HeaderText)
    End Sub

    ''' <summary>
    ''' Replaces the column headers text with one that has spaces between lower and upper case charactes.
    ''' </summary>
    ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub ParseHeaderText(ByVal grid As DataGridView)
      For Each column As DataGridViewColumn In grid.Columns
        column.ParseHeaderText()
      Next
    End Sub

    ''' <summary>
    ''' Replaces the column headers text with one that has spaces between lower and upper case charactes.
    ''' Operates only on visible headers.
    ''' </summary>
    ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub ParseVisibleHeaderText(ByVal grid As DataGridView)
      For Each column As DataGridViewColumn In grid.Columns
        If column.Visible Then
          column.ParseHeaderText()
        End If
      Next
    End Sub

    ''' <summary>
    ''' Replaces an existing column with a combo box column and returs reference to this column.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ReplaceWithComboColumn(ByVal grid As DataGridView, ByVal columnName As String) As DataGridViewComboBoxColumn
      Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
      Dim gridColumn As New System.Windows.Forms.DataGridViewComboBoxColumn
      gridColumn.Name = column.Name
      gridColumn.DataPropertyName = column.DataPropertyName
      gridColumn.HeaderText = column.HeaderText
      'gridColumn.IsDataBound = column.IsDataBound
      grid.Columns.Remove(column)
      ' this does not work right. Me._dataGridView.Columns.Insert(fieldColumn.Ordinal, gridColumn)
      grid.Columns.Insert(column.Index, gridColumn)
      Return gridColumn
    End Function

    ''' <summary>
    ''' Moves an existing column to the specified location.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function MoveTo(ByVal grid As DataGridView, ByVal columnName As String, ByVal location As Integer) As DataGridViewColumn
      Dim column As DataGridViewColumn = grid.Columns.Item(columnName)
      grid.Columns.Remove(column)
      grid.Columns.Insert(location, column)
      Return column
    End Function

    ''' <summary>
    ''' Returns the row selected based on the column value.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function FindRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As DataGridViewRow
      Dim foundRow As DataGridViewRow = Nothing
      For Each r As DataGridViewRow In grid.Rows
        If columnValue.Equals(r.Cells(columnName).Value) Then
          foundRow = r
          Exit For
        End If
      Next
      Return foundRow
    End Function

    ''' <summary>
    ''' Selects the row.
    ''' </summary>
    ''' <typeparam name="T">The type of the column value</typeparam>
    ''' <param name="grid">The grid.</param>
    ''' <param name="columnName">Name of the column.</param>
    ''' <param name="columnValue">The column value.</param>
    ''' <returns>True if the row was selected.</returns>
    <Extension()> _
    Public Function SelectRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As Boolean
      Dim foundRow As DataGridViewRow = FindRow(Of T)(grid, columnName, columnValue)
      If foundRow Is Nothing Then
        Return False
      Else
        foundRow.Selected = True
        Return True
      End If
    End Function

    ''' <summary>
    ''' Silently selects a grid row.
    ''' The control is disabled when set so that the 
    ''' handling of the changed event can be skipped.
    ''' </summary>
    ''' <typeparam name="T">The type of the column value</typeparam>
    ''' <param name="grid">The grid.</param>
    ''' <param name="columnName">Name of the column.</param>
    ''' <param name="columnValue">The column value.</param>
    ''' <returns>True if the row was selected.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentSelectRow(Of T)(ByVal grid As DataGridView, ByVal columnName As String, ByVal columnValue As T) As Boolean
      Dim enabled As Boolean = grid.Enabled
      Try
        grid.Enabled = False
        Return SelectRow(Of T)(grid, columnName, columnValue)
      Catch
        Throw
      Finally
        grid.Enabled = enabled
      End Try
    End Function

#End Region

#Region " LABEL SETTERS "

    ''' <summary>
    ''' Sets the <see cref="Label">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeTextSetter(ByVal control As Label, ByVal value As String) As String
      If control.InvokeRequired Then
        control.Invoke(New Action(Of Label, String)(AddressOf SafeTextSetter), New Object() {control, value})
      Else
        control.Text = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="Label">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="format">The text format.</param>
    ''' <param name="args">The format arguments.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeTextSetter(ByVal control As Label, ByVal format As String, ByVal ParamArray args() As Object) As String
      Return SafeTextSetter(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

#End Region

#Region " LIST CONTROLS: COMBO BOX; LIST BOX "

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> value by setting the Selected value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="control">The control.</param>
    ''' <param name="value">The value.</param><returns></returns>
    <Extension()> _
    Public Function SafeSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ComboBox, T)(AddressOf SafeSelectValue), New Object() {control, value})
      Else
        control.SelectedValue = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> value by setting the Selected value to the 
    ''' <paramref name="value">value</paramref>.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="control">The control.</param>
    ''' <param name="value">The value.</param><returns></returns>
    <Extension()> _
    Public Function SilentSelectValue(Of T)(ByVal control As ListControl, ByVal value As T) As T
      Dim enabled As Boolean = control.Enabled
      control.Enabled = False
      control.SelectedValue = value
      control.Enabled = enabled
      Return value
    End Function

    ''' <summary>
    ''' Selects the the <see cref="Control">combo box</see> value by setting the Selected value to the 
    ''' <paramref name="value">value</paramref>.
    ''' The control is disabled when set so that the handling of the changed event can be skipped.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="control">The control.</param>
    ''' <param name="value">The value.</param><returns></returns>
    <Extension()> _
    Public Function SafeSilentTextSetter(Of T)(ByVal control As ListControl, ByVal value As T) As T
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ComboBox, Object)(AddressOf SafeSilentTextSetter), New Object() {control, value})
      Else
        SafeSilentTextSetter(control, value)
      End If
      Return value
    End Function

#End Region

#Region " NUMERIC UP DOWN SETTERS "

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> scaled by the 
    ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
    ''' Retuns limited value (control value divided by the scaler).
    ''' The setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The control value divided by the scaler.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
      Return SafeValueSetter(control, scalar * value) / scalar
#If False Then
      If control.InvokeRequired Then
        control.Invoke(New Action(Of NumericUpDown, Decimal, Decimal)(AddressOf SafeValueSetter), New Object() {control, scalar, value})
        Return value
      Else
        Dim scaledValue As Decimal = Math.Max(control.Minimum, Math.Min(control.Maximum, scalar * value))
        valuesetter(control, scaledValue)
        Return scaledValue / scalar
      End If
#End If
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> scaled by the 
    ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
    ''' Retuns limited value (control value divided by the scaler).
    ''' This setter is thread safe.
    ''' Retuns limited value (control value divided by the scaler).
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The control value divided by the scaler.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
      Return SafeSilentValueSetter(control, scalar * value) / scalar
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
      If control.InvokeRequired Then
        control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf SafeValueSetter), New Object() {control, value})
      Else
        ValueSetter(control, value)
      End If
      Return control.Value
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' Retuns limited value (control value divided by the scaler).
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
      If control.InvokeRequired Then
        control.Invoke(New Action(Of NumericUpDown, Decimal)(AddressOf SafeValueSetter), New Object() {control, value})
      Else
        SilentValueSetter(control, value)
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
      Return SafeValueSetter(control, CDec(value))
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' Retuns limited value (control value divided by the scaler).
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
      Return SafeSilentValueSetter(control, CDec(value))
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> scaled by the 
    ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
    ''' Retuns limited value (control value divided by the scaler).
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The control value divided by the scaler.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
      Return ValueSetter(control, scalar * value) / scalar
#If False Then
      control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, scalar * value))
      Return control.Value / scalar
#End If
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> scaled by the 
    ''' <paramref name="scalar">scalar</paramref> and limited by the control range.
    ''' Retuns limited value (control value divided by the scaler).
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The control value divided by the scaler.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal scalar As Decimal, ByVal value As Decimal) As Decimal
      Return SilentValueSetter(control, scalar * value) / scalar
#If False Then
      control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, scalar * value))
      Return control.Value / scalar
#End If
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
      control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
      Return control.Value
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal value As Decimal) As Decimal
      Dim wasEnabled As Boolean = control.Enabled
      control.Enabled = False
      ValueSetter(control, value)
      control.Enabled = wasEnabled
      Return control.Value
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Double
      Return ValueSetter(control, CDec(value))
    End Function

    ''' <summary>
    ''' Sets the <see cref="NumericUpDown">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SilentValueSetter(ByVal control As NumericUpDown, ByVal value As Double) As Decimal
      Dim wasEnabled As Boolean = control.Enabled
      control.Enabled = False
      ValueSetter(control, CDec(value))
      control.Enabled = wasEnabled
      Return control.Value
    End Function


#End Region

#Region " TOOL STRIP PROGRESS BAR SETTERS "

    ''' <summary>
    ''' Sets the <see cref="ToolStripProgressBar">control</see> Enabled to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeEnabledSetter(ByVal control As ToolStripItem, ByVal value As Boolean) As Boolean
      If control.Owner.InvokeRequired Then
        control.Owner.Invoke(New Action(Of ToolStripItem, Boolean)(AddressOf SafeEnabledSetter), New Object() {control, value})
      Else
        control.Enabled = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="ToolStripProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
      If control.Owner.InvokeRequired Then
        control.Owner.Invoke(New Action(Of ToolStripProgressBar, Integer)(AddressOf SafeValueSetter), New Object() {control, value})
      Else
        value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
        control.Value = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Updates the <see cref="ToolStripProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueUpdater(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
      If control.Value <> value Then
        Return SafeValueSetter(control, value)
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="ToolStripItem">control</see> visible to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeVisibleSetter(ByVal control As ToolStripItem, ByVal value As Boolean) As Boolean
      If control.Owner.InvokeRequired Then
        control.Owner.Invoke(New Action(Of ToolStripItem, Boolean)(AddressOf SafeVisibleSetter), New Object() {control, value})
      Else
        control.Visible = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="ProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueSetter(ByVal control As ToolStripProgressBar, ByVal value As Integer) As Integer
      control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
      Return CInt(control.Value)
    End Function

#End Region

#Region " PROGRESS BAR SETTERS "

    ''' <summary>
    ''' Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see> center middle using the control font.
    ''' </summary>
    ''' <param name="control">The target progress bar to add text into</param>
    ''' <param name="value">The text to add into the progress bar. 
    ''' Leave null or empty to automatically add the percent.</param>
    ''' <remarks>
    ''' Call this method after changing the progress bar's value. 
    ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
    ''' call the Refresh method of the progress bar before calling this method.
    ''' </remarks>
    ''' <overloads>
    ''' Draws text into a control.
    ''' </overloads>
    ''' <shoutouts>
    ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
    ''' </shoutouts>
    <Extension()> _
    Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String)
      SafelyDrawText(control, value, Drawing.ContentAlignment.MiddleCenter, control.Font)
    End Sub

    ''' <summary>
    ''' Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>.
    ''' </summary>
    ''' <param name="control">The target progress bar to add text into</param>
    ''' <param name="value">The text to add into the progress bar. 
    ''' Leave null or empty to automatically add the percent.</param>
    ''' <param name="textAlign">Where the text is to be placed</param>
    ''' <remarks>
    ''' Call this method after changing the progress bar's value. 
    ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
    ''' call the Refresh method of the progress bar before calling this method.
    ''' </remarks>
    ''' <overloads>
    ''' Draws text into a control.
    ''' </overloads>
    ''' <shoutouts>
    ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
    ''' </shoutouts>
    <Extension()> _
    Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment)
      If String.IsNullOrEmpty(value) Then
        Dim fraction As Double = CDbl(control.Value - control.Minimum) / CDbl(control.Maximum - control.Minimum)
        value = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0%}", fraction)
      End If
      SafelyDrawText(CType(control, System.Windows.Forms.Control), value, textAlign, control.Font)
    End Sub

    ''' <summary>
    ''' Draws text into a  <see cref="System.Windows.Forms.ProgressBar">progress bar</see>.
    ''' </summary>
    ''' <param name="control">The target progress bar to add text into</param>
    ''' <param name="value">The text to add into the progress bar. 
    ''' Leave null or empty to automatically add the percent.</param>
    ''' <param name="textAlign">Where the text is to be placed</param>
    ''' <param name="textFont">The font the text should be drawn in</param>
    ''' <remarks>
    ''' Call this method after changing the progress bar's value. 
    ''' If for some reason, the changing of the progress bar's value doesn't refresh it and clear the previously drawn text, 
    ''' call the Refresh method of the progress bar before calling this method.
    ''' </remarks>
    ''' <overloads>
    ''' Draws text into a control.
    ''' </overloads>
    ''' <shoutouts>
    ''' Jacob Jordan http://www.dreamincode.net/forums/topic/94631-add-the-percent-into-a-progress-bar-updated/
    ''' </shoutouts>
    <Extension()> _
    Public Sub SafelyDrawText(ByVal control As System.Windows.Forms.ProgressBar, ByVal value As String, ByVal textAlign As Drawing.ContentAlignment, _
                                  ByVal textFont As System.Drawing.Font)
      If String.IsNullOrEmpty(value) Then
        control.Refresh()
        Dim fraction As Double = CDbl(control.Value - control.Minimum) / CDbl(control.Maximum - control.Minimum)
        value = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0%}", fraction)
      End If
      SafelyDrawText(CType(control, System.Windows.Forms.Control), value, textAlign, textFont)
    End Sub

    ''' <summary>
    ''' Sets the <see cref="ProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueSetter(ByVal control As ProgressBar, ByVal value As Integer) As Integer
      control.Value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
      Return CInt(control.Value)
    End Function

    ''' <summary>
    ''' Sets the <see cref="ProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ValueUpdater(ByVal control As ProgressBar, ByVal value As Integer) As Integer
      If control.Value <> value Then
        Return ValueSetter(control, value)
      End If
      Return CInt(control.Value)
    End Function

    ''' <summary>
    ''' Sets the <see cref="ProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueSetter(ByVal control As ProgressBar, ByVal value As Integer) As Integer
      If control.InvokeRequired Then
        control.Invoke(New Action(Of ProgressBar, Integer)(AddressOf SafeValueSetter), New Object() {control, value})
      Else
        value = Math.Max(control.Minimum, Math.Min(control.Maximum, value))
        control.Value = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Updates the <see cref="ProgressBar">control</see> value to the 
    ''' <paramref name="value">value</paramref> limited by the control range.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The extended numeric up down control.</param>
    ''' <param name="value">The candidate value.</param>
    ''' <returns>The limited value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeValueUpdater(ByVal control As ProgressBar, ByVal value As Integer) As Integer
      If control.Value <> value Then
        Return SafeValueSetter(control, value)
      End If
      Return value
    End Function

#End Region

#Region " STATUS BAR SETTERS "

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeTextSetter(ByVal control As StatusBarPanel, ByVal value As String) As String
      If String.IsNullOrEmpty(value) Then
        value = ""
      End If
      If control.Parent.InvokeRequired Then
        control.Parent.Invoke(New Action(Of StatusBarPanel, String)(AddressOf SafeTextSetter), New Object() {control, value})
      Else
        control.Text = value
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="TextBox">control</see> text to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function SafeToolTipTextSetter(ByVal control As StatusBarPanel, ByVal value As String) As String
      If String.IsNullOrEmpty(value) Then
        value = ""
      End If
      If control.Parent.InvokeRequired Then
        control.Parent.Invoke(New Action(Of StatusBarPanel, String)(AddressOf SafeToolTipTextSetter), New Object() {control, value})
      Else
        control.ToolTipText = value
      End If
      Return value
    End Function

#End Region

#Region " TEXT BOX SETTERS "

    ''' <summary>
    ''' Appends <paramref name="value">text</paramref> to the 
    ''' <see cref="TextBox">control</see>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function Append(ByVal control As TextBox, ByVal value As String) As String
      If String.IsNullOrEmpty(value) Then
        value = ""
      Else
        If control.InvokeRequired Then
          control.Invoke(New Action(Of TextBox, String)(AddressOf Append), New Object() {control, value})
        Else
          control.SelectionStart = control.Text.Length
          control.SelectionLength = 0
          If control.SelectionStart = 0 Then
            control.SelectedText = value
          Else
            control.SelectedText = Environment.NewLine & value
          End If
          control.SelectionStart = control.Text.Length
        End If
      End If
      Return value
    End Function

    ''' <summary>
    ''' Appends <paramref name="value">text</paramref> to the 
    ''' <see cref="TextBox">control</see>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="format">The text format.</param>
    ''' <param name="args">The format arguments.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function Append(ByVal control As TextBox, ByVal format As String, ByVal ParamArray args() As Object) As String
      Return Append(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

    ''' <summary>
    ''' Prepends <paramref name="value">text</paramref> to the 
    ''' <see cref="TextBox">control</see>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function Prepend(ByVal control As TextBox, ByVal value As String) As String
      If String.IsNullOrEmpty(value) Then
        value = ""
      Else
        If control.InvokeRequired Then
          control.Invoke(New Action(Of TextBox, String)(AddressOf Append), New Object() {control, value})
        Else
          control.SelectionStart = 0
          control.SelectionLength = 0
          control.SelectedText = value & Environment.NewLine
          control.SelectionLength = 0
        End If
      End If
      Return value
    End Function

    ''' <summary>
    ''' Prepends <paramref name="value">text</paramref> to the 
    ''' <see cref="TextBox">control</see>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">The text box control.</param>
    ''' <param name="format">The text format.</param>
    ''' <param name="args">The format arguments.</param>
    ''' <returns>value</returns>
    ''' <remarks>The value is set to empty if null or empty.</remarks>
    <Extension()> _
    Public Function Prepend(ByVal control As TextBox, ByVal format As String, ByVal ParamArray args() As Object) As String
      Return Prepend(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Function

#End Region

#Region " TOOL STRIP BUTTON SETTERS "

    ''' <summary>
    ''' Sets the <see cref="ToolStripButton">Tool Strip Button</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="control">Check box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeSilentCheckedSetter(ByVal control As ToolStripButton, ByVal value As Boolean) As Boolean
      If control.GetCurrentParent.InvokeRequired Then
        control.GetCurrentParent.Invoke(New Action(Of ToolStripButton, Boolean)(AddressOf SafeSilentCheckedSetter), New Object() {control, value})
      Else
        Dim wasEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.Checked = value
        control.Enabled = wasEnabled
      End If
      Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="ToolStripButton">Tool Strip Button</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="control">Check box control.</param>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function SafeCheckedSetter(ByVal control As ToolStripButton, ByVal value As Boolean) As Boolean
      If control.GetCurrentParent.InvokeRequired Then
        control.GetCurrentParent.Invoke(New Action(Of ToolStripButton, Boolean)(AddressOf SafeCheckedSetter), New Object() {control, value})
      Else
        control.Checked = value
      End If
      Return value
    End Function

#End Region

  End Module

End Namespace
