﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions
  ''' <summary>
  ''' Includes extensions for diagnostics.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2009 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>  
  Public Module [TraceExtensions]

    ''' <summary>
    ''' Derive a <see cref="SourceLevels">source level</see> for the trace switch given the 
    ''' <see cref="TraceLevel">trace level</see>.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ParseSourceLevel(ByVal value As TraceEventType) As SourceLevels

      ' My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), eventtype.ToString), SourceLevels)

      Select Case value
        Case TraceEventType.Error
          Return SourceLevels.Error
        Case TraceEventType.Information
          Return SourceLevels.Information
        Case TraceEventType.Resume
          Return SourceLevels.Information
        Case TraceEventType.Suspend
          Return SourceLevels.Off
        Case TraceEventType.Verbose
          Return SourceLevels.Verbose
        Case TraceEventType.Warning
          Return SourceLevels.Warning
        Case TraceEventType.Critical
          Return SourceLevels.Critical
        Case Else
          Return SourceLevels.Information
      End Select

    End Function

    ''' <summary>
    ''' Derive a <see cref="TraceLevel">trace level</see> given the <see cref="SourceLevels">source level</see> of a trace switch.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ParseSourceLevel(ByVal value As SourceLevels) As TraceEventType

      ' My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), eventtype.ToString), SourceLevels)

      Select Case value
        Case SourceLevels.Critical
          Return TraceEventType.Critical
        Case SourceLevels.Error
          Return TraceEventType.Error
        Case SourceLevels.Off
          Return TraceEventType.Suspend
        Case SourceLevels.Information
          Return TraceEventType.Information
        Case SourceLevels.Verbose
          Return TraceEventType.Verbose
        Case SourceLevels.Warning
          Return TraceEventType.Warning
        Case Else
          Return TraceEventType.Information
      End Select

    End Function

  End Module

End Namespace
