Namespace EventHandlerExtensions
    ''' <summary>
    ''' Includes extensions for safe triggering and safe cross threading handling of event.
    ''' </summary>
    ''' <remarks>
    ''' Shout outs to
    ''' http://www.codeproject.com/KB/cs/EventSafeTrigger.aspx
    ''' aeonhack http://www.dreamincode.net/forums/user/334492-aeonhack/
    ''' http://www.dreamincode.net/code/snippet5016.htm 
    ''' </remarks>
    ''' <license>
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="03/01/2010" by="David Hary" revision="1.2.3712.x">
    ''' Created
    ''' </history>
    Public Module [Extensions]

#Region " SAFE BEGIN INVOKE "

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Does not wait for the event handler to complete.
        ''' </summary>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <remarks>
        ''' Raise events in cross-thread operations, no more of those pesky: Cross-thread operation 
        ''' not valid: Control 'NAME' accessed from a thread other than the thread it was created on."
        ''' I have put together a simple class that reverses a string on a seperate thread and raises an event.
        ''' Visual Basic hides the delegates for events. Using this method required appending 'Event' to the name
        ''' of the Event, e.g., StoppedEvent.SafeBeginInvoke(me).
        ''' <para>
        ''' Benchmark:
        ''' </para>
        ''' Safe Begin Invoke Benchmarked at 0.05273ms
        ''' </remarks>
        ''' <shoutouts>
        ''' aeonhack at http://www.dreamincode.net/forums/user/334492-aeonhack/
        ''' Raise Events Cross-Thread at http://www.dreamincode.net/code/snippet5016.htm 
        ''' </shoutouts>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeBeginInvoke(ByVal value As EventHandler, ByVal sender As Object)
            SafeBeginInvoke(value, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Does not wait for the event handler to complete.
        ''' </summary>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The arguments for the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeBeginInvoke(ByVal value As EventHandler, ByVal sender As Object, ByVal e As EventArgs)
            If value IsNot Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Try
                                target.BeginInvoke(d, New Object() {sender, e})
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Does not wait for the event handler to complete.
        ''' </summary>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The arguments for the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeBeginInvoke(ByVal value As ComponentModel.PropertyChangedEventHandler, ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
            If value IsNot Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Try
                                target.BeginInvoke(d, New Object() {sender, e})
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next
            End If
        End Sub


        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Does not wait for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The arguments for the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeBeginInvoke(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            If value IsNot Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Try
                                target.BeginInvoke(d, New Object() {sender, e})
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Does not wait for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeBeginInvoke(Of TEventArgs As {EventArgs, New})(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object)
            SafeBeginInvoke(value, sender, New TEventArgs())
        End Sub

#End Region

#Region " SAFE INVOKE "

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <remarks>
        ''' Raise events in cross-thread operations, no more of those pesky: Cross-thread operation 
        ''' not valid: Control 'NAME' accessed from a thread other than the thread it was created on."
        ''' I have put together a simple class that reverses a string on a seperate thread and raises an event.
        ''' Visual Basic hides the delegates for events. Using this method required appending 'Event' to the name
        ''' of the Event, e.g., StoppedEvent.SafeInvoke(me).
        ''' <para>
        ''' Benchmark:
        ''' </para>
        ''' Safe Begin End Invoke Benchmarked at 0.054185ms    
        ''' </remarks>
        ''' <shoutouts>
        ''' aeonhack at http://www.dreamincode.net/forums/user/334492-aeonhack/
        ''' Raise Events Cross-Thread at http://www.dreamincode.net/code/snippet5016.htm 
        ''' </shoutouts>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeInvoke(ByVal value As EventHandler, ByVal sender As Object)
            SafeInvoke(value, sender, System.EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The arguments for the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeInvoke(ByVal value As EventHandler, ByVal sender As Object, ByVal e As EventArgs)
            If value IsNot Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Dim result As IAsyncResult = Nothing
                            Try
                                result = target.BeginInvoke(d, New Object() {sender, e})
                                If result IsNot Nothing Then
                                    target.EndInvoke(result)
                                End If
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The arguments for the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeInvoke(ByVal value As ComponentModel.PropertyChangedEventHandler, ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
            If value IsNot Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Dim result As IAsyncResult = Nothing
                            Try
                                result = target.BeginInvoke(d, New Object() {sender, e})
                                If result IsNot Nothing Then
                                    target.EndInvoke(result)
                                End If
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="e">The arguments for the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeInvoke(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
            If value IsNot Nothing Then
                For Each d As [Delegate] In value.GetInvocationList
                    If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                        Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                        If target.InvokeRequired Then
                            Try
                                Dim result As IAsyncResult = target.BeginInvoke(d, New Object() {sender, e})
                                If result IsNot Nothing Then
                                    target.EndInvoke(result)
                                End If
                            Catch ex As ObjectDisposedException
                            End Try
                        Else
                            d.DynamicInvoke(New Object() {sender, e})
                        End If
                    Else
                        d.DynamicInvoke(New Object() {sender, e})
                    End If
                Next
            End If
        End Sub

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        <System.Runtime.CompilerServices.Extension()> _
        Public Sub SafeInvoke(Of TEventArgs As {EventArgs, New})(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object)
            SafeInvoke(value, sender, New TEventArgs())
        End Sub

#End Region

#Region " EVENT HANDLER SYSTEM EVENT ARGS RETURN VALUE "

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        ''' <typeparam name="TReturnType">The return type of the event trigger.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="retrieveDataFunction">A function used to retrieve data from the event.</param>
        ''' <returns>Returns data retrieved from the event arguments.</returns> 
        <System.Runtime.CompilerServices.Extension()> _
        Public Function SafeInvoke(Of TEventArgs As {EventArgs, New}, TReturnType)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal retrieveDataFunction As Func(Of TEventArgs, TReturnType)) As TReturnType
            If retrieveDataFunction Is Nothing Then
                Throw New ArgumentNullException("retrieveDataFunction")
            End If
            If value IsNot Nothing Then
                Dim eventArgs As TEventArgs = New TEventArgs()
                SafeInvoke(value, sender, eventArgs)
                Dim returnData As TReturnType = retrieveDataFunction(eventArgs)
                Return returnData
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TReturnType">The return type of the event trigger.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="eventArgs">The arguments for the event.</param>
        ''' <param name="retrieveDataFunction">A function used to retrieve data from the event.</param>
        ''' <returns>Returns data retrieved from the event arguments.</returns>
        <System.Runtime.CompilerServices.Extension()> _
        Public Function SafeInvoke(Of TReturnType)(ByVal value As EventHandler, ByVal sender As Object, ByVal eventArgs As EventArgs, ByVal retrieveDataFunction As Func(Of EventArgs, TReturnType)) As TReturnType
            If retrieveDataFunction Is Nothing Then
                Throw New ArgumentNullException("retrieveDataFunction")
            End If

            If value IsNot Nothing Then
                SafeInvoke(value, sender, eventArgs)
                Dim returnData As TReturnType = retrieveDataFunction(eventArgs)
                Return returnData
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Safely triggers an event across threads. 
        ''' Waits for the event handler to complete.
        ''' </summary>
        ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        ''' <typeparam name="TReturnType">The return type of the event trigger.</typeparam>
        ''' <param name="value">The event to trigger.</param>
        ''' <param name="sender">The sender of the event.</param>
        ''' <param name="eventArgs">The arguments for the event.</param>
        ''' <param name="retrieveDataFunction">A function used to retrieve data from the event.</param>
        ''' <returns>Returns data retrieved from the event arguments.</returns> 
        <System.Runtime.CompilerServices.Extension()> _
        Public Function SafeInvoke(Of TEventArgs As EventArgs, TReturnType)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal eventArgs As TEventArgs, ByVal retrieveDataFunction As Func(Of TEventArgs, TReturnType)) As TReturnType
            If retrieveDataFunction Is Nothing Then
                Throw New ArgumentNullException("retrieveDataFunction")
            End If
            If value IsNot Nothing Then
                SafeInvoke(value, sender, eventArgs)
                Dim returnData As TReturnType = retrieveDataFunction(eventArgs)
                Return returnData
            Else
                Return Nothing
            End If
        End Function

#End Region

    End Module

End Namespace
