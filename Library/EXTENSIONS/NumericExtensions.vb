﻿Imports System.Runtime.CompilerServices
Namespace NumericExtensions
  ''' <summary>
  ''' Includes extensions for number objects.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
  Public Module [Extensions]

#Region " BYTE "

    ''' <summary>
    ''' Returns the maximum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Max(ByVal value As Byte, ByVal reference As Byte) As Byte
      If value >= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns the minimum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Min(ByVal value As Byte, ByVal reference As Byte) As Byte
      If value <= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>Returns an HEX string.
    ''' </summary>
    <Extension()> _
    Public Function ToHex(ByVal value As Byte, ByVal nibbleCount As Byte) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X}" & CStr(nibbleCount), value)
    End Function

    ''' <summary>Returns an HEX string caption with preceeding "0x".
    ''' </summary>
    <Extension()> _
    Public Function ToHexCaption(ByVal value As Byte, ByVal nibbleCount As Byte) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}" & CStr(nibbleCount), value)
    End Function

#End Region

#Region " DOUBLE "

    ''' <summary>
    ''' Returns true if the two values are within the specified absolute precision.
    ''' </summary>
    ''' <param name="value">Value</param>
    ''' <param name="reference">reference value</param>
    ''' <param name="precision">Absolute minimal diffecrence for relative equality.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Approximates(ByVal value As Decimal, ByVal reference As Decimal, ByVal precision As Decimal) As Boolean

      Return (reference = value) OrElse Math.Abs(value - reference) <= precision

    End Function

    ''' <summary>
    ''' Returns true if the two values are within the specified relative precision.
    ''' </summary>
    ''' <param name="value">Value</param>
    ''' <param name="reference">reference value</param>
    ''' <param name="significantDigits">Defines relative precising based on the significant digits of the 
    ''' <see cref="Hypotenuse"></see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Approximates(ByVal value As Decimal, ByVal reference As Decimal, ByVal significantDigits As Integer) As Boolean
      Return (reference = value) OrElse Math.Abs(value - reference) < Hypotenuse(value, reference) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))
    End Function

#End Region

#Region " DOUBLE "


    ''' <summary>
    ''' Returns true if the two values are within the specified absolute precision.
    ''' </summary>
    ''' <param name="value">Value</param>
    ''' <param name="reference">reference value</param>
    ''' <param name="precision">Absolute minimal diffecrence for relative equality.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Approximates(ByVal value As Double, ByVal reference As Double, ByVal precision As Double) As Boolean

      Return (reference = value) OrElse Math.Abs(value - reference) <= precision

    End Function

    ''' <summary>
    ''' Returns true if the two values are within the specified relative precision.
    ''' </summary>
    ''' <param name="value">Value</param>
    ''' <param name="reference">reference value</param>
    ''' <param name="significantDigits">Defines relative precising based on the significant digits of the 
    ''' <see cref="Hypotenuse"></see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Approximates(ByVal value As Double, ByVal reference As Double, ByVal significantDigits As Integer) As Boolean
      Return (reference = value) OrElse Math.Abs(value - reference) < Hypotenuse(value, reference) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))
    End Function

    ''' <summary>Returns a value that is rounded up to the specified significant digit.</summary>
    ''' <param name="value">value which to use in the calculation.</param>
    ''' <param name="relativeDecimal">The decimal digit relative to the most significant digit 
    '''   to use.</param>
    ''' <returns>A rounded up value to the specified relative digit</returns>
    ''' <example>
    '''   Unary.Ceiling(1.123456,2) returns 1.13.<p>
    '''   Unary.Ceiling(-1.123456,2) returns -1.12.</p>
    ''' </example>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Ceiling(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

      Dim decimalDigitValue As Double

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return value

      Else

        ' get the representation of the significant digit
        decimalDigitValue = DecimalValue(value, -relativeDecimal)

        ' get the new value
        Return decimalDigitValue * System.Math.Ceiling(value / decimalDigitValue)

      End If

    End Function

    ''' <summary>Returns the number of decimal places for displaying the 
    '''   auto value.</summary>
    <Extension()> _
    Public Function DecimalPlaces(ByVal value As Double) As Integer
      Dim candidate As Integer = 0
      value = Math.IEEERemainder(Math.Abs(value), 1)
      Dim minimum As Double = Math.Max(value / 1000.0, 0.0000001)
      Do While value > minimum
        candidate += 1
        value = Math.IEEERemainder(10 * value, 1)
      Loop
      Return candidate
    End Function

    ''' <summary>Returns the decimal value of a number.</summary>
    ''' <param name="value">Value which to use in the calculation</param>
    ''' <param name="decimalLeftShift">Decimal power by which to shift the digit.</param>
    ''' <returns>The decimal value of the value shifted by the specified decimalPlaces.</returns>
    ''' <example>
    '''   Unary.DecimalValue(1.123,1) returns 10.<p>
    '''   Unary.DecimalValue(11.23,1) returns 100.</p><p>
    '''   Unary.DecimalValue(-1.123,1) returns 10.</p><p>
    '''   Unary.DecimalValue(0.1234,1) returns 1.</p><p>
    '''   Unary.DecimalValue(11.12,0) returns 10.</p>
    ''' </example>
    <Extension()> _
    Public Function DecimalValue(ByVal value As Double, ByVal decimalLeftShift As Integer) As Double

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return 0

      Else

        ' get the representation of the most significant digit
        Return Math.Pow(10, value.Exponent() + decimalLeftShift)

      End If

    End Function

    ''' <summary>Returns the exponent.</summary>
    ''' <param name="value">A <see cref="System.Double">Double</see> value.</param>
    ''' <returns>The most significant decade of the value.</returns>
    ''' <example>
    '''   Unary.Exponent(1.12) returns 0.<p>
    '''   Unary.Decade(11.23) returns 1.</p><p>
    '''   Unary.Exponent(-1.123) returns 0.</p><p>
    '''   Unary.Exponent(0.1234) returns -1</p>
    ''' </example>
    ''' <seealso cref="Decimal"/>
    <Extension()> _
    Public Function Exponent(ByVal value As Double) As Integer

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return 0

      Else

        ' get the exponent based on the most significant digit.
        '     Return Convert.ToInt32(System.Math.Floor(System.Math.Log(System.Math.Abs(value))) / System.Math.Log(10.0#))
        Return Convert.ToInt32(System.Math.Floor(System.Math.Log10(System.Math.Abs(value))))
      End If

    End Function

    ''' <summary>Returns the exponent.</summary>
    ''' <param name="value">A <see cref="System.Double">Double</see> value.</param>
    ''' <returns>The most significant decade of the value.</returns>
    ''' <example>
    '''   Unary.Exponent(11.12) returns 0.<p>
    '''   Unary.Decade(111.23) returns 1.</p><p>
    '''   Unary.Exponent(1111.123) returns 3.</p><p>
    '''   Unary.Exponent(-11.12) returns 0.</p><p>
    '''   Unary.Decade(-111.23) returns 1.</p><p>
    '''   Unary.Exponent(-1111.123) returns 3.</p><p>
    '''   Unary.Exponent(0.1234) returns 0</p><p>
    '''   Unary.Exponent(0.01234) returns 0</p><p>
    '''   Unary.Exponent(0.0001234) returns -3</p><p>
    '''   Unary.Exponent(-0.1234) returns 0</p><p>
    '''   Unary.Exponent(-0.01234) returns 0</p><p>
    '''   Unary.Exponent(-0.001234) returns -3</p><p>
    '''   Unary.Exponent(-0.0001234) returns -3</p>
    ''' </example>
    ''' <seealso cref="DecimalValue"/>
    <Extension()> _
    Public Function Exponent(ByVal value As Double, ByVal useEngineeringScale As Boolean) As Integer

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return 0

      Else

        If useEngineeringScale Then
          ' Use a power of 10 that is a multiple of 3 (engineering scale)
          Return 3 * (Exponent(value) \ 3)
        Else
          Return Exponent(value)
        End If

      End If

    End Function

    ''' <summary>Returns a suggested fixed format based requested most
    '''  significant decimalPlaces.</summary>
    ''' <param name="value">Number to format</param>
    ''' <param name="decimalPlaces">The number of decimal places beyond the most
    '''     significant digit.</param>
    ''' <returns>A fix format string</returns>
    ''' <remarks></remarks>
    ''' <example>
    '''   Unary.FixedFormat(1.123,1) returns 0.0.<p>
    '''   Unary.FixedFormat(11.23,1) returns 0.</p><p>
    '''   Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
    '''   Unary.FixedFormat(0.1234,1) returns 0.0.</p>
    ''' </example>
    <Extension()> _
    Public Function FixedFormat(ByVal value As Double, ByVal decimalPlaces As Integer) As String

      decimalPlaces = decimalPlaces - value.Exponent()
      If decimalPlaces > 0 Then
        Return "0." & New String("0"c, decimalPlaces)
      Else
        Return "0"
      End If

    End Function

    ''' <summary>Returns a value that is rounded down to the specified significant digit.</summary>
    ''' <param name="value">value which to use in the calculation.</param>
    ''' <param name="relativeDecimal">The decimal digit relative to the most significant digit 
    '''   to use.</param>
    ''' <returns>A rounded down value to the specified relative digit</returns>
    ''' <remarks></remarks>
    ''' <example>
    '''   Unary.Floor(1.123,1) returns 1.1.<p>
    '''   Unary.Floor(11.23,1) returns 11.</p><p>
    '''   Unary.Floor(-1.123,1) returns -1.3.</p><p>
    '''   Unary.Floor(0.1234,1) returns 0.12.</p><p>
    '''   Unary.Floor(11.12,0) returns 10.</p>
    ''' </example>
    <Extension()> _
    Public Function Floor(ByVal value As Double, ByVal relativeDecimal As Integer) As Double

      Dim decimalDigitValue As Double

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return value

      Else

        ' get the representation of the significant digit
        decimalDigitValue = DecimalValue(value, -relativeDecimal)

        ' get the new value
        Return decimalDigitValue * System.Math.Floor(value / decimalDigitValue)

      End If

    End Function

    ''' <summary>Calculates sqrt(a^2 + b^2) without under/overflow.</summary>
    ''' <param name="value">a</param>
    ''' <param name="orthogonal">b</param>
    <Extension()> _
    Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
      Dim r As Double
      If Math.Abs(value) > Math.Abs(orthogonal) Then
        r = orthogonal / value
        r = Math.Abs(value) * Math.Sqrt((1 + r * r))
      ElseIf orthogonal <> 0 Then
        r = value / orthogonal
        r = Math.Abs(orthogonal) * Math.Sqrt((1 + r * r))
      Else
        r = 0.0
      End If
      Return r
    End Function

    ''' <summary>
    ''' Interpolate over the unity range.
    ''' </summary>
    ''' <param name="minValue">The minimum range point.</param>
    ''' <param name="maxValue">The maximum range point.</param>
    ''' <returns>Returns the interpolated output over the specified range relative to the 
    ''' value over the unity [0,1) range.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double) As Double
      Return value * (maxValue - minValue) + minValue
    End Function

    ''' <summary>Linearly interpolated value.</summary>
    ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
    ''' <param name="minValue">The first abscissa value.</param>
    ''' <param name="maxValue">The second abscissa value.</param>
    ''' <param name="minOutput">The first ordinate value.</param>
    ''' <param name="maxOutput">The second ordinate value.</param>
    ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
    '''   coordinates.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double, ByVal minOutput As Double, ByVal maxOutput As Double) As Double

      ' check if we have two distinct pairs
      If maxValue = minValue Then
        ' if we have identical value values, return the
        ' average of the ordinate value
        Return 0.5 * (minOutput + maxOutput)
      Else
        Return minOutput + (value - minValue) * (maxOutput - minOutput) / (maxValue - minValue)
      End If

    End Function

    ''' <summary>Linearly interpolated value.</summary>
    ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
    ''' <param name="point1">The first point.</param>
    ''' <param name="point2">The second point.</param>
    ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
    '''   coordinates.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Double, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Double

      ' check if we have two distinct pairs
      If point2.X = point2.X Then
        ' if we have identical X values, return the
        ' average of the ordinate value
        Return 0.5 * (point1.Y + point2.Y)
      Else
        Return point1.Y + (value - point1.X) * (point2.Y - point1.Y) / (point2.X - point1.X)
      End If

    End Function

    ''' <summary>Linearly interpolated value.</summary>
    ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
    ''' <param name="rectangle">The rectangle of coordinates.</param>
    ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
    '''   coordinates.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Double, ByVal rectangle As System.Drawing.RectangleF) As Double

      ' check if we have two distinct pairs
      If rectangle.Width = 0 Then
        ' if we have identical X values, return the
        ' average of the ordinate value
        Return rectangle.Y + 0.5 * rectangle.Height
      Else
        Return rectangle.Y + (value - rectangle.X) * rectangle.Height / rectangle.Width
      End If

    End Function

    ''' <summary>Calculate a base 10 logarithm in a safe manner to avoid math exceptions</summary>
    ''' <param name="value">The value for which the logarithm is to be calculated</param>
    ''' <returns>The value of the logarithm, or 0 if the <paramref name="x"/>
    ''' argument was negative or zero</returns>
    <Extension()> _
    Public Function Log10(ByVal value As Double) As Double
      Return Math.Log10(Math.Max(value, Double.Epsilon))
    End Function

    ''' <summary>
    ''' Returns the maximum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Max(ByVal value As Double, ByVal reference As Double) As Double
      If value >= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns the minimum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Min(ByVal value As Double, ByVal reference As Double) As Double
      If value <= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>Calculate the modulus (remainder) in a safe manner so that divide
    '''   by zero errors are avoided</summary>
    ''' <param name="value">The divisor</param>
    ''' <param name="dividend">The dividend</param>
    ''' <returns>the value of the modulus, or zero for the divide-by-zero case</returns>
    <Extension()> _
    Public Function [Mod](ByVal value As Double, ByVal dividend As Double) As Double

      If dividend = 0 Then
        Return 0
      End If
      Dim temp As Double = value / dividend
      Return dividend * (temp - Math.Floor(temp))
    End Function

#End Region

#Region " INTEGER "

    ''' <summary>
    ''' Returns the maximum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Max(ByVal value As Integer, ByVal reference As Integer) As Integer
      If value >= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns the minimum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Min(ByVal value As Integer, ByVal reference As Integer) As Integer
      If value <= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns an HEX string.
    ''' </summary>
    <Extension()> _
    Public Function ToHex(ByVal value As Integer, ByVal nibbleCount As Integer) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X" & CStr(nibbleCount) & "}", value)
    End Function

    ''' <summary>Returns an HEX string caption with preceeding "0x".
    ''' </summary>
    <Extension()> _
    Public Function ToHexCaption(ByVal value As Integer, ByVal nibbleCount As Integer) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X" & CStr(nibbleCount) & "}", value)
    End Function

    ''' <summary>
    ''' Truncates a value to the desired precision.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Truncate(ByVal value As Int32) As Int16

      value = value And UInt16.MaxValue ' &HFFFFI ' 65535
      If value > Int16.MaxValue Then
        value -= UInt16.MaxValue + 1 ' -65536
      End If
      Return Convert.ToInt16(value)

    End Function

#End Region

#Region " LONG "

    ''' <summary>
    ''' Returns the maximum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Max(ByVal value As Long, ByVal reference As Long) As Long
      If value >= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns the minimum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Min(ByVal value As Long, ByVal reference As Long) As Long
      If value <= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>Returns an HEX string.
    ''' </summary>
    <Extension()> _
    Public Function ToHex(ByVal value As Long, ByVal nibbleCount As Long) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X}" & CStr(nibbleCount), value)
    End Function

    ''' <summary>Returns an HEX string caption with preceeding "0x".
    ''' </summary>
    <Extension()> _
    Public Function ToHexCaption(ByVal value As Long, ByVal nibbleCount As Long) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}" & CStr(nibbleCount), value)
    End Function

    ''' <summary>
    ''' Truncates a value to the desired precision.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Truncate(ByVal value As Int64) As Int32

      value = value And UInt32.MaxValue ' ' &HFFFFFFFFI
      If value > Int32.MaxValue Then
        value -= UInt32.MaxValue + 1
      End If
      Return Convert.ToInt32(value)

    End Function


#End Region

#Region " SHORT "

    ''' <summary>
    ''' Returns the maximum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Max(ByVal value As Short, ByVal reference As Short) As Short
      If value >= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns the minimum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Min(ByVal value As Short, ByVal reference As Short) As Short
      If value <= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>Returns an HEX string.
    ''' </summary>
    <Extension()> _
    Public Function ToHex(ByVal value As Short, ByVal nibbleCount As Short) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:X}" & CStr(nibbleCount), value)
    End Function

    ''' <summary>Returns an HEX string caption with preceeding "0x".
    ''' </summary>
    <Extension()> _
    Public Function ToHexCaption(ByVal value As Short, ByVal nibbleCount As Short) As String
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}" & CStr(nibbleCount), value)
    End Function

    ''' <summary>
    ''' Truncates a value to the desired precision.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Truncate(ByVal value As Int16) As Byte

      Dim maxValue As Short = 2S * Byte.MaxValue - 1S
      value = value And maxValue  ' &HFFFFI ' 65535
      If value > Int16.MaxValue Then
        value -= maxValue + 1S ' -65536
      End If
      Return Convert.ToByte(value)

    End Function

#End Region

#Region " SINGLE "

    ''' <summary>
    ''' Returns true if the two values are within the specified absolute precision.
    ''' </summary>
    ''' <param name="value">Value</param>
    ''' <param name="reference">reference value</param>
    ''' <param name="precision">Absolute minimal diffecrence for relative equality.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Approximates(ByVal value As Single, ByVal reference As Single, ByVal precision As Single) As Boolean

      Return (reference = value) OrElse Math.Abs(value - reference) <= precision

    End Function

    ''' <summary>
    ''' Returns true if the two values are within the specified relative precision.
    ''' </summary>
    ''' <param name="value">Value</param>
    ''' <param name="reference">reference value</param>
    ''' <param name="significantDigits">Defines relative precising based on the significant digits of the 
    ''' <see cref="Hypotenuse"></see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Approximates(ByVal value As Single, ByVal reference As Single, ByVal significantDigits As Integer) As Boolean
      Return (reference = value) OrElse Math.Abs(value - reference) < Hypotenuse(value, reference) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))
    End Function

    ''' <summary>Returns a value that is rounded up to the specified significant digit.</summary>
    ''' <param name="value">value which to use in the calculation.</param>
    ''' <param name="relativeDecimal">The decimal digit relative to the most significant digit 
    '''   to use.</param>
    ''' <returns>A rounded up value to the specified relative digit</returns>
    ''' <example>
    '''   Unary.Ceiling(1.123456,2) returns 1.13.<p>
    '''   Unary.Ceiling(-1.123456,2) returns -1.12.</p>
    ''' </example>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Ceiling(ByVal value As Single, ByVal relativeDecimal As Integer) As Single

      Dim decimalDigitValue As Single

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return value

      Else

        ' get the representation of the significant digit
        decimalDigitValue = DecimalValue(value, -relativeDecimal)

        ' get the new value
        Return CSng(decimalDigitValue * System.Math.Ceiling(value / decimalDigitValue))

      End If

    End Function

    ''' <summary>Returns the number of decimal places for displaying the 
    '''   auto value.</summary>
    <Extension()> _
    Public Function DecimalPlaces(ByVal value As Single) As Integer
      Dim candidate As Integer = 0
      Dim remainder As Double = Math.IEEERemainder(Math.Abs(value), 1.0)
      Dim minimum As Double = Math.Max(remainder / 1000.0, 0.0000001)
      Do While remainder > minimum
        candidate += 1
        remainder = Math.IEEERemainder(10 * remainder, 1)
      Loop
      Return candidate
    End Function

    ''' <summary>Returns the decimal value of a number.</summary>
    ''' <param name="value">Value which to use in the calculation</param>
    ''' <param name="decimalLeftShift">Decimal power by which to shift the digit.</param>
    ''' <returns>The decimal value of the value shifted by the specified decimalPlaces.</returns>
    ''' <example>
    '''   Unary.DecimalValue(1.123,1) returns 10.<p>
    '''   Unary.DecimalValue(11.23,1) returns 100.</p><p>
    '''   Unary.DecimalValue(-1.123,1) returns 10.</p><p>
    '''   Unary.DecimalValue(0.1234,1) returns 1.</p><p>
    '''   Unary.DecimalValue(11.12,0) returns 10.</p>
    ''' </example>
    <Extension()> _
    Public Function DecimalValue(ByVal value As Single, ByVal decimalLeftShift As Integer) As Single

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return 0

      Else

        ' get the representation of the most significant digit
        Return CSng(Math.Pow(10, value.Exponent() + decimalLeftShift))

      End If

    End Function

    ''' <summary>Returns the exponent.</summary>
    ''' <param name="value">A <see cref="System.Single">Single</see> value.</param>
    ''' <returns>The most significant decade of the value.</returns>
    ''' <example>
    '''   Unary.Exponent(1.12) returns 0.<p>
    '''   Unary.Decade(11.23) returns 1.</p><p>
    '''   Unary.Exponent(-1.123) returns 0.</p><p>
    '''   Unary.Exponent(0.1234) returns -1</p>
    ''' </example>
    ''' <seealso cref="Decimal"/>
    <Extension()> _
    Public Function Exponent(ByVal value As Single) As Integer

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return 0

      Else

        ' get the exponent based on the most significant digit.
        '     Return Convert.ToInt32(System.Math.Floor(System.Math.Log(System.Math.Abs(value))) / System.Math.Log(10.0#))
        Return Convert.ToInt32(System.Math.Floor(System.Math.Log10(System.Math.Abs(value))))
      End If

    End Function

    ''' <summary>Returns the exponent.</summary>
    ''' <param name="value">A <see cref="System.Single">Single</see> value.</param>
    ''' <returns>The most significant decade of the value.</returns>
    ''' <example>
    '''   Unary.Exponent(11.12) returns 0.<p>
    '''   Unary.Decade(111.23) returns 1.</p><p>
    '''   Unary.Exponent(1111.123) returns 3.</p><p>
    '''   Unary.Exponent(-11.12) returns 0.</p><p>
    '''   Unary.Decade(-111.23) returns 1.</p><p>
    '''   Unary.Exponent(-1111.123) returns 3.</p><p>
    '''   Unary.Exponent(0.1234) returns 0</p><p>
    '''   Unary.Exponent(0.01234) returns 0</p><p>
    '''   Unary.Exponent(0.0001234) returns -3</p><p>
    '''   Unary.Exponent(-0.1234) returns 0</p><p>
    '''   Unary.Exponent(-0.01234) returns 0</p><p>
    '''   Unary.Exponent(-0.001234) returns -3</p><p>
    '''   Unary.Exponent(-0.0001234) returns -3</p>
    ''' </example>
    ''' <seealso cref="DecimalValue"/>
    <Extension()> _
    Public Function Exponent(ByVal value As Single, ByVal useEngineeringScale As Boolean) As Integer

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return 0

      Else

        If useEngineeringScale Then
          ' Use a power of 10 that is a multiple of 3 (engineering scale)
          Return 3 * (Exponent(value) \ 3)
        Else
          Return Exponent(value)
        End If

      End If

    End Function

    ''' <summary>Returns a suggested fixed format based requested most
    '''  significant decimalPlaces.</summary>
    ''' <param name="value">Number to format</param>
    ''' <param name="decimalPlaces">The number of decimal places beyond the most
    '''     significant digit.</param>
    ''' <returns>A fix format string</returns>
    ''' <remarks></remarks>
    ''' <example>
    '''   Unary.FixedFormat(1.123,1) returns 0.0.<p>
    '''   Unary.FixedFormat(11.23,1) returns 0.</p><p>
    '''   Unary.FixedFormat(-1.123,1) returns 0.0.</p><p>
    '''   Unary.FixedFormat(0.1234,1) returns 0.0.</p>
    ''' </example>
    <Extension()> _
    Public Function FixedFormat(ByVal value As Single, ByVal decimalPlaces As Integer) As String

      decimalPlaces = decimalPlaces - value.Exponent()
      If decimalPlaces > 0 Then
        Return "0." & New String("0"c, decimalPlaces)
      Else
        Return "0"
      End If

    End Function

    ''' <summary>Returns a value that is rounded down to the specified significant digit.</summary>
    ''' <param name="value">value which to use in the calculation.</param>
    ''' <param name="relativeDecimal">The decimal digit relative to the most significant digit 
    '''   to use.</param>
    ''' <returns>A rounded down value to the specified relative digit</returns>
    ''' <remarks></remarks>
    ''' <example>
    '''   Unary.Floor(1.123,1) returns 1.1.<p>
    '''   Unary.Floor(11.23,1) returns 11.</p><p>
    '''   Unary.Floor(-1.123,1) returns -1.3.</p><p>
    '''   Unary.Floor(0.1234,1) returns 0.12.</p><p>
    '''   Unary.Floor(11.12,0) returns 10.</p>
    ''' </example>
    <Extension()> _
    Public Function Floor(ByVal value As Single, ByVal relativeDecimal As Integer) As Single

      Dim decimalDigitValue As Single

      ' check if the value is zero
      If value = 0 Then

        ' if so, return This value
        Return value

      Else

        ' get the representation of the significant digit
        decimalDigitValue = DecimalValue(value, -relativeDecimal)

        ' get the new value
        Return CSng(decimalDigitValue * System.Math.Floor(value / decimalDigitValue))

      End If

    End Function

    ''' <summary>Calculates sqrt(a^2 + b^2) without under/overflow.</summary>
    ''' <param name="value">a</param>
    ''' <param name="orthogonal">b</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Hypotenuse(ByVal value As Single, ByVal orthogonal As Single) As Single
      Dim r As Single
      If Math.Abs(value) > Math.Abs(orthogonal) Then
        r = orthogonal / value
        r = CSng(Math.Abs(value) * Math.Sqrt((1 + r * r)))
      ElseIf orthogonal <> 0 Then
        r = value / orthogonal
        r = CSng(Math.Abs(orthogonal) * Math.Sqrt((1 + r * r)))
      Else
        r = 0.0
      End If
      Return r
    End Function

    ''' <summary>
    ''' Interpolate over the unity range.
    ''' </summary>
    ''' <param name="minValue">The minimum range point.</param>
    ''' <param name="maxValue">The maximum range point.</param>
    ''' <returns>Returns the interpolated output over the specified range relative to the 
    ''' value over the unity [0,1) range.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single) As Single
      Return value * (maxValue - minValue) + minValue
    End Function

    ''' <summary>Linearly interpolated value.</summary>
    ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
    ''' <param name="minValue">The first abscissa value.</param>
    ''' <param name="maxValue">The second abscissa value.</param>
    ''' <param name="minOutput">The first ordinate value.</param>
    ''' <param name="maxOutput">The second ordinate value.</param>
    ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
    '''   coordinates.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single, ByVal minOutput As Single, ByVal maxOutput As Single) As Single

      ' check if we have two distinct pairs
      If maxValue = minValue Then
        ' if we have identical value values, return the
        ' average of the ordinate value
        Return 0.5F * (minOutput + maxOutput)
      Else
        Return minOutput + (value - minValue) * (maxOutput - minOutput) / (maxValue - minValue)
      End If

    End Function

    ''' <summary>Linearly interpolated value.</summary>
    ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
    ''' <param name="point1">The first point.</param>
    ''' <param name="point2">The second point.</param>
    ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
    '''   coordinates.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Single, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Single

      ' check if we have two distinct pairs
      If point2.X = point2.X Then
        ' if we have identical X values, return the
        ' average of the ordinate value
        Return 0.5F * (point1.Y + point2.Y)
      Else
        Return point1.Y + (value - point1.X) * (point2.Y - point1.Y) / (point2.X - point1.X)
      End If

    End Function

    ''' <summary>Linearly interpolated value.</summary>
    ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
    ''' <param name="rectangle">The rectangle of coordinates.</param>
    ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
    '''   coordinates.</returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Interpolate(ByVal value As Single, ByVal rectangle As System.Drawing.RectangleF) As Single

      ' check if we have two distinct pairs
      If rectangle.Width = 0 Then
        ' if we have identical X values, return the
        ' average of the ordinate value
        Return rectangle.Y + 0.5F * rectangle.Height
      Else
        Return rectangle.Y + (value - rectangle.X) * rectangle.Height / rectangle.Width
      End If

    End Function

    ''' <summary>Calculate a base 10 logarithm in a safe manner to avoid math exceptions</summary>
    ''' <param name="value">The value for which the logarithm is to be calculated</param>
    ''' <returns>The value of the logarithm, or 0 if the <paramref name="x"/>
    ''' argument was negative or zero</returns>
    <Extension()> _
    Public Function Log10(ByVal value As Single) As Single
      Return CSng(Math.Log10(Math.Max(value, Single.Epsilon)))
    End Function

    ''' <summary>
    ''' Returns the maximum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Max(ByVal value As Single, ByVal reference As Single) As Single
      If value >= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>
    ''' Returns the minimum of the two values.
    ''' </summary>
    ''' <param name="value">Extended value</param>
    ''' <param name="reference">Referenced value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Min(ByVal value As Single, ByVal reference As Single) As Single
      If value <= reference Then
        Return value
      Else
        Return reference
      End If
    End Function

    ''' <summary>Calculate the modulus (remainder) in a safe manner so that divide
    '''   by zero errors are avoided</summary>
    ''' <param name="value">The divisor</param>
    ''' <param name="dividend">The dividend</param>
    ''' <returns>the value of the modulus, or zero for the divide-by-zero case</returns>
    <Extension()> _
    Public Function [Mod](ByVal value As Single, ByVal dividend As Single) As Single

      If dividend = 0 Then
        Return 0
      End If
      Dim temp As Single = value / dividend
      Return CSng(dividend * (temp - Math.Floor(temp)))
    End Function

#End Region

  End Module

End Namespace
