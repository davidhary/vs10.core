﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions
  ''' <summary>
  ''' Includes extensions for <see cref="Diagnostics.FileVersionInfo">File Version Information</see>.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2009 Integrated Scientific Resources, Inc.
  ''' Licensed under the Apache License Version 2.0. 
  ''' Unless required by applicable law or agreed to in writing, this software is provided
  ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
  ''' Created
  ''' </history>
  Public Module [Extensions]

    ''' <summary>
    ''' Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
    ''' than the right version or -1 if the left version is smaller (older) than the newer one.
    ''' </summary>
    ''' <param name="leftVersion">Left hand side version.</param>
    ''' <param name="rightVersion ">Right hand side version.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Compare(ByVal leftVersion As FileVersionInfo, ByVal rightVersion As FileVersionInfo) As Integer
      If leftVersion Is Nothing Then
        Throw New ArgumentNullException("leftVersion")
      End If
      If rightVersion Is Nothing Then
        Throw New ArgumentNullException("rightVersion")
      End If
      If leftVersion.ProductMajorPart > rightVersion.ProductMajorPart Then
        Return 1
      ElseIf leftVersion.ProductMajorPart < rightVersion.ProductMajorPart Then
        Return -1
      End If
      If leftVersion.ProductMinorPart > rightVersion.ProductMinorPart Then
        Return 1
      ElseIf leftVersion.ProductMinorPart < rightVersion.ProductMinorPart Then
        Return -1
      End If
      If leftVersion.ProductBuildPart > rightVersion.ProductBuildPart Then
        Return 1
      ElseIf leftVersion.ProductBuildPart < rightVersion.ProductBuildPart Then
        Return -1
      End If
      If leftVersion.ProductPrivatePart > rightVersion.ProductPrivatePart Then
        Return 1
      ElseIf leftVersion.ProductPrivatePart < rightVersion.ProductPrivatePart Then
        Return -1
      Else
        Return 0
      End If
    End Function

    Private Const referenceDate As String = "1/1/2000"
    ''' <summary>Returns the reference date for the build number.</summary>
    ''' <returns>A Date.</returns>
    Public Function BuildNumberReferenceDate() As Date
      Return Date.Parse(referenceDate, System.Globalization.CultureInfo.CurrentCulture)
    End Function


    ''' <summary>Returns the date corresponding to the 
    ''' <see cref="FileVersionInfo.ProductBuildPart">build number</see>
    ''' which is the day since the <see cref="referenceDate">build number reference date</see> of January 1, 2000
    ''' </summary>
    ''' <param name="value">Specifies the version information.</param>
    ''' <returns>A Date.</returns>
    <Extension()> _
    Public Function BuildDate(ByVal value As FileVersionInfo) As Date
      Return BuildNumberReferenceDate.Add(TimeSpan.FromDays(value.ProductBuildPart))
    End Function

    ''' <summary>
    ''' Returns a standard caption for forms.
    ''' </summary>
    ''' <param name="value">Specifies the version information.</param>
    ''' <returns>A <see cref="System.String">String</see> data type in the form
    '''   ProductName ww.xx.yyy.zzzzß
    '''   or
    '''   ProductName ww.xx.yyy.zzzza</returns>
    ''' <remarks>Use this function to set the captions for forms.</remarks>
    ''' <history date="09/05/03" by="David Hary" revision="1.0.1342.x">
    '''   Add methods not requiring assembly specifications
    ''' </history>
    <Extension()> _
    Public Function ExtendedCaption(ByVal value As FileVersionInfo) As String

      ' set the caption using the product name
      Dim builder As New System.Text.StringBuilder
      builder.Append(value.FileName)
      builder.Append(" ")
      builder.Append(value.ProductVersion)
      If value.ProductMajorPart < 1 Then
        builder.Append(".")
        Select Case value.ProductMinorPart
          Case 0
            builder.Append(Convert.ToChar(&H3B1))
            ' builder.Append(ChrW(&H3B1)) ' alpha Chr(224)
          Case 1
            builder.Append(Convert.ToChar(&H3B2))
            ' builder.Append(ChrW(&H3B2)) ' beta Chr(225)
          Case 2 To 8
            builder.Append(String.Format(System.Globalization.CultureInfo.CurrentCulture, "RC{0}", value.ProductMajorPart - 1))
          Case Else
            builder.Append("Gold")
        End Select
      End If
      Return builder.ToString

    End Function

  End Module
End Namespace
