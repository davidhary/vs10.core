﻿Imports System.Runtime.CompilerServices
Namespace VersionExtensions

  ''' <summary>
  ''' Includes extensions for File Version Information.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2009 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history> 
  Public Module [Extensions]

    ''' <summary>
    ''' Compares the two versions returning 0 if they are equal, 1 if left version is greater (newer)
    ''' than the right version or -1 if the left version is smaller (older) than the newer one.
    ''' </summary>
    ''' <param name="leftVersion">Left hand side version.</param>
    ''' <param name="rightVersion ">Right hand side version.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function Compare(ByVal leftVersion As System.Version, ByVal rightVersion As System.Version) As Integer
      If leftVersion Is Nothing Then
        Throw New ArgumentNullException("leftVersion")
      End If
      If rightVersion Is Nothing Then
        Throw New ArgumentNullException("rightVersion")
      End If
      If leftVersion.Major > rightVersion.Major Then
        Return 1
      ElseIf leftVersion.Major < rightVersion.Major Then
        Return -1
      End If
      If leftVersion.Minor > rightVersion.Minor Then
        Return 1
      ElseIf leftVersion.Minor < rightVersion.Minor Then
        Return -1
      End If
      If leftVersion.Build > rightVersion.Build Then
        Return 1
      ElseIf leftVersion.Build < rightVersion.Build Then
        Return -1
      End If
      If leftVersion.Revision > rightVersion.Revision Then
        Return 1
      ElseIf leftVersion.Revision < rightVersion.Revision Then
        Return -1
      Else
        Return 0
      End If
    End Function

    Private Const referenceDate As String = "1/1/2000"
    ''' <summary>Returns the reference date for the build number.</summary>
    ''' <returns>A Date.</returns>
    Public Function BuildNumberReferenceDate() As Date
      Return Date.Parse(referenceDate, System.Globalization.CultureInfo.CurrentCulture)
    End Function


    ''' <summary>Returns the date corresponding to the 
    ''' <see cref="system.version.Build">build number</see>
    ''' which is the day since the <see cref="referenceDate">build number reference date</see> of January 1, 2000
    ''' </summary>
    ''' <param name="value">Specifies the version information.</param>
    ''' <returns>A Date.</returns>
    <Extension()> _
    Public Function BuildDate(ByVal value As System.Version) As Date
      Return BuildNumberReferenceDate.Add(TimeSpan.FromDays(value.Build))
    End Function

  End Module

End Namespace
