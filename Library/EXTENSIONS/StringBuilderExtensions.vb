﻿Imports System.Runtime.CompilerServices
Namespace TextExtensions
  ''' <summary>
  ''' Includes extensions for <see cref="System.Text.StringBuilder">string builder</see>.
  ''' </summary>
  ''' <remarks></remarks>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
  Public Module [Extensions]

    ''' <summary>
    ''' Appends the text. A new line is added before the appended text if the 
    ''' <see cref="System.Text.StringBuilder">string builder</see> is not empty.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="lineData">New line text</param>
    ''' <remarks>When using <see cref="System.Text.StringBuilder.AppendLine">Append Line</see>, 
    ''' the <see cref="System.Text.StringBuilder">String Builder</see> the text followed by a new line. 
    ''' This procedure appends a new line before appending the text.</remarks>
    <Extension()> _
    Public Sub AppendLine(ByVal value As System.Text.StringBuilder, ByVal lineData As String)
      If Not String.IsNullOrEmpty(lineData) Then
        If value.Length > 0 Then
          value.AppendLine()
        End If
        value.Append(lineData)
      End If
    End Sub

    ''' <summary>
    ''' Appends the formatted text. A new line is added before the appended text if the 
    ''' <see cref="System.Text.StringBuilder">string builder</see> is not empty.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <param name="format">New line format</param>
    ''' <param name="args">Arguments for the line format.</param>
    ''' <remarks>When using <see cref="System.Text.StringBuilder.AppendLine">Append Line</see>, 
    ''' the <see cref="System.Text.StringBuilder">String Builder</see> the text followed by a new line. 
    ''' This procedure appends a new line before appending the text.</remarks>
    <Extension()> _
    Public Sub AppendLine(ByVal value As System.Text.StringBuilder, ByVal format As String, ByVal ParamArray args() As Object)
      If Not String.IsNullOrEmpty(format) Then
        If value.Length > 0 Then
          value.AppendLine()
        End If
        value.AppendFormat(Globalization.CultureInfo.CurrentCulture, format, args)
      End If
    End Sub

    ''' <summary>
    ''' Returns an array of strings split by the new line characters.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function ToArray(ByVal value As System.Text.StringBuilder) As String()
      Return value.ToString.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
    End Function

  End Module
End Namespace
