''' <summary>
''' Defines physical units and scales.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/05/2008" by="David Hary" revision="5.0.3017.x">
''' Based on SCPI Readings.
''' Created
''' </history>
<Serializable()> _
Public Class Units

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  Public Sub New()
    Me.New(PhysicalUnit.None, UnitsScale.None)
  End Sub

  Public Sub New(ByVal units As PhysicalUnit, ByVal scale As UnitsScale)
    MyBase.New()
    Me._description = ""
    Me._longUnits = ""
    Me._name = ""
    Me._prefix = ""
    Me.PhysicalUnits = units
    Me.UnitsScale = scale
  End Sub

  ''' <summary>
  ''' Cloining Constructor.
  ''' </summary>
  ''' <param name="model"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal model As Units)
    Me.New(model.PhysicalUnits, model.UnitsScale)
    Me._description = model._description
    Me._longUnits = model._longUnits
    Me._name = model._name
    Me._prefix = model._prefix
    Me._scaleFactor = model._scaleFactor
    Me._scaleValue = model._scaleValue
  End Sub

#End Region

#Region " UNITS SYMBOLS "

  Public Shared ReadOnly Property Alpha() As Char
    Get
      Return Convert.ToChar(&H3B1)   ' alpha Chr(224)
    End Get
  End Property

  Public Shared ReadOnly Property Beta() As Char
    Get
      Return Convert.ToChar(&H3B2)   ' beta Chr(225)
    End Get
  End Property

  Public Shared ReadOnly Property DegreesC() As Char
    Get
      Return Convert.ToChar(&H2103)   ' °C
    End Get
  End Property

  Public Shared ReadOnly Property DegreesF() As Char
    Get
      Return Convert.ToChar(&H2109)   ' °F
    End Get
  End Property

  ''' <summary>
  ''' Gets the symbol of Ohms
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared ReadOnly Property Omega() As Char
    Get
      Return Convert.ToChar(&H2126)   ' &H3A9 ChrW(&H2126) ' Omega
    End Get
  End Property

  ''' <summary>
  ''' Gets the symbol of Siemense
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared ReadOnly Property InvertedOmega() As Char
    Get
      Return Convert.ToChar(&H2127)   ' ChrW(&H2126) ' Omega
    End Get
  End Property

  Public Shared ReadOnly Property GreekMU() As Char
    Get
      Return Convert.ToChar(&HB5)   ' ChrW(&HB5)   ' "µ"
    End Get
  End Property

  Public Shared ReadOnly Property GreekEta() As Char
    Get
      Return Convert.ToChar(&H3B7)   '  ChrW(&H3B7)  ' "n"
    End Get
  End Property

#End Region

#Region " DESCRIPTION "

  Private _description As String
  ''' <summary>
  ''' Gets or sets the description in the form:
  ''' Name (long units, short units)
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Description() As String
    Get
      Return _description
    End Get
    Set(ByVal value As String)
      _description = value
      If String.IsNullOrEmpty(_description) Then
        _name = String.Empty
        _longUnits = String.Empty
        _shortUnits = String.Empty
      Else
        ' extract the name, long units, and short units.
        Dim values As String() = _description.Split("("c)
        If values.Length > 0 Then
          _name = values(0).Trim
          If values.Length > 1 Then
            values = values(1).Split(","c)
            If values.Length > 0 Then
              _longUnits = values(0).Trim
              If values.Length > 1 Then
                _shortUnits = values(1).Replace(")", "").Trim
              End If
            End If
          End If
        End If
      End If
    End Set
  End Property

  Private _longUnits As String
  Public Property LongUnits() As String
    Get
      Return _longUnits
    End Get
    Set(ByVal value As String)
      _longUnits = value
    End Set
  End Property

  Private _name As String
  Public Property Name() As String
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

  Private _prefix As String
  Public Property Prefix() As String
    Get
      Return _prefix
    End Get
    Set(ByVal value As String)
      _prefix = value
    End Set
  End Property

  Private _physicalUnits As PhysicalUnit
  Public Property PhysicalUnits() As PhysicalUnit
    Get
      Return _physicalUnits
    End Get
    Set(ByVal value As PhysicalUnit)
      _physicalUnits = value
      If value = PhysicalUnit.None Then
        Me.Description = String.Empty
      Else
        Me.Description = EnumExtensions.Description(value)
      End If
    End Set
  End Property

  ''' <summary>
  ''' Returns the units with prefix.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function ToString() As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                         "{0}{1}", Me._prefix, Me._shortUnits)
  End Function

#End Region

#Region " SCALING "

  Private _scaleFactor As Double
  ''' <summary>
  ''' Gets or sets the actual scale factor.  This is the magnitude by which to multiply a raw 
  ''' value to express it in the scaled units.  For instance, to format a value as Kilo, 
  ''' multiply the value by 0.001. 
  ''' </summary>
  Public Property ScaleFactor() As Double
    Get
      Return _scaleFactor
    End Get
    Set(ByVal value As Double)
      _scaleFactor = value
      _scaleValue = 1 / value
    End Set
  End Property

  Private _scaleValue As Double
  ''' <summary>Gets or sets the actual scale value.  This is one over the 
  ''' <see cref="ScaleFactor">scale factor</see>.  For instance the Kilo scale value
  ''' is 1000 whereas the scale factor is 0.001.
  ''' </summary>
  Public ReadOnly Property ScaleValue() As Double
    Get
      Return _scaleValue
    End Get
  End Property

  Private _shortUnits As String
  Public Property ShortUnits() As String
    Get
      Return _shortUnits
    End Get
    Set(ByVal value As String)
      _shortUnits = value
    End Set
  End Property

  Private _unitsScale As UnitsScale
  Public Property UnitsScale() As UnitsScale
    Get
      Return _unitsScale
    End Get
    Set(ByVal value As UnitsScale)
      _unitsScale = value
      Select Case value
        Case UnitsScale.Atto
          _scaleFactor = 1.0E+18
          _scaleValue = 1.0E-18
          _prefix = "a"
        Case UnitsScale.Centi
          _scaleFactor = 100.0
          _scaleValue = 0.01
          _prefix = "c"
        Case UnitsScale.Deci
          _scaleFactor = 10.0
          _scaleValue = 0.1
          _prefix = "d"
        Case UnitsScale.Deka
          _scaleFactor = 0.1
          _scaleValue = 10.0
          _prefix = "da"
        Case UnitsScale.Femto
          _scaleFactor = 1.0E+15
          _scaleValue = 0.000000000000001
          _prefix = "f"
        Case UnitsScale.Giga
          _scaleFactor = 0.000000001
          _scaleValue = 1000000000.0
          _prefix = "G"
        Case UnitsScale.Hecto
          _scaleFactor = 0.01
          _scaleValue = 100.0
          _prefix = "h"
        Case UnitsScale.Kilo
          _scaleFactor = 0.001
          _scaleValue = 1000.0
          _prefix = "k"
        Case UnitsScale.Mega
          _scaleFactor = 0.000001
          _scaleValue = 1000000.0
          _prefix = "M"
        Case UnitsScale.Micro
          _scaleFactor = 1000000.0
          _scaleValue = 0.000001
          _prefix = Convert.ToChar(&H3BC)  '  ChrW(&HB5)   ' "µ"
        Case UnitsScale.Milli
          _scaleFactor = 1000.0
          _scaleValue = 0.001
          _prefix = "m"
        Case UnitsScale.Nano
          _scaleFactor = 1000000000.0
          _scaleValue = 0.000000001
          _prefix = "n" ' ChrW(&H3B7)  ' "n"
        Case UnitsScale.None
          _scaleFactor = 1.0
          _scaleValue = 1.0
          _prefix = ""
        Case UnitsScale.Pico
          _scaleFactor = 1000000000000.0
          _scaleValue = 0.000000000001
          _prefix = "p"
        Case UnitsScale.Tera
          _scaleFactor = 0.000000000001
          _scaleValue = 1000000000000.0
          _prefix = "T"
        Case UnitsScale.Peta
          _scaleFactor = 0.000000000000001
          _scaleValue = 1.0E+15
          _prefix = "P"
        Case UnitsScale.Exa
          _scaleFactor = 1.0E-18
          _scaleValue = 1.0E+18
          _prefix = "E"
        Case UnitsScale.Kibi
          _scaleValue = Math.Pow(2, 10)
          _scaleFactor = 1 / _scaleValue
          _prefix = "Ki"
        Case UnitsScale.Mebi
          _scaleValue = Math.Pow(2, 20)
          _scaleFactor = 1 / _scaleValue
          _prefix = "Mi"
        Case UnitsScale.Gibi
          _scaleValue = Math.Pow(2, 30)
          _scaleFactor = 1 / _scaleValue
          _prefix = "Gi"
        Case UnitsScale.Tebi
          _scaleValue = Math.Pow(2, 40)
          _scaleFactor = 1 / _scaleValue
          _prefix = "Ti"
        Case UnitsScale.Pebi
          _scaleValue = Math.Pow(2, 50)
          _scaleFactor = 1 / _scaleValue
          _prefix = "Pi"
        Case UnitsScale.Exbi
          _scaleValue = Math.Pow(2, 60)
          _scaleFactor = 1 / _scaleValue
          _prefix = "Ei"
      End Select

    End Set
  End Property
#End Region

End Class

''' <summary>
''' Defines physical units and scales with formatting.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/05/2008" by="David Hary" revision="5.0.3017.x">
''' Based on SCPI Readings.
''' Created
''' </history>
<Serializable()> _
Public Class FormattedUnits
  Inherits Units

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  Public Sub New()
    Me.New(PhysicalUnit.None, UnitsScale.None)
  End Sub

  Public Sub New(ByVal units As PhysicalUnit, ByVal scale As UnitsScale)
    MyBase.New()
    Me.PhysicalUnits = units
    Me.UnitsScale = scale
    Me.ValueFormat = ""
  End Sub

  ''' <summary>
  ''' Cloining Constructor.
  ''' </summary>
  ''' <param name="model"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal model As FormattedUnits)
    MyBase.New(model)
    Me._valueFormat = model._valueFormat
  End Sub

#End Region

#Region " FORMAT "

  ''' <summary>
  ''' Returns a format string for using with the <see cref="String.Format"></see>
  ''' for display of the scaled value with units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuildUnitsStringFormat(ByVal format As String) As String
    Return "{0:" & format & "} {1}"
  End Function

  ''' <summary>
  ''' Returns a format string for using with the <see cref="String.Format"></see>
  ''' for display of the scaled value with units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuildFullStringFormat(ByVal format As String) As String
    Return "{0:" & format & "} {1}{2}"
  End Function

  ''' <summary>
  ''' Returns a format string for using with the <see cref="String.Format"></see>
  ''' for display of the scaled value without units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuildValueStringFormat(ByVal format As String) As String
    Return "{0:" & format & "}"
  End Function

  ''' <summary>
  ''' Returns a caption without units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ToCaption(ByVal format As String, ByVal value As Double?) As String
    If value.HasValue Then
      Return ToCaption(format, value.Value)
    Else
      Return ""
    End If
  End Function

  ''' <summary>
  ''' Returns a caption without units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ToCaption(ByVal format As String, ByVal value As Double) As String
    Return value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    ' Return String.Format(Globalization.CultureInfo.CurrentCulture, BuildFullStringFormat(format), value)
  End Function

  ''' <summary>
  ''' Returns a caption including value and units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ToCaption(ByVal format As String, ByVal value As IFormattable) As String
    Return value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    ' Return String.Format(Globalization.CultureInfo.CurrentCulture, BuildFullStringFormat(format), value)
  End Function

  ''' <summary>
  ''' Returns a caption including value and units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ToCaption(ByVal format As String, ByVal value As Double?, ByVal displayUnits As Units) As String
    If value.HasValue Then
      Return ToCaption(format, value.Value, displayUnits)
    Else
      Return ""
    End If
  End Function

  ''' <summary>
  ''' Returns a caption including value and units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ToCaption(ByVal format As String, ByVal value As Double, ByVal displayUnits As Units) As String
    Return value.ToString(format, Globalization.CultureInfo.CurrentCulture) & " " & displayUnits.ToString
    ' Return String.Format(Globalization.CultureInfo.CurrentCulture, BuildFullStringFormat(format), value)
  End Function

  ''' <summary>
  ''' Returns a caption including value and units.
  ''' </summary>
  ''' <param name="format"></param>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ToCaption(ByVal format As String, ByVal value As IFormattable, ByVal displayUnits As Units) As String
    Return value.ToString(format, Globalization.CultureInfo.CurrentCulture) & " " & displayUnits.ToString
    ' Return String.Format(Globalization.CultureInfo.CurrentCulture, BuildUnitsStringFormat(format), value, displayUnits.ToString)
  End Function

  Private _valueFormat As String
  ''' <summary>
  ''' Gets or sets the format for returning a value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property ValueFormat() As String
    Get
      Return _valueFormat
    End Get
    Set(ByVal value As String)
      _valueFormat = value
    End Set
  End Property

#End Region

End Class

''' <summary>
'''  Enumerates the physical units.
''' </summary>
''' <remarks></remarks>
Public Enum PhysicalUnit
  <ComponentModel.Description("None")> None
  <ComponentModel.Description("Acceleration (Meter per Square Second, m/s²)")> Acceleration
  <ComponentModel.Description("Amount Of Substance (Mole, mol)")> AmountOfSubstance
  <ComponentModel.Description("Area (Square Meter, m²)")> Area
  <ComponentModel.Description("Capacitance (Farad, F)")> Capacitance
  <ComponentModel.Description("Celsius Temperature (DegC, °C)")> CelsiusTemperature
  <ComponentModel.Description("Charge (Coulomb, Cb)")> Charge
  <ComponentModel.Description("Conductance (Siemense, S)")> Conductance
  <ComponentModel.Description("Current (Ampere, A)")> Current
  <ComponentModel.Description("Fahrenheit Temperature (DegF, °F)")> FahrenheitTemperature
  <ComponentModel.Description("Force (Kilogram Force, kgf)")> Force
  <ComponentModel.Description("Frequency (Hertz, Hz)")> Frequency
  <ComponentModel.Description("Inductance (Henry, H)")> Inductance
  <ComponentModel.Description("Length (Meter, m)")> Length
  <ComponentModel.Description("Luminous Intensity (Candela, cd)")> LuminousIntensity
  <ComponentModel.Description("Mass (Kilogram, kg")> Mass
  <ComponentModel.Description("Magnetic Flux (Weber, Wb")> MagneticFlux
  <ComponentModel.Description("Resistance (Ohm, Ohm")> Resistance
  <ComponentModel.Description("Temperature (Kelvin, K)")> Temperature
  <ComponentModel.Description("Time (Second, s)")> [Time]
  <ComponentModel.Description("Velocity (Meter per Second, m/s)")> Velocity
  <ComponentModel.Description("Voltage (Volt, V)")> Voltage
  <ComponentModel.Description("Volume (Cubic Meter, m·m²)")> Volume
End Enum

''' <summary>
''' Enumerates the international units scales.
''' </summary>
Public Enum UnitsScale
  <ComponentModel.Description("None")> None
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Atto")> _
                                              <ComponentModel.Description("Atto")> Atto
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Femto")> _
                                              <ComponentModel.Description("Femto")> Femto
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Pico")> _
                                              <ComponentModel.Description("Pico")> Pico
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Nano")> _
                                              <ComponentModel.Description("Nano")> Nano
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Micro")> _
                                              <ComponentModel.Description("Micro")> Micro
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Milli")> _
                                              <ComponentModel.Description("Milli")> Milli
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Centi")> _
                                             <ComponentModel.Description("Centi")> Centi
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Deci")> _
                                              <ComponentModel.Description("Deci")> Deci
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Deka")> _
                                             <ComponentModel.Description("Deka")> Deka
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Hecto")> _
                                              <ComponentModel.Description("Hecto")> Hecto
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Kilo")> _
                                              <ComponentModel.Description("Kilo")> Kilo
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Mega")> _
                                              <ComponentModel.Description("Mega")> Mega
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Giga")> _
                                              <ComponentModel.Description("Giga")> Giga
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Tera")> _
                                              <ComponentModel.Description("Tera")> Tera
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Peta")> _
                                              <ComponentModel.Description("Peta")> Peta
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Exa")> _
                                              <ComponentModel.Description("Exa")> Exa
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Kibi")> _
                                              <ComponentModel.Description("Kibi")> Kibi
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Mebi")> _
                                              <ComponentModel.Description("Mebi")> Mebi
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Gibi")> _
                                              <ComponentModel.Description("Gibi")> Gibi
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Tebi")> _
                                              <ComponentModel.Description("Tebi")> Tebi
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Pebi")> _
                                              <ComponentModel.Description("Pebi")> Pebi
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Exbi")> _
                                              <ComponentModel.Description("Exbi")> Exbi
End Enum

