﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
''' <summary>
''' An <see cref="ObservableCollection">observable collection</see> that passes through the 
''' <see cref="INestedPropertyChanged.NestedPropertyChanged">nested property changed event</see> rasied from its
''' nested items using the <see cref="INestedPropertyChanged.NestedPropertyChanged">property changed event</see>.
''' </summary>
''' <remarks>
''' See <see cref="NotifyParentObservableCollection">notify parent observable collection </see>  for more information.
''' </remarks>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Based on <see cref="NotifyParentObservableCollection">notify parent observable collection </see> by 
''' Michael Agroskin http://www.codeproject.com/KB/WPF/notifyparent2.aspx
''' Licensed under The Eclipse Public License 1.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/11/1/2011" by="David Hary" revision="1.2.4059.x">
''' Created
''' </history>
Public Class NestedObservableCollection(Of T)
  Inherits System.Collections.ObjectModel.ObservableCollection(Of T)
  Implements INestedPropertyChanged, System.Windows.IWeakEventListener

#Region " CONSTRUCTORS  and  DESTRUCTORS "


  ''' <summary>
  ''' Initializes a new instance of the <see cref="NestedObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="sourcename">The sourcename.</param>
  Public Sub New(ByVal sourceName As String)
    Me.New(sourceName, True)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NestedObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="sourceName">The source name.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> supports change notifications.</param>
  Public Sub New(ByVal sourceName As String, ByVal supportsChangeNotifications As Boolean)
    MyBase.New()
    Me.SupportsChangeNotifications = supportsChangeNotifications
    _sourceName = sourceName
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NestedObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="sourceName">The source name.</param>
  ''' <param name="collection">The collection.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> [supports change notifications].</param>
  Public Sub New(ByVal sourceName As String, ByVal collection As IEnumerable(Of T), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(collection)
    Me.SupportsChangeNotifications = supportsChangeNotifications
    _sourceName = sourceName
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NestedObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="list">The list.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> [supports change notifications].</param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification:="Rule broken by MSFT Observable Collection")> _
  Public Sub New(ByVal list As List(Of T), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(list)
    _supportsChangeNotifications = supportsChangeNotifications
  End Sub

#End Region

#Region " COLLECTION MANAGEMENT "

  ''' <summary>
  ''' Attaches the handlers.
  ''' </summary>
  ''' <param name="items">The items.</param>
  Private Sub AttachHandlers(ByVal items As IEnumerable)
    If Not items Is Nothing Then
      For Each Item As INestedPropertyChanged In items.OfType(Of INestedPropertyChanged)()
        NestedPropertyChangedWeakEventManager.AddListener(Item, Me)
      Next Item
      For Each Item As INotifyPropertyChanged In items.OfType(Of INotifyPropertyChanged)()
        PropertyChangedWeakEventManager.AddListener(Item, Me)
      Next Item
    End If
  End Sub

  ''' <summary>
  ''' Removes all items from the collection.
  ''' </summary>
  Protected Overrides Sub ClearItems()
    If SupportsChangeNotifications Then
      DetachHandlers(Items)
    End If
    MyBase.ClearItems()
  End Sub

  ''' <summary>
  ''' Detaches the handlers.
  ''' </summary>
  ''' <param name="items">The items.</param>
  Private Sub DetachHandlers(ByVal items As IEnumerable)
    If Not items Is Nothing Then
      For Each Item As INestedPropertyChanged In items.OfType(Of INestedPropertyChanged)()
        NestedPropertyChangedWeakEventManager.RemoveListener(Item, Me)
      Next Item
      For Each Item As INotifyPropertyChanged In items.OfType(Of INotifyPropertyChanged)()
        PropertyChangedWeakEventManager.RemoveListener(Item, Me)
      Next Item
    End If
  End Sub

#End Region

#Region " I NESTED PROPERTY CHANGED "

  ''' <summary>
  ''' Occurs when collection item (child) property changed.
  ''' </summary>
  Public Event NestedPropertyChanged As System.EventHandler(Of NestedPropertyChangedEventArgs) Implements INestedPropertyChanged.NestedPropertyChanged

  Private _sourceName As String
  ''' <summary>
  ''' Gets or sets the name of the source.
  ''' Uniquely identifies the source for the property name change.
  ''' </summary>
  ''' <value>
  ''' The name of the source.
  ''' </value>
  Public Property SourceName() As String Implements INestedPropertyChanged.SourceName
    Get
      Return _sourceName
    End Get
    Set(ByVal value As String)
      _sourceName = ""
    End Set
  End Property

#End Region

#Region " EVENT MANAGEMENT "

  ''' <summary>
  ''' Called when an object implementing the <see cref="INestedPropertyChanged">nested property changed</see>
  ''' interface sends a propery changed message.
  ''' </summary>
  ''' <param name="e">The <see cref="isr.Core.NestedPropertyChangedEventArgs" /> instance containing the event data.</param>
  ''' <remarks>
  ''' This event is called when handling a weak event from the <see cref="NestedPropertyChangedWeakEventManager">nested property changed weak event handler</see>
  ''' which is used to implement weak events for the Nested Propery Changed implementation.
  ''' </remarks>
  Protected Sub OnNestedPropertyChanged(ByVal e As NestedPropertyChangedEventArgs)
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, _
                                      New NestedPropertyChangedEventArgs(Me, e.Sources, e.PropertyName))
  End Sub

  ''' <summary>
  ''' Raises the <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.CollectionChanged" /> event with the provided arguments.
  ''' </summary>
  ''' <param name="e">Arguments of the event being raised.</param>
  Protected Overrides Sub OnCollectionChanged(ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    If SupportsChangeNotifications Then
      If e.Action = Specialized.NotifyCollectionChangedAction.Add Then
        AttachHandlers(e.NewItems)
      ElseIf e.Action = Specialized.NotifyCollectionChangedAction.Remove Then
        DetachHandlers(e.OldItems)
      End If
    End If
    MyBase.OnCollectionChanged(e)
  End Sub

  ''' <summary>
  ''' Receives events from the centralized event manager.
  ''' </summary>
  ''' <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
  ''' <param name="sender">Object that originated the event.</param>
  ''' <param name="e">Event data.</param>
  ''' <returns>
  ''' <c>true</c> if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager" />
  ''' The method should return false if it receives an event that it does not recognize or handle.
  ''' </returns>
  Public Function ReceiveWeakEvent(ByVal managerType As System.Type, ByVal sender As Object, ByVal e As System.EventArgs) As Boolean Implements System.Windows.IWeakEventListener.ReceiveWeakEvent
    If Not SupportsChangeNotifications Then
      Return True
    End If
    If managerType Is GetType(NestedPropertyChangedWeakEventManager) Then
      OnNestedPropertyChanged(CType(e, NestedPropertyChangedEventArgs))
    End If
    If managerType Is GetType(PropertyChangedWeakEventManager) Then
      OnNestedPropertyChanged(New NestedPropertyChangedEventArgs(sender, CType(e, PropertyChangedEventArgs).PropertyName))
    End If
    Return True
  End Function

  Private _supportsChangeNotifications As Boolean
  ''' <summary>
  ''' Gets or sets a value indicating whether the collection supports change notifications.
  ''' This applies to both list and item changes.
  ''' </summary>
  ''' <value>
  ''' 
  ''' <c>true</c> if the collection supports change notifications; otherwise, <c>false</c>.
  ''' 
  ''' </value>
  Public Property SupportsChangeNotifications() As Boolean
    Get
      Return _supportsChangeNotifications
    End Get
    Set(ByVal value As Boolean)
      _supportsChangeNotifications = value
      If _supportsChangeNotifications Then
        AttachHandlers(Items)
      Else
        DetachHandlers(Items)
      End If
    End Set
  End Property

#End Region

End Class

#Region " I NESTED PROPERTY CHANGED "

''' <summary>
''' The custom property changed event arguments for notifications from the chain of items nested within the collection.
''' This includes reference to the <see cref="NestedPropertyChangedEventArgs.Sources">stack of sources</see>.
''' </summary>
''' <remarks>
''' The first source in the nested chain might be implementing the <see cref="ComponentModel.INotifyPropertyChanged">notify property changed</see>
''' or the <see cref="INestedPropertyChanged">nested property changed</see> interface. The former would be the initial object
''' in the nested chain.
''' </remarks>
Public Class NestedPropertyChangedEventArgs
  Inherits System.ComponentModel.PropertyChangedEventArgs

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NestedPropertyChangedEventArgs" /> class.
  ''' </summary>
  ''' <param name="source">The source implementing the <see cref="ComponentModel.INotifyPropertyChanged">notify property changed</see>
  ''' (first source only) or the <see cref="INestedPropertyChanged">nested property changed</see> interface.</param>
  ''' <param name="propertyName">Name of the property.</param>
  ''' <remakrs>
  ''' Starts a new nested chain of resources.
  ''' </remakrs>
  Public Sub New(ByVal source As Object, ByVal propertyName As String)
    MyBase.New(propertyName)
    _sources = New Stack(Of Object)
    _sources.Push(source)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NestedPropertyChangedEventArgs" /> class.
  ''' </summary>
  ''' <param name="source">The source implementing the <see cref="ComponentModel.INotifyPropertyChanged">notify property changed</see>
  ''' (first source only) or the <see cref="INestedPropertyChanged">nested property changed</see> interface.</param>
  ''' <param name="sources">The sources.</param>
  ''' <param name="propertyName">Name of the property.</param>
  ''' <remakrs>
  ''' Extends the nested chain of resources by adding the nest source to the chain.
  ''' </remakrs>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")> _
  Public Sub New(ByVal source As Object, ByVal sources As Stack(Of Object), ByVal propertyName As String)
    MyBase.New(propertyName)
    _sources = New Stack(Of Object)(sources.ToArray.Reverse)
    _sources.Push(source)
  End Sub

  Private _sources As Stack(Of Object)
  ''' <summary>
  ''' Gets the <see cref="Stack">chain</see> of sources pointing to the 
  ''' original source sending the notification.
  ''' </summary>
  ''' <value>
  ''' The stack of sources.
  ''' </value>
  ''' <remarks>
  ''' The first source in the nested chain might be implementing the <see cref="ComponentModel.INotifyPropertyChanged">notify property changed</see>
  ''' or the <see cref="INestedPropertyChanged">nested property changed</see> interface. All subsequent sources must be implementing
  ''' the latter interface.
  ''' </remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")> _
  Public ReadOnly Property Sources() As Stack(Of Object)
    Get
      Return _sources
    End Get
  End Property

End Class

''' <summary>
''' The contract implemented by sources raising the 
''' <see cref="NestedPropertyChangedEventArgs">nested property changed event information</see>.
''' </summary>
Public Interface INestedPropertyChanged
  'Inherits ComponentModel.INotifyPropertyChanged

  ''' <summary>
  ''' Gets or sets the name of the source.
  ''' Uniquely identifies the source for the property name change.
  ''' </summary>
  ''' <value>
  ''' The name of the source.
  ''' </value>
  Property SourceName() As String

  ''' <summary>
  ''' Occurs when the nested property changed.
  ''' </summary>
  Event NestedPropertyChanged As System.EventHandler(Of NestedPropertyChangedEventArgs)

End Interface

#End Region

#Region " NESTED PROPERTY CHANGED WEAK EVENT MANAGER "

''' <summary>
''' The weak event manager for notifying of changes in a property of a nested sources 
''' implementing the <see cref="INestedPropertyChanged">nested property changed</see> contract.
''' </summary>
Public Class NestedPropertyChangedWeakEventManager
  Inherits System.Windows.WeakEventManager
  Private Sub New()
  End Sub

  ''' <summary>
  ''' Adds the listener.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="listener">The listener.</param>
  Public Shared Sub AddListener(ByVal source As INestedPropertyChanged, ByVal listener As System.Windows.IWeakEventListener)
    CurrentManager.ProtectedAddListener(source, listener)
  End Sub

  ''' <summary>
  ''' Removes the listener.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="listener">The listener.</param>
  Public Shared Sub RemoveListener(ByVal source As INestedPropertyChanged, ByVal listener As System.Windows.IWeakEventListener)
    CurrentManager.ProtectedRemoveListener(source, listener)
  End Sub

  ''' <summary>
  ''' When overridden in a derived class, starts listening for the event being managed. 
  ''' After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)" />  is first called, 
  ''' the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)" /> 
  ''' or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)" /> 
  ''' whenever the relevant event from the provided source is handled.
  ''' </summary>
  ''' <param name="source">The source to begin listening on.</param>
  Protected Overrides Sub StartListening(ByVal source As Object)
    AddHandler CType(source, INestedPropertyChanged).NestedPropertyChanged, AddressOf DeliverEvent
  End Sub

  ''' <summary>
  ''' When overridden in a derived class, stops listening on the provided source for the event being managed.
  ''' </summary>
  ''' <param name="source">The source to stop listening on.</param>
  Protected Overrides Sub StopListening(ByVal source As Object)
    RemoveHandler CType(source, INestedPropertyChanged).NestedPropertyChanged, AddressOf DeliverEvent
  End Sub

  ''' <summary>
  ''' Gets the current manager.
  ''' </summary>
  ''' 
  Private Shared ReadOnly Property CurrentManager() As NestedPropertyChangedWeakEventManager
    Get
      Dim managerType As Type = GetType(NestedPropertyChangedWeakEventManager)
      Dim manager As NestedPropertyChangedWeakEventManager = CType(GetCurrentManager(managerType), NestedPropertyChangedWeakEventManager)
      If manager Is Nothing Then
        manager = New NestedPropertyChangedWeakEventManager()
        SetCurrentManager(managerType, manager)
      End If
      Return manager
    End Get
  End Property

End Class

#End Region
