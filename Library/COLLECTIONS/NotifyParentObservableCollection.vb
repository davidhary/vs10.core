Imports System.Collections.ObjectModel
Imports System.ComponentModel
''' <summary>
''' An <see cref="ObservableCollection">observable collection</see> that passes through the 
''' <see cref="INotifyPropertyChanged.PropertyChanged">property changed event</see> of its items (children)
''' and using the <see cref="IChildPropertyChanged">child property changed event</see>.
''' </summary>
''' <remarks>
''' There are many examples on the web for implementing some kind of weak listener � but interestingly
''' almost all of them have some flaw. In the end we settled on using WeakEventManager and its associated IWeakEventListener interface.
''' All other solutions create an instance of the wrapper class attached to the source with a strong reference, and to the target with
''' a weak reference. Every time the source event fires, the wrapper checks if the target is still alive. If the target is dead, the wrapper
''' detaches from the source and can be garbage collected. 
''' Unfortunately, this is a potential memory leak. If the source events never fire, thousands of wrappers can be leaked as discussed here:
''' Solving the Problem with Events http://diditwith.net/2007/03/23/SolvingTheProblemWithEventsWeakEventHandlers.aspx;
''' Weak Event Handlers Weak Events in C# http://www.codeproject.com/KB/cs/WeakEvents.aspx.
''' Looks like that the MSFT way makes most sense: create a single STATIC event wrapper per event type, and clean up unused connections 
''' on the background thread. This <see cref="NotifyParentObservableCollection">collection</see> forwards children 
''' <see cref="INotifyPropertyChanged">notify property changed</see> events to interested listeners using 
''' the <see cref="System.Windows.WeakEventManager">weak event manager</see> just like BindingList supposedly did.
''' </remarks>
''' <license>
''' (c) 2011 Michael Agroskin.
''' http://www.codeproject.com/KB/WPF/notifyparent2.aspx
''' Licensed under The Eclipse Public License 1.0; 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Class NotifyParentObservableCollection(Of T)
    Inherits System.Collections.ObjectModel.ObservableCollection(Of T)
  Implements IChildPropertyChanged, System.Windows.IWeakEventListener

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NotifyParentObservableCollection(Of T)" /> class.
  ''' </summary>
  Public Sub New()
    Me.New(True)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NotifyParentObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> supports change notifications.</param>
  Public Sub New(ByVal supportsChangeNotifications As Boolean)
    Me.SupportsChangeNotifications = supportsChangeNotifications
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NotifyParentObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="collection">The collection.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> supports change notifications.</param>
  Public Sub New(ByVal collection As IEnumerable(Of T), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(collection)
    supportsChangeNotifications = supportsChangeNotifications
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="NotifyParentObservableCollection(Of T)" /> class.
  ''' </summary>
  ''' <param name="list">The list.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> supports change notifications.</param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification:="Rule broken by MSFT Observable Collection")> _
  Public Sub New(ByVal list As List(Of T), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(list)
    _supportsChangeNotifications = supportsChangeNotifications
  End Sub

#End Region

#Region " COLLECTION MANAGEMENT "

  ''' <summary>
  ''' Attaches the handlers.
  ''' </summary>
  ''' <param name="items">The items.</param>
  Private Sub AttachHandlers(ByVal items As IEnumerable)
    If Not items Is Nothing Then
      For Each Item As INotifyPropertyChanged In items.OfType(Of INotifyPropertyChanged)()
        PropertyChangedWeakEventManager.AddListener(Item, Me)
      Next Item
    End If
  End Sub

  ''' <summary>
  ''' Removes all items from the collection.
  ''' </summary>
  Protected Overrides Sub ClearItems()
    If SupportsChangeNotifications Then
      DetachHandlers(Items)
    End If
    MyBase.ClearItems()
  End Sub

  ''' <summary>
  ''' Detaches the handlers.
  ''' </summary>
  ''' <param name="items">The items.</param>
  Private Sub DetachHandlers(ByVal items As IEnumerable)
    If Not items Is Nothing Then
      For Each Item As INotifyPropertyChanged In items.OfType(Of INotifyPropertyChanged)()
        PropertyChangedWeakEventManager.RemoveListener(Item, Me)
      Next Item
    End If
  End Sub

#End Region

#Region " I CHILD PROPERTY CHANGED "

  ''' <summary>
  ''' Occurs when collection item (child) property changed.
  ''' </summary>
  Public Event ChildPropertyChanged As System.EventHandler(Of ChildPropertyChangedEventArgs) Implements IChildPropertyChanged.ChildPropertyChanged

#End Region

#Region " EVENT MANAGEMENT "

  Private _supportsChangeNotifications As Boolean
  ''' <summary>
  ''' Gets or sets a value indicating whether the collection supports change notifications.
  ''' This applies to both list and item changes.
  ''' </summary>
  ''' <value>
  ''' 
  ''' <c>true</c> if the collection supports change notifications; otherwise, <c>false</c>.
  ''' 
  ''' </value>
  Public Property SupportsChangeNotifications() As Boolean
    Get
      Return _supportsChangeNotifications
    End Get
    Set(ByVal value As Boolean)
      _supportsChangeNotifications = value
      If _supportsChangeNotifications Then
        AttachHandlers(Items)
      Else
        DetachHandlers(Items)
      End If
    End Set
  End Property

  ''' <summary>
  ''' Called when collection item (child) property changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="source">The source.</param>
  ''' <param name="propertyname">The propertyname.</param>
  Private Sub OnChildPropertyChanged(ByVal sender As Object, ByVal source As Object, ByVal propertyname As String)
    EventHandlerExtensions.SafeInvoke(ChildPropertyChangedEvent, sender, New ChildPropertyChangedEventArgs(source, propertyname))
  End Sub

  ''' <summary>
  ''' Raises the <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.CollectionChanged" /> event with the provided arguments.
  ''' </summary>
  ''' <param name="e">Arguments of the event being raised.</param>
  Protected Overrides Sub OnCollectionChanged(ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    If SupportsChangeNotifications Then
      If e.Action = Specialized.NotifyCollectionChangedAction.Add Then
        AttachHandlers(e.NewItems)
      ElseIf e.Action = Specialized.NotifyCollectionChangedAction.Remove Then
        DetachHandlers(e.OldItems)
      End If
    End If
    MyBase.OnCollectionChanged(e)
  End Sub

  ''' <summary>
  ''' Receives events from the centralized event manager.
  ''' </summary>
  ''' <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
  ''' <param name="sender">Object that originated the event.</param>
  ''' <param name="e">Event data.</param><returns>
  ''' true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager" />
  ''' handling in WPF�to register a listener for an event that the listener does not handle. 
  ''' Regardless, the method should return false if it receives an event that it does not recognize or handle.
  ''' </returns>
  Public Function ReceiveWeakEvent(ByVal managerType As System.Type, ByVal sender As Object, ByVal e As System.EventArgs) As Boolean Implements System.Windows.IWeakEventListener.ReceiveWeakEvent
    If managerType Is GetType(PropertyChangedWeakEventManager) Then
      If SupportsChangeNotifications Then
        OnChildPropertyChanged(Me, sender, CType(e, PropertyChangedEventArgs).PropertyName)
      End If
    End If
    Return True
  End Function

#End Region

End Class

#Region " I CHILD PROPERTY CHANGED "

''' <summary>
''' The custom property changed event arguments for notifications from collection items (children) properties.
''' This includes reference to the <see cref="ChildPropertyChangedEventArgs.Source">collection item (child or element)</see>.
''' </summary>
Public Class ChildPropertyChangedEventArgs
  Inherits System.ComponentModel.PropertyChangedEventArgs

  ''' <summary>
  ''' Initializes a new instance of the <see cref="ChildPropertyChangedEventArgs" /> class.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="propertyName">The property name.</param>
  Public Sub New(ByVal source As Object, ByVal propertyName As String)
    MyBase.New(propertyName)
    _source = source
  End Sub

  Private _source As Object
  ''' <summary>
  ''' Gets or sets the source sending the notification.
  ''' </summary>
  ''' <value>
  ''' The source.
  ''' </value>
  Public Property Source() As Object
    Get
      Return _source
    End Get
    Set(ByVal value As Object)
      _source = value
    End Set
  End Property

End Class

''' <summary>
''' The contract implemented by collection item (child) raising the 
''' <see cref="ChildPropertyChangedEventArgs">property changed event information</see>.
''' </summary>
Public Interface IChildPropertyChanged
  Event ChildPropertyChanged As System.EventHandler(Of ChildPropertyChangedEventArgs)
End Interface

#End Region

#Region " CHILD PROPERTY CHANGED WEAK EVENT MANAGER "

''' <summary>
''' The weak event manager for notifying of changes in collection item (child) properties.
''' </summary>
Public Class ChildPropertyChangedEventManager
  Inherits System.Windows.WeakEventManager
  Private Sub New()
  End Sub

  ''' <summary>
  ''' Adds the listener.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="listener">The listener.</param>
  Public Shared Sub AddListener(ByVal source As IChildPropertyChanged, ByVal listener As System.Windows.IWeakEventListener)
    CurrentManager.ProtectedAddListener(source, listener)
  End Sub

  ''' <summary>
  ''' Removes the listener.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="listener">The listener.</param>
  Public Shared Sub RemoveListener(ByVal source As IChildPropertyChanged, ByVal listener As System.Windows.IWeakEventListener)
    CurrentManager.ProtectedRemoveListener(source, listener)
  End Sub

  ''' <summary>
  ''' When overridden in a derived class, starts listening for the event being managed. 
  ''' After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)" />  is first called, 
  ''' the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)" /> 
  ''' or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)" /> 
  ''' whenever the relevant event from the provided source is handled.
  ''' </summary>
  ''' <param name="source">The source to begin listening on.</param>
  Protected Overrides Sub StartListening(ByVal source As Object)
    AddHandler CType(source, IChildPropertyChanged).ChildPropertyChanged, AddressOf DeliverEvent
  End Sub

  ''' <summary>
  ''' When overridden in a derived class, stops listening on the provided source for the event being managed.
  ''' </summary>
  ''' <param name="source">The source to stop listening on.</param>
  Protected Overrides Sub StopListening(ByVal source As Object)
    RemoveHandler CType(source, IChildPropertyChanged).ChildPropertyChanged, AddressOf DeliverEvent
  End Sub

  ''' <summary>
  ''' Gets the current manager.
  ''' </summary>
  ''' 
  Private Shared ReadOnly Property CurrentManager() As ChildPropertyChangedEventManager
    Get
      Dim managerType As Type = GetType(ChildPropertyChangedEventManager)
      Dim manager As ChildPropertyChangedEventManager = CType(GetCurrentManager(managerType), ChildPropertyChangedEventManager)
      If manager Is Nothing Then
        manager = New ChildPropertyChangedEventManager()
        SetCurrentManager(managerType, manager)
      End If
      Return manager
    End Get
  End Property

End Class

#End Region

#Region " ANY PROPERTY CHANGED WEAK EVENT MANAGER "

''' <summary>
''' The weak event manager for handling property changes from elements implementing 
''' the <see cref="system.ComponentModel.INotifyPropertyChanged">notify property changed</see>
''' interface. This includes both list and item properties.
''' </summary>
Public Class PropertyChangedWeakEventManager
  Inherits System.Windows.WeakEventManager
  Private Sub New()
  End Sub

  ''' <summary>
  ''' Adds the listener.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="listener">The listener.</param>
  Public Shared Sub AddListener(ByVal source As System.ComponentModel.INotifyPropertyChanged, ByVal listener As System.Windows.IWeakEventListener)
    CurrentManager.ProtectedAddListener(source, listener)
  End Sub

  ''' <summary>
  ''' Removes the listener.
  ''' </summary>
  ''' <param name="source">The source.</param>
  ''' <param name="listener">The listener.</param>
  Public Shared Sub RemoveListener(ByVal source As System.ComponentModel.INotifyPropertyChanged, ByVal listener As System.Windows.IWeakEventListener)
    CurrentManager.ProtectedRemoveListener(source, listener)
  End Sub

  ''' <summary>
  ''' When overridden in a derived class, starts listening for the event being managed. 
  ''' After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)" />  is first called, 
  ''' the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)" /> 
  ''' or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)" /> 
  ''' whenever the relevant event from the provided source is handled.
  ''' </summary>
  ''' <param name="source">The source to begin listening on.</param>
  Protected Overrides Sub StartListening(ByVal source As Object)
    StartListening(CType(source, System.ComponentModel.INotifyPropertyChanged))
  End Sub

  ''' <summary>
  ''' Starts the listening.
  ''' </summary>
  ''' <param name="source">The source.</param>
  Private Overloads Sub StartListening(ByVal source As System.ComponentModel.INotifyPropertyChanged)
    AddHandler source.PropertyChanged, AddressOf DeliverEvent
  End Sub

  ''' <summary>
  ''' When overridden in a derived class, stops listening on the provided source for the event being managed.
  ''' </summary>
  ''' <param name="source">The source to stop listening on.</param>
  Protected Overrides Sub StopListening(ByVal source As Object)
    StopListening(CType(source, System.ComponentModel.INotifyPropertyChanged))
  End Sub

  ''' <summary>
  ''' Stops the listening.
  ''' </summary>
  ''' <param name="source">The source.</param>
  Protected Overloads Sub StopListening(ByVal source As System.ComponentModel.INotifyPropertyChanged)
    RemoveHandler source.PropertyChanged, AddressOf DeliverEvent
  End Sub

  ''' <summary>
  ''' Gets the current manager.
  ''' </summary>
  ''' 
  Private Shared ReadOnly Property CurrentManager() As PropertyChangedWeakEventManager
    Get
      Dim managerType As Type = GetType(PropertyChangedWeakEventManager)
      Dim manager As PropertyChangedWeakEventManager = CType(GetCurrentManager(managerType), PropertyChangedWeakEventManager)
      If manager Is Nothing Then
        manager = New PropertyChangedWeakEventManager()
        SetCurrentManager(managerType, manager)
      End If
      Return manager
    End Get
  End Property
End Class

#End Region
