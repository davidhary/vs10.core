﻿''' <summary>
''' The contract implemented by presettable and resettable cached values.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' The cached value can be preset to its preset value or to its reset value.
''' In both cases, it is assumed that these are also the actual values.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Based on <see cref="ICachedStructure(Of T)">Cachaded Structure</see>
''' Created
''' </history>
Public Interface IPresettableCache(Of T As Structure)

  Inherits IPresettableCachedElementBase, ICachedStructure(Of T), IPresettable(Of T), IResettable(Of T), IClearable(Of T), IApproximateable

End Interface

''' <summary>
''' Implements a generic <see cref="IPresettableCache(Of T)">Cached Value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Based on <see cref="ICachedStructure(Of T)">Cachaded Structure</see>
''' Created
''' </history>
Public Class PresettableCache(Of T As Structure)

  Inherits CachedStructure(Of T)
  Implements IPresettableCache(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The  <see cref="PresettableCache">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As PresettableCache(Of T))

    Me.New(model.Name, model.CacheValue, model.ClearValue, model.PresetValue, model.DefaultValue, model.ActualValue)

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T)

    MyBase.New(name, cached)
    _clearValue = cached

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T, ByVal defaultValue As T, ByVal preset As T, ByVal cleared As T)

    MyBase.New(name, cached)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T, ByVal defaultValue As T, ByVal preset As T, ByVal cleared As T)

    MyBase.New(cached)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T, ByVal defaultValue As T, ByVal preset As T, ByVal cleared As T, ByVal actual As T)

    MyBase.New(cached, actual)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T, ByVal defaultValue As T?, ByVal preset As T?, ByVal cleared As T?, ByVal actual As T)

    MyBase.New(name, cached, actual)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="PresettableCache(Of T)">Presettable cache</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see>, <see cref="ActualValue">actual</see>, 
  ''' <see cref="DefaultValue">default</see>, <see cref="ClearValue">clear</see> and <see cref="PresetValue">preset</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As PresettableCache(Of T), ByVal right As PresettableCache(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue) _
           AndAlso left.DefaultValue.Equals(right.DefaultValue) _
           AndAlso left.ClearValue.Equals(right.ClearValue) _
           AndAlso left.PresetValue.Equals(right.PresetValue)
  End Function

#End Region

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="ClearValue">clear value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Clear() As Boolean Implements IClearable.Clear
    If Me._clearValue.HasValue Then
      MyBase.ActualValue = Me._clearValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="PresetValue">preset value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    If Me._presetValue.HasValue Then
      MyBase.ActualValue = Me._presetValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="DefaultValue">default value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    If Me._defaultValue.HasValue Then
      MyBase.ActualValue = Me._defaultValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " CASTS "
#If False Then
  ''' <summary>Creates a new <see cref="T:ScpiValue"></see> object initialized to a specified value.</summary>
  ''' <returns>A <see cref="T:ScpiValue"></see> object whose <see cref="P:ScpiValue.Value"></see> 
  ''' property is initialized with the value parameter.</returns>
  ''' <param name="value">A value type.</param>
  Public Shared Widening Operator CType(ByVal value As Nullable(Of T)) As ScpiValue(Of T)
    If value.HasValue Then
      Return New ScpiValue(Of T)(value.Value)
    Else
      Return New ScpiValue(Of T)()
    End If
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ScpiValue"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ScpiValue"></see> value.</param>
  Public Shared Narrowing Operator CType(ByVal value As PresettableCache(Of T As Structure)) As IPresettableCachedElementBase
    Return value.Value
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ScpiValue"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ScpiValue"></see> value.</param>
  Public Shared Narrowing Operator CType(ByVal value As ScpiValue(Of T)) As Nullable(Of T)
    Return value.Value
  End Operator

  ''' <summary>Returns the value of a specified <see cref="T:ScpiValue"></see> value.</summary>
  ''' <returns>The value of the <see cref="P:ScpiValue.Value"></see> property for the value parameter.</returns>
  ''' <param name="value">A <see cref="T:ScpiValue"></see> value.</param>
  Public Shared Narrowing Operator CType(ByVal value As ScpiValue(Of T)) As T
    Return value.Value.Value
  End Operator
#End If
#End Region

#Region " IPRESETTABLE CACHE "

  Private _clearValue As T?
  Public Property ClearValue() As T? Implements IClearable(Of T).ClearValue
    Get
      Return _clearValue
    End Get
    Set(ByVal value As T?)
      _clearValue = value
    End Set
  End Property

  Private _presetValue As T?
  Public Property PresetValue() As T? Implements IPresettable(Of T).PresetValue
    Get
      Return _presetValue
    End Get
    Set(ByVal value As T?)
      _presetValue = value
    End Set
  End Property

  Private _defaultValue As T?
  Public Property DefaultValue() As T? Implements IResettable(Of T).DefaultValue
    Get
      Return _defaultValue
    End Get
    Set(ByVal value As T?)
      _defaultValue = value
    End Set
  End Property

#End Region

#Region " I CACHED STRUCTURE "

  ''' <summary>
  ''' Determines whether the supplied value is Actual.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value equals the <see cref="ActualValue">actual value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overrides Function IsActualValue(ByVal value As T) As Boolean
    Return Me.Equals(value, Me.ActualValue)
  End Function

  ''' <summary>
  ''' Determines whether the supplied value is new.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value is not equal to the <see cref="CacheValue">cached value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overrides Function IsNewValue(ByVal value As T) As Boolean
    Return Not Me.Equals(value, Me.CacheValue)
  End Function

#End Region

#Region " I APPROXIMATEABLE "

  Private _Epsilon As Object
  ''' <summary>
  ''' Gets the minimum noticable difference between the values.
  ''' This is the tolerance ot resulution of the values.
  ''' </summary>
  ''' <value>
  ''' The epsilon.
  ''' </value>
  Public ReadOnly Property Epsilon() As Object Implements IApproximateable.Epsilon
    Get
      Return _Epsilon
    End Get
  End Property

  ''' <summary>Determines if the two specified <see cref="T:Double">Double</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Shared Function Approximates(ByVal left As Double, ByVal right As Double, ByVal epsilon As Double) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Double">Double</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Double, ByVal right As Double) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Double))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Single">Single</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Single, ByVal right As Single, ByVal epsilon As Single) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Single">Single</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Single, ByVal right As Single) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Single))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Decimal">Decimal</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Decimal, ByVal right As Decimal) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Decimal))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Decimal">Decimal</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Decimal, ByVal right As Decimal, ByVal epsilon As Decimal) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:integer">integer</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Integer, ByVal right As Integer) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Integer))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:integer">integer</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Integer, ByVal right As Integer, ByVal epsilon As Integer) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

#End Region

End Class

