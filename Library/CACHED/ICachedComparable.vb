﻿''' <summary>
''' The contract implemented by Comparable Cached values.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
Public Interface ICachedComparable(Of T As IComparable)

  Inherits ICachedElementBase
  Property ActualValue() As T
  Property CacheValue() As T
  Overloads Function Equals(ByVal value As ICachedComparable(Of T)) As Boolean
  Property Setter() As Action(Of T)

End Interface

''' <summary>
''' Implements a generic <see cref="ICachedComparable(Of T)">Cached Value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
Public Class CachedComparable(Of T As IComparable)

  Inherits CachedElementBase
  Implements ICachedComparable(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the parameter.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T)

    MyBase.New(name)
    _cacheValue = cached

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the parameter.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T)

    MyBase.New()
    _cacheValue = cached

  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The  <see cref="CachedComparable">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As CachedComparable(Of T))

    Me.New(model.Name, model._cacheValue, model._actualValue)

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T, ByVal actual As T)

    MyBase.New()
    _cacheValue = cached
    _actualValue = actual
    MyBase.IsActual = actual.Equals(cached)
    MyBase.HasActualValue = True

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T, ByVal actual As T)

    Me.New(name, cached)
    _actualValue = actual
    MyBase.IsActual = actual.Equals(cached)
    MyBase.HasActualValue = True

  End Sub

#End Region

#Region " DELEGATES "

  Private _setter As Action(Of T)
  ''' <summary>
  ''' Gets or sets the action for setting the actual value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Setter() As Action(Of T) Implements ICachedComparable(Of T).Setter
    Get
      Return _setter
    End Get
    Set(ByVal value As Action(Of T))
      _setter = value
    End Set
  End Property

#End Region

#Region " EQUALS "

  ''' <summary>Determines if the two specified <see cref="CachedComparable(Of T)">cached comparable</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As CachedComparable(Of T), ByVal right As CachedComparable(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.Equals(left.CacheValue, right.CacheValue) _
           AndAlso left.Equals(left.ActualValue, right.ActualValue)
#If False Then

    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue)
#End If
  End Function

  ''' <summary>Indicates whether the current <see cref="T:CachedComparable(Of T)"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CachedComparable(Of T)"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Overrides Function Equals(ByVal value As Object) As Boolean
    Return value IsNot Nothing AndAlso (Object.ReferenceEquals(Me, value)) OrElse CachedComparable(Of T).Equals(Me, TryCast(value, CachedComparable(Of T)))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:CachedComparable(Of T)"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CachedComparable(Of T)"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Function Equals(ByVal value As ICachedComparable(Of T)) As Boolean Implements ICachedComparable(Of T).Equals
    If value Is Nothing Then
      Throw New ArgumentNullException("value")
    End If
    Return Equals(Me, value)
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As T, ByVal right As T) As Boolean
    Return left.Equals(right)
  End Function
#End If

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return _cacheValue.GetHashCode Xor _actualValue.GetHashCode
  End Function

#End Region

#Region " READING "

  Private Function _reading(ByVal value As Boolean) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, MyBase.ReadingFormat, IIf(value, 1, -1))
  End Function

  Private Function _reading(ByVal value As T) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, MyBase.ReadingFormat, value)
  End Function

  ''' <summary>
  ''' Returns the reading in the specified format.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides ReadOnly Property Reading() As String
    Get
      If String.IsNullOrEmpty(MyBase.ReadingFormat) Then
        Return _actualValue.ToString()
      Else
        Return _reading(_actualValue)
      End If
    End Get
  End Property

#End Region

#Region " VALUES "

  ''' <summary>
  ''' Apply changes and return true if a change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Apply() As Boolean

    If Not Me.IsActual Then
      Me.Setter.Invoke(Me.CacheValue)
      Return True
    End If
    Return False

  End Function

  Private _actualValue As T
  ''' <summary>
  ''' Gets or sets the actual value stored in the instrument.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property ActualValue() As T Implements ICachedComparable(Of T).ActualValue
    Get
      Return Me._actualValue
    End Get
    Set(ByVal Value As T)
      ' set fresh if we have a new cached value 
      ' commented out for using resolution. MyBase.IsFresh = Value.CompareTo(Me._cacheValue) <> 0
      MyBase.IsFresh = Not Me.Equals(Value, Me._cacheValue)
      Me._actualValue = Value
      Me._cacheValue = Value
      MyBase.IsActual = True
      MyBase.HasActualValue = True
    End Set
  End Property

  Private _cacheValue As T
  ''' <summary>
  ''' Gets or sets the cached value of the Cached Value.
  ''' This value is updated whenever the actual value is set.
  ''' </summary>
  Public Property CacheValue() As T Implements ICachedComparable(Of T).CacheValue
    Get
      Return Me._cacheValue
    End Get
    Set(ByVal Value As T)
      Me._cacheValue = Value
      ' commented out for using resolution. MyBase.IsActual = Value.CompareTo(Me._actualValue) = 0
      MyBase.IsActual = Me.Equals(Value, Me._actualValue)
    End Set
  End Property

  ''' <summary>
  ''' Returns the default string representation of the Cached Value.
  ''' </summary>
  Public Overrides Function ToString() As String
    If Me.HasActualValue Then
      Return CachedComparable(Of T).ToString(Me._cacheValue, Me._actualValue)
    Else
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},N/A)", CacheValue)
    End If
  End Function

  ''' <summary>Returns the default string representation of the Cached Value.</summary>
  Private Overloads Shared Function ToString(ByVal cached As T, ByVal actual As T) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", cached, actual)
  End Function

#End Region

End Class

