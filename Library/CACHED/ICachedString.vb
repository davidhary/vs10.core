﻿''' <summary>
''' The contract implemented by Cached String Values.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Interface ICachedString

  Inherits ICachedElementBase
  Property ActualValue() As String
  Property CacheValue() As String
  Overloads Function Equals(ByVal value As ICachedString) As Boolean
  Function IsActualValue(ByVal value As String) As Boolean
  Function IsNewValue(ByVal value As String) As Boolean
  Property Setter() As Action(Of String)

End Interface

''' <summary>
''' Implements a generic <see cref="ICachedString">Cached Value</see>.
''' </summary>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Class CachedString

  Inherits CachedElementBase
  Implements ICachedString

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the parameter.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As String)
    MyBase.New(name)
    _cacheValue = cached
  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the parameter.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As String)

    MyBase.New()
    _cacheValue = cached

  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The  <see cref="ICachedString">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As ICachedString)
    Me.New(model.Name, model.CacheValue, model.ActualValue)
  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As String, ByVal actual As String)

    Me.New(name, cached)
    _actualValue = actual
    MyBase.IsActual = actual.Equals(cached)
    MyBase.HasActualValue = True

  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="ICachedString">cached string</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As ICachedString, ByVal right As ICachedString) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue)
  End Function

#End Region

#Region " DELEGATES "

  Private _setter As Action(Of String)
  ''' <summary>
  ''' Gets or sets the action for setting the actual value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Setter() As Action(Of String) Implements ICachedString.Setter
    Get
      Return _setter
    End Get
    Set(ByVal value As Action(Of String))
      _setter = value
    End Set
  End Property

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:CachedString"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CachedString"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Overrides Function Equals(ByVal value As Object) As Boolean
    Return value IsNot Nothing AndAlso (Object.ReferenceEquals(Me, value) OrElse CachedString.Equals(Me, TryCast(value, ICachedString)))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:CachedString"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CachedString"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>2</filterpriority> 
  Public Overloads Function Equals(ByVal value As ICachedString) As Boolean Implements ICachedString.Equals
    If value Is Nothing Then
      Throw New ArgumentNullException("value")
    End If
    Return Equals(Me, value)
  End Function

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return _cacheValue.GetHashCode Xor _actualValue.GetHashCode
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As String, ByVal right As  T ) As Boolean
    Return left.Equals(right)
  End Function
#End If

#End Region

#Region " READING "

  ''' <summary>
  ''' Returns the reading in the specified format.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Public Overrides ReadOnly Property Reading() As String
    Get
      If String.IsNullOrEmpty(_actualValue) Then
        Return ""
      Else
        Return _actualValue
      End If
    End Get
  End Property

#End Region

#Region " I CACHED STRUCTURE "

  ''' <summary>
  ''' Determines whether the supplied value is Actual.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value equals the <see cref="ActualValue">actual value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overridable Function IsActualValue(ByVal value As String) As Boolean Implements ICachedString.IsActualValue
    Return Me.Equals(value, Me.ActualValue)
  End Function

  ''' <summary>
  ''' Determines whether the supplied value is new.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value is not equal to the <see cref="CacheValue">cached value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overridable Function IsNewValue(ByVal value As String) As Boolean Implements ICachedString.IsNewValue
    Return Not Me.Equals(value, Me.CacheValue)
  End Function

  ''' <summary>
  ''' Apply changes and return true if a change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Apply() As Boolean
    If Not Me.IsActual Then
      Me.Setter.Invoke(Me.CacheValue)
      Return True
    End If
    Return False

  End Function

  Private _actualValue As String
  ''' <summary>
  ''' Gets or sets the actual value stored in the instrument.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable Property ActualValue() As String Implements ICachedString.ActualValue
    Get
      Return Me._actualValue
    End Get
    Set(ByVal Value As String)
      ' set fresh if we have an actual value that is different from the previous cached value 
      ' commented out for using resolution. MyBase.IsFresh = Not Value.Equals(Me._cacheValue)
      MyBase.IsFresh = Me.IsNewValue(Value)
      Me._actualValue = Value
      Me._cacheValue = Value
      MyBase.IsActual = True
      MyBase.HasActualValue = True
    End Set
  End Property

  Private _cacheValue As String
  ''' <summary>
  ''' Gets or sets the cached value of the Cached Value.
  ''' This value is updated whenever the actual value is set.
  ''' </summary>
  Public Overridable Property CacheValue() As String Implements ICachedString.CacheValue
    Get
      Return Me._cacheValue
    End Get
    Set(ByVal Value As String)
      Me._cacheValue = Value
      ' commented out for using resolution. MyBase.IsActual = Value.Equals(Me._actualValue)
      MyBase.IsActual = Me.IsActualValue(Value)
    End Set
  End Property

  ''' <summary>
  ''' Returns the default string representation of the Cached Value.
  ''' </summary>
  Public Overrides Function ToString() As String
    If Me.HasActualValue Then
      Return CachedString.ToString(Me._cacheValue, Me._actualValue)
    Else
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},N/A)", CacheValue)
    End If
  End Function

  ''' <summary>Returns the default string representation of the Cached Value.</summary>
  Private Overloads Shared Function ToString(ByVal cached As String, ByVal actual As String) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", cached, actual)
  End Function

#End Region

End Class

