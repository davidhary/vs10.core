﻿''' <summary>
''' The contract implemented by all Cached Values.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' The base cached value is typeless and forms a collection of based cached elements that can be used
''' to combile all element types in a single collections.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Interface ICachedElementBase

  Function Apply() As Boolean
  Function Equals(ByVal value As Object) As Boolean
  Function GetHashCode() As Integer
  Property Getter() As Action
  Property HasActualValue() As Boolean
  Property IsActual() As Boolean
  Property IsFresh() As Boolean
  Property Name() As String
  ReadOnly Property Reading() As String
  Property ReadingFormat() As String
  Function ToString() As String

End Interface

''' <summary>
''' Implements an abstruct <see cref="ICachedElementBase">Cached Value</see>
''' yo be used by typed cached values.
''' </summary>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public MustInherit Class CachedElementBase

  Implements ICachedElementBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CachedElementBase" /> class.
  ''' </summary>
  Protected Sub New()

    Me.New("")

  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CachedElementBase" /> class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Protected Sub New(ByVal name As String)

    MyBase.New()
    _name = name

  End Sub

#End Region

#Region " DELEGATES "

  Private _getter As Action
  ''' <summary>
  ''' Gets or sets the action for getting the actual value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Getter() As Action Implements ICachedElementBase.Getter
    Get
      Return _getter
    End Get
    Set(ByVal value As Action)
      _getter = value
    End Set
  End Property

#End Region

#Region " EQUALS "

  ''' <summary>Determines if the two specified <see cref="T:Object">Objects</see> 
  ''' have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns></returns>
  <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")> _
  Public Overloads Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
    Return left.Equals(right)
  End Function

  ''' <summary>Indicates whether the current <see cref="T:ICachedElementBase"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:ICachedElementBase"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public MustOverride Overloads Overrides Function Equals(ByVal value As Object) As Boolean Implements ICachedElementBase.Equals

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  MustOverride Overloads Overrides Function GetHashCode() As Int32 Implements ICachedElementBase.GetHashCode

#End Region

#Region " READING "

  Private _readingFormat As String = ""
  ''' <summary>
  ''' Gets or sets the reading format.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks>
  ''' The format specification differs between <see cref="IFormattable">Formattabe</see>
  ''' and non  <see cref="IFormattable">Formattabe</see> chached values.
  ''' Formattable values use the <see cref="ToString"></see> functuon
  ''' requiring values in the format 'xxx', e.g., '0.0#'.
  ''' Non Formattable values use the <see cref="String.Format"></see> functuon 
  ''' requiring values in the format {0:xxx}, e.g., {0:0.0#}.
  ''' </remarks>
  Public Property ReadingFormat() As String Implements ICachedElementBase.ReadingFormat
    Get
      Return _readingFormat
    End Get
    Set(ByVal value As String)
      _readingFormat = value
    End Set
  End Property

  ''' <summary>
  ''' Returns the reading in the specified format.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public MustOverride ReadOnly Property Reading() As String Implements ICachedElementBase.Reading

#End Region

#Region " I CACHED STRUCTURE "

  ''' <summary>
  ''' Apply changes and return true if a change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public MustOverride Function Apply() As Boolean Implements ICachedElementBase.Apply

  Private _hasActualValue As Boolean
  ''' <summary>
  ''' Gets or sets the condition indicating if the actual value was set.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property HasActualValue() As Boolean Implements ICachedElementBase.HasActualValue
    Get
      Return _hasActualValue
    End Get
    Set(ByVal value As Boolean)
      _hasActualValue = value
      If Not value Then
        Me.IsActual = False
      End If
    End Set
  End Property

  Private _isActual As Boolean
  ''' <summary>Returns true if the cached value equals the actual value.
  ''' </summary>
  ''' <value></value>
  Public Property IsActual() As Boolean Implements ICachedElementBase.IsActual
    Get
      Return _isActual
    End Get
    Protected Set(ByVal Value As Boolean)
      _isActual = Value
    End Set
  End Property

  Private _isFresh As Boolean
  ''' <summary>Returns true if the Cache Value is set to a new actual value.
  ''' </summary>
  ''' <value></value>
  Public Property IsFresh() As Boolean Implements ICachedElementBase.IsFresh
    Get
      Return _isFresh
    End Get
    Protected Set(ByVal Value As Boolean)
      _isFresh = Value
    End Set
  End Property

  Private _name As String
  ''' <summary>
  ''' Gets or sets the item name.
  ''' Facilitates detecting which item is not set.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Name() As String Implements ICachedElementBase.Name
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

  ''' <summary>
  ''' Returns the default string representation of the Cached Value.
  ''' </summary>
  MustOverride Overrides Function ToString() As String Implements ICachedElementBase.ToString

#End Region

End Class

''' <summary>
''' Implements a list of <see cref="ICachedElementBase">cached values</see>.
''' Detects if any item on the list changed.
''' </summary>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Class CachedElementCollection
  Inherits Collections.Generic.List(Of ICachedElementBase)

  ''' <summary>
  ''' Return true if all values are set to their actual instrument value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function AllActual() As Boolean

    For Each Item As ICachedElementBase In MyBase.ToArray()
      If Not Item.IsActual Then
        Return False
      End If
    Next
    Return True
  End Function

  ''' <summary>
  ''' Apply all changes and return true if any change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Apply() As Boolean

    Dim anyChangeApplied As Boolean
    For Each Item As ICachedElementBase In MyBase.ToArray()
      If Item.Apply() Then
        anyChangeApplied = True
      End If
    Next
    Return anyChangeApplied

  End Function

  ''' <summary>
  ''' Return the list of names of items which values are not actual.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NonActual() As String

    Dim value As New System.Text.StringBuilder
    Dim itemIndex As Integer = 0
    For Each Item As ICachedElementBase In MyBase.ToArray()
      If Not Item.IsActual Then
        If value.Length > 0 Then
          value.Append(",")
        End If
        If String.IsNullOrEmpty(Item.Name) Then
          value.AppendFormat("Item{0}", itemIndex)
        Else
          value.Append(Item.Name)
        End If
      End If
      itemIndex += 1
    Next
    Return value.ToString
  End Function

  ''' <summary>
  ''' Get new actual values and return true if any actual value was refreshed.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Refresh() As Boolean

    Dim anyItemRefreshed As Boolean
    For Each Item As ICachedElementBase In MyBase.ToArray()
      Item.Getter.Invoke()
      anyItemRefreshed = True
    Next
    Return anyItemRefreshed

  End Function

End Class

''' <summary>
''' The contract implemented by presettable and resettable cached values.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' The cached value can be preset to its preset value or to its reset value.
''' In both cases, it is assumed that these are also the actual values.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Interface IPresettableCachedElementBase

  Inherits ICachedElementBase, IPresettable, IResettable, IClearable

End Interface

''' <summary>
''' Implements a list of <see cref="IPresettableCache">cached comparable values</see>.
''' Detects if any item on the list changed.
''' </summary>
''' <remarks></remarks>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Based on <see cref="ICachedElementBase">Cachaded Structure</see>
''' Created
''' </history>
Public Class PresettableCachedCollection
  Inherits Collections.Generic.List(Of IPresettableCachedElementBase)
  Implements IClearable, IPresettable, IResettable

  ''' <summary>
  ''' Return true if all values are set to their actual instrument value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function AllActual() As Boolean

    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      If Not Item.IsActual Then
        Return False
      End If
    Next
    Return True
  End Function

  ''' <summary>
  ''' Apply all changes and return true if any change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Apply() As Boolean

    Dim anyChangeApplied As Boolean
    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      If Item.Apply() Then
        anyChangeApplied = True
      End If
    Next
    Return anyChangeApplied

  End Function

  ''' <summary>
  ''' Return the list of names of items which values are not actual.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NonActual() As String

    Dim value As New System.Text.StringBuilder
    Dim itemIndex As Integer = 0
    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      If Not Item.IsActual Then
        If value.Length > 0 Then
          value.Append(",")
        End If
        If String.IsNullOrEmpty(Item.Name) Then
          value.AppendFormat("Item{0}", itemIndex)
        Else
          value.Append(Item.Name)
        End If
      End If
      itemIndex += 1
    Next
    Return value.ToString
  End Function

  ''' <summary>
  ''' Get new actual values and return true if any actual value was refreshed.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Refresh() As Boolean

    Dim anyItemRefreshed As Boolean
    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      Item.Getter.Invoke()
      anyItemRefreshed = True
    Next
    Return anyItemRefreshed

  End Function

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets all values to the clear value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ClearValues() As Boolean Implements IClearable.Clear
    Dim anyItemCleared As Boolean
    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      Item.Clear()
      anyItemCleared = True
    Next
    Return anyItemCleared
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Resets all to the preset values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    Dim anyItemPreset As Boolean
    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      Item.Preset()
      anyItemPreset = True
    Next
    Return anyItemPreset
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Resets all to the default values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    Dim anyItemReset As Boolean
    For Each Item As IPresettableCachedElementBase In MyBase.ToArray()
      Item.Reset()
      anyItemReset = True
    Next
    Return anyItemReset
  End Function

#End Region

End Class

