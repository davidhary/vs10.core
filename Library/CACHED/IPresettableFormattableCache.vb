﻿''' <summary>
''' The contract implemented by Presettable, Formattable and Resettable cached value.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' The cached value can be preset to its preset value or to its reset value.
''' In both cases, it is assumed that these are also the actual values.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Interface IPresettableFormattableCache(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits IPresettableCachedElementBase, ICachedStructure(Of T), IPresettable(Of T), IResettable(Of T), IClearable(Of T), IApproximateable

End Interface

''' <summary>
''' Implements a generic <see cref="IPresettableFormattableCache(Of T)">Cached Value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Class PresettableFormattableCache(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits CachedFormattable(Of T)
  Implements IPresettableFormattableCache(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The  <see cref="PresettableFormattableCache">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As PresettableFormattableCache(Of T))

    Me.New(model.Name, model.CacheValue, model.ClearValue, model.PresetValue, model.DefaultValue, model.ActualValue)

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' Sets the default, clear and preset values to the cached value.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T)

    MyBase.New(name, cached)
    _clearValue = cached

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T, ByVal defaultValue As T, ByVal preset As T, ByVal cleared As T)

    MyBase.New(name, cached)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T, ByVal defaultValue As T, ByVal preset As T, ByVal cleared As T)

    MyBase.New(cached)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T, ByVal defaultValue As T, ByVal preset As T, ByVal cleared As T, ByVal actual As T)

    MyBase.New(cached, actual)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <param name="cleared">The value to which the instance values are set upon <see cref="Clear"/>.</param>
  ''' <param name="preset">The value to which the instance values are set upon <see cref="Preset"/>.</param>
  ''' <param name="defaultValue">The value to which the instance values are set upon <see cref="Reset"/>.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T, ByVal defaultValue As T?, ByVal preset As T?, ByVal cleared As T?, ByVal actual As T)

    MyBase.New(name, cached, actual)
    _clearValue = cleared
    _presetValue = preset
    _defaultValue = defaultValue

  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="PresettableFormattableCache(Of T)">Presettable cache</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see>, <see cref="ActualValue">actual</see>, 
  ''' <see cref="DefaultValue">default</see>, <see cref="ClearValue">clear</see> and <see cref="PresetValue">preset</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As PresettableFormattableCache(Of T), ByVal right As PresettableFormattableCache(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue) _
           AndAlso left.DefaultValue.Equals(right.DefaultValue) _
           AndAlso left.ClearValue.Equals(right.ClearValue) _
           AndAlso left.PresetValue.Equals(right.PresetValue)
  End Function

#End Region

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="ClearValue">clear value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Clear() As Boolean Implements IClearable.Clear
    If Me._clearValue.HasValue Then
      MyBase.ActualValue = Me._clearValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="PresetValue">preset value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    If Me._presetValue.HasValue Then
      MyBase.ActualValue = Me._presetValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="DefaultValue">default value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    If Me._defaultValue.HasValue Then
      MyBase.ActualValue = Me._defaultValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " I PRESETTABLE FORMATTABLE CACHE "

  Private _clearValue As T?
  Public Property ClearValue() As T? Implements IPresettableFormattableCache(Of T).ClearValue
    Get
      Return _clearValue
    End Get
    Set(ByVal value As T?)
      _clearValue = value
    End Set
  End Property

  Private _presetValue As T?
  Public Property PresetValue() As T? Implements IPresettableFormattableCache(Of T).PresetValue
    Get
      Return _presetValue
    End Get
    Set(ByVal value As T?)
      _presetValue = value
    End Set
  End Property

  Private _defaultValue As T?
  Public Property DefaultValue() As T? Implements IPresettableFormattableCache(Of T).DefaultValue
    Get
      Return _defaultValue
    End Get
    Set(ByVal value As T?)
      _defaultValue = value
    End Set
  End Property

#End Region

End Class

