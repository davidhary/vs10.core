﻿''' <summary>
''' The contract implemented by <see cref="IFormattable">formattable</see> and <see cref="IApproximateable">Approximateable</see> Cached Values.
''' A Cached Value has cached and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter cached and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Interface ICachedFormattable(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits ICachedStructure(Of T), IApproximateable
  Overloads Function Equals(ByVal value As ICachedFormattable(Of T)) As Boolean

End Interface

''' <summary>
''' Implements a generic <see cref="ICachedFormattable(Of T)">Cached Value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
Public Class CachedFormattable(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits CachedStructure(Of T)
  Implements ICachedFormattable(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <param name="cached">Specifies the cached value of the parameter.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T)
    MyBase.New(name, cached)
  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the parameter.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T)
    MyBase.New(cached)
  End Sub

  ''' <summary>The Copy Constructor</summary>
  ''' <param name="model">The  <see cref="CachedFormattable">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As CachedFormattable(Of T))
    Me.New(model.Name, model.CacheValue, model.ActualValue)
  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal cached As T, ByVal actual As T)
    MyBase.New(cached, actual)
  End Sub

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="cached">Specifies the cached value of the Cached Value.</param>
  ''' <param name="actual">Specifies the actual value of the Cached Value.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal cached As T, ByVal actual As T)
    MyBase.New(name, cached, actual)
  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="CachedFormattable(Of T)">formattable cached</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As CachedFormattable(Of T), ByVal right As CachedFormattable(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If

    Return left.Equals(left.CacheValue, right.CacheValue) _
           AndAlso left.Equals(left.ActualValue, right.ActualValue)
#If False Then
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue)
#End If

  End Function

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:CachedFormattable"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CachedFormattable"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Overrides Function Equals(ByVal value As Object) As Boolean
    Return value IsNot Nothing AndAlso (Object.ReferenceEquals(Me, value) OrElse CachedFormattable(Of T).Equals(Me, TryCast(value, CachedFormattable(Of T))))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:ICachedFormattable"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:ICachedFormattable"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Function Equals(ByVal value As ICachedFormattable(Of T)) As Boolean Implements ICachedFormattable(Of T).Equals
    If value Is Nothing Then
      Throw New ArgumentNullException("value")
    End If
    Return Me.Equals(Me, value)
  End Function

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return MyBase.GetHashCode
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As T, ByVal right As  T ) As Boolean
    Return left.Equals(right)
  End Function
#End If

#End Region

#Region " I CACHED STRUCTURE "

  ''' <summary>
  ''' Determines whether the supplied value is Actual.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value equals the <see cref="ActualValue">actual value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overrides Function IsActualValue(ByVal value As T) As Boolean
    Return Me.Equals(value, Me.ActualValue)
  End Function

  ''' <summary>
  ''' Determines whether the supplied value is new.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value is not equal to the <see cref="CacheValue">cached value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overrides Function IsNewValue(ByVal value As T) As Boolean
    Return Not Me.Equals(value, Me.CacheValue)
  End Function

#End Region

#Region " I APPROXIMATEABLE "

  Private _Epsilon As Object
  ''' <summary>
  ''' Gets the minimum noticable difference between the values.
  ''' This is the tolerance ot resulution of the values.
  ''' </summary>
  ''' <value>
  ''' The epsilon.
  ''' </value>
  Public ReadOnly Property Epsilon() As Object Implements IApproximateable.Epsilon
    Get
      Return _Epsilon
    End Get
  End Property

  Public Sub EpsilonSetter(ByVal value As T)
    _Epsilon = value
  End Sub

  ''' <summary>Determines if the two specified <see cref="T:Double">Double</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Shared Function Approximates(ByVal left As Double, ByVal right As Double, ByVal epsilon As Double) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Double">Double</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Double, ByVal right As Double) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Double))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Single">Single</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Single, ByVal right As Single, ByVal epsilon As Single) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Single">Single</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Single, ByVal right As Single) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Single))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Decimal">Decimal</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Decimal, ByVal right As Decimal) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Decimal))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Decimal">Decimal</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Decimal, ByVal right As Decimal, ByVal epsilon As Decimal) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:integer">integer</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Integer, ByVal right As Integer) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Integer))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:integer">integer</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Integer, ByVal right As Integer, ByVal epsilon As Integer) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

#End Region

End Class

