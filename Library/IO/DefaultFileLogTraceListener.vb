﻿Imports Microsoft.VisualBasic.Logging
Imports System.Globalization
Imports System.Security.Permissions

''' <summary>
''' Replaces the standard event log.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="10/30/08" by="David Hary" revision="1.00.3225.x">
''' Created
''' </history>
Public Class DefaultFileLogTraceListener

  Inherits Microsoft.VisualBasic.Logging.FileLogTraceListener

  ''' <summary>
  ''' Gets the default listener name.
  ''' </summary>
  ''' <remarks></remarks>
  Public Const DefaultTraceListenerName As String = "FileLog"

  ''' <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.TraceListener"></see> class using the specified name as the listener.</summary>
  ''' <param name="designMode">True if running in design mode</param>
  ''' <param name="allUsers">True if using all users folder under vista.</param>
  Public Sub New(ByVal designMode As Boolean, ByVal allUsers As Boolean)

    Me.New(DefaultTraceListenerName, DefaultTraceListenerFolderPathName(designMode, allUsers))

  End Sub

  ''' <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.TraceListener"></see> class using the specified name as the listener.</summary>
  ''' <param name="name">The name of the <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
  Public Sub New(ByVal name As String, ByVal folderName As String)
    MyBase.New(name)

    ' unused  Debug.Assert(designModeSetter())

    ' set the default properties of the log file name 
    Me.LogFileCreationSchedule = LogFileCreationScheduleOption.Daily
    Me.Location = LogFileLocation.Custom
    Me.Delimiter = ", "
    Me.CustomLocation = folderName
    Me.IncludeHostName = False
    Me.AutoFlush = True
    Me.TraceOutputOptions = TraceOptions.DateTime Or TraceOptions.LogicalOperationStack

    ' note that the trace event cache time format is not set corectly to -8 hours offset.
    ' Rather, it gives us a -4 hours offset.

  End Sub

  Private _usingUniversalTime As Boolean
  ''' <summary>
  ''' Gets or sets the condition for using universal time.  When using usinversal time, the 
  ''' format is autmatically modified to add a 'T' suffix if not set.
  ''' Defaults to True.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property UsingUniversalTime() As Boolean
    Get
      Return _usingUniversalTime
    End Get
    Set(ByVal value As Boolean)
      _usingUniversalTime = value
      If value AndAlso Not _timeFormat.EndsWith("T", StringComparison.OrdinalIgnoreCase) Then
        _timeFormat &= "T"
      End If
    End Set
  End Property

  Private _timeFormat As String = "HH:mm:ss:fff"
  ''' <summary>
  ''' Gets or sets the time format.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property TimeFormat() As String
    Get
      Return _timeFormat
    End Get
    Set(ByVal value As String)
      _timeFormat = value
    End Set
  End Property

  ''' <summary>Writes trace information, a message and event information to the output file or stream.
  ''' </summary>
  ''' <param name="source">A name of the trace source that invoked this method. </param>
  ''' <param name="message">A message to write.</param>
  ''' <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that contains the current process ID, thread ID, and stack trace information.</param>
  ''' <param name="eventType">One of the <see cref="T:System.Diagnostics.TraceEventType"></see> enumeration values.</param>
  ''' <param name="id">A numeric identifier for the event.</param>
  ''' <filterpriority>1</filterpriority>
  ''' <history>
  ''' Use higher resolution time.
  ''' Remote Date assuming using one file per day.
  ''' Move fixed information up front.
  ''' Include source with host.
  ''' </history>
  ''' <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" /></PermissionSet>
  <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)> _
  Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType, _
                                  ByVal id As Integer, ByVal message As String)

    If Not String.IsNullOrEmpty(message) And ((Me.Filter Is Nothing) OrElse Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing)) Then

      If message.Contains(Environment.NewLine) Then
        For Each line As String In message.Split(Environment.NewLine.ToCharArray)
          TraceEvent(eventCache, source, eventType, id, line.Trim)
        Next
        Return
      End If
      Dim builder As New System.Text.StringBuilder

      builder.Append(eventType.ToString.Substring(0, 2) & Me.Delimiter)

      builder.Append((id.ToString(CultureInfo.InvariantCulture) & Me.Delimiter))

      ' builder.Append(([Enum].GetName(GetType(TraceEventType), eventType) & Me.Delimiter))
      If Me.IncludeHostName Then
        builder.Append(Me.HostName & "." & source & Me.Delimiter)
      End If

      If ((Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime) Then
        Dim logTime As DateTime = eventCache.DateTime
        If Not _usingUniversalTime Then
          logTime = logTime.Add(DateTimeOffset.Now.Offset)
        End If
        builder.Append(logTime.ToString(_timeFormat, CultureInfo.InvariantCulture) & Me.Delimiter)
        ' builder.Append((Me.Delimiter & eventCache.DateTime.ToString("u", CultureInfo.InvariantCulture)))
      End If

      If ((Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId) Then
        builder.Append(eventCache.ProcessId.ToString(CultureInfo.InvariantCulture) & Me.Delimiter)
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId) Then
        builder.Append(eventCache.ThreadId & Me.Delimiter)
      End If
      If ((Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp) Then
        builder.Append(eventCache.Timestamp.ToString(CultureInfo.InvariantCulture) & Me.Delimiter)
      End If

      builder.Append(message)

      If ((Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack) Then
        builder.Append((Me.Delimiter & StackToString(eventCache.LogicalOperationStack)))
      End If

      If ((Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack) Then
        builder.Append((Me.Delimiter & eventCache.Callstack))
      End If

      Me.WriteLine(builder.ToString)

    End If
  End Sub

  Private m_HostName As String
  Private ReadOnly Property HostName() As String
    Get
      If (Me.m_HostName = "") Then
        Me.m_HostName = Environment.MachineName
      End If
      Return Me.m_HostName
    End Get
  End Property

  <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")> _
  Private Shared Function StackToString(ByVal stack As Stack) As String
    Dim enumerator As IEnumerator = Nothing
    Dim length As Integer = ", ".Length
    Dim builder As New System.Text.StringBuilder
    Try
      enumerator = stack.GetEnumerator
      Do While enumerator.MoveNext
        builder.Append((enumerator.Current.ToString & ", "))
      Loop
    Finally
      If TypeOf enumerator Is IDisposable Then
        TryCast(enumerator, IDisposable).Dispose()
      End If
    End Try
    builder.Replace("""", """""")
    If (builder.Length >= length) Then
      builder.Remove((builder.Length - length), length)
    End If
    Return ("""" & builder.ToString & """")
  End Function

  ''' <summary>
  ''' Returns the default folder name for tracing.
  ''' </summary>
  ''' <param name="designMode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function DefaultTraceListenerFolderPathName(ByVal designMode As Boolean, ByVal allUsers As Boolean) As String
    Const defaultFolderTitle As String = "logs"
    If designMode OrElse (Environment.OSVersion.Version.Major <= 5) Then
      Return System.IO.Path.Combine(My.Application.Info.DirectoryPath, "..\" & defaultFolderTitle)
    ElseIf allUsers Then
      Return System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, defaultFolderTitle)
    Else
      Return System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, defaultFolderTitle)
    End If
  End Function

End Class

#Region " ORIGINAL WRITE "

#If False Then

  ''' <summary>Writes trace information, a message and event information to the output file or stream.</summary>
  ''' <param name="source">A name of the trace source that invoked this method. </param>
  ''' <param name="message">A message to write.</param>
  ''' <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that contains the current process ID, thread ID, and stack trace information.</param>
  ''' <param name="eventType">One of the <see cref="T:System.Diagnostics.TraceEventType"></see> enumeration values.</param>
  ''' <param name="id">A numeric identifier for the event.</param>
  ''' <filterpriority>1</filterpriority>
  ''' <PermissionSet><IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" /><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" /></PermissionSet>
  <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)> _
  Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType, ByVal id As Integer, ByVal message As String)
    If ((Me.Filter Is Nothing) OrElse Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing)) Then
      Dim builder As New StringBuilder
      builder.Append((source & Me.Delimiter))
      builder.Append(([Enum].GetName(GetType(TraceEventType), eventType) & Me.Delimiter))
      builder.Append((id.ToString(CultureInfo.InvariantCulture) & Me.Delimiter))
      builder.Append(message)
      If ((Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack) Then
        builder.Append((Me.Delimiter & eventCache.Callstack))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack) Then
        builder.Append((Me.Delimiter & StackToString(eventCache.LogicalOperationStack)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime) Then
        builder.Append((Me.Delimiter & eventCache.DateTime.ToString("o", CultureInfo.InvariantCulture)))
        ' builder.Append((Me.Delimiter & eventCache.DateTime.ToString("u", CultureInfo.InvariantCulture)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId) Then
        builder.Append((Me.Delimiter & eventCache.ProcessId.ToString(CultureInfo.InvariantCulture)))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId) Then
        builder.Append((Me.Delimiter & eventCache.ThreadId))
      End If
      If ((Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp) Then
        builder.Append((Me.Delimiter & eventCache.Timestamp.ToString(CultureInfo.InvariantCulture)))
      End If
      If Me.IncludeHostName Then
        builder.Append((Me.Delimiter & Me.HostName))
      End If
      Me.WriteLine(builder.ToString)
    End If
  End Sub


#End If
#End Region

