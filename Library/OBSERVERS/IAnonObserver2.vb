﻿''' <summary>
''' The contract implemented by the Anonymous Observers.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
Public Interface IAnonObserver2

  Inherits IObserver

  ''' <summary>
  ''' Gets reference to the anon publisher.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property IPublisher() As isr.Core.IAnonPublisher2

#Region " SAMPLE CODE "

#If False Then

  ''' <summary>
  ''' Notifies of changes in 
  ''' <see cref="TesterInfoPublisher.BroadcastLevel">broadcast level.</see>
  ''' </summary>
  ''' <remarks></remarks>
  Event BroadcastLevelChanged As EventHandler(Of System.EventArgs)

  ''' <summary>
  ''' Applies changes in 
  ''' <see cref="TesterInfoPublisher.BroadcastLevel">broadcast level.</see>.
  ''' </summary>
  ''' <remarks></remarks>
  Sub OnBroadcastLevelChanged()

#End If

#End Region

End Interface
