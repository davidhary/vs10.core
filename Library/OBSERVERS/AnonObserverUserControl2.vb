﻿Imports System.ComponentModel
''' <summary>
''' Base observer implementing the <see cref="IAnonObserver2">Anonymous Observer</see> contract.
''' </summary>
''' <remarks>
''' A control class must not be abstract (Must Inherit) otherwise, the designer will not launch.
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
<Drawing.ToolboxBitmap(GetType(AnonObserverUserControl2), "Resources.AnonObserverUserControl2")> _
Public Class AnonObserverUserControl2
    Inherits System.Windows.Forms.UserControl
    Implements IAnonObserver2

#Region " CONSTRUCTORS  and  DESTRUCTORS "

#If False Then
  <System.ComponentModel.Description("Anon Observer User Control 2"), System.Drawing.ToolboxBitmap(GetType(AnonObserverUserControl2))> 
#End If

    Protected Sub New(ByVal publisher As IAnonPublisher2)
        MyBase.New()
        _IPublisher = publisher
    End Sub

#End Region

#Region " I NOTIFY PROPERTY CHANGED "

    ''' <summary>
    ''' Raised whenever a property is changed.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary>
    ''' Raised to notify the binadable object of a change.
    ''' </summary>
    ''' <param name="name"></param>
    ''' <remarks></remarks>
    Protected Overridable Sub OnPropertyChanged(ByVal name As String)
        EventHandlerExtensions.SafeInvoke(PropertyChangedEvent, Me, New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary>
    ''' Raised to notify the binadable object of a change.
    ''' </summary>
    ''' <param name="name"></param>
    ''' <remarks>Strips the "set_" prefix derived from using reflection to
    ''' get the current function name from a property set construct.</remarks>
    Protected Overridable Sub OnSetPropertyChanged(ByVal name As String)
        Me.OnPropertyChanged(name.TrimStart("set_".ToCharArray()))
    End Sub

#End Region

#Region " I ANON OBSERVER "

    Private _isObserved As Boolean
    ''' <summary>Gets or sets true if the control is visible to the user.
    ''' A table is updated only if the control is visible as well as the
    ''' tab holding the table.</summary>
    <System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), _
     System.ComponentModel.Browsable(False)> _
    Public Overridable Property IsObserved() As Boolean Implements IAnonObserver2.IsObserved
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")> _
        Get
            Return _isObserved
        End Get
        Set(ByVal Value As Boolean)
            _isObserved = Value
        End Set
    End Property

    Public Overridable Sub OnObserved() Implements IAnonObserver2.OnObserved
        Me.Invalidate()
    End Sub

#End Region

#Region " SAMPLE CODE "

#If False Then

  Public Event BroadcastLevelChanged(ByVal sender As Object, ByVal e As System.EventArgs) Implements IAnonObserver2.BroadcastLevelChanged

  Protected Overridable Sub OnBroadcastLevelChanged() Implements IAnonObserver2.OnBroadcastLevelChanged
  End Sub

  Private Sub _IPublisher_BroadcastLevelChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _IPublisher.BroadcastLevelChanged
    OnBroadcastLevelChanged()
    if  BroadcastLevelChangedevent isnot nothing then  BroadcastLevelChangedevent(Me, e)
  End Sub

#End If

#End Region

#Region " PUBISHER "

    Private WithEvents _IPublisher As IAnonPublisher2
    ''' <summary>
    ''' Gets reference to the tester information publisher.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property IPublisher() As IAnonPublisher2 Implements IAnonObserver2.IPublisher
        Get
            Return _IPublisher
        End Get
    End Property

#End Region

#Region " IRESETTABLE  "

    ''' <summary>
    ''' Returns settings to a known state.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Function ResetKnownState() As Boolean Implements IAnonObserver2.Reset
    End Function

#End Region

End Class
