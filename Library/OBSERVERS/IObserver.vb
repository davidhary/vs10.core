﻿''' <summary>
''' The contract implemented by Observers.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/08/2010" by="David Hary" revision="1.2.3691.x">
''' Created
''' </history>
Public Interface IObserver

  Inherits System.ComponentModel.INotifyPropertyChanged, isr.Core.IResettable

#Region " DISPLAY MANAGEMENT "

  ''' <summary>Gets or sets true if the control is visible to the user.
  ''' A table is updated only if the control is visible as well as the
  ''' tab holding the table.</summary>
  Property IsObserved() As Boolean

  ''' <summary>
  ''' Refreshes the the display panel.
  ''' </summary>
  ''' <remarks></remarks>
  Sub OnObserved()

#End Region


End Interface
