﻿''' <summary>
''' Defines a boolean formatted element.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/18/2011" by="David Hary" revision="1.2.4066.x">
''' Created
''' </history>
Public Structure BooleanFormatterParser
  Implements IFormatterParser(Of Boolean)

  ''' <summary>
  ''' Initializes a new instance of the <see cref="BooleanFormatterParser" /> struct.
  ''' </summary>
  ''' <param name="trueValue">The true value.</param>
  ''' <param name="falseValue">The false value.</param>
  Public Sub New(ByVal trueValue As String, ByVal falseValue As String)
    _trueValue = trueValue
    _falseValue = falseValue
  End Sub

#Region " EQUALS "

  ''' <summary>Determines if the two specified <see cref="BooleanFormatterParser">boolean element</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="TrueValue">true</see> and <see cref="falsevalue">false</see> values.
  ''' </remarks>
  Public Overloads Shared Function Equals(ByVal left As BooleanFormatterParser, ByVal right As BooleanFormatterParser) As Boolean
    Return left.TrueValue.Equals(right.TrueValue) _
           AndAlso left.FalseValue.Equals(right.FalseValue)
  End Function

  ''' <summary>Indicates whether the current <see cref="T:BooleanElement"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:BooleanElement"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="obj">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overrides Function Equals(ByVal obj As Object) As Boolean
    Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse _
                                      (Me.GetType() Is obj.GetType AndAlso Me.Equals(CType(obj, BooleanFormatterParser))))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:BooleanElement"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:BooleanElement"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">A value.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Function Equals(ByVal value As BooleanFormatterParser) As Boolean
    Return Object.ReferenceEquals(Me, value) OrElse BooleanFormatterParser.Equals(Me, value)
  End Function

  ''' <summary>Returns True if the specified <see cref="T:BooleanElement"></see> value
  ''' equals the <paramref name="left">value</paramref>.</summary>
  ''' <returns>.</returns>
  ''' <param name="left">A <see cref="T:BooleanElement"></see> value.</param>
  ''' <param name="right">A <see cref="T:BooleanElement"></see> value.</param>
  Public Overloads Shared Operator =(ByVal left As BooleanFormatterParser, ByVal right As BooleanFormatterParser) As Boolean
    Return right.Equals(left)
  End Operator

  ''' <summary>Returns True if the specified <see cref="T:BooleanElement"></see> value
  ''' is not equal the <paramref name="left">value</paramref>.</summary>
  ''' <returns>.</returns>
  ''' <param name="left">A <see cref="T:BooleanElement"></see> value.</param>
  ''' <param name="right">A <see cref="T:BooleanElement"></see> value.</param>
  Public Overloads Shared Operator <>(ByVal left As BooleanFormatterParser, ByVal right As BooleanFormatterParser) As Boolean
    Return Not right.Equals(left)
  End Operator

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overrides Function GetHashCode() As Integer
    Dim hashCode As Integer
    If Not String.IsNullOrEmpty(_trueValue) Then
      hashCode = hashCode Xor _trueValue.GetHashCode
    End If
    If Not String.IsNullOrEmpty(_falseValue) Then
      hashCode = hashCode Xor _falseValue.GetHashCode
    End If
    Return hashCode
  End Function

#End Region

  Private _trueValue As String
  ''' <summary>
  ''' Gets the <c>true</c> value.
  ''' </summary>
  ''' 
  Public ReadOnly Property TrueValue() As String
    Get
      Return _trueValue
    End Get
  End Property

  Private _falseValue As String
  ''' <summary>
  ''' Gets the <c>false</c> value.
  ''' </summary>
  ''' 
  Public ReadOnly Property FalseValue() As String
    Get
      Return _falseValue
    End Get
  End Property

  ''' <summary>
  ''' Returns the text representation of the boolean value
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Text(ByVal value As Boolean) As String Implements IFormatter(Of Boolean).Text
    If value Then
      Return _trueValue
    Else
      Return _falseValue
    End If
  End Function

  ''' <summary>
  ''' Parses the specified value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the value equals the <see cref="TrueValue">True Value</see>, 
  ''' <c>false</c> if the value equals the <see cref="FalseValue">False Value</see>; otherwise
  ''' null.
  ''' </returns>
  Public Function Parse(ByVal value As String) As Boolean? Implements IParser(Of Boolean).Parse
    If value.Equals(_trueValue, StringComparison.OrdinalIgnoreCase) Then
      Return True
    ElseIf value.Equals(_falseValue, StringComparison.OrdinalIgnoreCase) Then
      Return False
    Else
      Return New Boolean?
    End If
  End Function

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the value can be parsed.
  ''' </returns>
  Public Function TryParse(ByVal value As String) As Boolean Implements IParser(Of Boolean).TryParse
    If value.Equals(_trueValue, StringComparison.OrdinalIgnoreCase) Then
      Return True
    ElseIf value.Equals(_falseValue, StringComparison.OrdinalIgnoreCase) Then
      Return True
    Else
      Return False
    End If
  End Function

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <param name="result">is set to <c>true</c> value equals the <see cref="TrueValue">True Value</see>, 
  ''' <c>false</c> if the value equals the <see cref="FalseValue">False Value</see>; otherwise
  ''' null.</param>
  ''' <returns>
  ''' <c>true</c> if the value was parsed.
  ''' </returns>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#", Justification:="This is the standard call format for this method in Visual Studio")> _
  Public Function TryParse(ByVal value As String, ByRef result As Boolean) As Boolean Implements IParser(Of Boolean).TryParse
    If value.Equals(_trueValue, StringComparison.OrdinalIgnoreCase) Then
      result = True
      Return True
    ElseIf value.Equals(_falseValue, StringComparison.OrdinalIgnoreCase) Then
      result = False
      Return True
    Else
      Return False
    End If
  End Function

End Structure

