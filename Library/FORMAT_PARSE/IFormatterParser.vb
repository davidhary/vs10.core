﻿''' <summary>
''' Defines the contract that must be implemented by booolean formatters.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/18/2011" by="David Hary" revision="1.2.4066.x">
''' Created
''' </history>
Public Interface IFormatter(Of T As {Structure})

  ''' <summary>
  ''' Returns the text representation of the value
  ''' </summary>
  ''' <param name="value">The value.</param><returns></returns>
  Function Text(ByVal value As T) As String

End Interface

''' <summary>
''' Defines the contract that must be implemented by booolean parsers.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/18/2011" by="David Hary" revision="1.2.4066.x">
''' Created
''' </history>
Public Interface IParser(Of T As {Structure})

  ''' <summary>
  ''' Parses the specified value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>The parsed value</returns>
  Function Parse(ByVal value As String) As T?

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the value can be parsed.
  ''' </returns>
  Function TryParse(ByVal value As String) As Boolean

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <param name="result">is set to the parsed value.</param>
  ''' <returns>
  ''' <c>true</c> if the value was parsed.
  ''' </returns>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#", Justification:="This is the standard call format for this method in Visual Studio")> _
  Function TryParse(ByVal value As String, ByRef result As T) As Boolean

End Interface

''' <summary>
''' Defines the contract that must be implemented by booolean formated and parsers.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/18/2011" by="David Hary" revision="1.2.4066.x">
''' Created
''' </history>
Public Interface IFormatterParser(Of T As {Structure})
  Inherits IFormatter(Of T), IParser(Of T)
End Interface

