﻿''' <summary>
''' Defines an enumerated list of <see cref="BooleanFormatterParser">boolean formatted element</see>.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/18/2011" by="David Hary" revision="1.2.4066.x">
''' Created
''' </history>
Public Class EnumeratedBooleanBase
  Implements IFormatterParser(Of Boolean)

  ''' <summary>
  ''' Initializes a new instance of the <see cref="EnumeratedBooleanBase" /> class.
  ''' This class has an empty <see cref="Elements">list</see> of <see cref="BooleanFormatterParser">boolean</see>
  ''' elements.
  ''' </summary>
  Public Sub New()
    MyBase.New()
    _elements = New List(Of BooleanFormatterParser)
  End Sub

  Private Shared _OneZero As New BooleanFormatterParser("1", "0")
  ''' <summary>
  ''' Gets the one/zero element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property OneZero() As BooleanFormatterParser
    Get
      Return _OneZero
    End Get
  End Property

  Private Shared _OnOff As New BooleanFormatterParser("ON", "OFF")
  ''' <summary>
  ''' Gets the on off element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property OnOff() As BooleanFormatterParser
    Get
      Return _OnOff
    End Get
  End Property

  Private Shared _PF As New BooleanFormatterParser("P", "F")
  ''' <summary>
  ''' Gets the pass/fail element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property PF() As BooleanFormatterParser
    Get
      Return _PF
    End Get
  End Property

  Private Shared _PassFail As New BooleanFormatterParser("PASS", "FAIL")
  ''' <summary>
  ''' Gets the pass/fail element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property PassFail() As BooleanFormatterParser
    Get
      Return _PassFail
    End Get
  End Property

  Private Shared _TF As New BooleanFormatterParser("T", "F")
  ''' <summary>
  ''' Gets the T/F element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property TF() As BooleanFormatterParser
    Get
      Return _TF
    End Get
  End Property

  Private Shared _trueFalse As New BooleanFormatterParser("TRUE", "FALSE")
  ''' <summary>
  ''' Gets the true/false element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property TrueFalse() As BooleanFormatterParser
    Get
      Return _trueFalse
    End Get
  End Property

  Private Shared _YN As New BooleanFormatterParser("Y", "N")
  ''' <summary>
  ''' Gets the y/n element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property YN() As BooleanFormatterParser
    Get
      Return _YN
    End Get
  End Property

  Private Shared _YesNo As New BooleanFormatterParser("YES", "NO")
  ''' <summary>
  ''' Gets the yes/no element.
  ''' </summary>
  ''' 
  Public Shared ReadOnly Property YesNo() As BooleanFormatterParser
    Get
      Return _YesNo
    End Get
  End Property

  ''' <summary>
  ''' Adds the specified value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the element was added or false if the element true or faluse values already exist.
  ''' </returns>
  ''' <remarks>
  ''' The element is added unless its true or false values conflict with existing element. 
  ''' Namely, its true value is already defined as a false value in another boolan element
  ''' or its false value is already defined as a true value in another boolan element
  ''' </remarks>
  Private Function _add(ByVal value As BooleanFormatterParser) As Boolean
    Dim result As Boolean
    If _elements.Contains(value) Then
      Return True
    ElseIf Me.TryParse(value.TrueValue, result) AndAlso result = False Then
      ' ignore if the true value is already is a false value in anotehr boolan element
      Return False
    ElseIf Me.TryParse(value.FalseValue, result) AndAlso result = True Then
      Return False
    Else
      _elements.Add(value)
    End If
  End Function

  ''' <summary>
  ''' Adds the specified value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the element was added or false if the element true or faluse values already exist.
  ''' </returns>
  Public Function Add(ByVal value As BooleanFormatterParser) As Boolean
    Return Me._add(value)
  End Function
  Private _elements As IList(Of BooleanFormatterParser)
  ''' <summary>
  ''' Returns the collection of <see cref="BooleanFormatterParser">boolean elements</see>.
  ''' </summary><returns></returns>
  Public Function Elements() As ReadOnlyCollection(Of BooleanFormatterParser)
    Return New ReadOnlyCollection(Of BooleanFormatterParser)(_elements)
  End Function

  ''' <summary>
  ''' Parses the specified value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the value equals to any <see cref="Elements">enumerated</see> True Value,
  ''' <c>false</c> to any <see cref="Elements">enumerated</see> False Value; otherwise
  ''' null.
  ''' </returns>
  Public Function Parse(ByVal value As String) As Boolean? Implements IParser(Of Boolean).Parse
    Dim result As Boolean
    For Each element As BooleanFormatterParser In _elements
      If element.TryParse(value, result) Then
        Return result
      End If
    Next
    Return New Boolean?
  End Function

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <param name="result">is set to <c>true</c> value equals to any <see cref="Elements">enumerated</see> True Value, 
  ''' <c>false</c> if the value equals  any <see cref="Elements">enumerated</see> False Value; otherwise
  ''' null.</param>
  ''' <returns>
  ''' <c>true</c> if the value was parsed.
  ''' </returns>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#", Justification:="This is the standard call format for this method in Visual Studio")> _
  Public Function TryParse(ByVal value As String, ByRef result As Boolean) As Boolean Implements IParser(Of Boolean).TryParse
    For Each element As BooleanFormatterParser In _elements
      If element.TryParse(value, result) Then
        Return result
      End If
    Next
    Return False
  End Function

  ''' <summary>
  ''' Tries to parse the value.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <returns>
  ''' <c>true</c> if the value can be parsed.
  ''' </returns>
  Public Function TryParse(ByVal value As String) As Boolean Implements IParser(Of Boolean).TryParse
    Dim result As Boolean
    For Each element As BooleanFormatterParser In _elements
      If element.TryParse(value, result) Then
        Return True
      End If
    Next
    Return False
  End Function

  ''' <summary>
  ''' Returns the text representation of the boolean value of the first element.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Text(ByVal value As Boolean) As String Implements IFormatter(Of Boolean).Text
    If _elements.Count > 0 Then
      Return _elements.Item(0).Text(value)
    Else
      Return "UNKNOWN"
    End If
  End Function

End Class

''' <summary>
''' Defines an enumerated list of <see cref="BooleanFormatterParser">boolean formatted element</see>.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/21/2011" by="David Hary" revision="1.2.4038.x">
''' Created
''' </history>
Public Class EnumeratedBoolean
  Inherits EnumeratedBooleanBase
  Implements IFormatterParser(Of Boolean)

  ''' <summary>
  ''' Initializes a new instance of the <see cref="EnumeratedBoolean" /> class.
  ''' </summary>
  Public Sub New()
    MyBase.New()
    MyBase.Add(EnumeratedBooleanBase.TrueFalse)
    MyBase.Add(EnumeratedBooleanBase.TF)
    MyBase.Add(EnumeratedBooleanBase.OneZero)
    MyBase.Add(EnumeratedBooleanBase.OnOff)
    MyBase.Add(EnumeratedBooleanBase.PassFail)
    MyBase.Add(EnumeratedBooleanBase.PF)
    MyBase.Add(EnumeratedBooleanBase.YesNo)
    MyBase.Add(EnumeratedBooleanBase.YN)
  End Sub

End Class

