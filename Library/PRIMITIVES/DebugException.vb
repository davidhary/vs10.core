Imports System.Runtime.CompilerServices
''' <summary>
''' Exception that is typically not meant to be handled but rather
''' reports for logging and debugging purposes.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/11/2010" by="Mr.PoorEnglish" revision="1.2.3753.x">
''' From Async Worker a Type Safe Backgroun Worker By Mr.PoorEnglish
''' http://www.codeproject.com/KB/vb/AsyncWorker2.aspx
''' Created
''' </history>
<Serializable()> _
Public Class DebugException
  Inherits Exception

  ''' <summary>Constructor with no parameters.</summary>
  Public Sub New()
    MyBase.New()
  End Sub

  ''' <summary>Constructor specifying the Message to be set.</summary>
  ''' <param name="message">Specifies the exception message.</param>
  Public Sub New(ByVal message As String)
    MyBase.New(message)
  End Sub

  ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
  ''' <param name="message">Specifies the exception message.</param>
  ''' <param name="innerException">Sets a reference to the InnerException.</param>
  Public Sub New(ByVal message As String, ByVal innerException As Exception)
    MyBase.New(message, innerException)
  End Sub

  ''' <summary>Constructor used for deserialization of the exception class.</summary>
  ''' <param name="info">Represents the SerializationInfo of the exception.</param>
  ''' <param name="context">Represents the context information of the exception.</param>
  Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)
    MyBase.New(info, context)
  End Sub

  ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
  ''' <param name="info">Represents the SerializationInfo of the exception.</param>
  ''' <param name="context">Represents the context information of the exception.</param>
  ''' <history date="02/28/04" by="David Hary" revision="1.0.1519.x">
  '''  Create
  ''' </history>
  <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> _
  Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, _
      ByVal context As System.Runtime.Serialization.StreamingContext)
    MyBase.GetObjectData(info, context)
  End Sub

End Class

Namespace ExceptionExtensions

  ''' <summary>
  ''' Includes exception extensions to instantiates <see cref="DebugException">debug exceptions.</see>.
  ''' </summary>
  ''' <license>
  ''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ''' </license>
  ''' <history date="04/11/2010" by="Mr.PoorEnglish" revision="1.2.3753.x">
  ''' From Async Worker a Type Safe Backgroun Worker By Mr.PoorEnglish
  ''' http://www.codeproject.com/KB/vb/AsyncWorker2.aspx
''' Created
''' </history>
  Public Module [Extensions]

    ''' <summary>
    ''' Constructs a new <see cref="DebugException">debug exception</see> with concatenated messages.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="sender"></param>
    ''' <param name="messages"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function DebugExceptionGetter(Of T As Class)( _
          ByVal sender As T, ByVal ParamArray messages As Object()) As DebugException
      Return New DebugException(String.Concat(sender.ToString(), Environment.NewLine, String.Concat(messages)))
    End Function

    ''' <summary>
    ''' Constructs a new <see cref="DebugException">debug exception</see> with concatenated messages.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="sender"></param>
    ''' <param name="details">Specifies the message details and format.</param>
    ''' <param name="args">Specifies the format arguments.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function DebugExceptionGetter(Of T As Class)( _
          ByVal sender As T, ByVal details As String, ByVal ParamArray args() As Object) As DebugException
      Return New DebugException(String.Concat(sender.ToString(), Environment.NewLine, _
                                              String.Format(Globalization.CultureInfo.CurrentCulture, details, args)))
    End Function

    ''' <summary>
    ''' Constructs a new <see cref="DebugException">debug exception</see> with concatenated messages.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="sender"></param>
    ''' <param name="innerException"></param>
    ''' <param name="messages"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function DebugExceptionGetter(Of T As Class)(ByVal sender As T, ByVal innerException As Exception, _
                                                        ByVal ParamArray messages As Object()) As DebugException
      Return New DebugException(String.Concat(sender.ToString(), Environment.NewLine, _
                                              String.Concat(messages)), innerException)
    End Function

    ''' <summary>
    ''' Constructs a new <see cref="DebugException">debug exception</see> with concatenated messages.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="sender"></param>
    ''' <param name="innerException"></param>
    ''' <param name="details">Specifies the message details and format.</param>
    ''' <param name="args">Specifies the format arguments.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function DebugExceptionGetter(Of T As Class)(ByVal sender As T, ByVal innerException As Exception, _
                                                        ByVal details As String, ByVal ParamArray args() As Object) As DebugException
      Return New DebugException(String.Concat(sender.ToString(), _
                                              Environment.NewLine, String.Format(Globalization.CultureInfo.CurrentCulture, details, args)), _
                                innerException)
    End Function

  End Module

End Namespace
