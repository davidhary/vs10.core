
''' <summary>
'''   Defines an event arguments class for messages.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/01/2009" by="David Hary" revision="1.1.3408.x">
''' Created
''' </history>
Public Class MessageEventArgs
  Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Default constructor.</summary>
  Public Sub New()
    Me.New(TraceEventType.Information, TraceEventType.Information, "", "")
    '_isCanceled = False
  End Sub

  ''' <summary>
  ''' Construtor for this class.
  ''' </summary>
  ''' <param name="synopsis">Specifies the message synopsis.</param>
  ''' <param name="format">Specifies the message formatting string</param>
  ''' <param name="args">Specifies the arguments.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
    Me.New(TraceEventType.Information, TraceEventType.Information, synopsis, format, args)
  End Sub

  ''' <summary>
  ''' Construtor for this class.
  ''' </summary>
  ''' <param name="synopsis">Specifies the message synopsis.</param>
  ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace and broadcast levels</see></param>
  ''' <param name="format">Specifies the message formatting string</param>
  ''' <param name="args">Specifies the arguments.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal trace As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
    Me.New(trace, trace, synopsis, format, args)
  End Sub

  ''' <summary>
  ''' Construtor for this class.
  ''' </summary>
  ''' <param name="broadcast">Specifies the <see cref="System.Diagnostics.TraceEventType">broadcast level</see></param>
  ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace and broadcast levels</see></param>
  ''' <param name="synopsis">Specifies the message synopsis.</param>
  ''' <param name="format">Specifies the message formatting string</param>
  ''' <param name="args">Specifies the arguments.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal broadcast As TraceEventType, ByVal trace As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
    Me.New(New ExtendedMessage(broadcast, trace, synopsis, format, args))
  End Sub

  ''' <summary>
  ''' Main constructor.
  ''' </summary>
  ''' <param name="details">Specifies the message details.
  ''' </param>
  ''' <param name="synopsis">Specifies the message synopsis.
  ''' </param>
  Public Sub New(ByVal synopsis As String, ByVal details As String)
    Me.new(TraceEventType.Information, TraceEventType.Information, synopsis, details)
  End Sub

  ''' <summary>
  ''' Full constructor.
  ''' </summary>
  ''' <param name="message">Specifies the extended message.
  ''' </param>
  ''' <remarks></remarks>
  Public Sub New(ByVal message As ExtendedMessage)
    MyBase.new()
    _extendedMessage = message
    _extendedMessage.Format = "{0:HH:mm:ss.fff}, {1}, {2}, {3}"
  End Sub

#End Region

#Region " PROPERTIES "

  Private _extendedMessage As ExtendedMessage
  ''' <summary>
  ''' Gets the extended message.
  ''' </summary>
  ''' <remarks></remarks>
  Public ReadOnly Property ExtendedMessage() As ExtendedMessage
    Get
      Return _extendedMessage
    End Get
  End Property

  ''' <summary>Gets the message details.</summary>
  Public ReadOnly Property Details() As String
    Get
      Return _extendedMessage.Details
    End Get
  End Property

  ''' <summary>
  ''' Gets an empty <see cref="MessageEventArgs">arguments</see>.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Shadows ReadOnly Property Empty() As MessageEventArgs
    Get
      Return New MessageEventArgs
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets the default format for displaying the message
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Format() As String
    Get
      Return _extendedMessage.Format
    End Get
    Set(ByVal value As String)
      If Not String.IsNullOrEmpty(value) Then
        _extendedMessage.Format = value
      End If
    End Set
  End Property

  ''' <summary>Gets or sets the event synopsis.
  ''' </summary>
  Public Property Synopsis() As String
    Get
      Return _extendedMessage.Synopsis
    End Get
    Set(ByVal value As String)
      _extendedMessage.Synopsis = value
    End Set
  End Property

  ''' <summary>Gets the event time stamp.</summary>
  Public ReadOnly Property Timestamp() As DateTime
    Get
      Return _extendedMessage.Timestamp
    End Get
  End Property

  ''' <summary>
  ''' Gets the trace level.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property TraceLevel() As TraceEventType
    Get
      Return _extendedMessage.TraceLevel
    End Get
  End Property

  ''' <summary>
  ''' Gets the Broadcast level.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property BroadcastLevel() As TraceEventType
    Get
      Return _extendedMessage.BroadcastLevel
    End Get
  End Property

#End Region

#Region " METHODS "

  ''' <summary>
  ''' Returns a compand message based on the default format.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function ToString() As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, _extendedMessage.Format, _
                         Me.Timestamp, Me.TraceLevel, Me.Synopsis, Me.Details)
  End Function

#End Region

End Class