﻿Imports isr.Core.TextExtensions
''' <summary>Extends the string builder class.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/24/2009" by="David Hary" revision="1.2.3615.x">
''' Created
''' </history>
Public Class MyStringBuilder

  ''' <summary>
  ''' Constructs a new count down stop watch with the specified <paramref name="duration">duration</paramref>
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()
    MyBase.New()
    _builder = New System.Text.StringBuilder
  End Sub

  ''' <summary>
  ''' Constructs a new count down stop watch with the specified <paramref name="duration">duration</paramref>
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New(ByVal capacity As Integer)
    MyBase.New()
    _builder = New System.Text.StringBuilder(capacity)
  End Sub

  ''' <summary>
  ''' Constructs a new count down stop watch with the specified <paramref name="duration">duration</paramref>
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New(ByVal capacity As Integer, ByVal maximumCapacity As Integer)
    MyBase.New()
    _builder = New System.Text.StringBuilder(capacity, maximumCapacity)
  End Sub

  Private _builder As System.Text.StringBuilder
  ''' <summary>
  ''' Returns reference to the builder for methods that are not implemented here.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property Builder() As Text.StringBuilder
    Get
      Return _builder
    End Get
  End Property

  ''' <summary>
  ''' Append the contents using the specified value.
  ''' </summary>
  ''' <param name="value">Contants</param>
  ''' <remarks></remarks>
  Public Sub Append(ByVal value As String)
    If Not String.IsNullOrEmpty(value) Then
      _builder.Append(value)
    End If
  End Sub

  ''' <summary>
  ''' Append the contents using the specified format and arguments.
  ''' </summary>
  ''' <param name="format">New line format</param>
  ''' <param name="args">Arguments for the line format.</param>
  ''' <remarks></remarks>
  Public Sub AppendFormat(ByVal format As String, ByVal ParamArray args() As Object)
    If Not String.IsNullOrEmpty(format) Then
      _builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, format, args)
    End If
  End Sub

  ''' <summary>
  ''' Add a line if not empty and add the content in the new line.
  ''' </summary>
  ''' <param name="format">New line format</param>
  ''' <param name="args">Arguments for the line format.</param>
  ''' <remarks></remarks>
  Public Sub AppendLine(ByVal format As String, ByVal ParamArray args() As Object)
    _builder.AppendLine(format, args)
  End Sub

  ''' <summary>
  ''' Add a line if not empty and add the content in the new line.
  ''' </summary>
  ''' <param name="value ">New line contents</param>
  ''' <remarks></remarks>
  Public Sub AppendLine(ByVal value As String)
    If Not String.IsNullOrEmpty(value) Then
      If _builder.Length > 0 Then
        _builder.AppendLine()
      End If
      _builder.Append(value)
    End If
  End Sub

  ''' <summary>
  ''' Clears the builder
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Clear()
    If Me._builder.Capacity > 0 Then
      _builder = New System.Text.StringBuilder(Me._builder.Capacity, Me._builder.MaxCapacity)
    Else
      _builder = New System.Text.StringBuilder
    End If
  End Sub

  ''' <summary>
  ''' Clears the builder and append the specified value.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Clear(ByVal value As String)
    Me.Clear()
    Me.Append(value)
  End Sub

  ''' <summary>
  ''' Clears the builder and append the specified value.
  ''' </summary>
  ''' <param name="format">New line format</param>
  ''' <param name="args">Arguments for the format.</param>
  ''' <remarks></remarks>
  Public Sub Clear(ByVal format As String, ByVal ParamArray args() As Object)
    Me.Clear()
    Me.AppendFormat(format, args)
  End Sub

  ''' <summary>
  ''' Gets or sets the maximum capacity of the string builder.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Capacity() As Integer
    Get
      Return Me._builder.Capacity
    End Get
    Set(ByVal value As Integer)
      Me._builder.Capacity = value
    End Set
  End Property
  ''' <summary>
  ''' Returns an array of strings split by the new line characters.
  ''' </summary>
  ''' <remarks></remarks>
  Public Function ToArray() As String()
    Return _builder.ToArray()
  End Function

  ''' <summary>
  ''' Returns the contents and a string.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overrides Function ToString() As String
    Return _builder.ToString
  End Function

End Class
