﻿''' <summary>
''' Parses the stack trace as provided by <see cref="Environment.StackTrace">the environment</see>.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/10/2009" by="David Hary" revision="1.2.3478">
''' Created
''' </history>
Public Class StackTraceParser

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <param name="stackTrace">Normally formatted Stack Trace</param>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer)
    MyBase.new()
    _userCallStack = New System.Text.StringBuilder(&HFF)
    _callStack = New System.Text.StringBuilder(&HFF)
    _frameworkLocations = New String() {"at System", "at Microsoft"}
    _parse(stackTrace, skipLines, totalLines)
  End Sub

  ''' <summary>
  ''' Constructs this class using the <see cref="Environment.StackTrace">envirnment stack trace</see>.
  ''' </summary>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal skipLines As Integer, ByVal totalLines As Integer)
    Me.new(Environment.StackTrace, skipLines, totalLines)
  End Sub

  Private _callStack As System.Text.StringBuilder
  ''' <summary>
  ''' Gets a normally formatted Call Sstack.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property CallStack() As String
    Get
      Return _callStack.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Get
  End Property

  Private Shared _frameworkLocations As String()
  ''' <summary>
  ''' Returns true if the line is a framework line.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function IsFrameworkLine(ByVal value As String) As Boolean
    For Each locationName As String In _frameworkLocations
      If value.TrimStart.StartsWith(locationName) Then
        Return True
      End If
    Next
    Return False
  End Function

  ''' <summary>
  ''' Parses the stack trace into the user and full call stacks.
  ''' </summary>
  ''' <param name="stackTrace">Normally formatted Stack Trace</param>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <remarks></remarks>
  Public Sub Parse(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer)
    _parse(stackTrace, skipLines, totalLines)
  End Sub

  ''' <summary>
  ''' Parses the <see cref="Environment.StackTrace">environment stack trace</see>.
  ''' </summary>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <remarks></remarks>
  Public Sub Parse(ByVal skipLines As Integer, ByVal totalLines As Integer)
    _parse(skipLines, totalLines)
  End Sub

  ''' <summary>
  ''' Parses the <see cref="Environment.StackTrace">environment stack trace</see>.
  ''' </summary>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <remarks></remarks>
  Private Sub _parse(ByVal skipLines As Integer, ByVal totalLines As Integer)
    Me._parse(Environment.StackTrace, skipLines, totalLines)
  End Sub

  ''' <summary>
  ''' Parses the stack trace into the user and full call stacks.
  ''' </summary>
  ''' <param name="stackTrace">Normally formatted Stack Trace</param>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <remarks></remarks>
  Private Sub _parse(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer)
    _userCallStack = New System.Text.StringBuilder(&HFF)
    _callStack = New System.Text.StringBuilder(&HFF)
    If String.IsNullOrEmpty(stackTrace) Then
      Return
    End If
    Dim callStack As String() = stackTrace.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
    If callStack.Length <= 0 Then
      Return
    End If
    Dim isFrameworkBlock As Boolean = False
    Dim lineNumber As Integer = 0
    Dim lineCount As Integer = 0
    For Each line As String In callStack
      If totalLines = 0 OrElse lineCount <= totalLines Then
        lineNumber += 1
        If lineNumber > skipLines Then
          If StackTraceParser.IsFrameworkLine(line) Then
            isFrameworkBlock = True
            _callStack.AppendLine(line)
          Else
            If isFrameworkBlock Then
              ' if previously had extenal code, append
              _userCallStack.AppendLine("   at [External Code]")
              isFrameworkBlock = False
            End If
            _callStack.AppendLine(line)
            _userCallStack.AppendLine(line)
            lineCount += 1
          End If
        End If
      End If
    Next
  End Sub

  Private _userCallStack As System.Text.StringBuilder
  ''' <summary>
  ''' Gets the user call stack, which includes no .NET Framework functions.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function UserCallStack() As String
    Return _userCallStack.ToString.TrimEnd(Environment.NewLine.ToCharArray)
  End Function

  ''' <summary>
  ''' Parses and returns the user call stack.
  ''' </summary>
  ''' <param name="stackTrace">Normally formatted Stack Trace</param>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function UserCallStack(ByVal stackTrace As String, ByVal skipLines As Integer, ByVal totalLines As Integer) As String
    Dim parser As New StackTraceParser(stackTrace, skipLines, totalLines)
    Return parser.UserCallStack
  End Function

  ''' <summary>
  ''' Parses and returns the user call stack using the <see cref="Environment.StackTrace">envirnment stack trace</see>.
  ''' </summary>
  ''' <param name="skipLines">Specifies the number of lines to skip. Two lines include the envirnoment methods</param>
  ''' <param name="totalLines">Specifies the maximum number of lines to include in the trace. Use 0 to 
  ''' include all lines.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function UserCallStack(ByVal skipLines As Integer, ByVal totalLines As Integer) As String
    Dim parser As New StackTraceParser(skipLines, totalLines)
    Return parser.UserCallStack
  End Function

End Class
