﻿''' <summary>
''' Implements a queue for holding messages.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/24/2009" by="David Hary" revision="1.1.3401.x">
''' Created
''' </history>
Public Class MessageQueue
  Inherits System.Collections.Generic.Queue(Of String)

  Implements IMessageQueuing

  ''' <summary>
  ''' Constructs this class.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()
    MyBase.new()
  End Sub

  ''' <summary>
  ''' Create a new class from the given entity.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal value As MessageQueue)
    Me.New()
    SyncLock _queueLocker
      MyBase.Clear()
      Do While value.Count > 0
        MyBase.Enqueue(value.Dequeue)
      Loop
      MyBase.Reverse()
    End SyncLock
  End Sub

  ''' <summary>
  ''' Clears the messages.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub ClearMessages() Implements IMessageQueuing.ClearMessages
    SyncLock _queueLocker
      MyBase.Clear()
    End SyncLock
  End Sub

  Private _queueLocker As New Object
  ''' <summary>
  ''' Returns all last interface messages.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function DequeueAll() As String Implements IMessageQueuing.DequeueMessages
    SyncLock _queueLocker
      If Not MyBase.Any Then
        Return ""
      End If
      Dim messages As New System.Text.StringBuilder
      Do While MyBase.Count > 0
        If messages.Length > 0 Then
          messages.AppendLine()
        End If
        messages.Append(MyBase.Dequeue)
      Loop
      Return messages.ToString
    End SyncLock
  End Function

  ''' <summary>
  ''' Thread save enqueu.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function _enqueue(ByVal value As String) As String
    SyncLock _queueLocker
      MyBase.Enqueue(value)
    End SyncLock
    Return value
  End Function

  ''' <summary>
  ''' Enqueues a message.
  ''' </summary>
  ''' <param name="value">Specifies the message to enqueu</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overloads Function Enqueue(ByVal value As String) As String Implements IMessageQueuing.EnqueueMessage
    Return _enqueue(value)
  End Function

  ''' <summary>
  ''' Enqueues a message.
  ''' </summary>
  ''' <param name="format">Specifies the message format.</param>
  ''' <param name="args">Specifies the message arguments.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overloads Function Enqueue(ByVal format As String, ByVal ParamArray args() As Object) As String Implements IMessageQueuing.EnqueueFormat
    Return _enqueue(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
  End Function

  ''' <summary>
  ''' Returns true if the queue has messages.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function HasItems() As Boolean Implements IMessageQueuing.HasMessages
    Return MyBase.Any
  End Function

  ''' <summary>
  ''' Returns all interface messages without clearing the queue.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function ToString() As String Implements IMessageQueuing.ToString
    SyncLock _queueLocker
      If Not MyBase.Any Then
        Return ""
      End If
      Dim builder As New System.Text.StringBuilder
      If MyBase.Any Then
        Dim messages(MyBase.Count - 1) As String
        MyBase.CopyTo(messages, 0)
        For Each item As String In messages
          If Not String.IsNullOrEmpty(item) Then
            If builder.Length > 0 Then
              builder.AppendLine()
            End If
            builder.Append(item)
          End If
        Next
        Return builder.ToString
      Else
        Return ""
      End If
    End SyncLock
  End Function

End Class
