Imports System
Imports System.ComponentModel
''' <summary>
''' A class to invoke property setters of a ISynchronizeInvoke object
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/24/2006" by="Simone Romano" revision="1.0.2519.x">
''' Created
''' </history>
Public Class ThreadSafePropertySetter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Inizializza una nuova istanza dell'oggetto ThreadSafePropertySetter
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New(ByVal syncInvokeObject As ISynchronizeInvoke)
    Me._syncInvokeObject = syncInvokeObject
  End Sub

#End Region

#Region " Public Methods "

  ''' <summary>
  ''' Sets the control property in a thead safe manner.
  ''' </summary>
  ''' <typeparam name="T"></typeparam>
  ''' <param name="ctrl"></param>
  ''' <param name="propertyName"></param>
  ''' <param name="propertyValue"></param>
  ''' <remarks></remarks>
  Public Sub SetCtrlProperty(Of T)(ByVal ctrl As Object, ByVal propertyName As String, ByVal propertyValue As T)
    SetObjectProperty(ctrl, propertyName, propertyValue)
  End Sub

  ''' <summary>
  ''' Sets a control Checked value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlChecked(ByVal control As Windows.Forms.Control, ByVal value As Boolean)
    SetCtrlProperty(Of Boolean)(control, "Checked", value)
  End Sub

  ''' <summary>
  ''' Sets a control Enabled value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlEnabled(ByVal control As Windows.Forms.Control, ByVal value As Boolean)
    SetCtrlProperty(Of Boolean)(control, "Enabled", value)
  End Sub

  ''' <summary>
  ''' Sets a control text value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlText(ByVal control As Windows.Forms.Control, ByVal value As String)
    SetCtrlProperty(Of String)(control, "Text", value)
  End Sub

  ''' <summary>
  ''' Sets a <see cref="Double"/> control text value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlText(ByVal control As Windows.Forms.Control, ByVal value As Double)
    Me.SetControlText(control, value.ToString(Globalization.CultureInfo.CurrentCulture))
  End Sub

  ''' <summary>
  ''' Sets a <see cref="Integer"/> control text value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlText(ByVal control As Windows.Forms.Control, ByVal value As Integer)
    Me.SetControlText(control, CStr(value))
  End Sub

  ''' <summary>
  ''' Sets a parameter array control text value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="format"></param>
  ''' <param name="values"></param>
  ''' <remarks></remarks>
  Public Sub SetControlText(ByVal control As Windows.Forms.Control, ByVal format As String, ByVal ParamArray values() As Object)
    Me.SetControlText(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, values))
  End Sub

  ''' <summary>
  ''' Sets a control Integer value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlValue(ByVal control As Windows.Forms.Control, ByVal value As Integer)
    SetCtrlProperty(Of Integer)(control, "Value", value)
  End Sub

  ''' <summary>
  ''' Sets a control Decimal value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlValue(ByVal control As Windows.Forms.Control, ByVal value As Decimal)
    SetCtrlProperty(Of Decimal)(control, "Value", value)
  End Sub

  ''' <summary>
  ''' Sets a control Visible value.
  ''' </summary>
  ''' <param name="control"></param>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub SetControlVisible(ByVal control As Windows.Forms.Control, ByVal value As Boolean)
    SetCtrlProperty(Of Boolean)(control, "Visible", value)
  End Sub

#End Region

#Region " Protected Methods "

  Private Delegate Sub SetCtrlPropertyDelegate(ByVal ctrl As Object, ByVal propertyName As String, ByVal propertyValue As Object)

  ''' <summary>
  ''' Sets the proerty value in a safe thread mode.
  ''' </summary>
  ''' <param name="ctrl">Reference to the control which property is set.</param>
  ''' <param name="propertyName"></param>
  ''' <param name="propertyValue"></param>
  ''' <remarks></remarks>
  Protected Sub SetObjectProperty(ByVal ctrl As Object, ByVal propertyName As String, ByVal propertyValue As Object)
    If ctrl Is Nothing Then
      Throw New ArgumentNullException("ctrl")
    End If
    If propertyValue Is Nothing Then
      Throw New ArgumentNullException("propertyValue")
    End If
    If _syncInvokeObject.InvokeRequired Then
      _syncInvokeObject.Invoke(New SetCtrlPropertyDelegate(AddressOf SetCtrlProperty), New Object() {ctrl, propertyName, propertyValue})
      Return
    End If
    Dim info As Reflection.PropertyInfo = ctrl.GetType.GetProperty(propertyName)
    If info IsNot Nothing Then
      If propertyValue Is Nothing Then
        'This doesn't throw a null-reference exception in case you want to set to 
        'nothing a Control Property
        info.SetValue(ctrl, Nothing, Nothing)
      ElseIf info.PropertyType.IsAssignableFrom(propertyValue.GetType) Then
        info.SetValue(ctrl, propertyValue, Nothing)
      End If
    End If
  End Sub

#End Region

#Region " Fields "
  Private _syncInvokeObject As ISynchronizeInvoke
#End Region

End Class

