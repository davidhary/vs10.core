﻿''' <summary>
''' A stop watch capable of counting down.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/06/2009" by="David Hary" revision="1.1.3413.x">
''' Move from Support and simplify.
''' </history>
''' <history date="05/06/2009" by="David Hary" revision="1.1.3413.x">
''' Created
''' </history>
Public Class CountdownStopwatch
  Inherits Diagnostics.Stopwatch

  ''' <summary>
  ''' Constructs a new count down stop watch with the specified <paramref name="duration">duration</paramref>
  ''' </summary>
  ''' <param name="duration">Specifies the stop watch duration</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal duration As TimeSpan)

    MyBase.New()
    _duration = duration

  End Sub

  ''' <summary>
  ''' Constructs a new count down stop watch with the specified <paramref name="duration">duration</paramref>
  ''' </summary>
  ''' <param name="duration">Specifies the stop watch duration</param>
  ''' <remarks></remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix", Justification:="Must be the same name as in the base class")> _
  Public Overloads Shared Function StartNew(ByVal duration As TimeSpan) As CountdownStopwatch
    Return New CountdownStopwatch(duration)
  End Function

  Private _duration As TimeSpan = New TimeSpan(0)
  ''' <summary>Gets or sets the duration of the time spanner.</summary>
  ''' <value><c>Duration</c>is a TimeSpan property.</value>
  Public Property Duration() As TimeSpan
    Get
      Return _duration
    End Get
    Set(ByVal value As TimeSpan)
      _duration = value
    End Set
  End Property

  ''' <summary>Returns true if count down completed.</summary>
  ''' <returns>Returns true if count down is done.</returns>
  Public Function CountedDown() As Boolean
    Return Me.Elapsed > _duration
  End Function

  ''' <summary>
  ''' Stores and resets time to zero.
  ''' </summary>
  ''' <remarks></remarks>
  Public Overloads Sub Reset()
    MyBase.Reset()
  End Sub

End Class
