''' <summary>Permits counting down using long integer.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="06/13/2006" by="David Hary" revision="1.0.2355.x">
''' Created
''' </history>
Public Class DownCounter

#Region " COUNTER "

  ''' <summary>
  ''' Decrement the count.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Countdown()
    _currentValue -= _decrementValue
  End Sub

  Private _currentValue As Integer
  ''' <summary>
  ''' Gets the current value of the counter.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property CurrentValue() As Integer
    Get
      Return _currentValue
    End Get
  End Property

  Private _decrementValue As Integer
  ''' <summary>
  ''' Gets or set the amout the count is decremented by on each countdown.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property DecrementValue() As Integer
    Get
      Return _decrementValue
    End Get
    Set(ByVal Value As Integer)
      _decrementValue = Value
    End Set
  End Property

  Private _initialValue As Integer
  ''' <summary>
  ''' Gets or sets the initial count down value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property InitialValue() As Integer
    Get
      Return _initialValue
    End Get
    Set(ByVal Value As Integer)
      _initialValue = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets the outcome of the countdown.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsCountdownDone() As Boolean
    Get
      Return _currentValue < 0
    End Get
  End Property

  ''' <summary>
  ''' Restart from the initial value.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Restart()
    _currentValue = _initialValue
  End Sub

#End Region

End Class

