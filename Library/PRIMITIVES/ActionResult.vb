﻿''' <summary>
''' Action result entity.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/13/2011" by="David Hary" revision="1.0.4273.x">
''' Created
''' </history>
''' <history date="08/20/2011" by="David Hary" revision="1.0.4249.x">
''' Convert to a class to allow setters and getters.
''' </history>
''' <history date="09/16/2011" by="David Hary" revision="1.0.4275.x">
''' Add generic result.
''' </history>
Public Class ActionResult

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Creates a clear entity.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        _Clear()
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal value As String)
        Me.New()
        Me.Setter(value)
    End Sub

    ''' <summary>
    ''' Sets the detailed message; action result is okay.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New()
        Me.Setter(format, args)
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="isFailed">The Action Result failure condition.</param>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal isFailed As Boolean, ByVal value As String)
        Me.New()
        Me.Setter(isFailed, value)
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="ex">The Action Result exception.</param>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal ex As Exception, ByVal value As String)
        Me.New()
        Me.Setter(ex, value)
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="isFailed">The Action Result failure condition.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal isFailed As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New()
        Me.Setter(isFailed, format, args)
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="ex">The Action Result exception.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New()
        Me.Setter(ex, format, args)
    End Sub

    ''' <summary>
    ''' Clears this instance.
    ''' </summary>
    Private Sub _Clear()
        _ActionResults = New List(Of ActionResult)
        _details = ""
        _failed = False
        _Exception = Nothing
    End Sub

    ''' <summary>
    ''' Clears this instance.
    ''' </summary>
    Public Sub Clear()
        _Clear()
    End Sub

#End Region

#Region " DATA MEMBERS "

    ''' <summary>
    ''' Determines whether this instance has info.
    ''' </summary><returns>
    '''   <c>true</c> if this instance has info; otherwise, <c>false</c>.
    ''' </returns>
    Public Function HasInfo() As Boolean
        Return Not String.IsNullOrEmpty(_details)
    End Function

    ''' <summary>
    ''' Determines whether the action result had an exception.
    ''' </summary><returns>
    '''   <c>true</c> if the action result had an exception; otherwise, <c>false</c>.
    ''' </returns>
    Public Function HasException() As Boolean
        Return Me.Exception IsNot Nothing
    End Function

    Private _details As String
    ''' <summary>
    ''' Gets the detailed message.
    ''' </summary>
    ''' <value>
    ''' The message.
    ''' </value>
    Public ReadOnly Property Details As String
        Get
            Return _details
        End Get
    End Property

    Private _Exception As Exception
    ''' <summary>
    ''' Gets the exception.
    ''' </summary>
    ''' <value>
    ''' The exception.
    ''' </value>
    Public ReadOnly Property Exception() As Exception
        Get
            Return _Exception
        End Get
    End Property

    Private _failed As Boolean
    ''' <summary>
    ''' Gets a value indicating whether this <see cref="ActionResult" /> failed.
    ''' </summary>
    ''' <value>
    '''   <c>true</c> if failed; otherwise, <c>false</c>.
    ''' </value>
    Public ReadOnly Property Failed() As Boolean
        Get
            Return _failed
        End Get
    End Property

#End Region

#Region " ACTION RESULTS "

    ''' <summary>
    ''' Adds the action results to the list.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Overridable Function Add(ByVal value As ActionResult) As ActionResult
        _ActionResults.Add(value)
        Return Me
    End Function

    Private _ActionResults As List(Of ActionResult)
    ''' <summary>
    ''' Gets or sets the list of previous action results.
    ''' </summary>
    ''' <returns>
    ''' The action results.
    ''' </returns>
    Public Function ActionResults() As ActionResult()
        Return _ActionResults.ToArray
    End Function

    ''' <summary>
    ''' Returns details from all results.
    ''' </summary>
    ''' <param name="separator">The separator.</param><returns></returns>
    Public Function AllDetails(ByVal separator As String) As String
        Dim s As New System.Text.StringBuilder
        If Me._ActionResults IsNot Nothing AndAlso (Me._ActionResults.Count > 0) Then
            For Each ar As ActionResult In Me._ActionResults
                If ar.HasInfo Then
                    If s.Length > 0 Then
                        s.Append(separator)
                    End If
                    s.Append(ar.AllDetails(separator))
                End If
            Next
        End If
        If Me.HasInfo Then
            If s.Length > 0 Then
                s.Append(separator)
            End If
            s.Append(Me.Details)
        End If
        If Me.Exception IsNot Nothing Then
            If s.Length > 0 Then
                s.Append(separator)
            End If
            s.Append(Me.Exception.ToString)
        End If
        Return s.ToString
    End Function

#End Region

#Region " SETTERS "

    ''' <summary>
    ''' Sets the detailed message.
    ''' Result is okay.
    ''' </summary>
    ''' <param name="value">The details.</param>
    Public Sub Setter(ByVal value As String)
        _Clear()
        _details = value
    End Sub

    ''' <summary>
    ''' Sets the detailed message.
    ''' Result is okay.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub Setter(ByVal format As String, ByVal ParamArray args() As Object)
        Me.Setter(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="ex">The Action Result exception.</param>
    ''' <param name="value">The details.</param>
    Public Sub Setter(ByVal ex As Exception, ByVal value As String)
        Me.Setter(value)
        If ex IsNot Nothing Then
            _failed = True
            _Exception = ex
        End If
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="isFailed">The Action Result failure condition.</param>
    ''' <param name="value">The details.</param>
    Public Sub Setter(ByVal isFailed As Boolean, ByVal value As String)
        Me.Setter(value)
        _failed = isFailed
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="isFailed">The Action Result failure condition.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub Setter(ByVal isFailed As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.Setter(format, args)
        _failed = isFailed
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="ex">The Action Result exception.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub Setter(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        Me.Setter(format, args)
        If ex IsNot Nothing Then
            _failed = True
            _Exception = ex
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="ActionResult" /> is equal to this instance.
    ''' </summary>
    ''' <param name="obj">The obj.</param><returns></returns>
    Public Overloads Function Equals(ByVal obj As ActionResult) As Boolean
        Return obj IsNot Nothing AndAlso _
            (Me.Details IsNot Nothing AndAlso Me.Details.Equals(obj.Details)) AndAlso _
            Me.Exception.Equals(obj.Exception) AndAlso _
            Me.Failed.Equals(obj.Failed)
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    ''' </summary>
    ''' <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param><returns>
    '''   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
    ''' </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse ActionResult.Equals(Me, CType(obj, ActionResult)))
    End Function

    ''' <summary>
    ''' Returns a hash code for this instance.
    ''' </summary><returns>
    ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
    ''' </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Details.GetHashCode Xor Me.Failed.GetHashCode Xor Me.Exception.GetHashCode
    End Function

    ''' <summary>
    ''' Implements the operator =.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator =(ByVal left As ActionResult, ByVal right As ActionResult) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse left.Equals(right)
    End Operator

    ''' <summary>
    ''' Implements the operator &lt;&gt;.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator <>(ByVal left As ActionResult, ByVal right As ActionResult) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse Not left.Equals(right)
    End Operator

#End Region

End Class

''' <summary>
''' Action result entity with generic result.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/16/2011" by="David Hary" revision="1.0.4275.x">
''' Create result.
''' </history>
Public Class ActionResult(Of T)
    Inherits ActionResult

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ActionResult(Of T)" /> class.
    ''' </summary>
    ''' <param name="result">The result.</param>
    Public Sub New(ByVal result As T)
        MyBase.New()
        Me._Result = result
    End Sub

    ''' <summary>
    ''' Sets the detailed message; action result is okay.
    ''' </summary>
    ''' <param name="result">The result.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal result As T, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(result)
        Me.Setter(format, args)
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="result">The result.</param>
    ''' <param name="isFailed">The Action Result failure condition.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal result As T, ByVal isFailed As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(result)
        Me.Setter(isFailed, format, args)
    End Sub

    ''' <summary>
    ''' Sets the detailed message and Action Result failure condition.
    ''' </summary>
    ''' <param name="result">The result.</param>
    ''' <param name="ex">The Action Result exception.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal result As T, ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(result)
        Me.Setter(ex, format, args)
    End Sub


#End Region

    Private _Result As T
    ''' <summary>
    ''' Gets or sets the result.
    ''' This allows returning a result as an object in addition to 
    ''' the result information.
    ''' </summary>
    ''' <value>The result.</value>
    ''' <remarks></remarks>
    Public Overridable Property Result() As T
        Get
            Return _Result
        End Get
        Set(ByVal value As T)
            _Result = value
        End Set
    End Property

    ''' <summary>
    ''' Return the new <see cref="ActionResult">action result</see> after adding this instance into it.
    ''' </summary>
    ''' <param name="value">The new parent result.</param>
    Public Overridable Overloads Function Add(ByVal value As ActionResult) As ActionResult(Of T)
        MyBase.Add(value)
        Return Me
    End Function

    ''' <summary>
    ''' Return the new <see cref="ActionResult">action result</see> after adding this instance into it.
    ''' </summary>
    ''' <param name="value">The new parent result.</param>
    Public Overridable Overloads Function AddIfFailedOrInfo(ByVal value As ActionResult) As ActionResult(Of T)
        If value.Failed OrElse value.HasInfo Then
            MyBase.Add(value)
        End If
        Return Me
    End Function

    ''' <summary>
    ''' Return the new <see cref="ActionResult">action result</see> after adding this instance into it.
    ''' </summary>
    ''' <param name="value">The new parent result.</param>
    Public Overloads Function Insert(ByVal value As ActionResult(Of T)) As ActionResult(Of T)
        value.Add(Me)
        If Not value.HasInfo Then
            value.Setter(Me.Details)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Return the new <see cref="ActionResult">action result</see> after adding this instance into it
    ''' if this instance failed or has info.
    ''' </summary>
    ''' <param name="value">The new parent result.</param>
    Public Overloads Function InsertIfFailedOrInfo(ByVal value As ActionResult(Of T)) As ActionResult(Of T)
        If Me.Failed OrElse Me.HasInfo Then
            value.Add(Me)
            If Not value.HasInfo Then
                value.Setter(Me.Details)
            End If
        End If
        Return value
    End Function

End Class
