''' <summary>
''' Defines an Trace Message.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/17/2011" by="David Hary" revision="1.0.4368.x">
''' Created based on legacy core exrtended message.
''' </history>
<Serializable()> _
Public Class TraceMessage

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        Me.New(String.Empty)
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="details">Specifies the message details.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal details As String)
        Me.New(TraceEventType.Information, details)
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="format">Specifies the message formatting string</param>
    ''' <param name="args">Specifies the arguments.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(TraceEventType.Information, format, args)
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="traceLevel">Specifies the <see cref="System.Diagnostics.TraceEventType">trace level</see></param>
    ''' <param name="format">Specifies the message formatting string</param>
    ''' <param name="args">Specifies the arguments.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal traceLevel As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New()
        _traceMessageFormat = "{0:HH:mm:ss.fff}, {1}, {2}"
        If String.IsNullOrEmpty(format) Then
            _details = ""
        ElseIf args.Count = 0 Then
            _details = format
        Else
            _details = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        End If
        _timestamp = Date.Now
        _traceLevel = traceLevel
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="details">Specifies the message details.</param>
    ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace level</see></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal trace As TraceEventType, ByVal details As String)
        Me.New(trace, String.Empty, details)
    End Sub

    ''' <summary>
    ''' Gets an empty <see cref="TraceMessage">Trace Message</see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Empty() As TraceMessage
        Get
            Return New TraceMessage()
        End Get
    End Property

#End Region

#Region " PROPERTIES "

    Private _details As String
    ''' <summary>Gets or sets the Trace Message message.
    ''' </summary>
    Public Property Details() As String
        Get
            Return _details
        End Get
        Set(ByVal value As String)
            _details = value
        End Set
    End Property

    Private _traceMessageFormat As String
    ''' <summary>
    ''' Gets or sets the default format for displaying the message
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TraceMessageFormat() As String
        Get
            Return _traceMessageFormat
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                _traceMessageFormat = value
            End If
        End Set
    End Property

    Private _timestamp As DateTime
    ''' <summary>Gets the Trace Message time stamp.
    ''' </summary>
    Public ReadOnly Property Timestamp() As DateTime
        Get
            Return _timestamp
        End Get
    End Property

    Private _traceLevel As System.Diagnostics.TraceEventType
    ''' <summary>Gets or sets the
    ''' <see cref="System.Diagnostics.TraceEventType">trace level</see>.</summary>
    Public Property TraceLevel() As System.Diagnostics.TraceEventType
        Get
            Return _traceLevel
        End Get
        Set(ByVal value As System.Diagnostics.TraceEventType)
            _traceLevel = value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Returns a compand message based on the default format.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, _traceMessageFormat, Me.Timestamp, Me.TraceLevel, Me.Details)
    End Function

#End Region

End Class
