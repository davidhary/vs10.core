﻿''' <summary>
''' Action information entity.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/27/2011" by="David Hary" revision="1.0.4164.x">
''' Created
''' </history>
''' <history date="08/20/2011" by="David Hary" revision="1.0.4249.x">
''' Convert to a class to allow setters and getters.
''' </history>
Public Class ActionInfo

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' Creates a clear entity.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        _Clear()
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal value As String)
        Me.New()
        Me.DetailsSetter(value)
        Me.SynopsisSetter(value)
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        Me.New()
        Me.DetailsSetter(format, args)
        Me.SynopsisSetter(_details)
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal level As TraceLevel, ByVal value As String)
        Me.New()
        Me.DetailsSetter(level, value)
        Me.SynopsisSetter(level, value)
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub New(ByVal level As TraceLevel, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New()
        Me.DetailsSetter(level, format, args)
        Me.SynopsisSetter(level, _details)
    End Sub

    ''' <summary>
    ''' Clears this instance.
    ''' </summary>
    Private Sub _Clear()
        _details = ""
        _synopsis = ""
        _level = TraceLevel.Off
        _timestamp = DateTime.MinValue
    End Sub

    ''' <summary>
    ''' Clears this instance.
    ''' </summary>
    Public Sub Clear()
        _Clear()
    End Sub

#End Region

#Region " DATA MEMBERS "

    ''' <summary>
    ''' Determines whether this instance has info.
    ''' </summary><returns>
    '''   <c>true</c> if this instance has info; otherwise, <c>false</c>.
    ''' </returns>
    Public Function HasInfo() As Boolean
        Return Not String.IsNullOrEmpty(_details)
    End Function

    Private _details As String
    ''' <summary>
    ''' Gets or sets the detailed message.
    ''' </summary>
    ''' <value>
    ''' The message.
    ''' </value>
    Public ReadOnly Property Details As String
        Get
            Return _details
        End Get
    End Property

    Private _level As Diagnostics.TraceLevel
    ''' <summary>
    ''' Gets the action info level. <see cref="Diagnostics.TraceLevel.[Error]">error</see> indicates an error.
    ''' </summary>
    ''' 
    Public ReadOnly Property Level As Diagnostics.TraceLevel
        Get
            Return _level
        End Get
    End Property

    ''' <summary>
    ''' Gets a value indicating whether the <see cref="ActionInfo">action info</see> is reporting an error.
    ''' </summary>
    ''' <value>
    '''   <c>true</c> if occurred; otherwise, <c>false</c>.
    ''' </value>
    Public ReadOnly Property IsError As Boolean
        Get
            Return _level = TraceLevel.Error
        End Get
    End Property

    Private _synopsis As String
    ''' <summary>
    ''' Gets or sets the synopsis.
    ''' </summary>
    ''' <value>
    ''' The synopsis.
    ''' </value>
    Public ReadOnly Property Synopsis As String
        Get
            Return _synopsis
        End Get
    End Property

    Private _timestamp As DateTime
    ''' <summary>
    ''' Gets the time stamp.
    ''' </summary>
    Public ReadOnly Property Timestamp() As DateTime
        Get
            Return _timestamp
        End Get
    End Property

#End Region

#Region " SETTERS "

    ''' <summary>
    ''' Sets the detailed message.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="value">The details.</param>
    Public Sub DetailsSetter(ByVal value As String)
        _timestamp = Date.Now
        _details = value
        If _level = TraceLevel.Off AndAlso Not String.IsNullOrEmpty(value) Then
            _level = TraceLevel.Error
        End If
    End Sub

    ''' <summary>
    ''' Sets the detailed message.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub DetailsSetter(ByVal format As String, ByVal ParamArray args() As Object)
        Me.DetailsSetter(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary>
    ''' Sets the detailed message and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="value">The details.</param>
    Public Sub DetailsSetter(ByVal level As TraceLevel, ByVal value As String)
        Me.DetailsSetter(value)
        _level = level
    End Sub

    ''' <summary>
    ''' Sets the detailed message and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub DetailsSetter(ByVal level As TraceLevel, ByVal format As String, ByVal ParamArray args() As Object)
        Me.DetailsSetter(format, args)
        _level = level
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Sub Setter(ByVal value As String)
        Me.DetailsSetter(value)
        Me.SynopsisSetter(value)
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub Setter(ByVal format As String, ByVal ParamArray args() As Object)
        Me.DetailsSetter(format, args)
        Me.SynopsisSetter(_details)
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="value">The value.</param>
    Public Sub Setter(ByVal level As TraceLevel, ByVal value As String)
        Me.DetailsSetter(level, value)
        Me.SynopsisSetter(level, value)
    End Sub

    ''' <summary>
    ''' Sets the detailed and synopsis information and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub Setter(ByVal level As TraceLevel, ByVal format As String, ByVal ParamArray args() As Object)
        Me.DetailsSetter(level, format, args)
        Me.SynopsisSetter(level, _details)
    End Sub

    ''' <summary>
    ''' Sets the synopsis.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Sub SynopsisSetter(ByVal value As String)
        _timestamp = Date.Now
        _synopsis = value
        If _level = TraceLevel.Off AndAlso Not String.IsNullOrEmpty(value) Then
            _level = TraceLevel.Error
        End If
    End Sub

    ''' <summary>
    ''' Sets the error Synopsis.
    ''' Sets the <see cref="Level">level</see> to <see cref="TraceLevel.[Error]">Error.</see>.
    ''' </summary>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub SynopsisSetter(ByVal format As String, ByVal ParamArray args() As Object)
        Me.SynopsisSetter(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary>
    ''' Sets the synopsis.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="value">The value.</param>
    Public Sub SynopsisSetter(ByVal level As TraceLevel, ByVal value As String)
        Me.SynopsisSetter(value)
        _level = level
    End Sub

    ''' <summary>
    ''' Sets the error Synopsis and action info level.
    ''' </summary>
    ''' <param name="level">The action info level.</param>
    ''' <param name="format">The format.</param>
    ''' <param name="args">The args.</param>
    Public Sub SynopsisSetter(ByVal level As TraceLevel, ByVal format As String, ByVal ParamArray args() As Object)
        Me.SynopsisSetter(format, args)
        _level = level
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="ActionInfo" /> is equal to this instance.
    ''' </summary>
    ''' <param name="obj">The obj.</param><returns></returns>
    Public Overloads Function Equals(ByVal obj As ActionInfo) As Boolean
        Return obj IsNot Nothing AndAlso
            (Me.Details IsNot Nothing AndAlso Me.Details.Equals(obj.Details)) AndAlso
            (Me.Synopsis IsNot Nothing AndAlso Me.Details.Equals(obj.Synopsis)) AndAlso
            Me.Level.Equals(obj.Level)
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    ''' </summary>
    ''' <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param><returns>
    '''   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
    ''' </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse ActionInfo.Equals(Me, CType(obj, ActionInfo)))
    End Function

    ''' <summary>
    ''' Returns a hash code for this instance.
    ''' </summary><returns>
    ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
    ''' </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Details.GetHashCode Xor Synopsis.GetHashCode Xor Me.Level.GetHashCode
    End Function

    ''' <summary>
    ''' Implements the operator =.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator =(ByVal left As ActionInfo, ByVal right As ActionInfo) As Boolean
        Return ((left Is Nothing) AndAlso (right Is Nothing)) OrElse
                left.Equals(right)
    End Operator

    ''' <summary>
    ''' Implements the operator &lt;&gt;.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator <>(ByVal left As ActionInfo, ByVal right As ActionInfo) As Boolean
        Return ((left Is Nothing) AndAlso (right IsNot Nothing)) OrElse
                Not left.Equals(right)
    End Operator

#End Region

End Class
