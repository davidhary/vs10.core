﻿#Region "References"
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
#End Region ' References
''' <summary>
''' Provides a class that gives us the same features as a [FlagsAttribute] 
''' enum with methods to query the state of the register but allows us to use
''' any (appropriately defined) enumeration.
''' </summary>
''' <typeparam name="TEnumeration">An enumerated type.</typeparam>
''' <license>
''' (c) 2010 Craig Greenock. cgreenock@bcs.org.uk
''' Licensed under The Code Project Open License.
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/03/2010" by="David Hary" revision="1.2.3989.x">
''' http://www.codeproject.com/KB/string/stringconversion.aspx
''' Created
''' </history>
Public Class Register(Of TEnumeration)

#Region "Declarations"

    ' At design time therere's no indication that type T allows
    ' bitwise operations so we get compile errors so we'll 
    ' represent the enumerated type as an integer.
    Private _register As Integer

#End Region ' Declarations

#Region "Construction"

    ''' <summary>
    ''' Instantiate a fully cleared register.
    ''' </summary>
    Public Sub New()
        Clear()
    End Sub

    ''' <summary>
    ''' Instantiate a register with a given starting state.
    ''' </summary>
    ''' <param name="value">Logical OR of one or more enumerated values</param>
    Public Sub New(ByVal value As TEnumeration)
        Clear()
        [Set](value)
    End Sub

#End Region ' Construction

#Region "Public Methods"

    ''' <summary>
    ''' Clear the flag register completely.
    ''' </summary>
    Public Function Clear() As TEnumeration
        _register = clearAll
        Return Me.State
    End Function

    ''' <summary>
    ''' Clear one or more flags in the register.
    ''' </summary>
    ''' <param name="value">Logical OR of one or more enumerated values</param>
    Public Function Clear(ByVal value As TEnumeration) As TEnumeration
        Dim flag As Integer = Convert.ToInt32(value, Globalization.CultureInfo.InstalledUICulture)
        Dim mask As Integer = setAll And Not flag
        _register = _register And mask
        Return Me.State
    End Function

    ''' <summary>
    ''' Set all flags in the register
    ''' </summary>
    Public Function [Set]() As TEnumeration
        _register = setAll
        Return Me.State
    End Function

    ''' <summary>
    ''' Set one or more flags in the register.
    ''' </summary>
    ''' <param name="value">Logical OR of one or more enumerated values</param>
    Public Function [Set](ByVal value As TEnumeration) As TEnumeration
        Dim flag As Integer = Convert.ToInt32(value, Globalization.CultureInfo.InstalledUICulture)
        _register = (_register Or flag)
        Return Me.State
    End Function

    ''' <summary>
    ''' Query whether one or more flags are set.
    ''' </summary>
    ''' <param name="value">Logical OR of one or more enumerated values</param>
    ''' <returns>True if flag(s) set</returns>
    Public Function IsSet(ByVal value As TEnumeration) As Boolean
        Dim flag As Integer = Convert.ToInt32(value, Globalization.CultureInfo.InstalledUICulture)
        Return (_register And flag) = flag
    End Function

    ''' <summary>
    ''' Return the current state of the register as the 
    ''' enumerated type used to instantiate this object.
    ''' </summary>
    Public ReadOnly Property State() As TEnumeration
        Get
            Return CType(System.Enum.ToObject(GetType(TEnumeration), _register), TEnumeration)
        End Get
    End Property

#End Region ' Public Methods

#Region "Private Methods"

    ''' <summary>
    ''' A convenient way to reset the flag register.
    ''' </summary>
    Private Shared ReadOnly Property clearAll() As Integer
        Get
            Return 0
        End Get
    End Property

    ''' <summary>
    ''' A convenient way to set the flag register in one step.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")> _
    Private ReadOnly Property setAll() As Integer
        Get
            Dim register As Integer = clearAll
            For Each flag As Integer In System.Enum.GetValues(GetType(TEnumeration))
                register = register Or flag
            Next flag
            Return register
        End Get
    End Property

#End Region ' Private Methods

End Class
