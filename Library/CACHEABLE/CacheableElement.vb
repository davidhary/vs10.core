﻿''' <summary>
''' The contract implemented by elements that can be updated, namely, their values can be applied or refreshed.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Interface IUpdateable
  Inherits INestedPropertyChanged

  ''' <summary>
  ''' Applies changes and returns true if a change was applied. Same as write.
  ''' </summary><returns></returns>
  Function Apply() As Boolean

  ''' <summary>
  ''' Refreshes this instance. Same as read.
  ''' </summary><returns></returns>
  Function Refresh() As Boolean

  ''' <summary>
  ''' Gets a value indicating whether this instance is actual.
  ''' </summary>
  ''' <value>
  '''   <c>true</c> if this instance is actual; otherwise, <c>false</c>.
  ''' </value>
  ReadOnly Property IsActual() As Boolean

End Interface

''' <summary>
''' The contract implemented by unique elements that can be updated, namely, their values can be applied or refreshed.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Interface IUniqueUpdatable
  Inherits IUpdateable, IKeyable
End Interface

''' <summary>
''' The contract implemented by all cached values.
''' A cached value has Cacheable and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter Cacheable and actual values are consolidated. That is 
''' done by setting the actual value.
''' The base cached value is typeless and forms a collection of based Cacheable elements that can be used
''' to combile all element types in a single collections.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public Interface ICacheableElement
  Inherits IUniqueUpdatable
  Function Equals(ByVal value As Object) As Boolean
  Function GetHashCode() As Integer
  Property Getter() As Action
  Property HasActualValue() As Boolean
  ReadOnly Property IsFresh() As Boolean
  ReadOnly Property Reading() As String
  Property ReadingFormat() As String
  Function ToString() As String
End Interface


''' <summary>
''' Implements an abstruct <see cref="ICacheableElement">cached value</see>
''' yo be used by typed cached values.
''' </summary>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public MustInherit Class CacheableElementBase

  Implements ICacheableElement

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableElementBase" /> class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Protected Sub New(ByVal name As String)

    MyBase.New()
    If String.IsNullOrEmpty(name) Then
      Throw New ArgumentNullException("name")
    End If
    _UniqueKey = name

  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableString" /> class.
  ''' This is a copy constructor.
  ''' </summary>
  ''' <param name="model">The  <see cref="CacheableStructure">paramter</see> object from which to copy</param>
  Protected Sub New(ByVal model As ICacheableElement)
    Me.New(model.UniqueKey)
    Me._getter = model.Getter
    Me._hasActualValue = model.HasActualValue
    Me._isActual = model.IsActual
    Me._isFresh = model.IsFresh
    Me._reading = model.Reading
    Me._readingFormat = model.ReadingFormat
  End Sub

#End Region

#Region " DELEGATES "

  Private _getter As Action
  ''' <summary>
  ''' Gets or sets the action for getting the actual value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Getter() As Action Implements ICacheableElement.Getter
    Get
      Return _getter
    End Get
    Set(ByVal value As Action)
      _getter = value
    End Set
  End Property

#End Region

#Region " EQUALS "

  ''' <summary>Determines if the two specified <see cref="T:Object">objects</see> 
  ''' have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")> _
  Public Overloads Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
    Return left.Equals(right)
  End Function

  ''' <summary>Indicates whether the current <see cref="T:ICacheableElement"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:ICacheableElement"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public MustOverride Overloads Overrides Function Equals(ByVal value As Object) As Boolean Implements ICacheableElement.Equals

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  MustOverride Overloads Overrides Function GetHashCode() As Int32 Implements ICacheableElement.GetHashCode

#End Region

#Region " READING "

  Private _reading As String
  ''' <summary>
  ''' Gets the reading i the defiend reading format.
  ''' </summary>
  ''' 
  Public ReadOnly Property Reading() As String Implements ICacheableElement.Reading
    Get
      Return _reading
    End Get
  End Property

  ''' <summary>
  ''' Sets the reading.
  ''' </summary>
  ''' <param name="value">The value.</param>
  Protected Sub ReadingSetter(ByVal value As String)
    If String.IsNullOrEmpty(Me.Reading) OrElse Not Me.Reading.Equals(value) Then
      _reading = value
      Me.OnPropertyChanged("Reading")
    End If
  End Sub

  Private _readingFormat As String = ""
  ''' <summary>
  ''' Gets or sets the reading format.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks>
  ''' The format specification differs between <see cref="IFormattable">Formattabe</see>
  ''' and non  <see cref="IFormattable">Formattabe</see> chached values.
  ''' Formattable values use the <see cref="ToString"></see> functuon
  ''' requiring values in the format 'xxx', e.g., '0.0#'.
  ''' Non Formattable values use the <see cref="String.Format"></see> functuon 
  ''' requiring values in the format {0:xxx}, e.g., {0:0.0#}.
  ''' </remarks>
  Public Property ReadingFormat() As String Implements ICacheableElement.ReadingFormat
    Get
      Return _readingFormat
    End Get
    Set(ByVal value As String)
      If String.IsNullOrEmpty(Me.ReadingFormat) OrElse Not Me.ReadingFormat.Equals(value) Then
        _readingFormat = value
        Me.OnPropertyChanged("ReadingFormat")
      End If
    End Set
  End Property

#End Region

#Region " I CACHEABLE ELEMENT "

  ''' <summary>
  ''' Applies changes and returns true if a change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public MustOverride Function Apply() As Boolean Implements IUpdateable.Apply

  Private _hasActualValue As Boolean
  ''' <summary>
  ''' Gets or sets the condition indicating if the actual value was set.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property HasActualValue() As Boolean Implements ICacheableElement.HasActualValue
    Get
      Return _hasActualValue
    End Get
    Set(ByVal value As Boolean)
      If Me.HasActualValue <> value Then
        _hasActualValue = value
        Me.OnPropertyChanged("HasActualValue")
      End If
    End Set
  End Property

  ''' <summary>
  ''' Sets the value indicating whether the cached value of this instance is actual.
  ''' </summary>
  ''' <param name="value">if set to <c>true</c> [value].</param>
  Protected Sub IsActualSetter(ByVal value As Boolean)
    If Me.IsActual <> value Then
      _isActual = value
      Me.OnPropertyChanged("IsActual")
    End If
  End Sub

  Private _isActual As Boolean
  ''' <summary>
  ''' Gets a value indicating whether the value of this instance is actual.
  ''' </summary>
  ''' <value>
  '''   <c>true</c> if the instance value is actual; otherwise, <c>false</c>.
  ''' </value>
  Public ReadOnly Property IsActual() As Boolean Implements IUpdateable.IsActual
    Get
      Return _isActual
    End Get
  End Property

  Private _isFresh As Boolean
  ''' <summary>
  ''' Gets a value indicating whether this instance is fresh.
  ''' </summary>
  ''' <value>
  '''   <c>true</c> if this instance value is fresh; otherwise, <c>false</c>.
  ''' </value>
  Public ReadOnly Property IsFresh() As Boolean Implements ICacheableElement.IsFresh
    Get
      Return _isFresh
    End Get
  End Property

  ''' <summary>
  ''' Sets the value indicating whether the value of this instance is Fresh.
  ''' </summary>
  ''' <param name="value">if set to <c>true</c> [value].</param>
  Protected Sub IsFreshSetter(ByVal value As Boolean)
    If Me.IsFresh <> value Then
      _isFresh = value
      Me.OnPropertyChanged("IsFresh")
    End If
  End Sub

  Private _UniqueKey As String
  ''' <summary>
  ''' Gets or sets the item unique key or name.
  ''' Facilitates detecting which item is not set. Must be unique.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property UniqueKey() As String Implements IKeyable.UniqueKey, INestedPropertyChanged.SourceName
    Get
      Return _UniqueKey
    End Get
    Set(ByVal value As String)
      If String.IsNullOrEmpty(Me.UniqueKey) OrElse Not Me.UniqueKey.Equals(value) Then
        _UniqueKey = value
        Me.OnPropertyChanged("Name")
      End If
    End Set
  End Property

  ''' <summary>
  ''' Returns the default string representation of the cached value.
  ''' </summary>
  MustOverride Overrides Function ToString() As String Implements ICacheableElement.ToString

  ''' <summary>
  ''' Refreshes this instance if is has not actual value or the cached value
  ''' is difference from the actual value. Same as read.
  ''' 
  ''' </summary><returns></returns>
  Public Overridable Function Refresh() As Boolean Implements IUpdateable.Refresh
    If Not (Me.HasActualValue AndAlso Me.IsActual) Then
      Me.Getter.Invoke()
      Return True
    Else
      Return False
    End If
  End Function

#End Region

#Region " I PROPERTY CHANGED "

  ''' <summary>
  ''' Occurs when nested property changed.
  ''' </summary>
  Public Event NestedPropertyChanged(ByVal sender As Object, ByVal e As NestedPropertyChangedEventArgs) Implements INestedPropertyChanged.NestedPropertyChanged

  ''' <summary>
  ''' Called when a property changes.
  ''' </summary>
  ''' <param name="propertyName">The property name.</param>
  Protected Sub OnPropertyChanged(ByVal propertyName As String)
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, New NestedPropertyChangedEventArgs(Me, propertyName))
  End Sub

#If False Then
  ''' <summary>
  ''' Occurs when a property value changes.
  ''' </summary>
  Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

  ''' <summary>
  ''' Called when a property changes.
  ''' </summary>
  ''' <param name="propertyName">The property name.</param>
  Protected Sub OnPropertyChanged(ByVal propertyName As String)
    EventHandlerExtensions.SafeInvoke(PropertyChangedEvent, Me, New ComponentModel.PropertyChangedEventArgs(propertyName))
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, New NestedPropertyChangedEventArgs(Me, propertyName))
  End Sub
#End If

#End Region

End Class

''' <summary>
''' Contains a list of <see cref="IUniqueUpdatable">unique updatable elements</see>.
''' </summary>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public Class UniqueUpdatableCollection
  Inherits Collections.Generic.List(Of IUniqueUpdatable)
  Implements IUpdateable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableCollection" /> class.
  ''' </summary>
  Public Sub New()
    MyBase.New()
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableCollection" /> class.
  ''' </summary>
  ''' <param name="collection">The collection.</param>
  Public Sub New(ByVal collection As IEnumerable(Of IUniqueUpdatable))
    MyBase.New(collection)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableCollection" /> class.
  ''' </summary>
  ''' <param name="list">The list.</param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification:="Rule broken by MSFT Observable Collection")> _
  Public Sub New(ByVal list As List(Of IUniqueUpdatable))
    MyBase.New(list)
  End Sub

#End Region

#Region " UPDATEABLE "

  ''' <summary>
  ''' Return true if all values are set to their actual instrument value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsActual() As Boolean Implements IUpdateable.IsActual
    Get
      For Each Item As ICacheableElement In MyBase.ToArray()
        If Not Item.IsActual Then
          Return False
        End If
      Next
      Return True
    End Get
  End Property


  ''' <summary>
  ''' Apply all changes and return true if any change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Apply() As Boolean Implements IUpdateable.Apply

    Dim anyChangeApplied As Boolean
    For Each Item As ICacheableElement In MyBase.ToArray()
      If Item.Apply() Then
        anyChangeApplied = True
      End If
    Next
    Return anyChangeApplied

  End Function

  ''' <summary>
  ''' Returns the comma-seperated names or item indexes of elements which values are not actual.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NonActual() As String

    Dim value As New System.Text.StringBuilder
    Dim itemIndex As Integer = 0
    For Each Item As ICacheableElement In MyBase.ToArray()
      If Not Item.IsActual Then
        If value.Length > 0 Then
          value.Append(",")
        End If
        If String.IsNullOrEmpty(Item.UniqueKey) Then
          value.AppendFormat("Item{0}", itemIndex)
        Else
          value.Append(Item.UniqueKey)
        End If
      End If
      itemIndex += 1
    Next
    Return value.ToString
  End Function

  ''' <summary>
  ''' Get new actual values and return true if any actual value was refreshed.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Refresh() As Boolean Implements IUpdateable.Refresh

    Dim anyItemRefreshed As Boolean
    For Each Item As ICacheableElement In MyBase.ToArray()
      Item.Getter.Invoke()
      anyItemRefreshed = True
    Next
    Return anyItemRefreshed

  End Function


  Public Event NestedPropertyChanged(ByVal sender As Object, ByVal e As NestedPropertyChangedEventArgs) Implements INestedPropertyChanged.NestedPropertyChanged

  Private _name As String
  Public Property SourceName() As String Implements INestedPropertyChanged.SourceName
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

#End Region

End Class



''' <summary>
''' Contains a list of <see cref="IUniqueUpdatable">unique updatable elements</see>.
''' Detects if any item on the list changed.
''' </summary>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public Class UniqueUpdatableObservableCollection
  Inherits NestedObservableCollection(Of IUniqueUpdatable)
  Implements IUpdateable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="sourcename">The sourcename.</param>
  Public Sub New(ByVal sourceName As String)
    MyBase.New(sourceName)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="sourceName">The source name.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> supports change notifications.</param>
  Public Sub New(ByVal sourceName As String, ByVal supportsChangeNotifications As Boolean)
    MyBase.New(sourceName, supportsChangeNotifications)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="sourceName">The source name.</param>
  ''' <param name="collection">The collection.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> [supports change notifications].</param>
  Public Sub New(ByVal sourceName As String, ByVal collection As IEnumerable(Of IUniqueUpdatable), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(sourceName, collection, supportsChangeNotifications)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UniqueUpdatableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="list">The list.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> [supports change notifications].</param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification:="Rule broken by MSFT Observable Collection")> _
  Public Sub New(ByVal list As List(Of IUniqueUpdatable), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(list, supportsChangeNotifications)
  End Sub

#End Region

#Region " UPDATEABLE "

  ''' <summary>
  ''' Return true if all values are set to their actual instrument value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsActual() As Boolean Implements IUpdateable.IsActual
    Get
      For Each Item As ICacheableElement In MyBase.ToArray()
        If Not Item.IsActual Then
          Return False
        End If
      Next
      Return True
    End Get
  End Property

  ''' <summary>
  ''' Apply all changes and return true if any change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Apply() As Boolean Implements IUpdateable.Apply

    Dim anyChangeApplied As Boolean
    For Each Item As ICacheableElement In MyBase.ToArray()
      If Item.Apply() Then
        anyChangeApplied = True
      End If
    Next
    Return anyChangeApplied

  End Function

  ''' <summary>
  ''' Returns the comma-seperated names or item indexes of elements which values are not actual.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NonActual() As String

    Dim value As New System.Text.StringBuilder
    Dim itemIndex As Integer = 0
    For Each Item As ICacheableElement In MyBase.ToArray()
      If Not Item.IsActual Then
        If value.Length > 0 Then
          value.Append(",")
        End If
        If String.IsNullOrEmpty(Item.UniqueKey) Then
          value.AppendFormat("Item{0}", itemIndex)
        Else
          value.Append(Item.UniqueKey)
        End If
      End If
      itemIndex += 1
    Next
    Return value.ToString
  End Function

  ''' <summary>
  ''' Get new actual values and return true if any actual value was refreshed.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Refresh() As Boolean Implements IUpdateable.Refresh

    Dim anyItemRefreshed As Boolean
    For Each Item As ICacheableElement In MyBase.ToArray()
      Item.Getter.Invoke()
      anyItemRefreshed = True
    Next
    Return anyItemRefreshed

  End Function

#End Region

End Class


''' <summary>
''' The contract implemented by  unique <see cref="IClearable">clearable</see>, <see cref="IPresettable">presettable</see>, 
''' <see cref="IResettable">resettable</see> and <see cref="IUniqueUpdatable">unique updatable</see> elements.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public Interface IUniquePresettableUpdatable

  Inherits IUniqueUpdatable, IPresettable, IResettable, IClearable

End Interface


''' <summary>
''' Implements a list of <see cref="IUniquePresettableUpdatable">presettable and cacheable values</see>.
''' Detects if any item on the list changed.
''' </summary>
''' <remarks></remarks>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Based on <see cref="ICacheableElement">Cached Structure</see>
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public Class PresettableCacheableCollection
  Inherits Collections.Generic.List(Of IUniquePresettableUpdatable)
  Implements IClearable, IPresettable, IResettable, IUpdateable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableCollection" /> class.
  ''' </summary>
  Public Sub New()
    MyBase.New()
    _name = ""
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableCollection" /> class.
  ''' </summary>
  ''' <param name="collection">The collection.</param>
  Public Sub New(ByVal collection As IEnumerable(Of IUniquePresettableUpdatable))
    MyBase.New(collection)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableCollection" /> class.
  ''' </summary>
  ''' <param name="list">The list.</param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification:="Rule broken by MSFT Observable Collection")> _
  Public Sub New(ByVal list As List(Of IUniquePresettableUpdatable))
    MyBase.New(list)
  End Sub

#End Region

#Region " I UPDATEABLE "

  ''' <summary>
  ''' Return true if all values are set to their actual instrument value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsActual() As Boolean Implements IUpdateable.IsActual
    Get
      For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
        If Not Item.IsActual Then
          Return False
        End If
      Next
      Return True
    End Get
  End Property

  ''' <summary>
  ''' Apply all changes and return true if any change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Apply() As Boolean Implements IUpdateable.Apply

    Dim anyChangeApplied As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      If Item.Apply() Then
        anyChangeApplied = True
      End If
    Next
    Return anyChangeApplied

  End Function

  ''' <summary>
  ''' Returns the comma-seperated names or item indexes of elements which values are not actual.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NonActual() As String

    Dim value As New System.Text.StringBuilder
    Dim itemIndex As Integer = 0
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      If Not Item.IsActual Then
        If value.Length > 0 Then
          value.Append(",")
        End If
        If String.IsNullOrEmpty(Item.UniqueKey) Then
          value.AppendFormat("Item{0}", itemIndex)
        Else
          value.Append(Item.UniqueKey)
        End If
      End If
      itemIndex += 1
    Next
    Return value.ToString
  End Function

  ''' <summary>
  ''' Get new actual values and return true if any actual value was refreshed.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Refresh() As Boolean Implements IUpdateable.Refresh

    Dim anyItemRefreshed As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      anyItemRefreshed = Item.Refresh()
    Next
    Return anyItemRefreshed

  End Function

  Public Event NestedPropertyChanged(ByVal sender As Object, ByVal e As NestedPropertyChangedEventArgs) Implements INestedPropertyChanged.NestedPropertyChanged

  Private _name As String
  Public Property SourceName() As String Implements INestedPropertyChanged.SourceName
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

#End Region

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets all values to the clear value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ClearValues() As Boolean Implements IClearable.Clear
    Dim anyItemCleared As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      Item.Clear()
      anyItemCleared = True
    Next
    Return anyItemCleared
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Resets all to the preset values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    Dim anyItemPreset As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      Item.Preset()
      anyItemPreset = True
    Next
    Return anyItemPreset
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Resets all to the default values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    Dim anyItemReset As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      Item.Reset()
      anyItemReset = True
    Next
    Return anyItemReset
  End Function

#End Region

End Class


''' <summary>
''' Implements a list of <see cref="IUniquePresettableUpdatable">presettable and cacheable values</see>.
''' Detects if any item on the list changed.
''' </summary>
''' <remarks></remarks>
''' <history date="08/31/2009" by="David Hary" revision="1.2.3530.x">
''' Based on <see cref="ICacheableElement">Cached Structure</see>
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached element implementation.
''' </history>
Public Class PresettableCacheableObservableCollection
  Inherits NestedObservableCollection(Of IUniquePresettableUpdatable)
  Implements IClearable, IPresettable, IResettable, IUpdateable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="sourcename">The sourcename.</param>
  Public Sub New(ByVal sourceName As String)
    MyBase.New(sourceName)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="sourceName">The source name.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> supports change notifications.</param>
  Public Sub New(ByVal sourceName As String, ByVal supportsChangeNotifications As Boolean)
    MyBase.New(sourceName, supportsChangeNotifications)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="sourceName">The source name.</param>
  ''' <param name="collection">The collection.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> [supports change notifications].</param>
  Public Sub New(ByVal sourceName As String, ByVal collection As IEnumerable(Of IUniquePresettableUpdatable), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(sourceName, collection, supportsChangeNotifications)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableObservableCollection" /> class.
  ''' </summary>
  ''' <param name="list">The list.</param>
  ''' <param name="supportsChangeNotifications">if set to <c>true</c> [supports change notifications].</param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification:="Rule broken by MSFT Observable Collection")> _
  Public Sub New(ByVal list As List(Of IUniquePresettableUpdatable), ByVal supportsChangeNotifications As Boolean)
    MyBase.New(list, supportsChangeNotifications)
  End Sub

#End Region

#Region " I UPDATEABLE "

  ''' <summary>
  ''' Return true if all values are set to their actual instrument value.
  ''' </summary>
  Public ReadOnly Property IsActual() As Boolean Implements IUpdateable.IsActual
    Get
      For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
        If Not Item.IsActual Then
          Return False
        End If
      Next
      Return True
    End Get
  End Property


  ''' <summary>
  ''' Apply all changes and return true if any change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Apply() As Boolean Implements IUpdateable.Apply

    Dim anyChangeApplied As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      If Item.Apply() Then
        anyChangeApplied = True
      End If
    Next
    Return anyChangeApplied

  End Function

  ''' <summary>
  ''' Returns the comma-seperated names or item indexes of elements which values are not actual.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function NonActual() As String

    Dim value As New System.Text.StringBuilder
    Dim itemIndex As Integer = 0
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      If Not Item.IsActual Then
        If value.Length > 0 Then
          value.Append(",")
        End If
        If String.IsNullOrEmpty(Item.UniqueKey) Then
          value.AppendFormat("Item{0}", itemIndex)
        Else
          value.Append(Item.UniqueKey)
        End If
      End If
      itemIndex += 1
    Next
    Return value.ToString
  End Function

  ''' <summary>
  ''' Get new actual values and return true if any actual value was refreshed.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Refresh() As Boolean Implements IUpdateable.Refresh

    Dim anyItemRefreshed As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      anyItemRefreshed = Item.Refresh()
    Next
    Return anyItemRefreshed

  End Function

#End Region

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets all values to the clear value.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ClearValues() As Boolean Implements IClearable.Clear
    Dim anyItemCleared As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      Item.Clear()
      anyItemCleared = True
    Next
    Return anyItemCleared
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Resets all to the preset values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    Dim anyItemPreset As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      Item.Preset()
      anyItemPreset = True
    Next
    Return anyItemPreset
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Resets all to the default values
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    Dim anyItemReset As Boolean
    For Each Item As IUniquePresettableUpdatable In MyBase.ToArray()
      Item.Reset()
      anyItemReset = True
    Next
    Return anyItemReset
  End Function

#End Region

End Class

