﻿''' <summary>
''' The contract implemented by Presettable, Formattable and Resettable cached value.
''' A cached value has Cacheable and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter Cacheable and actual values are consolidated. That is 
''' done by setting the actual value.
''' The cached value can be preset to its preset value or to its reset value.
''' In both cases, it is assumed that these are also the actual values.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached structure implementation.
''' </history>
Public Interface IPresettableCacheableValue(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits IUniquePresettableUpdatable, ICacheableValue(Of T), IPresettable(Of T), IResettable(Of T), IClearable(Of T), IApproximateable

End Interface


''' <summary>
''' Implements a generic <see cref="IPresettableCacheableValue(Of T)">cached value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached structure implementation.
''' </history>
Public Class PresettableCacheableValue(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits CacheableValue(Of T)
  Implements IPresettableCacheableValue(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableValue" /> class
  ''' for <see cref="T:Integer">Integer</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Integer)
    MyBase.New(name, tolerance)
    _clearValue = New Nullable(Of T)
    _presetValue = New Nullable(Of T)
    _defaultValue = New Nullable(Of T)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableValue" /> class
  ''' for <see cref="T:Decimal">Decimal</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Decimal)
    MyBase.New(name, tolerance)
    _clearValue = New Nullable(Of T)
    _presetValue = New Nullable(Of T)
    _defaultValue = New Nullable(Of T)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableValue" /> class
  ''' for <see cref="T:Double">Double</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Double)
    MyBase.New(name, tolerance)
    _clearValue = New Nullable(Of T)
    _presetValue = New Nullable(Of T)
    _defaultValue = New Nullable(Of T)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableValue" /> class
  ''' for <see cref="T:Single">Single</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Single)
    MyBase.New(name, tolerance)
    _clearValue = New Nullable(Of T)
    _presetValue = New Nullable(Of T)
    _defaultValue = New Nullable(Of T)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="PresettableCacheableValue" /> class.
  ''' This is a copy constructor.
  ''' </summary>
  ''' <param name="model">The  <see cref="CacheableStructure">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As IPresettableCacheableValue(Of T))
    ' TODO: This needs to be fixed for the generic number types.
    MyBase.New(model)
    Me._clearValue = model.ClearValue
    Me._defaultValue = model.DefaultValue
    Me._presetValue = model.PresetValue
  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="PresettableCacheableValue(Of T)">Presettable Value</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see>, <see cref="ActualValue">actual</see>, 
  ''' <see cref="DefaultValue">default</see>, <see cref="ClearValue">clear</see> and <see cref="PresetValue">preset</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As PresettableCacheableValue(Of T), ByVal right As PresettableCacheableValue(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue) _
           AndAlso left.DefaultValue.Equals(right.DefaultValue) _
           AndAlso left.ClearValue.Equals(right.ClearValue) _
           AndAlso left.PresetValue.Equals(right.PresetValue)
  End Function

#End Region

#Region " ICLEARABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="ClearValue">clear value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Clear() As Boolean Implements IClearable.Clear
    If Me.ClearValue.HasValue Then
      MyBase.ActualValue = Me.ClearValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IPRESETTABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="PresetValue">preset value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Preset() As Boolean Implements IPresettable.Preset
    If Me.PresetValue.HasValue Then
      MyBase.ActualValue = Me.PresetValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " IRESETABLE "

  ''' <summary>
  ''' Sets the actual value to the <see cref="DefaultValue">default value</see>
  ''' and tag the value as <see cref="IsActual">actual.</see>
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Reset() As Boolean Implements IResettable.Reset
    If Me.DefaultValue.HasValue Then
      MyBase.ActualValue = Me.DefaultValue.Value
    Else
      MyBase.HasActualValue = False
    End If
    Return True
  End Function

#End Region

#Region " I PRESETTABLE FORMATTABLE CACHE "

  Private _clearValue As T?
  ''' <summary>
  ''' Gets or sets the value that is set when the structure is cleared.
  ''' </summary>
  ''' 
  Public Property ClearValue() As T? Implements IPresettableCacheableValue(Of T).ClearValue
    Get
      Return _clearValue
    End Get
    Set(ByVal value As T?)
      If Me.ClearValue Is Nothing OrElse Not Me.ClearValue.Equals(value) Then
        _clearValue = value
        Me.OnPropertyChanged("ClearValue")
      End If
    End Set
  End Property

  Private _defaultValue As T?
  ''' <summary>
  ''' Gets or sets the value that is set when the structure is reset. This is the default value.
  ''' </summary>
  ''' 
  Public Property DefaultValue() As T? Implements IPresettableCacheableValue(Of T).DefaultValue
    Get
      Return _defaultValue
    End Get
    Set(ByVal value As T?)
      If Me.DefaultValue Is Nothing OrElse Not Me.DefaultValue.Equals(value) Then
        _defaultValue = value
        Me.OnPropertyChanged("DefaultValue")
      End If
    End Set
  End Property

  Private _presetValue As T?
  ''' <summary>
  ''' Gets or sets the value that is set when the structure is preset.
  ''' </summary>
  ''' 
  Public Property PresetValue() As T? Implements IPresettableCacheableValue(Of T).PresetValue
    Get
      Return _presetValue
    End Get
    Set(ByVal value As T?)
      If Me.PresetValue Is Nothing OrElse Not Me.PresetValue.Equals(value) Then
        _presetValue = value
        Me.OnPropertyChanged("PresetValue")
      End If
    End Set
  End Property

#End Region

End Class

