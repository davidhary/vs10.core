﻿''' <summary>
''' The contract implemented by <see cref="IFormattable">formattable</see> and <see cref="IApproximateable">Approximateable</see> cached values.
''' A cached value has Cacheable and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter Cacheable and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached structure implementation.
''' </history>
Public Interface ICacheableValue(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits ICacheableStructure(Of T), IApproximateable
  Overloads Function Equals(ByVal value As ICacheableValue(Of T)) As Boolean

End Interface

''' <summary>
''' An example for creating a generic number type.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <remarks>
''' Shoutouts to Rudedog2
''' http://social.msdn.microsoft.com/forums/en-US/csharplanguage/thread/8ca1c069-beb5-45bf-b796-5f529282ddf8/
''' </remarks>
Public Class GenericNumber(Of T As {Structure})

  Private number As T

  Private _value As Object
  Public Property Value() As Object
    Get
      Return _value
    End Get
    Set(ByVal value As Object)
      _value = value
    End Set
  End Property

  Public Function [Get]() As T
    Me.number = CType(Me.Value, T)
    Return number
  End Function

  Public Sub [Set](ByVal value As T)
    Me.Value = CType(value, T)
    Me.number = value
  End Sub

  Public Sub New(ByVal value As Single)
    _value = Nothing
    number = Nothing
    Convert.ChangeType(_value, TypeCode.Single, Globalization.CultureInfo.CurrentCulture)
    _value = value
    number = CType(_value, T)
  End Sub

  Public Sub New(ByVal value As Double)
    _value = Nothing
    number = Nothing
    Convert.ChangeType(_value, TypeCode.Double, Globalization.CultureInfo.CurrentCulture)
    _value = value
    number = CType(_value, T)
  End Sub

  Public Sub New(ByVal value As Integer)
    _value = Nothing
    number = Nothing
    Convert.ChangeType(_value, TypeCode.Int32, Globalization.CultureInfo.CurrentCulture)
    _value = value
    number = CType(_value, T)
  End Sub

End Class

''' <summary>
''' Implements a generic <see cref="ICacheableValue(Of T)">cached value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="09/26/2009" by="David Hary" revision="1.2.3556.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached structure implementation.
''' </history>
Public Class CacheableValue(Of T As {Structure, IFormattable, IEquatable(Of T)})

  Inherits CacheableStructure(Of T)
  Implements ICacheableValue(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableValue" /> class
  ''' for <see cref="T:Integer">Integer</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Integer)
    MyBase.New(name)
    _actualValue = Nothing
    Convert.ChangeType(_actualValue, TypeCode.Int32, Globalization.CultureInfo.CurrentCulture)
    _cacheValue = Nothing
    Convert.ChangeType(_cacheValue, TypeCode.Int32, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = Nothing
    Convert.ChangeType(_Epsilon, TypeCode.Int32, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = tolerance
    _newValue = Nothing
    Convert.ChangeType(_newValue, TypeCode.Int32, Globalization.CultureInfo.CurrentCulture)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableValue" /> class
  ''' for <see cref="T:Decimal">Decimal</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Decimal)
    MyBase.New(name)
    _actualValue = Nothing
    Convert.ChangeType(_actualValue, TypeCode.Decimal, Globalization.CultureInfo.CurrentCulture)
    _cacheValue = Nothing
    Convert.ChangeType(_cacheValue, TypeCode.Decimal, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = Nothing
    Convert.ChangeType(_Epsilon, TypeCode.Decimal, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = tolerance
    _newValue = Nothing
    Convert.ChangeType(_newValue, TypeCode.Decimal, Globalization.CultureInfo.CurrentCulture)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableValue" /> class
  ''' for <see cref="T:Double">Double</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Double)
    MyBase.New(name)
    _actualValue = Nothing
    Convert.ChangeType(_actualValue, TypeCode.Double, Globalization.CultureInfo.CurrentCulture)
    _cacheValue = Nothing
    Convert.ChangeType(_cacheValue, TypeCode.Double, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = Nothing
    Convert.ChangeType(_Epsilon, TypeCode.Double, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = tolerance
    _newValue = Nothing
    Convert.ChangeType(_newValue, TypeCode.Double, Globalization.CultureInfo.CurrentCulture)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableValue" /> class
  ''' for <see cref="T:Single">Single</see> type.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String, ByVal tolerance As Single)
    MyBase.New(name)
    _actualValue = Nothing
    Convert.ChangeType(_actualValue, TypeCode.Single, Globalization.CultureInfo.CurrentCulture)
    _cacheValue = Nothing
    Convert.ChangeType(_cacheValue, TypeCode.Single, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = Nothing
    Convert.ChangeType(_Epsilon, TypeCode.Single, Globalization.CultureInfo.CurrentCulture)
    _Epsilon = tolerance
    _newValue = Nothing
    Convert.ChangeType(_newValue, TypeCode.Single, Globalization.CultureInfo.CurrentCulture)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableValue" /> class.
  ''' This is a copy constructor.
  ''' </summary>
  ''' <param name="model">The  <see cref="CacheableStructure">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As ICacheableValue(Of T))
    ' TODO: This needs to be fixed for the generic number types.
    MyBase.New(model)
    Me._Epsilon = model.Epsilon
  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="CacheableValue">cacheable value</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As CacheableValue(Of T), ByVal right As CacheableValue(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.Equals(left.CacheValue, right.CacheValue) _
           AndAlso left.Equals(left.ActualValue, right.ActualValue)
  End Function

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:CacheableValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CacheableValue"></see> value; 
  ''' otherwise, <c>false</c>.</returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Overrides Function Equals(ByVal value As Object) As Boolean
    Return value IsNot Nothing AndAlso (Object.ReferenceEquals(Me, value) OrElse CacheableValue(Of T).Equals(Me, TryCast(value, CacheableValue(Of T))))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:CacheableValue"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CacheableValue"></see> value; 
  ''' otherwise, <c>false</c>.</returns>
  ''' <param name="value">The value to compare.</param>
  ''' <remarks>
  ''' The two Parameters are the same if they have the same actual and cached values.
  ''' </remarks>
  Public Overloads Function Equals(ByVal value As ICacheableValue(Of T)) As Boolean Implements ICacheableValue(Of T).Equals
    If value Is Nothing Then
      Throw New ArgumentNullException("value")
    End If
    Return Me.Equals(Me, value)
  End Function

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return MyBase.GetHashCode
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As T, ByVal right As  T ) As Boolean
    Return left.Equals(right)
  End Function
#End If

#End Region

#Region " I CACHED VALUE "

  ''' <summary>
  ''' Determines whether the cached and actual values are the same.
  ''' </summary>
  ''' <returns>
  '''   <c>true</c> if the <see cref="CacheValue">cached</see> equals the <see cref="ActualValue">actual</see> value; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overrides Function IsActualValue() As Boolean
    Return Me.Equals(_cacheValue, _actualValue)
  End Function

  Private _newValue As Object
  ''' <summary>
  ''' Determines whether the supplied value is new.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value is not equal to the <see cref="CacheValue">cached value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overrides Function IsNewValue(ByVal value As T) As Boolean
    _newValue = value
    Return Not Me.Equals(_newValue, _cacheValue)
  End Function

  Private _actualValue As Object
  ''' <summary>
  ''' Gets or sets the actual value stored in the instrument.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Property ActualValue() As T
    Get
      Return MyBase.ActualValue
    End Get
    Set(ByVal Value As T)
      _actualValue = CType(Value, T)
      MyBase.ActualValue = Value
    End Set
  End Property

  Private _cacheValue As Object
  ''' <summary>
  ''' Gets or sets the cached value of the cached value.
  ''' This value is updated whenever the actual value is set.
  ''' </summary>
  Public Overrides Property CacheValue() As T
    Get
      Return MyBase.CacheValue
    End Get
    Set(ByVal Value As T)
      _cacheValue = CType(Value, T)
      MyBase.CacheValue = Value
    End Set
  End Property

#End Region

#Region " I APPROXIMATEABLE "

  Private _Epsilon As Object
  ''' <summary>
  ''' Gets the minimum noticable difference between the values.
  ''' This is the tolerance ot resulution of the values.
  ''' </summary>
  ''' <value>
  ''' The epsilon.
  ''' </value>
  Public ReadOnly Property Epsilon() As Object Implements IApproximateable.Epsilon
    Get
      Return _Epsilon
    End Get
  End Property

  ''' <summary>Determines if the two specified <see cref="T:Double">Double</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Shared Function Approximates(ByVal left As Double, ByVal right As Double, ByVal epsilon As Double) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Double">Double</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Double, ByVal right As Double) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Double))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Single">Single</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Single, ByVal right As Single, ByVal epsilon As Single) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Single">Single</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Single, ByVal right As Single) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Single))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Decimal">Decimal</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Decimal, ByVal right As Decimal) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Decimal))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:Decimal">Decimal</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Decimal, ByVal right As Decimal, ByVal epsilon As Decimal) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

  ''' <summary>Determines if the two specified <see cref="T:integer">integer</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Public Overloads Function Equals(ByVal left As Integer, ByVal right As Integer) As Boolean
    Return Approximates(left, right, CType(Me.Epsilon, Integer))
  End Function

  ''' <summary>Determines if the two specified <see cref="T:integer">integer</see> 
  ''' objects have the same approximate value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param>
  ''' <param name="epsilon">The minimum distinguishable difference.</param>
  ''' <returns>Returns <c>True</c> if <paramref name="left">left hand value</paramref> is the same 
  ''' as the <paramref name="right">right hand value</paramref>value within the <see cref="Epsilon"></see>;
  ''' otherwise, <c>False</c></returns>
  Private Overloads Shared Function Equals(ByVal left As Integer, ByVal right As Integer, ByVal epsilon As Integer) As Boolean
    If epsilon > 0 Then
      Return Math.Abs(left - right) < epsilon
    Else
      Return left.Equals(right)
    End If
  End Function

#End Region

End Class

