﻿''' <summary>
''' The contract implemented by cached values of defined Structure types.
''' A cached value has Cacheable and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter Cacheable and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <typeparam name="T">Specifies the comparable type parameter of the generic class.</typeparam>
''' <remarks>
''' The Cacheable structures are different from the resettable structure defined for SCPI code 
''' in the fact that the cacued structures work both for message-based and non-message-based
''' setters and getters.
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached structure implementation.
''' </history>
Public Interface ICacheableStructure(Of T As Structure)

  Inherits ICacheableElement
  Property ActualValue() As T
  Property CacheValue() As T
  Overloads Function Equals(ByVal value As ICacheableStructure(Of T)) As Boolean
#If False Then
  Function IsActualValue(ByVal value As T) As Boolean
  Function IsNewValue(ByVal value As T) As Boolean
#End If
  Property Setter() As Action(Of T)

End Interface


''' <summary>
''' Implements a generic <see cref="ICacheableStructure(Of T)">cached value</see>.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' Created
''' </history>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' From Cached structure implementation.
''' </history>
Public Class CacheableStructure(Of T As Structure)

  Inherits CacheableElementBase
  Implements ICacheableStructure(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableStructure" /> class.
  ''' </summary>
  ''' <param name="name">The instance name.</param>
  ''' <remarks></remarks>
  Public Sub New(ByVal name As String)
    MyBase.New(name)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableStructure" /> class.
  ''' This is a copy constructor.
  ''' </summary>
  ''' <param name="model">The  <see cref="CacheableStructure">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As ICacheableStructure(Of T))

    MyBase.New(model)
    Me._cacheValue = model.CacheValue
    Me._actualValue = model.ActualValue
    Me._setter = model.Setter

  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="CacheableStructure">cacheable structure</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As CacheableStructure(Of T), ByVal right As CacheableStructure(Of T)) As Boolean
    If left Is Nothing Then
      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If

    Return left.Equals(left.CacheValue, right.CacheValue) _
           AndAlso left.Equals(left.ActualValue, right.ActualValue)
#If False Then
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue)
#End If

  End Function

#End Region

#Region " DELEGATES "

  Private _setter As Action(Of T)
  ''' <summary>
  ''' Gets or sets the action for setting the actual value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Setter() As Action(Of T) Implements ICacheableStructure(Of T).Setter
    Get
      Return _setter
    End Get
    Set(ByVal value As Action(Of T))
      _setter = value
    End Set
  End Property

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:CacheableStructure"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CacheableStructure"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Overrides Function Equals(ByVal value As Object) As Boolean
    Return value IsNot Nothing AndAlso (Object.ReferenceEquals(Me, value) OrElse CacheableStructure(Of T).Equals(Me, TryCast(value, CacheableStructure(Of T))))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:CacheableStructure"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CacheableStructure"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Function Equals(ByVal value As ICacheableStructure(Of T)) As Boolean Implements ICacheableStructure(Of T).Equals
    If value Is Nothing Then
      Throw New ArgumentNullException("value")
    End If
    Return Equals(Me, value)
  End Function

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return _cacheValue.GetHashCode Xor _actualValue.GetHashCode
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As T, ByVal right As  T ) As Boolean
    Return left.Equals(right)
  End Function
#End If

#End Region

#Region " READING = FORMATTNG "

  Protected Function ToReading(ByVal value As Boolean) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, Me.ReadingFormat, IIf(value, 1, -1))
  End Function

  Protected Function ToReading(ByVal value As T) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, MyBase.ReadingFormat, value)
  End Function

#End Region

#Region " I CACHEABLE STRUCTURE "

  ''' <summary>
  ''' Determines whether the cached and actual values are the same.
  ''' </summary>
  ''' <returns>
  '''   <c>true</c> if the <see cref="CacheValue">cached</see> equals the <see cref="ActualValue">actual</see> value; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overridable Function IsActualValue() As Boolean
    Return Me.Equals(Me.CacheValue, Me.ActualValue)
  End Function

  ''' <summary>
  ''' Determines whether the supplied value is new.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value is not equal to the <see cref="CacheValue">cached value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Overridable Function IsNewValue(ByVal value As T) As Boolean
    Return Not Me.Equals(value, Me.CacheValue)
  End Function

  ''' <summary>
  ''' Apply changes and return true if a change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Apply() As Boolean
    If Not Me.IsActual Then
      Me.Setter.Invoke(Me.CacheValue)
      Return True
    End If
    Return False

  End Function

  Private _actualValue As T
  ''' <summary>
  ''' Gets or sets the actual value stored in the instrument.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable Property ActualValue() As T Implements ICacheableStructure(Of T).ActualValue
    Get
      Return _actualValue
    End Get
    Set(ByVal Value As T)
      If Not Me.ActualValue.Equals(Value) Then
        _actualValue = Value
        Me.OnPropertyChanged("ActualValue")
        Me.ReadingSetter(ToReading(_actualValue))
      End If
      MyBase.HasActualValue = True
      Me.CacheValue = Value
    End Set
  End Property

  Private _cacheValue As T
  ''' <summary>
  ''' Gets or sets the cached value of the cached value.
  ''' This value is updated whenever the actual value is set.
  ''' </summary>
  Public Overridable Property CacheValue() As T Implements ICacheableStructure(Of T).CacheValue
    Get
      Return _cacheValue
    End Get
    Set(ByVal Value As T)
      Me.IsFreshSetter(Me.IsNewValue(Value))
      If Me.IsFresh Then
        _cacheValue = Value
        Me.OnPropertyChanged("CacheValue")
      End If
      MyBase.IsActualSetter(Me.IsActualValue())
    End Set
  End Property

  ''' <summary>
  ''' Returns the default string representation of the cached value.
  ''' </summary>
  Public Overrides Function ToString() As String
    If Me.HasActualValue Then
      Return CacheableStructure(Of T).ToString(Me.CacheValue, Me.ActualValue)
    Else
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},N/A)", CacheValue)
    End If
  End Function

  ''' <summary>Returns the default string representation of the cached value.</summary>
  Private Overloads Shared Function ToString(ByVal Cacheable As T, ByVal actual As T) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", Cacheable, actual)
  End Function

#End Region

End Class

