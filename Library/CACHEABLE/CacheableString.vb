﻿''' <summary>
''' The contract implemented by Cacheable String Values.
''' A cached value has Cacheable and actual values with an indication that
''' the cached value changed and is different from the actual value.
''' When applied the parameter Cacheable and actual values are consolidated. That is 
''' done by setting the actual value.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Interface ICacheableString

  Inherits ICacheableElement
  Property ActualValue() As String
  Property CacheValue() As String
  Overloads Function Equals(ByVal value As ICacheableString) As Boolean
  Function IsActualValue(ByVal value As String) As Boolean
  Function IsNewValue(ByVal value As String) As Boolean
  Property Setter() As Action(Of String)

End Interface


''' <summary>
''' Implements a generic <see cref="ICacheableString">cached value</see>.
''' </summary>
''' <remarks></remarks>
''' <history date="02/06/2011" by="David Hary" revision="1.2.4054.x">
''' Created
''' </history>
Public Class CacheableString

  Inherits CacheableElementBase
  Implements ICacheableString

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableString" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New(name)
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="CacheableString" /> class.
  ''' This is a copy constructor.
  ''' </summary>
  ''' <param name="model">The  <see cref="CacheableStructure">paramter</see> object from which to copy</param>
  Public Sub New(ByVal model As ICacheableString)
    MyBase.new(model)
    Me._cacheValue = model.CacheValue
    Me._actualValue = model.ActualValue
    Me._setter = model.Setter
  End Sub

#End Region

#Region " SHARED "

  ''' <summary>Determines if the two specified <see cref="ICacheableString">cacheable string</see> 
  ''' objects have the same value.
  ''' </summary>
  ''' <param name="left">The left value.</param>
  ''' <param name="right">The right value.</param><returns></returns>
  ''' <remarks>
  ''' The two objects are the same if they have the same <see cref="CacheValue">cached</see> and <see cref="ActualValue">actual</see> values.
  ''' </remarks>
  Private Overloads Shared Function Equals(ByVal left As ICacheableString, ByVal right As ICacheableString) As Boolean
    If left Is Nothing Then

      Throw New ArgumentNullException("left")
    End If
    If right Is Nothing Then
      Throw New ArgumentNullException("right")
    End If
    Return left.CacheValue.Equals(right.CacheValue) _
           AndAlso left.ActualValue.Equals(right.ActualValue)
  End Function

#End Region

#Region " DELEGATES "

  Private _setter As Action(Of String)
  ''' <summary>
  ''' Gets or sets the action for setting the actual value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property Setter() As Action(Of String) Implements ICacheableString.Setter
    Get
      Return _setter
    End Get
    Set(ByVal value As Action(Of String))
      _setter = value
    End Set
  End Property

#End Region

#Region " EQUALS "

  ''' <summary>Indicates whether the current <see cref="T:CacheableString"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:CacheableString"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Overrides Function Equals(ByVal value As Object) As Boolean
    Return value IsNot Nothing AndAlso (Object.ReferenceEquals(Me, value) OrElse CacheableString.Equals(Me, TryCast(value, ICacheableString)))
  End Function

  ''' <summary>Indicates whether the current <see cref="T:ICacheableString"></see> value is equal to 
  ''' a specified object.</summary>
  ''' <returns><c>true</c> if the other parameter is equal to the current <see cref="T:ICacheableString"></see> value; 
  ''' otherwise, <c>false</c>. 
  ''' </returns>
  ''' <param name="value">An object.</param>
  ''' <filterpriority>1</filterpriority> 
  Public Overloads Function Equals(ByVal value As ICacheableString) As Boolean Implements ICacheableString.Equals
    If value Is Nothing Then
      Throw New ArgumentNullException("value")
    End If
    Return Equals(Me, value)
  End Function

  ''' <summary>
  ''' Returns a hash code for this instance.
  ''' </summary><returns>
  ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
  ''' </returns>
  Public Overloads Overrides Function GetHashCode() As Int32
    Return _cacheValue.GetHashCode Xor _actualValue.GetHashCode
  End Function

#If False Then
  Public Overloads Function Equals(ByVal left As String, ByVal right As  T ) As Boolean
    Return left.Equals(right)
  End Function
#End If

#End Region

#Region " I CACHEABLE STRING "

  ''' <summary>
  ''' Determines whether the supplied value is Actual.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value equals the <see cref="ActualValue">actual value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Function IsActualValue(ByVal value As String) As Boolean Implements ICacheableString.IsActualValue
    Return Me.Equals(value, Me.ActualValue)
  End Function

  ''' <summary>
  ''' Determines whether the supplied value is new.
  ''' </summary>
  ''' <param name="value">The value.</param><returns>
  '''   <c>true</c> if the specified value is not equal to the <see cref="CacheValue">cached value</see>; otherwise, <c>false</c>.
  ''' </returns>
  Protected Function IsNewValue(ByVal value As String) As Boolean Implements ICacheableString.IsNewValue
    Return Not Me.Equals(value, Me.CacheValue)
  End Function

  ''' <summary>
  ''' Apply changes and return true if a change was applied.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Apply() As Boolean
    If Not Me.IsActual Then
      Me.Setter.Invoke(Me.CacheValue)
      Return True
    End If
    Return False

  End Function

  Private _actualValue As String
  ''' <summary>
  ''' Gets or sets the actual value stored in the instrument.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable Property ActualValue() As String Implements ICacheableString.ActualValue
    Get
      Return _actualValue
    End Get
    Set(ByVal Value As String)
      If String.IsNullOrEmpty(Me.ActualValue) OrElse Not Me.ActualValue.Equals(Value) Then
        _actualValue = Value
        Me.OnPropertyChanged("ActualValue")
        Me.ReadingSetter(_actualValue)
      End If
      MyBase.HasActualValue = Not Value Is Nothing
      Me.CacheValue = Value
    End Set
  End Property

  Private _cacheValue As String
  ''' <summary>
  ''' Gets or sets the cached value of the cached value.
  ''' This value is updated whenever the actual value is set.
  ''' </summary>
  Public Overridable Property CacheValue() As String Implements ICacheableString.CacheValue
    Get
      Return _cacheValue
    End Get
    Set(ByVal Value As String)
      Me.IsFreshSetter(Me.IsNewValue(Value))
      If Me.IsFresh Then
        _cacheValue = Value
        Me.OnPropertyChanged("CacheValue")
      End If
      MyBase.IsActualSetter(Me.IsActualValue(Value))
    End Set
  End Property

  ''' <summary>
  ''' Returns a <see cref="System.String" /> that represents this instance.
  ''' </summary>
  Public Overrides Function ToString() As String
    If Me.HasActualValue Then
      Return CacheableString.ToString(Me.CacheValue, Me.ActualValue)
    Else
      Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},N/A)", CacheValue)
    End If
  End Function

  ''' <summary>
  ''' Returns a <see cref="System.String" /> that represents this instance.
  ''' </summary>
  ''' <param name="value">The value.</param>
  ''' <param name="actual">The actual value.</param><returns>
  ''' A <see cref="System.String" /> that represents this instance.
  ''' </returns>
  Private Overloads Shared Function ToString(ByVal value As String, ByVal actual As String) As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", value, actual)
  End Function

#End Region

End Class

