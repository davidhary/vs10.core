Imports System
Imports System.Collections.Generic
Imports isr.Core.EnumExtensions

''' <summary>
''' A class to encapsulate specific enumerations in cases where speed imporvements are
''' desired. Reported 100000 operations time reduced from 3.9 to .57 million ticks.
''' With using a creatable class, imrovement increased to 10 fold.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/04/2008" by="Stewart and StageSoft" revision="1.2.3411.x">
''' From Strong-Type and Efficient .NET Enum By Hana Giat
''' http://www.codeproject.com/KB/cs/efficient_strong_enum.aspx
''' Created
''' </history>
Public Class EnumExtender(Of T)

  ''' <summary>
  ''' Contructor for the class instantiates all elements.
  ''' With built-in elements, subsequent operations are fairly speedy.
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()

    _t = GetType(T)

    _values = System.Enum.GetValues(_t)

    _names = System.Enum.GetNames(_t)

    ' _descriptions = isr.Core.EnumExtensions.Descriptions(T)
    Dim values As New System.Collections.Generic.List(Of String)
    For Each value As System.Enum In System.Enum.GetValues(_t)
      values.Add(value.Description())
    Next value
    _descriptions = values.ToArray

    _nameValuePairs = New Dictionary(Of String, T)(_names.Length)

    _lowerCaseNameValuePairs = New Dictionary(Of String, T)(_names.Length)

    _enumValueDescriptionPairs = New Dictionary(Of T, String)(_names.Length)

    _valueEnumValuePairs = New Dictionary(Of Integer, T)(_names.Length)

    Dim index As Integer = 0
    For Each name As String In _names

      Dim enumValue As T = CType(System.Enum.Parse(_t, name), T)

      _nameValuePairs.Add(name, enumValue)

      _enumValueDescriptionPairs.Add(enumValue, _descriptions(index))

      _lowerCaseNameValuePairs.Add(name.ToLower(Globalization.CultureInfo.CurrentCulture), enumValue)

      _valueEnumValuePairs.Add(Convert.ToInt32(enumValue, Globalization.CultureInfo.CurrentCulture), enumValue)

      index += 1
    Next name

  End Sub

  ''' <summary>
  ''' Dictionary of enum value and description pairs.
  ''' </summary>
  Private _enumValueDescriptionPairs As Dictionary(Of T, String)

  ''' <summary>
  ''' Gets or sets the type.
  ''' </summary>
  ''' <remarks></remarks>
  Private _t As Type

  Private _descriptions As String()
  ''' <summary>
  ''' Gets the descriptions.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetDescriptions() As String()
    Return _descriptions
  End Function

  ''' <summary>
  ''' Returns the descriptions of an flags enumerated list masked by the specified value.
  ''' </summary>
  ''' <param name="mask">Allows returning only selected items.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetDescriptions(ByVal mask As Integer) As String()
    Dim index As Integer = 0
    Dim values As New System.Collections.Generic.List(Of String)
    For Each value As Object In _values
      If (CInt(value) And mask) <> 0 Then
        values.Add(_descriptions(index))
      End If
      index += 1
    Next value
    Return values.ToArray
  End Function

  ''' <summary>
  ''' Gets the of the enum value.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetName(ByVal value As Object) As String
    ' We can hold an additional dictionary to map T -> string.
    ' In my specific usage, this usages is rare, so I selected the lower performance option
    Try
      Dim enumValue As T = CType(value, T)
      For Each pair As KeyValuePair(Of String, T) In _nameValuePairs
        ' Dim x As Integer = Convert.ToInt32(pair.Value, Globalization.CultureInfo.CurrentCulture)
        If pair.Value.Equals(enumValue) Then
          Return pair.Key
        End If
      Next pair
    Catch
      Throw New ArgumentException("Cannot convert " & value.ToString & " to " & _t.ToString(), "value")
    End Try
    Return Nothing ' should never happen

  End Function

  Private _names As String()
  ''' <summary>
  ''' Returns teh names.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetNames() As String()
    Return _names
  End Function

  ''' <summary>
  ''' Returns the Names of an flags enumerated list masked by the specified value.
  ''' </summary>
  ''' <param name="mask">Allows returning only selected items.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetNames(ByVal mask As Integer) As String()
    Dim index As Integer = 0
    Dim values As New System.Collections.Generic.List(Of String)
    For Each value As Object In _values
      If (CInt(value) And mask) <> 0 Then
        values.Add(_names(index))
      End If
      index += 1
    Next value
    Return values.ToArray
  End Function

  Private _values As System.Array
  ''' <summary>
  ''' Returns the array of enum values.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetValues() As Array
    Return _values
  End Function

  ''' <summary>
  ''' Returns the Values of an flags enumerated list masked by the specified value.
  ''' </summary>
  ''' <param name="mask">Allows returning only selected items.</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetValues(ByVal mask As Integer) As Integer()
    Dim index As Integer = 0
    Dim values As New System.Collections.Generic.List(Of Integer)
    For Each value As Object In _values
      If (CInt(value) And mask) <> 0 Then
        values.Add(CInt(value))
      End If
      index += 1
    Next value
    Return values.ToArray
  End Function

  ''' <summary>
  ''' Returns a <see cref="IList"/> of value and description pairs..
  ''' </summary>
  ''' <returns>An <see cref="IList"/> containing the enumerated type value and description.</returns>
  ''' <remarks>
  ''' <example>
  ''' The first step is to add a description attribute to your enum.
  ''' <code>
  '''     Public Enum SimpleEnum2
  '''       [System.ComponentModel.Description("Today")] Today
  '''       [System.ComponentModel.Description("Last 7 days")] Last7
  '''       [System.ComponentModel.Description("Last 14 days")] Last14
  '''       [System.ComponentModel.Description("Last 30 days")] Last30
  '''       [System.ComponentModel.Description("All")] All
  '''     End Enum
  '''     Public SimpleEfficientEnum As New EfficientEnum(of SimpleEnum2)
  '''     Dim combo As ComboBox = New ComboBox()
  '''     combo.DataSource = SimpleEfficientEnum.GetValueDescriptionPairs(GetType(SimpleEnum2))
  '''     combo.DisplayMember = "Value"
  '''     combo.ValueMember = "Key"
  '''     dim selectedEnum as SimpleEnum2 = SimpleEfficientEnum.ToObjectFromPair(combo.SelectedItem)
  ''' </code>
  ''' This works with any control that supports data binding, including the 
  ''' <see cref="System.Windows.Forms.ToolStripComboBox">tool strip combo box</see>, 
  ''' although you will need to cast the <see cref="System.Windows.Forms.ToolStripComboBox.Control">control property</see>
  ''' to a <see cref="System.Windows.Forms.ComboBox">Combo Box</see> to get to the DataSource property. 
  ''' In that case, you will also want to perform the same cast when you are referencing the 
  ''' selected value to work with it as your enum type.
  ''' </example>
  ''' </remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")> _
  Public Function GetValueDescriptionPairs() As IList
    Return _enumValueDescriptionPairs.ToList
  End Function

  ''' <summary>
  ''' Gets the underlying type.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")> _
  Public Function GetUnderlyingType() As Type
    ' Seems like this is good enough. 
    Return System.Enum.GetUnderlyingType(_t)
  End Function

  ''' <summary>
  ''' Returns true if the enumerated value is deifined in this enumeration.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
  Public Function IsDefined(ByVal value As Object) As Boolean
    Try
      Return IsDefined(CInt(Microsoft.VisualBasic.Fix(value)))
    Catch
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Returns true if the enumerated value is deifined in this enumeration.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
  Public Function IsDefined(ByVal value As Integer) As Boolean
    Try
      Return _valueEnumValuePairs.ContainsKey(value)
    Catch
      Return False
    End Try
  End Function

  ''' <summary>
  ''' Dictionary of name value pairs used for parsing case-sensitive values.
  ''' </summary>
  Private _nameValuePairs As Dictionary(Of String, T)

  ''' <summary>
  ''' Returns the enum value for the given name.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Parse(ByVal value As String) As T
    Return _nameValuePairs(value) ' Exception will be thrown if the value not found
  End Function

  ''' <summary>
  ''' Will serve us in the Parse method, with ignoreCase == true.
  ''' It is possible not to hold this member and to define a Comparer class -
  ''' But my main goal here is the performance at runtime.
  ''' </summary>
  Private _lowerCaseNameValuePairs As Dictionary(Of String, T)

  ''' <summary>
  ''' Returns the enum value for the given name.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <param name="ignoreCase"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function Parse(ByVal value As String, ByVal ignoreCase As Boolean) As T
    If ignoreCase Then
      Dim valLower As String = value.ToLower(Globalization.CultureInfo.CurrentCulture)
      Return _lowerCaseNameValuePairs(valLower)
    Else
      Return Parse(value)
    End If
  End Function

  ''' <summary>
  ''' Returns the value converted to enum.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToObject(ByVal value As Object) As T
    Try
      Dim wholeValue As Integer = CInt(Microsoft.VisualBasic.Fix(value))
      Return ToObject(wholeValue)
    Catch e1 As InvalidCastException
      Throw New ArgumentException("Cannot convert " & value.ToString & " to integer", "value")
    End Try
    'If an exception is coming from ToObject(intval) do not catch it here.
  End Function

  ''' <summary>
  ''' Will serve us in the ToObject method
  ''' </summary>
  Private _valueEnumValuePairs As Dictionary(Of Integer, T)
  ''' <summary>
  ''' Returns the value converted to enum.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToObject(ByVal value As Integer) As T
    Return _valueEnumValuePairs(value)
  End Function

  ''' <summary>
  ''' Returns the value converted to enum from the given value name pair.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToObjectFromPair(ByVal value As Object) As T
    Return ToObject(CType(value, KeyValuePair(Of T, String)).Key)
  End Function

End Class

