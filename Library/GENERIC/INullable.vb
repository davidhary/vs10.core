﻿''' <summary>
''' The contract implemented by entities that might have null values.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="05/15/2009" by="David Hary" revision="1.1.3422.x">
''' Created
''' </history>
<CLSCompliant(False)> _
Public Interface INullable(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable, IConvertible})

  ''' <summary>
  ''' Gets or sets the invalid value. This value is returned is a setter failed setting
  ''' the value.
  ''' </summary>
  Property InvalidValue() As T

  ''' <summary>
  ''' Gets the condition for determining if the <see cref="Value"></see> was set to a valid value
  ''' </summary>
  ReadOnly Property HasValue() As Boolean

  ''' <summary>
  ''' Gets or sets the condition for returning the missing value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property IsMissing() As Boolean

  ''' <summary>Gets or sets the missing value.
  ''' </summary>
  Property MissingValue() As T

  ''' <summary>
  ''' Sets <see cref="HasValue"></see> to false thus, effectively, quickly 'constructing' a new
  ''' value.
  ''' </summary>
  Sub Nullify()

  ''' <summary>
  ''' Try setting a new value from <see cref="Double">double type</see>.
  ''' </summary>
  ''' <param name="value">The value which to set.</param>
  ''' <returns>Returns True if value was set.</returns>
  ''' <remarks></remarks>
  Function TrySetValue(ByVal value As Double) As Boolean

  ''' <summary>
  ''' Try setting a new value from a generic type.
  ''' </summary>
  ''' <param name="value">The value which to set.</param>
  ''' <typeparam name="TX">Specifies the generic type</typeparam>
  ''' <returns></returns>
  ''' <remarks>This works from numeric types but not from a string.</remarks>
  Function TrySetValue(Of TX As IConvertible)(ByVal value As TX) As Boolean

  ''' <summary>
  ''' Try setting a new value from a <see cref="String"></see> type.
  ''' </summary>
  ''' <param name="value">The value which to set.</param>
  ''' <returns>Returns True if value was set.</returns>
  ''' <remarks>TC cannot convert from String to Number.</remarks>
  Function TrySetValue(ByVal value As String) As Boolean

  ''' <summary>Gets or sets the value.
  ''' </summary>
  Property Value() As T

End Interface
