﻿''' <summary>
''' Defines event arguments accepting a generic argument.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/05/2008" by="Stewart and StageSoft" revision="1.15.2926.x">
''' http://www.codeproject.com/KB/cs/GenericEventArgs.aspx
''' Created
''' </history>
Public Class GenericEventArgs(Of TArgs)
  Inherits EventArgs

  Public Sub New(ByVal argument As TArgs)
    MyBase.New()
    _arg = argument
    _timestamp = DateTime.Now
  End Sub

  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")> _
  Public Shadows ReadOnly Property Empty() As GenericEventArgs(Of TArgs)
    Get
      Return New GenericEventArgs(Of TArgs)(Nothing)
    End Get
  End Property

  Private _arg As TArgs
  Public Property Arg() As TArgs
    Get
      Return _arg
    End Get
    Set(ByVal value As TArgs)
      _arg = value
    End Set
  End Property

  ''' <summary>Gets or sets the error time stamp.</summary>
  Private _timestamp As DateTime

  ''' <summary>Gets the error time stamp.</summary>
  Public ReadOnly Property Timestamp() As DateTime
    Get
      Return _timestamp
    End Get
  End Property

  ''' <summary>Gets or sets the condition for flagging user or element cancellation.</summary>
  Private _isCanceled As Boolean

  ''' <summary>Gets or sets the cancelation flag.
  ''' </summary>
  Public Property IsCanceled() As Boolean
    Get
      Return _isCanceled
    End Get
    Set(ByVal Value As Boolean)
      _isCanceled = Value
    End Set
  End Property

End Class

