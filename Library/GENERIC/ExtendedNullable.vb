''' <summary>
''' Extends the .NET nullable Double.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/14/2007" by="David Hary" revision="1.1.2874.x">
''' Created
''' </history>
<CLSCompliant(False)> _
Public Class ExtendedNullable(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable, IConvertible})
  Implements IActionInfo, IBoundable(Of T), IField, INullable(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Construtor for this class.
  ''' </summary>
  Public Sub New()

    MyBase.New()

    _format = "0.000E00"
    _hasValue = False

    ' set default length to zero disabling validation of length
    _maxLength = 0
    _minLength = 0

  End Sub

  ''' <summary>
  ''' Creates a copy of the specified value.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal value As ExtendedNullable(Of T))
    Me.New()
    Me._format = value._format
    Me._hasValue = value._hasValue
    Me._highLimit = value._highLimit
    Me._invalidValue = value._invalidValue
    Me._isHigh = value._isHigh
    Me._isLow = value._isLow
    Me._isMissing = value._isMissing
    Me._lowLimit = value._lowLimit
    Me._maxLength = value._maxLength
    Me._minLength = value._minLength
    Me._missingValue = value._missingValue
    Me._value = value._value
  End Sub

#End Region

#Region " IACTION INFO "

  ''' <summary>
  ''' Clears the outcome values.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub clearActionInfo()
    _synopsis = ""
    _details = ""
    _hint = ""
  End Sub

  Private _details As String
  ''' <summary>
  ''' Returns a details of the last action.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property Details() As String Implements IActionInfo.Details
    Get
      Return _details
    End Get
  End Property

  Private _hint As String
  ''' <summary>
  ''' Returns a hint about the last action.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property Hint() As String Implements IActionInfo.Hint
    Get
      Return _hint
    End Get
  End Property

  Private _instanceName As String
  ''' <summary>Gets or sets the instance name.</summary>
  Public Property InstanceName() As String Implements IActionInfo.InstanceName
    Get
      Return _instanceName
    End Get
    Set(ByVal value As String)
      _instanceName = value
    End Set
  End Property

  Private _synopsis As String
  ''' <summary>
  ''' Returns a synopsis of the last failed action.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property Synopsis() As String Implements IActionInfo.Synopsis
    Get
      Return _synopsis
    End Get
  End Property

#End Region

#Region " IBOUNDABLE "

  Private _highLimit As T
  ''' <summary>Gets or sets the high limit.
  ''' </summary>
  Public Property HighLimit() As T Implements IBoundable(Of T).HighLimit
    Get
      Return _highLimit
    End Get
    Set(ByVal value As T)
      _highLimit = value
    End Set
  End Property

  Private _isHigh As Boolean
  ''' <summary>
  ''' Gets the condition telling if the value is lower than the <see cref="HighLimit">high limit</see>.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsHigh() As Boolean Implements IBoundable(Of T).IsHigh
    Get
      Return _isHigh
    End Get
  End Property

  Private _isLow As Boolean
  ''' <summary>
  ''' Gets the condition telling if the value is lower than the <see cref="LowLimit">low limit</see>.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property IsLow() As Boolean Implements IBoundable(Of T).IsLow
    Get
      Return _isLow
    End Get
  End Property

  Private _lowLimit As T
  ''' <summary>Gets or sets the low limit.
  ''' </summary>
  Public Property LowLimit() As T Implements IBoundable(Of T).LowLimit
    Get
      Return _lowLimit
    End Get
    Set(ByVal value As T)
      _lowLimit = value
    End Set
  End Property

#End Region

#Region " IFIELD "

  ''' <summary>
  ''' Gets the field value.
  ''' </summary>
  Public ReadOnly Property FieldValue() As String Implements IField.Caption
    Get
      Return String.Format(Globalization.CultureInfo.CurrentCulture, _
            "{0:" & _format & "}", Me.Value)
    End Get
  End Property

  Private _format As String
  ''' <summary>Gets or sets the format string for returning the field value.
  ''' </summary>
  Public Property Format() As String Implements IField.Format
    Get
      Return _format
    End Get
    Set(ByVal value As String)
      _format = value
    End Set
  End Property

  Private _maxLength As Integer
  ''' <summary>Gets or sets the maximum length of the field value.
  ''' </summary>
  Public Property MaxLength() As Integer Implements IField.MaxLength
    Get
      Return _maxLength
    End Get
    Set(ByVal value As Integer)
      _maxLength = value
    End Set
  End Property

  Private _minLength As Integer
  ''' <summary>Gets or sets the Minimum length of the field value.
  ''' </summary>
  Public Property MinLength() As Integer Implements IField.MinLength
    Get
      Return _minLength
    End Get
    Set(ByVal value As Integer)
      _minLength = value
    End Set
  End Property

  ''' <summary>
  ''' Returns the <see cref="FieldValue">field value</see>.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function ToString() As String Implements IField.ToString
    Return Me.FieldValue
  End Function

  ''' <summary>
  ''' Validates the field value for length.
  ''' </summary>
  Public Function ValidateFieldValue() As Boolean Implements IField.ValidateFieldValue

    clearActionInfo()
    Dim length As Integer
    length = Me.FieldValue.Length
    _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
      " {0} length ('{1}') of formatted ('{2}') Field Value ('{3}') of value ('{4}')", Me.InstanceName, length, _format, Me.FieldValue, _value)
    If _minLength > 0 And length < _minLength Then
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed validating minimum length.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
        "{0} must be at least {1}.", Details, _minLength)
      _hint = "The numeric value may be too small or the numeric field format string incompatible with the expected size."
      Return False
    ElseIf _maxLength > 0 And length > _maxLength Then
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed validating maximum length.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
        "{0} must be at most {1}.", _details, _maxLength)
      _hint = "The numeric value may be too large or the numeric field format string incompatible with the expected size."
      Return False
    Else
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} verified.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} passed length verification.", _details)
      Return True
    End If
  End Function

#End Region

#Region " INULLABLE "

  Private _invalidValue As T
  ''' <summary>
  ''' Gets or sets the invalid value. This value is returned is a setter failed setting
  ''' the value.
  ''' </summary>
  Public Property InvalidValue() As T Implements INullable(Of T).InvalidValue
    Get
      Return _invalidValue
    End Get
    Set(ByVal value As T)
      _invalidValue = value
    End Set
  End Property

  Private _hasValue As Boolean
  ''' <summary>
  ''' Gets the condition for determining if the <see cref="Value"></see> was set to a valid value
  ''' </summary>
  Public ReadOnly Property HasValue() As Boolean Implements INullable(Of T).HasValue
    Get
      Return _hasValue
    End Get
  End Property

  Private _isMissing As Boolean
  ''' <summary>
  ''' Gets or sets the condition for returning the missing value.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property IsMissing() As Boolean Implements INullable(Of T).IsMissing
    Get
      Return _isMissing
    End Get
    Set(ByVal value As Boolean)
      _isMissing = value
    End Set
  End Property

  Private _missingValue As T
  ''' <summary>Gets or sets the missing value.
  ''' </summary>
  Public Property MissingValue() As T Implements INullable(Of T).MissingValue
    Get
      Return _missingValue
    End Get
    Set(ByVal value As T)
      _missingValue = value
    End Set
  End Property

  ''' <summary>
  ''' Sets <see cref="HasValue"></see> to false thus, effectively, quickly 'constructing' a new
  ''' value.
  ''' </summary>
  Public Sub Nullify() Implements INullable(Of T).Nullify
    _hasValue = False
  End Sub

  ''' <summary>
  ''' Try setting a new value from <see cref="Double">double type</see>.
  ''' </summary>
  ''' <param name="value">The value which to set.</param>
  ''' <returns>Returns True if value was set.</returns>
  ''' <remarks></remarks>
  Public Overridable Function TrySetValue(ByVal value As Double) As Boolean Implements INullable(Of T).TrySetValue

    clearActionInfo()
    Dim tc As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(value.GetType())
    If tc Is Nothing Then
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed setting value.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed creating a type converter for the value='{1}'.", Me.InstanceName, value)
      _hint = "I am clueless"
      Return False
    ElseIf Not tc.CanConvertTo(GetType(T)) Then
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed setting value.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} cannot convert the value='{1}' to '{2}' type.", Me.InstanceName, value, GetType(T))
      _hint = "Most likely, this method failed converting a Double type to the desired type.  Please report to the developer so that the new type could be added explicistly to the convertion scheme."
      Return False
    Else
      Me.Value = CType(tc.ConvertTo(value, GetType(T)), T)
      Return True
    End If

  End Function

  ''' <summary>
  ''' Try setting a new value from a generic type.
  ''' </summary>
  ''' <param name="value">The value which to set.</param>
  ''' <typeparam name="TX">Specifies the generic type</typeparam>
  ''' <returns>Returns True if value was set.</returns>
  ''' <remarks>This works from numeric types but not from a string.</remarks>
  Public Overridable Function TrySetValue(Of TX As IConvertible)(ByVal value As TX) As Boolean Implements INullable(Of T).TrySetValue

    clearActionInfo()
    Dim tc As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(GetType(TX))
    If tc Is Nothing Then
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed setting value.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed creating a type converter for the value='{1}'.", Me.InstanceName, value)
      _hint = "I am clueless"
      Return False
    ElseIf Not tc.CanConvertTo(GetType(T)) Then
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed setting value.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} cannot convert the value='{1}' to '{2}' type.", Me.InstanceName, value, GetType(T))
      _hint = "Most likely, this method failed converting a Double type to the desired type.  Please report to the developer so that the new type could be added explicistly to the convertion scheme."
      Return False
    Else
      Me.Value = CType(tc.ConvertTo(value, GetType(T)), T)
      Return True
    End If

  End Function

  ''' <summary>
  ''' Try setting a new value from a <see cref="String"></see> type.
  ''' </summary>
  ''' <param name="value">The value which to set.</param>
  ''' <returns>Returns True if value was set.</returns>
  ''' <remarks>TC cannot convert from String to Number.</remarks>
  Public Overridable Function TrySetValue(ByVal value As String) As Boolean Implements INullable(Of T).TrySetValue

    clearActionInfo()
    If GetType(T).Equals(GetType(Double)) Then
    End If
    Dim a As Double
    If System.Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, _
                              System.Globalization.CultureInfo.CurrentCulture, a) Then
      Return Me.TrySetValue(a)
    Else
      _synopsis = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed parsing value.", Me.InstanceName)
      _details = String.Format(Globalization.CultureInfo.CurrentCulture, _
          "{0} failed parsing the value='{1}' to '{2}' type.", Me.InstanceName, value, a.GetType)
      _hint = "Check the value as it most likely does not represent a number."
      Return False
    End If

  End Function

  Private _value As T
  ''' <summary>Gets or sets the value.
  ''' </summary>
  Public Property Value() As T Implements INullable(Of T).Value
    Get
      If Me._isMissing Then
        Return _missingValue
      ElseIf Me._hasValue Then
        Return _value
      Else
        Return _invalidValue
      End If
    End Get
    Set(ByVal value As T)
      _value = value
      Me._hasValue = True
      Me._isLow = False
      Me._isHigh = False
      If _value.CompareTo(Me._lowLimit) < 0 Then
        Me._isLow = True
      ElseIf _value.CompareTo(Me._highLimit) > 0 Then
        Me._isHigh = True
      End If
    End Set
  End Property

#End Region

End Class

