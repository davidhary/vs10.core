﻿Imports isr.Core
Imports System.Collections
Imports System.Linq
Module Startup

  Sub Main()

    Dim success As Boolean
    Dim weakEvents As WeakEvents = Nothing

    Try

      Console.WriteLine("Initializing test class {0}", "WeakEvents")
      weakEvents = New WeakEvents
      AddHandler WeakEvents.PropertyChanged, AddressOf weakEvents_PropertyChanged
      AddHandler weakEvents.SelectorsCollectionChanged, AddressOf weakEvents_SelectorsCollectionChanged
      AddHandler weakEvents.WordsCollectionChanged, AddressOf weakEvents_WordsCollectionChanged
      Console.WriteLine("Hit CR to continue")
      Console.ReadKey()

      Dim TempLookupItems() As LookupItem = {New LookupItem With {.Value = 1, .Caption = "0x00000001"}, _
                                            New LookupItem With {.Value = 2, .Caption = "0x00000010"}, _
                                            New LookupItem With {.Value = 4, .Caption = "0x00000100"}, _
                                            New LookupItem With {.Value = 8, .Caption = "0x00001000"}}


      For Each item As LookupItem In TempLookupItems
        Console.WriteLine("Adding lookup item {0}", item.Caption)
        weakEvents.Selectors.Add(item)
        Console.WriteLine("Hit CR to continue")
        Console.ReadKey()
      Next

      Dim itemNumber As Integer = 3
      Console.WriteLine("Selecting item  {0}", itemNumber)
      weakEvents.Selectors(itemNumber).IsSelected = Not weakEvents.Selectors(itemNumber).IsSelected
      Console.WriteLine("Hit CR to continue")
      Console.ReadKey()

      Console.WriteLine("Adding a word to {0}", "WeakEvents")
      weakEvents.Words.Add(New LookupItem With {.Index = weakEvents.Words.Count, .Caption = "Really?"})
      Console.WriteLine("Hit CR to continue")
      Console.ReadKey()

      success = True

    Catch ex As Exception

      ' log the exception
      Console.Error.WriteLine(ex.ToString)

    Finally

      If weakEvents IsNot Nothing Then
        RemoveHandler weakEvents.PropertyChanged, AddressOf weakEvents_PropertyChanged
        RemoveHandler weakEvents.SelectorsCollectionChanged, AddressOf weakEvents_SelectorsCollectionChanged
        RemoveHandler weakEvents.WordsCollectionChanged, AddressOf weakEvents_WordsCollectionChanged
      End If

      Console.Error.WriteLine("Hit CR to end")
      Console.ReadKey()

      ' flush the log.
      My.Application.Log.DefaultFileLogWriter.Flush()

      ' For some reason the event handling set in the Settings class dos not really work.
      My.Settings.Save()

      ' do some garbage collection
      System.GC.Collect()

      If success Then
        ' exit with success code
        Environment.Exit(0)
      Else
        ' exit with an error code
        Environment.Exit(-1)
      End If

    End Try

  End Sub

  Private Sub weakEvents_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
    Console.WriteLine("Property changed: {0}", e.PropertyName)
  End Sub

  Private Sub weakEvents_SelectorsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    Console.WriteLine("Selectors Collection Changed. Action: {0}", e.Action)
  End Sub

  Private Sub weakEvents_WordsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    Console.WriteLine("Words Collection Changed. Action: {0}", e.Action)
  End Sub

End Module

#Region " TEST.NOTIFYPARENT.COLLECTIONS.STEP2 "

''' <summary>
''' Utility class useful for various lookup lists and selectors
''' </summary>
Public Class LookupItem
  Implements System.ComponentModel.INotifyPropertyChanged
  Private _index As Integer
  Public Property Index() As Integer
    Get
      Return _index
    End Get
    Set(ByVal value As Integer)
      _index = value
      OnPropertyChanged("Index")
    End Set
  End Property

  Private _isSelected As Boolean
  Public Property IsSelected() As Boolean
    Get
      Return _isSelected
    End Get
    Set(ByVal value As Boolean)
      _isSelected = value
      OnPropertyChanged("IsSelected")
    End Set
  End Property

  Private _caption As String
  Public Property Caption() As String
    Get
      Return _caption
    End Get
    Set(ByVal value As String)
      _caption = value
      OnPropertyChanged("Caption")
    End Set
  End Property

  Private _value As Object
  Public Property Value() As Object
    Get
      Return _value
    End Get
    Set(ByVal value As Object)
      _value = value
      OnPropertyChanged("Value")
    End Set
  End Property

  Private _type As String
  Public Property Type() As String
    Get
      Return _type
    End Get
    Set(ByVal value As String)
      _type = value
      OnPropertyChanged("Type")
    End Set
  End Property

  Public Event PropertyChanged As ComponentModel.PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

  Protected Sub OnPropertyChanged(ByVal propertyname As String)
    If Not PropertyChangedEvent Is Nothing Then
      RaiseEvent PropertyChanged(Me, New ComponentModel.PropertyChangedEventArgs(propertyname))
    End If
  End Sub

End Class

''' <summary>
''' Interaction logic for WeakEvents.xaml
''' </summary>
Partial Public Class WeakEvents

  Implements System.ComponentModel.INotifyPropertyChanged, System.Windows.IWeakEventListener

  Public Sub New()

    Me.populateSelectors(New NotifyParentObservableCollection(Of LookupItem))
    ' trick number 1: converting string into char[]

    ' trick number 2: generating sequential numbers using LINQ
    'TODO: INSTANT VB TODO TASK: Assignments within expressions are not supported in VB.NET
    'ORIGINAL LINE: Words = New NotifyParentObservableCollection(Of LookupItem)("Hello world from WPF".Split(" ".ToCharArray()). Select((word, index) => New LookupItem { Caption = word, Index = index }));
    ' Words = New NotifyParentObservableCollection(Of LookupItem)("Hello world from WPF".Split(" ".ToCharArray()).  Select((word), index) => New LookupItem With {.Caption = word, .Index = index}))
    Words = New NotifyParentObservableCollection(Of LookupItem)
    For Each w As String In "Hello world from WPF".Split(" "c)
      Words.Add(New LookupItem With {.Caption = Word, .Index = Words.Count})
    Next

  End Sub

  ''' <summary>
  ''' Populates the selectors.
  ''' </summary>
  ''' <param name="value">The value.</param>
  Private Sub populateSelectors(ByVal value As NotifyParentObservableCollection(Of LookupItem))
    If Not _selectors Is Nothing Then
      ChildPropertyChangedEventManager.RemoveListener(_selectors, Me)
      Specialized.CollectionChangedEventManager.RemoveListener(_selectors, Me)
    End If
    _selectors = value
    If Not _selectors Is Nothing Then
      ChildPropertyChangedEventManager.AddListener(_selectors, Me)
      Specialized.CollectionChangedEventManager.AddListener(_selectors, Me)
    End If
    OnPropertyChanged("Selectors")
  End Sub

  Private _selectors As NotifyParentObservableCollection(Of LookupItem)
  Public Property Selectors() As NotifyParentObservableCollection(Of LookupItem)
    Get
      Return _selectors
    End Get
    Private Set(ByVal value As NotifyParentObservableCollection(Of LookupItem))
      populateSelectors(value)
    End Set
  End Property

  Public Event SelectorsCollectionChanged As EventHandler(Of Specialized.NotifyCollectionChangedEventArgs)

  Private Sub OnSelectorsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    SelectorsCollectionChangedEvent(Me, e)
  End Sub

  Public Event WordsCollectionChanged As EventHandler(Of Specialized.NotifyCollectionChangedEventArgs)

  Private Sub OnWordsCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    WordsCollectionChangedEvent(Me, e)
  End Sub

  Private Sub OnSelectorsChildPropertyChanged(ByVal sender As Object, ByVal e As ChildPropertyChangedEventArgs)
    Dim item As LookupItem = TryCast(e.Source, LookupItem)
    If e.PropertyName = "IsSelected" Then
      If item.IsSelected Then
        Hex = Hex Or CInt(Microsoft.VisualBasic.Fix(item.Value))
      Else
        Hex = Hex And Not CInt(Microsoft.VisualBasic.Fix(item.Value))
      End If
    End If
  End Sub

  Private _hex As Integer
  Public Property Hex() As Integer
    Get
      Return _hex
    End Get
    Set(ByVal value As Integer)
      _hex = value
      OnPropertyChanged("Hex")
    End Set
  End Property

  Private _word As String
  Public Property Word() As String
    Get
      Return _word
    End Get
    Set(ByVal value As String)
      _word = value
      OnPropertyChanged("Word")
    End Set
  End Property

  Private _phrase As String
  Public Property Phrase() As String
    Get
      Return _phrase
    End Get
    Set(ByVal value As String)
      _phrase = value
      OnPropertyChanged("Phrase")
    End Set
  End Property

  Private _words As NotifyParentObservableCollection(Of LookupItem)
  Public Property Words() As NotifyParentObservableCollection(Of LookupItem)
    Get
      Return _words
    End Get
    Private Set(ByVal value As NotifyParentObservableCollection(Of LookupItem))
      If Not _words Is Nothing Then
        ChildPropertyChangedEventManager.RemoveListener(_words, Me)
      End If
      _words = value
      If Not _words Is Nothing Then
        ChildPropertyChangedEventManager.AddListener(_words, Me)
        ' Phrase = String.Join(" ", New String() {"", ""}) ' CType(From word In _words, String()) Orderby word.Index Select word.Caption)
        ' Phrase = String.Join(" ", CType(From word In _words, String()) Orderby word.Index Select word.Caption)
        Dim linqQuery = From word In _words Order By word.Index Select word.Caption
        Phrase = String.Join(" ", linqQuery.ToArray)
      End If
      OnPropertyChanged("Words")
    End Set
  End Property

  Private Sub OnWordsChildPropertyChanged(ByVal sender As Object, ByVal e As ChildPropertyChangedEventArgs)
    Dim item As LookupItem = TryCast(e.Source, LookupItem)
    If e.PropertyName = "Caption" Then
      Word = item.Caption
      If Not Words Is Nothing Then
        Dim linqQuery = From word In _words Order By word.Index Select word.Caption
        Phrase = String.Join(" ", linqQuery.ToArray)
      End If
    End If
  End Sub

  Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

  Private Sub OnPropertyChanged(ByVal propertyname As String)
    If Not PropertyChangedEvent Is Nothing Then
      PropertyChangedEvent(Me, New ComponentModel.PropertyChangedEventArgs(propertyname))
    End If
  End Sub

  Public Function ReceiveWeakEvent(ByVal managerType As Type, ByVal sender As Object, ByVal e As EventArgs) As Boolean Implements System.Windows.IWeakEventListener.ReceiveWeakEvent
    If managerType Is GetType(Specialized.CollectionChangedEventManager) Then
      ' Put all your CollectionChanged event handlers here
      If sender Is Selectors Then
        OnSelectorsCollectionChanged(sender, CType(e, Specialized.NotifyCollectionChangedEventArgs))
      ElseIf sender Is Words Then
        OnWordsCollectionChanged(sender, CType(e, Specialized.NotifyCollectionChangedEventArgs))
      End If
    ElseIf managerType Is GetType(ChildPropertyChangedEventManager) Then
      ' Put all your ChildPropertyChanged event handlers here
      If sender Is Selectors Then
        OnSelectorsChildPropertyChanged(sender, CType(e, ChildPropertyChangedEventArgs))
      ElseIf sender Is Words Then
        OnWordsChildPropertyChanged(sender, CType(e, ChildPropertyChangedEventArgs))
      End If
    ElseIf managerType Is GetType(PropertyChangedWeakEventManager) Then
      ' Put all your PropertyChanged event handlers here
    End If
    Return True
  End Function

End Class

#End Region
