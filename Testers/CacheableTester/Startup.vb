﻿Imports isr.Core
Imports System.Collections
Imports System.Linq
Imports System.ComponentModel

''' <summary>
''' Dmonstrates using the <see cref="NestedObservableCollection(Of T)">nested collection</see> 
''' with <see cref="CacheableElementBase">Cacheable</see> elements.
''' Does not include testing of the resettable functionality.
''' </summary>
''' <remarks></remarks>
Module Startup

  Dim success As Boolean

  Sub Main()

  End Sub

End Module

#Region " TEST CACHEABLE COLLECTIONS "

#Region " FUNCTIONS "

''' <summary>
''' The base function.
''' </summary>
Public Class FunctionBase
  Implements IUniquePresettableUpdatable, System.Windows.IWeakEventListener

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="FunctionBase" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New()
    _name = name
    _presettableElements = New PresettableCacheableObservableCollection(_name)
  End Sub

#End Region

#Region " I UPDATEABLE "

  Private _name As String
  ''' <summary>
  ''' Gets or sets the name of the source.
  ''' Uniquely identifies the source for the property name change.
  ''' </summary>
  ''' <value>
  ''' The name of the source.
  ''' </value>
  Public Property SourceName() As String Implements isr.Core.INestedPropertyChanged.SourceName, isr.Core.IKeyable.UniqueKey
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

  Public Overridable Function Apply() As Boolean Implements isr.Core.IUpdateable.Apply
    _presettableElements.Apply()
  End Function

  Public Overridable ReadOnly Property IsActual() As Boolean Implements isr.Core.IUpdateable.IsActual
    Get
      Return _presettableElements.IsActual
    End Get
  End Property

  Public Overridable Function Refresh() As Boolean Implements isr.Core.IUpdateable.Refresh
    _presettableElements.Refresh()
  End Function

#End Region

#Region " I RESETTABLE "

  Private _presettableElements As PresettableCacheableObservableCollection
  Protected Sub AddElement(ByVal value As IUniquePresettableUpdatable)
    _presettableElements.Add(value)
  End Sub

  Public Overridable Function Clear() As Boolean Implements isr.Core.IClearable.Clear
    _presettableElements.Clear()
  End Function

  Public Overridable Function Preset() As Boolean Implements isr.Core.IPresettable.Preset
    _presettableElements.Preset()
  End Function

  Public Overridable Function Reset() As Boolean Implements isr.Core.IResettable.Reset
    _presettableElements.Refresh()
  End Function

#End Region

#Region " I NESTED PROPERTY CHANGED "

  ''' <summary>
  ''' Occurs when the nested property changed.
  ''' </summary>
  Public Event NestedPropertyChanged(ByVal sender As Object, ByVal e As isr.Core.NestedPropertyChangedEventArgs) Implements isr.Core.INestedPropertyChanged.NestedPropertyChanged

  ''' <summary>
  ''' Called when a property changes.
  ''' </summary>
  ''' <param name="propertyName">The property name.</param>
  Protected Sub OnPropertyChanged(ByVal propertyName As String)
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, New NestedPropertyChangedEventArgs(Me, propertyName))
  End Sub

  ''' <summary>
  ''' Raises the <see cref="E:PropertyChanged" /> event.
  ''' </summary>
  ''' <param name="e">The <see cref="isr.Core.NestedPropertyChangedEventArgs" /> instance containing the event data.</param>
  Protected Sub OnPropertyChanged(ByVal e As NestedPropertyChangedEventArgs)
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, New NestedPropertyChangedEventArgs(Me, e.Sources, e.PropertyName))
  End Sub

#End Region

#Region " WEAK EVENT HANDLING "

  Public Event ElementCollectionChanged As EventHandler(Of Specialized.NotifyCollectionChangedEventArgs)

  Private Sub OnElementCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    ElementCollectionChangedEvent(Me, e)
  End Sub

  ''' <summary>
  ''' Receives events from the centralized event manager.
  ''' </summary>
  ''' <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
  ''' <param name="sender">Object that originated the event.</param>
  ''' <param name="e">Event data.</param><returns>
  ''' true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager" /> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
  ''' </returns>
  Public Function ReceiveWeakEvent(ByVal managerType As Type, ByVal sender As Object, ByVal e As EventArgs) As Boolean Implements System.Windows.IWeakEventListener.ReceiveWeakEvent

    If managerType Is GetType(Specialized.CollectionChangedEventManager) Then

      ' Put all your CollectionChanged event handlers here
      If sender Is _presettableElements Then
        OnElementCollectionChanged(sender, CType(e, Specialized.NotifyCollectionChangedEventArgs))
      End If

    ElseIf managerType Is GetType(NestedPropertyChangedWeakEventManager) Then

      ' propagate the nested changes up the chain
      Me.OnPropertyChanged(CType(e, NestedPropertyChangedEventArgs))

    ElseIf managerType Is GetType(PropertyChangedWeakEventManager) Then

      ' Put all your PropertyChanged event handlers here
      Me.OnPropertyChanged(CType(e, PropertyChangedEventArgs).PropertyName)

    End If

    Return True

  End Function

#End Region

End Class

Public Class SenseFunction
  Inherits FunctionBase

  ''' <summary>
  ''' Initializes a new instance of the <see cref="SenseFunction" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New(name)

    _range = New PresettableCacheableValue(Of Double)("Range", 0.000001)
    MyBase.AddElement(_range)
    _aperture = New PresettableCacheableValue(Of Double)("Aperture", 0.000001)
    MyBase.AddElement(_aperture)

  End Sub

  Private _range As PresettableCacheableValue(Of Double)
  Public ReadOnly Property Range() As PresettableCacheableValue(Of Double)
    Get
      Return _range
    End Get
  End Property

  Private _aperture As PresettableCacheableValue(Of Double)
  Public ReadOnly Property Aperture() As PresettableCacheableValue(Of Double)
    Get
      Return _aperture
    End Get
  End Property

End Class

Public Class SourceFunction
  Inherits FunctionBase

  ''' <summary>
  ''' Initializes a new instance of the <see cref="SourceFunction" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New(name)

    _range = New PresettableCacheableValue(Of Double)("Range", 0.0001)
    MyBase.AddElement(_range)
    _level = New PresettableCacheableValue(Of Double)("Level", 0.0001)
    MyBase.AddElement(_level)

  End Sub

  Private _range As PresettableCacheableValue(Of Double)
  Public ReadOnly Property Range() As PresettableCacheableValue(Of Double)
    Get
      Return _range
    End Get
  End Property

  Private _level As PresettableCacheableValue(Of Double)
  Public ReadOnly Property Level() As PresettableCacheableValue(Of Double)
    Get
      Return _Level
    End Get
  End Property

End Class

#End Region

#Region " SUB SYSTEMS "

''' <summary>
''' The base subsystem.
''' </summary>
Public Class SubsystemBase
  Implements IUniquePresettableUpdatable, System.Windows.IWeakEventListener

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="FunctionBase" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New()
    _name = name
    _presettableElements = New PresettableCacheableObservableCollection(_name)
  End Sub

#End Region

#Region " I UPDATEABLE "

  Private _name As String
  ''' <summary>
  ''' Gets or sets the name of the source.
  ''' Uniquely identifies the source for the property name change.
  ''' </summary>
  ''' <value>
  ''' The name of the source.
  ''' </value>
  Public Property SourceName() As String Implements isr.Core.INestedPropertyChanged.SourceName, isr.Core.IKeyable.UniqueKey
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

  Public Overridable Function Apply() As Boolean Implements isr.Core.IUpdateable.Apply
    _presettableElements.Apply()
  End Function

  Public Overridable ReadOnly Property IsActual() As Boolean Implements isr.Core.IUpdateable.IsActual
    Get
      Return _presettableElements.IsActual
    End Get
  End Property

  Public Overridable Function Refresh() As Boolean Implements isr.Core.IUpdateable.Refresh
    _presettableElements.Refresh()
  End Function

#End Region

#Region " I RESETTABLE "

  Private _presettableElements As PresettableCacheableObservableCollection
  Protected Sub AddElement(ByVal value As IUniquePresettableUpdatable)
    _presettableElements.Add(value)
  End Sub

  Public Overridable Function Clear() As Boolean Implements isr.Core.IClearable.Clear
    _presettableElements.Clear()
  End Function

  Public Overridable Function Preset() As Boolean Implements isr.Core.IPresettable.Preset
    _presettableElements.Preset()
  End Function

  Public Overridable Function Reset() As Boolean Implements isr.Core.IResettable.Reset
    _presettableElements.Refresh()
  End Function

#End Region

#Region " I NESTED PROPERTY CHANGED "

  ''' <summary>
  ''' Occurs when the nested property changed.
  ''' </summary>
  Public Event NestedPropertyChanged(ByVal sender As Object, ByVal e As isr.Core.NestedPropertyChangedEventArgs) Implements isr.Core.INestedPropertyChanged.NestedPropertyChanged

  ''' <summary>
  ''' Called when a property changes.
  ''' </summary>
  ''' <param name="propertyName">The property name.</param>
  Protected Sub OnPropertyChanged(ByVal propertyName As String)
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, New NestedPropertyChangedEventArgs(Me, propertyName))
  End Sub

  ''' <summary>
  ''' Raises the <see cref="E:PropertyChanged" /> event.
  ''' </summary>
  ''' <param name="e">The <see cref="isr.Core.NestedPropertyChangedEventArgs" /> instance containing the event data.</param>
  Protected Sub OnPropertyChanged(ByVal e As NestedPropertyChangedEventArgs)
    EventHandlerExtensions.SafeInvoke(NestedPropertyChangedEvent, Me, New NestedPropertyChangedEventArgs(Me, e.Sources, e.PropertyName))
  End Sub

#End Region

#Region " WEAK EVENT HANDLING "

  Public Event ElementCollectionChanged As EventHandler(Of Specialized.NotifyCollectionChangedEventArgs)

  Private Sub OnElementCollectionChanged(ByVal sender As Object, ByVal e As Specialized.NotifyCollectionChangedEventArgs)
    ElementCollectionChangedEvent(Me, e)
  End Sub

  ''' <summary>
  ''' Receives events from the centralized event manager.
  ''' </summary>
  ''' <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
  ''' <param name="sender">Object that originated the event.</param>
  ''' <param name="e">Event data.</param><returns>
  ''' true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager" /> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
  ''' </returns>
  Public Function ReceiveWeakEvent(ByVal managerType As Type, ByVal sender As Object, ByVal e As EventArgs) As Boolean Implements System.Windows.IWeakEventListener.ReceiveWeakEvent

    If managerType Is GetType(Specialized.CollectionChangedEventManager) Then

      ' Put all your CollectionChanged event handlers here
      If sender Is _presettableElements Then
        OnElementCollectionChanged(sender, CType(e, Specialized.NotifyCollectionChangedEventArgs))
      End If

    ElseIf managerType Is GetType(NestedPropertyChangedWeakEventManager) Then

      ' propagate the nested changes up the chain
      Me.OnPropertyChanged(CType(e, NestedPropertyChangedEventArgs))

    ElseIf managerType Is GetType(PropertyChangedWeakEventManager) Then

      ' Put all your PropertyChanged event handlers here
      Me.OnPropertyChanged(CType(e, PropertyChangedEventArgs).PropertyName)

    End If

    Return True

  End Function

#End Region

End Class


#End Region

Public Class SenseSubsystem
  Implements IUniquePresettableUpdatable

  ''' <summary>
  ''' Initializes a new instance of the <see cref="SourceFunction" /> class.
  ''' </summary>
  ''' <param name="name">The name.</param>
  Public Sub New(ByVal name As String)
    MyBase.New()
    _name = name

  End Sub


  Private _senseFunctions As PresettableCacheableObservableCollection

  Private _currentSenseFunction As SenseFunction

  Private _voltageSenseFunction As SenseFunction

  Private _name As String
  ''' <summary>
  ''' Gets or sets the name of the source.
  ''' Uniquely identifies the source for the property name change.
  ''' </summary>
  ''' <value>
  ''' The name of the source.
  ''' </value>
  Public Property SourceName() As String Implements isr.Core.INestedPropertyChanged.SourceName, isr.Core.IKeyable.UniqueKey
    Get
      Return _name
    End Get
    Set(ByVal value As String)
      _name = value
    End Set
  End Property

  Public Function Clear() As Boolean Implements isr.Core.IClearable.Clear

  End Function

  Public Event NestedPropertyChanged(ByVal sender As Object, ByVal e As isr.Core.NestedPropertyChangedEventArgs) Implements isr.Core.INestedPropertyChanged.NestedPropertyChanged

  Public Function Preset() As Boolean Implements isr.Core.IPresettable.Preset

  End Function

  Public Function Reset() As Boolean Implements isr.Core.IResettable.Reset

  End Function

  Public Function Apply() As Boolean Implements isr.Core.IUpdateable.Apply

  End Function

  Public ReadOnly Property IsActual() As Boolean Implements isr.Core.IUpdateable.IsActual
    Get

    End Get
  End Property

  Public Function Refresh() As Boolean Implements isr.Core.IUpdateable.Refresh

  End Function
End Class

Public Class SourceSubsystem

  Private _sourceFunctions As PresettableCacheableObservableCollection

  Private _currentSourceFunction As SourceFunction

  Private _voltageSourceFunction As SourceFunction

End Class

Public Class Instrument

End Class

#End Region
