Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports SafeTrigger

Namespace SafeTriggerSample

  Public Class [MyClass]
    Public Event MyEvent As EventHandler(Of MyEventArgs)

    Public Function TestEventSafeTriggerPattern() As String
      Return OnMyEventSafeTrigger("SafeTrigger Pattern Data")
    End Function

    Public Function TestEventTraditionalPattern() As String
      Return OnMyEventTraditional("Traditional Pattern Data")
    End Function

    Protected Overridable Function OnMyEventSafeTrigger(ByVal eventData As String) As String
      ' Here we use the safe trigger extension
      'method to trigger the event.

      'We can provide a function that can be
      'used to extract return data from the event.

      'It is important to still consider the use of
      'a protected "On..." method so that inheriting
      'classes can still override this behaviour.

      Return MyEventEvent.SafeTrigger(Me, New MyEventArgs(eventData), e >= e.ReturnData)
    End Function

    Protected Overridable Function OnMyEventTraditional(ByVal eventData As String) As String
      ' Here we use the more traditional pattern to trigger the event.

      Dim localEventCopy As EventHandler(Of MyEventArgs) = MyEventEvent
      If Not localEventCopy Is Nothing Then
        Dim eventArgs As MyEventArgs = New MyEventArgs(eventData)
        localEventCopy(Me, eventArgs)
        Return eventArgs.ReturnData
      Else
        Return Nothing
      End If
    End Function
  End Class
End Namespace
