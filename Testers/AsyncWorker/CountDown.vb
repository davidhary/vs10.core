﻿Imports System.Threading
Imports System.Windows.Forms

Public NotInheritable Class CountDown : Implements IDisposable

   Public Class TickEventargs
      Inherits EventArgs
      Public ReadOnly Counter As Integer
      Public Sub New(ByVal counter As Integer)
         Me.Counter = counter
      End Sub
   End Class

   Public Event Tick As EventHandler(Of TickEventargs)
   Private Sub OnTick(ByVal e As TickEventargs)
      RaiseEvent Tick(Me, e)
   End Sub
   Private _OnTick As Action(Of TickEventargs) = AddressOf OnTick
   Public Event IsRunningChanged As EventHandler
   Private Sub OnIsRunningChanged()
      RaiseEvent IsRunningChanged(Me, EventArgs.Empty)
   End Sub

   Private _OnIsRunningChanged As action = AddressOf OnIsRunningChanged
   Private _Counter As Integer
   Private _Timer As New System.Threading.Timer(AddressOf Timer_Tick, Nothing, Timeout.Infinite, 1000)
   Private _IsRunning As Boolean = False

   Private Sub Timer_Tick(ByVal state As Object)
      _OnTick.NotifyGui(New TickEventargs(_Counter))      'raise event
      If _Counter = 0 Then
         IsRunning = False
      Else
         _Counter -= 1
      End If
   End Sub

   Public Property IsRunning() As Boolean
      Get
         Return _IsRunning
      End Get
      Set(ByVal value As Boolean)
         If _IsRunning = value Then Return
         _IsRunning = value
         If _IsRunning Then
            _Timer.Change(0, 1000)
         Else
            _Timer.Change(Timeout.Infinite, 1000)
         End If
         'raise event
         _OnIsRunningChanged.NotifyGui()
      End Set
   End Property

   Public Sub Start(ByVal initValue As Integer)
      _Counter = initValue
      IsRunning = True
   End Sub

   Public Sub Dispose() Implements IDisposable.Dispose
      _Timer.Dispose()
   End Sub
End Class
