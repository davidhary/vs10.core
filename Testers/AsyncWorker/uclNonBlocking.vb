﻿Imports System.Drawing
Imports System.Windows.Forms

Partial Public Class uclNonBlocking : Inherits UserControl
   'a very simple remodelling changes the forwarding design (see uclStillBlocking) to unblocking

   Private Sub ucl_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown
      'EvaluateData(DateTime.Now, e.Location);
      AsyncWorkerX.RunAsync(AddressOf EvaluateData, DateTime.Now, e.Location)
   End Sub

   Private Sub EvaluateData(ByVal t As DateTime, ByVal p As Point)
      System.Threading.Thread.Sleep(1000)
      Dim s = String.Format("Position {0} / {1}" & vbLf & "clicked at {2:T}", p.X, p.Y, t)
      'DisplayResult(s, p);
      AsyncWorkerX.NotifyGui(AddressOf DisplayResult, s, p)
   End Sub

   Private Sub DisplayResult(ByVal s As String, ByVal p As Point)
      label1.Text = s
      label1.Location = p - label1.Size
   End Sub

End Class
