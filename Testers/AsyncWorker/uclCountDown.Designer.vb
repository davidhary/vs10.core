﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uclCountDown
   Inherits System.Windows.Forms.UserControl

   'UserControl overrides dispose to clean up the component list.
   <System.Diagnostics.DebuggerNonUserCode()> _
   Protected Overrides Sub Dispose(ByVal disposing As Boolean)
      Try
         If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
         End If
      Finally
         MyBase.Dispose(disposing)
      End Try
   End Sub

   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer

   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.  
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> _
   Private Sub InitializeComponent()
      Me.tableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
      Me.label1 = New System.Windows.Forms.Label
      Me.ckIsRunning = New System.Windows.Forms.CheckBox
      Me.udInitValue = New System.Windows.Forms.NumericUpDown
      Me.tableLayoutPanel1.SuspendLayout()
      CType(Me.udInitValue, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.SuspendLayout()
      '
      'tableLayoutPanel1
      '
      Me.tableLayoutPanel1.ColumnCount = 3
      Me.tableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
      Me.tableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
      Me.tableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
      Me.tableLayoutPanel1.Controls.Add(Me.label1, 2, 1)
      Me.tableLayoutPanel1.Controls.Add(Me.ckIsRunning, 1, 1)
      Me.tableLayoutPanel1.Controls.Add(Me.udInitValue, 0, 1)
      Me.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.tableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
      Me.tableLayoutPanel1.Name = "tableLayoutPanel1"
      Me.tableLayoutPanel1.Padding = New System.Windows.Forms.Padding(4)
      Me.tableLayoutPanel1.RowCount = 3
      Me.tableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
      Me.tableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
      Me.tableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
      Me.tableLayoutPanel1.Size = New System.Drawing.Size(199, 101)
      Me.tableLayoutPanel1.TabIndex = 3
      '
      'label1
      '
      Me.label1.BackColor = System.Drawing.SystemColors.Control
      Me.label1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.label1.Location = New System.Drawing.Point(133, 31)
      Me.label1.Name = "label1"
      Me.label1.Size = New System.Drawing.Size(59, 38)
      Me.label1.TabIndex = 4
      Me.label1.Text = "label1"
      Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
      '
      'ckIsRunning
      '
      Me.ckIsRunning.AutoSize = True
      Me.ckIsRunning.BackColor = System.Drawing.SystemColors.Control
      Me.ckIsRunning.Dock = System.Windows.Forms.DockStyle.Fill
      Me.ckIsRunning.Location = New System.Drawing.Point(70, 34)
      Me.ckIsRunning.Name = "ckIsRunning"
      Me.ckIsRunning.Size = New System.Drawing.Size(57, 32)
      Me.ckIsRunning.TabIndex = 5
      Me.ckIsRunning.Text = "IsRunning"
      Me.ckIsRunning.UseVisualStyleBackColor = False
      '
      'udInitValue
      '
      Me.udInitValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
      Me.udInitValue.BackColor = System.Drawing.SystemColors.Control
      Me.udInitValue.Location = New System.Drawing.Point(7, 39)
      Me.udInitValue.Name = "udInitValue"
      Me.udInitValue.Size = New System.Drawing.Size(57, 22)
      Me.udInitValue.TabIndex = 6
      Me.udInitValue.Value = New Decimal(New Integer() {10, 0, 0, 0})
      '
      'uclAsyncWorker
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 16.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.BackColor = System.Drawing.Color.MistyRose
      Me.Controls.Add(Me.tableLayoutPanel1)
      Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Margin = New System.Windows.Forms.Padding(4)
      Me.Name = "uclAsyncWorker"
      Me.Size = New System.Drawing.Size(199, 101)
      Me.tableLayoutPanel1.ResumeLayout(False)
      Me.tableLayoutPanel1.PerformLayout()
      CType(Me.udInitValue, System.ComponentModel.ISupportInitialize).EndInit()
      Me.ResumeLayout(False)

   End Sub
   Private WithEvents tableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
   Private WithEvents label1 As System.Windows.Forms.Label
   Private WithEvents ckIsRunning As System.Windows.Forms.CheckBox
   Private WithEvents udInitValue As System.Windows.Forms.NumericUpDown

End Class
