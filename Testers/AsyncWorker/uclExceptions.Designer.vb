﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uclExceptions
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uclExceptions))
      Me.groupBox1 = New System.Windows.Forms.GroupBox
      Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
      Me.btSynchron = New System.Windows.Forms.Button
      Me.btWithoutEndInvoke = New System.Windows.Forms.Button
      Me.btAsyncworker = New System.Windows.Forms.Button
      Me.btWithEndInvoke = New System.Windows.Forms.Button
      Me.btBackgroundworker = New System.Windows.Forms.Button
      Me.Label1 = New System.Windows.Forms.Label
      Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
      Me.groupBox1.SuspendLayout()
      Me.SplitContainer1.Panel1.SuspendLayout()
      Me.SplitContainer1.Panel2.SuspendLayout()
      Me.SplitContainer1.SuspendLayout()
      Me.SuspendLayout()
      '
      'groupBox1
      '
      Me.groupBox1.Controls.Add(Me.btSynchron)
      Me.groupBox1.Controls.Add(Me.btWithoutEndInvoke)
      Me.groupBox1.Controls.Add(Me.btAsyncworker)
      Me.groupBox1.Controls.Add(Me.btWithEndInvoke)
      Me.groupBox1.Controls.Add(Me.btBackgroundworker)
      Me.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.groupBox1.Location = New System.Drawing.Point(0, 0)
      Me.groupBox1.Name = "groupBox1"
      Me.groupBox1.Size = New System.Drawing.Size(172, 202)
      Me.groupBox1.TabIndex = 5
      Me.groupBox1.TabStop = False
      Me.groupBox1.Text = "generate Exception"
      '
      'SplitContainer1
      '
      Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
      Me.SplitContainer1.IsSplitterFixed = True
      Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
      Me.SplitContainer1.Name = "SplitContainer1"
      '
      'SplitContainer1.Panel1
      '
      Me.SplitContainer1.Panel1.Controls.Add(Me.groupBox1)
      '
      'SplitContainer1.Panel2
      '
      Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
      Me.SplitContainer1.Size = New System.Drawing.Size(605, 202)
      Me.SplitContainer1.SplitterDistance = 172
      Me.SplitContainer1.TabIndex = 4
      '
      'btSynchron
      '
      Me.btSynchron.AutoSize = True
      Me.btSynchron.Location = New System.Drawing.Point(3, 17)
      Me.btSynchron.Name = "btSynchron"
      Me.btSynchron.Size = New System.Drawing.Size(60, 23)
      Me.btSynchron.TabIndex = 4
      Me.btSynchron.Text = "synchron"
      Me.btSynchron.UseVisualStyleBackColor = True
      '
      'btWithoutEndInvoke
      '
      Me.btWithoutEndInvoke.AutoSize = True
      Me.btWithoutEndInvoke.Location = New System.Drawing.Point(3, 45)
      Me.btWithoutEndInvoke.Name = "btWithoutEndInvoke"
      Me.btWithoutEndInvoke.Size = New System.Drawing.Size(161, 23)
      Me.btWithoutEndInvoke.TabIndex = 0
      Me.btWithoutEndInvoke.Text = "asynchron, without EndInvoke"
      Me.btWithoutEndInvoke.UseVisualStyleBackColor = True
      '
      'btAsyncworker
      '
      Me.btAsyncworker.AutoSize = True
      Me.btAsyncworker.Location = New System.Drawing.Point(3, 126)
      Me.btAsyncworker.Name = "btAsyncworker"
      Me.btAsyncworker.Size = New System.Drawing.Size(100, 23)
      Me.btAsyncworker.TabIndex = 3
      Me.btAsyncworker.Text = "with Asyncworker"
      Me.btAsyncworker.UseVisualStyleBackColor = True
      '
      'btWithEndInvoke
      '
      Me.btWithEndInvoke.AutoSize = True
      Me.btWithEndInvoke.Location = New System.Drawing.Point(3, 72)
      Me.btWithEndInvoke.Name = "btWithEndInvoke"
      Me.btWithEndInvoke.Size = New System.Drawing.Size(146, 23)
      Me.btWithEndInvoke.TabIndex = 1
      Me.btWithEndInvoke.Text = "asynchron, with EndInvoke"
      Me.btWithEndInvoke.UseVisualStyleBackColor = True
      '
      'btBackgroundworker
      '
      Me.btBackgroundworker.AutoSize = True
      Me.btBackgroundworker.Location = New System.Drawing.Point(3, 99)
      Me.btBackgroundworker.Name = "btBackgroundworker"
      Me.btBackgroundworker.Size = New System.Drawing.Size(129, 23)
      Me.btBackgroundworker.TabIndex = 2
      Me.btBackgroundworker.Text = "with Backgroundworker"
      Me.btBackgroundworker.UseVisualStyleBackColor = True
      '
      'Label1
      '
      Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
      Me.Label1.Location = New System.Drawing.Point(0, 0)
      Me.Label1.Name = "Label1"
      Me.Label1.Size = New System.Drawing.Size(429, 202)
      Me.Label1.TabIndex = 0
      Me.Label1.Text = resources.GetString("Label1.Text")
      '
      'uclExceptions
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.Controls.Add(Me.SplitContainer1)
      Me.Name = "uclExceptions"
      Me.Size = New System.Drawing.Size(605, 202)
      Me.groupBox1.ResumeLayout(False)
      Me.groupBox1.PerformLayout()
      Me.SplitContainer1.Panel1.ResumeLayout(False)
      Me.SplitContainer1.Panel2.ResumeLayout(False)
      Me.SplitContainer1.ResumeLayout(False)
      Me.ResumeLayout(False)

   End Sub
   Private WithEvents groupBox1 As System.Windows.Forms.GroupBox
   Private WithEvents btAsyncworker As System.Windows.Forms.Button
   Private WithEvents btWithoutEndInvoke As System.Windows.Forms.Button
   Private WithEvents btBackgroundworker As System.Windows.Forms.Button
   Private WithEvents btWithEndInvoke As System.Windows.Forms.Button
   Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
   Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
   Friend WithEvents Label1 As System.Windows.Forms.Label
   Private WithEvents btSynchron As System.Windows.Forms.Button

End Class
