﻿Imports System.Drawing
Imports System.Windows.Forms
Imports System.ComponentModel

Partial Public Class uclBgwPrgbar : Inherits UserControl

   Private Structure Data2Thread
      Public Time As DateTime
      Public Pt As Point
   End Structure
   Private Structure Data2Gui
      Public Text As String
      Public Pt As Point
   End Structure

   Public Sub New()
      InitializeComponent()
      UpdateGui()
   End Sub

   Private Sub UpdateGui()
      btCancel.Visible = BackgroundWorker1.IsBusy
      progressBar1.Visible = BackgroundWorker1.IsBusy
      If BackgroundWorker1.IsBusy Then
         RemoveHandler Me.MouseDown, AddressOf ucl_MouseDown
         progressBar1.Value = 0
      Else
         AddHandler Me.MouseDown, AddressOf ucl_MouseDown
      End If
   End Sub

   Private Sub ucl_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
      Dim d As New Data2Thread() With {.Time = DateTime.Now, .Pt = e.Location}
      BackgroundWorker1.RunWorkerAsync(d)
      UpdateGui()
   End Sub

   Private Sub backgroundWorker1_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) _
         Handles BackgroundWorker1.DoWork
      'unpack arguments
      Dim d As Data2Thread = CType(e.Argument, Data2Thread)
      Dim p = d.Pt
      Dim t = d.Time
      'evaluate data
      For i = 0 To 299
         System.Threading.Thread.Sleep(10)
         BackgroundWorker1.ReportProgress(i \ 3)         'reports 300 "progresses"
         If BackgroundWorker1.CancellationPending Then
            e.Cancel = True
            Return
         End If
      Next
      'pack results
      Dim d2g As New Data2Gui() With { _
         .Pt = p, _
         .Text = String.Format("Position {0} / {1}" & vbLf & "clicked at {2:T}", p.X, p.Y, t) _
      }
      e.Result = d2g
   End Sub

   Private Sub backgroundWorker1_RunWorkerCompleted( _
         ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
         Handles BackgroundWorker1.RunWorkerCompleted
      UpdateGui()
      If e.Cancelled Then
         label1.Text = "Canceled"
         Return
      End If
      Dim d2g As Data2Gui = CType(e.Result, Data2Gui)      'unpack results
      label1.Text = d2g.Text
      label1.Location = d2g.Pt - label1.Size
   End Sub

   Private Sub btCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btCancel.Click
      BackgroundWorker1.CancelAsync()
   End Sub

   Private Sub backgroundWorker1_ProgressChanged( _
         ByVal sender As Object, ByVal e As ProgressChangedEventArgs) _
         Handles BackgroundWorker1.ProgressChanged
      progressBar1.Value = e.ProgressPercentage
   End Sub

End Class
